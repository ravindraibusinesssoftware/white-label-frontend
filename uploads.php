<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W3D2T8');</script>
<!-- End Google Tag Manager -->


<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

<title>Knight Capital Funding</title>
<link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
<link rel="stylesheet" href="css/bootstrap.min.css" >
<link rel="stylesheet" href="css/font-awesome.min.css" >
<link rel="stylesheet" href="css/custom.css" >
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,800,700' rel='stylesheet' type='text/css'>

</head>

<body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3D2T8"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- <?php include 'header.php'; ?> -->

    <header>
      <div class="container">
       <div class="row">
         <div class="col-sm-6 clearfix col-sm-push-6">
           <div class="header_right pull-right">
            <a id="telNum" href="tel:8554624249">CALL NOW: 855-462-4249</a>
          </div>
        </div>
        <div class="col-sm-6 clearfix col-sm-pull-6">
         <div class="header_left pull-left">
          <h1 class="logo">
           <a href="">
            <img src="images/white-kcf-logo.png">
          </a>
        </h1>
      </div>
    </div>

  </div>
</div>
</header>


<div class="normal_wrapper">



 <div class="container">
   <div class="section-3">
    <div class="wrapper-500">
     <h4 class="funding-sub-head-3"><span>Help Us Find You</span></h4>
     <div id="get-started">

      <div class="form-group">
       <label for="email">Email Address :</label>
       <input name="email" type="text" class="form-control" id="email" placeholder="Enter Your Email Address"  required>
       <span class="error" style="display: none;">Enter Valid email address </span>

     </div>
     <button type="submit" class="btn btn-default Continue_upload section_btn hover_ease btn_disable">Continue</button>

   </div>

   <div class="Continue_upload_loading" style="display: none;">
    <i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
  </div>

</div>
</div>

<div class="section-4" style="display: none">
  <div class="wrapper-600">
   <h4 class="funding-sub-head-3"><span>Upload Your Last 3 Months of Business Bank Statements</span></h4>
   <p class="sec16_text">In order to get you funded today, please upload *all pages* of your last 3 months business bank statements. You can download your statements directly from your online banking portal.</p>
   <div class="uploads_wrapper">
    <form>
     <h4 style="color:#ffffff; padding-top: 20px; padding-bottom: 20px;">Please Upload Your Last 3 Months Bank Statements Here</h4>
     <ul>
      <li>
       <label class="upload_here fileupload1 hover_ease"><input type="file" name="fileupload1" value="fileupload1" id="fileupload1" accept=".pdf" ></label>
     </li>
     <li>
       <label class="upload_here fileupload2 hover_ease"><input type="file" name="fileupload2" value="fileupload2" id="fileupload2" accept=".pdf" ></label>
     </li>
     <li>
       <label class="upload_here fileupload3 hover_ease"><input type="file" name="fileupload3" value="fileupload3" id="fileupload3" accept=".pdf" ></label>
     </li>
   </ul>
 </form>
</div>

<button class="btn btn-default section_btn section-15-next hover_ease btn_disable">DONE</button>
</div>
</div>

<div class="section-16"  style="display: none;">
  <div class="wrapper-500">

   <div class="thank-you-wrapper">
    <h2 class="thank-you-head">Thank You</h2>
    <p class="thank-you-desc">Your application is on its way. Please expect a phone call from us to discuss your funding options.
    We look forward to speaking to you soon.</p>
  </div>
</div>
</div>


<div class="real-time-busi">
  <div class="Progress_bar hidden-xs">
    <span class="Progress_Status" style="width:90%">90%</span>
  </div>

  <h4 class="real-time-head">We Use Real-Time Business Data To Make Fast Funding Decissions</h4>
  <div class="real-time-desc">
   <ul>
    <li>Funding Answer In 24hours Or Less</li>
    <li>100% Free, No Obligation Quote</li>
    <li>Won't Affect Your Credit Score</li>
    <li>Information Will Never Be Sold</li>
  </ul>
</div>
<div class="real-time-imgs">
 <img src="images/satisfaction guaranteed.png">
 <img src="images/Godaddy_20SSL.png">
</div>
</div>

</div>

</div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript" src="js/bootstrap.min.js" ></script>
<!-- <script type="text/javascript" src="js/custom.js"></script> -->
<script type="text/javascript" src="js/gasalesforce.js"></script>
<script src="https://rawgit.com/RobinHerbots/Inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<script type="text/javascript">

$.getJSON("https://jsonip.com/?callback=?", function (data) {
        //console.log(data);
        user_ip = data.ip;
      });




var $regexemail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


$('#email').on('keyup change',function(){

  var email = $("#email").val();
  $("#email").css('border-color','');
  $('.error').fadeOut();

    //console.log(email);
    if ($('#email').val().match($regexemail)) {
      $('.Continue_upload').removeClass('btn_disable');
    }
    else{
      $('.Continue_upload').addClass('btn_disable');
    }
  });



$('#fileupload1, #fileupload2 ,#fileupload3').on('change',function(){

	var fileupload1 = $("#fileupload1").val();
	var fileupload2 = $("#fileupload2").val();
	var fileupload3 = $("#fileupload3").val();


	if (fileupload1 != ''  || fileupload2 != '' || fileupload3 != ''){
		$('.section-15-next').removeClass('btn_disable');
	}else {
		$('.section-15-next').addClass('btn_disable');
	}
});





$(".Continue_upload").click(function(){

	$('.Continue_upload_loading').fadeIn();

	var Step_Number =1;
	$.ajax({
		type: "POST",
		url: "ajaxmobile.php",
		data:{
			'email':$("#email").val(),
			'Step_Number':Step_Number,
			"Page":"Uploads"
		},
		success: function(data){
			//console.log(data);
			//console.log('data');
			var data = JSON.parse(data);
			if( data.step  == 0 ){
				$('.error').fadeIn();
				$('#email').css('border-color','red');
				$('.Continue_upload_loading').fadeOut();
				setTimeout(function(){
          $("#email").css('border-color','');
          $('.error').fadeOut();
        }, 5000);
			}else if( data.step  == 1 ){
        var data_json={StepNumber: "7", LeadId: data.LeadId, step: 1};
        $.ajax({
          type: "POST",
          url: "ajaxcontinueapp.php",
          data:{
            'data_json':data_json,
          },
          success: function(data){
            if(data){
              window.location.href='/';
            }
          }
        });
      }

    }
  });
});


$(".section-15-next").click(function(){
	var Step_Number = 8;
	var formData = new FormData(document.querySelector("form"));
	$.ajax({
		url: "amazons3/amazons3/public/aws.php",
		type: "POST",
		data: formData,
		processData: false,
		contentType: false,
		success: function(result){
			console.log(result);
		}
	});
	$.ajax({
		type: "POST",
		url: "ajaxcreateleadsf.php",
		data:{'Step_Number':Step_Number},
		success: function(data){
			$('.Continue_upload_loading , .section-3, .section-4').fadeOut();
			setTimeout(function(){
       $('.section-16').fadeIn();
     }, 500);

			$('.Progress_Status').html('100%').css({'width':'100%'});
		}
	});
});



$("#fileupload1").change(function () {

	if($(this).val != ''){
		var fileExtension = ['pdf'];
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			//alert("Only formats are allowed : "+fileExtension.join(', '));
			alert("Only 'Pdf' format is allowed");
		}
		$('.fileupload1').addClass('added');
	}else {
		$('.fileupload1').removeClass('added');
	}
});

$("#fileupload2").change(function () {

	if($(this).val != ''){
		var fileExtension = ['pdf'];
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			//alert("Only formats are allowed : "+fileExtension.join(', '));
			alert("Only 'Pdf' format is allowed");
		}
		$('.fileupload2').addClass('added');
	}else {
		$('.fileupload2').removeClass('added');
	}
});

$("#fileupload3").change(function () {

	if($(this).val != ''){
		var fileExtension = ['pdf'];
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			//alert("Only formats are allowed : "+fileExtension.join(', '));
			alert("Only 'Pdf' format is allowed");
		}
		$('.fileupload3').addClass('added');
	}else {
		$('.fileupload3').removeClass('added');
	}
});

</script>
</body>
</html>
