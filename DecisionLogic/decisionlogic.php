<?php
error_reporting(E_ALL);
ini_set('display_errors',0);

define('fullpath', $_SERVER['DOCUMENT_ROOT'] . '/');

require_once fullpath.'utils.php';

class DecisionLogic {

  public function __construct(){
    $this->SoapClient=new SoapClient('https://integration.decisionlogic.com/integration-v2.asmx?wsdl');
    $this->DataBasePDO=new DataBasePDO();
    $this->UtilsClass = new UtilsClass();
  }

  public function WRCCodeAjax() {
    if (!empty($_COOKIE['RCcookie'])) {
      return $_COOKIE['RCcookie'];
    }
  }

  public function merchantCompletedRenewalFlow($campaignInfo){
    $campaignInfo=json_decode($campaignInfo,true);

    if (preg_match('#^00Q#', $campaignInfo['salesForceId']) === 1) {
      $idfor='WhoId';
    } else {
      $idfor='WhatId';
    }

    $id=trim($campaignInfo['salesForceId']);
    print_r($id);
    if(!empty($campaignInfo['utm_campaign'])){
      $records[0] = new SObject();
      $records[0]->Id = $id;
      $records[0]->fields = array(
          'pi__utm_campaign__c'=>$campaignInfo['utm_campaign']
      );
      $records[0]->type = 'Lead';
      $response=SalesforceUtils::updateObject($records);
    }


    $this->UtilsClass->updateTask($idfor,$id,'Merchant Completed Renewal Flow '.$campaignInfo['utm_campaign']);

    $msg = "<div>".$campaignInfo['firstname']." completed the online KCF Renewal Flow. Please review ";
    $msg .= "<a href='https://na48.salesforce.com/" . $id . "'>" . " Salesforce Link" . "</a></div>";

    $postFieldsToMention = new stdClass();

    $postFieldsToMention->body->messageSegments[0]->type = 'Text';
    $postFieldsToMention->body->messageSegments[0]->text = 'Renewal Flow Completed by Merchant '.$campaignInfo['utm_campaign'];

    $postFieldsToMention->body->messageSegments[1]->type = 'Mention';
    $postFieldsToMention->body->messageSegments[1]->id = $campaignInfo['ownerid'];

    $postFieldsToMention->body->messageSegments[2]->type = 'Mention';
    $postFieldsToMention->body->messageSegments[2]->id = RENEWALS_TEAM;

    $postFieldsToMention->feedElementType = 'FeedItem';
    $postFieldsToMention->subjectId = $id;
    $postFieldsToMention = json_encode($postFieldsToMention);

    SalesforceUtils::tagInSalesforce($id, $postFieldsToMention);

    // if(deployType=='PROD'){
    //   $this->UtilsClass->sendEmailWithCC('it@knightcapitalfunding.com', $campaignInfo['ownerEmail'],'Renewal Flow Completed by Merchant', wordwrap($msg, 70, "\r\n"),'retention@knightcapitalfunding.com');
    //   if($campaignInfo['campaignStatus']=='CF_PaidRenewal'){
    //     $this->UtilsClass->sendEmail('info@knightcapitalfunding.com', $campaignInfo['email'],'We received your application', $this->templateForCFREnewals($campaignInfo['firstname']));
    //   }
    // }elseif(deployType=='DEV'){
    //   $this->UtilsClass->sendEmailWithCC('it@knightcapitalfunding.com', $campaignInfo['ownerEmail'],'Renewal Flow Completed by Merchant', wordwrap($msg, 70, "\r\n"),'poorna@ibusinesssoftware.in');
    //   if($campaignInfo['campaignStatus']=='CF_PaidRenewal'){
    //     $this->UtilsClass->sendEmail('info@knightcapitalfunding.com', $campaignInfo['email'],'We received your application', $this->templateForCFREnewals($campaignInfo['firstname']));
    //   }
    // }

  }

  public function checkVDL() {
    if (!empty($_COOKIE['RCcookie'])) {
      $credential = $this->DecisionLogicCredentials();
      $serviceKey = $credential['serviceKey'];
      $siteUserGuid = $credential['siteUserGuid'];
      $requestCode = $_COOKIE['RCcookie'];
      $params = array(
        'serviceKey' => $serviceKey,
        'siteUserGuid' => $siteUserGuid,
        'requestCode' => $requestCode,
      );
      $data = $this->SoapClient->GetReportDetailFromRequestCode7($params);
      $SoapArrayJson = json_encode($data);
      //$SoapArray=json_decode($SoapArrayJson, true);
      return $SoapArrayJson;
    }
  }

  public function DecisionLogicCredentials() {
    return array("serviceKey" => "N3H9J9JAQC2Z",
      "siteUserGuid" => "54923a08-3902-45c9-a62f-e5d1c5d91ace",
      "customerId" => "Integration",
      "profileGuid" => profileGuid,
    );
  }

  public function templateForCFREnewals($firstname){
    return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head><meta http-equiv="Content-Type" content="text/html;" /><title>Email Newsletter</title></head><body style="font-family: Helvetica, Arial, sans-serif; font-size: 12px">
    <p><a name="_GoBack"></a> Hi '.$firstname.'</p>
    
    <p>&nbsp;</p>
    
    <p>We have received your application for working capital and will review it right away. If your application is approved and funded, you will receive a $250 Amazon gift card.</p>
    
    <p>&nbsp;</p>
    
    <p>Thank you for choosing Knight Capital Funding!</p>
    
    <p>&nbsp;</p>
    
    <p><font face="Helvetica, serif"><font size="2">Best,</font></font></p>
    
    <p><font face="Helvetica, serif"><font size="2"><b>Orlando Lacayo</b></font></font></p>
    
    <p><font face="Helvetica, serif"><font size="2">Customer Service Manager</font></font></p>
    
    <p><font face="Helvetica, serif"><font size="2">Knight Capital Funding </font></font></p>
    
    <p><font face="Helvetica, serif"><font size="2">| T: 855-4624249</font></font></p>
    
    <p><font face="Helvetica, serif"><font size="2">| W: </font></font><a href="http://www.knightcapitalfunding.com/"><font color="#1155cc"><font face="Helvetica, serif"><font size="2"><u>www.KnightCapitalFunding.com</u></font></font></font></a></p>
    
    <p>&nbsp;</p>
    
    <p><img align="bottom" border="0" height="36" name="image1.png" src="file:///C:/Users/Dileep/AppData/Local/Temp/lu139721pfmm.tmp/lu139721pfmx_tmp_a15d5035d59546f3.png" width="193"></p>
    
    <p>&nbsp;</p>
    
    <p>&nbsp;</p>
    
    <p align="justify"><font size="1"><i>All applications received are subject to approval and underwriting. </i></font><font size="1"><i>All Amazon gift cards are processed once a month. If your application is approved and funded, you will receive an email with a link to the Amazon gift card after processing. .</i></font><font size="1"><i>The contents of this e-mail are the property of Knight Capital LLC and its affiliates.&nbsp; It this e-mail was not addressed to you, you have no legal right to read it.&nbsp; If you think you received this e-mail in error, please notify the sender.&nbsp; Do not forward or copy without permission of the sender.&nbsp; This message may contain an advertisement of a product or service, and thus may constitute a commercial electronic mail message under US law.&nbsp; You may unsubscribe at any time from receiving commercial electronic messages from Knight Capital LLC and its affiliates by clicking on the following link: </i></font><font color="#0563c1"><u><a href="http://unsubscribe.knightcapitalfunding.com/"><font color="#000000"><font size="1"><i>http://unsubscribe.knightcapitalfunding.com</i></font></font></a></u></font></p>
    
    <p>&nbsp;</p>
    
    <p align="center"><font size="1"><i>Knight Capital Funding, 9 East Loockerman St., Ste. 3A-543, Dover, DE 19901; </i></font><font color="#0563c1"><u><a href="http://www.knightcapitalfunding.com/"><font color="#000000"><font size="1"><i>www.knightcapitalfunding.com</i></font></font></a></u></font><font size="1"><i>.</i></font></p>
    </body></html>';
  }

  public function CreateRequestCode($contentServiceId) {
    $credential = $this->DecisionLogicCredentials();
    $serviceKey = $credential['serviceKey'];
    $siteUserGuid = $credential['siteUserGuid'];
    $profileGuid = $credential['profileGuid'];
    $customerId = $credential['customerId'];

    if(!empty($_COOKIE['campaign_key'])){
      $Sql = 'SELECT * FROM campaign WHERE campaign_key="' . $_COOKIE['campaign_key'].'"';
    }else{
      $Sql = 'SELECT * FROM submissions WHERE ID=' . $_COOKIE['LeadId'];
    }

    $UserDataRC = $this->DataBasePDO->getOneRow($Sql);

    if (!empty($_COOKIE['RCcookie'])) {
      $params = array(
        'serviceKey' => $serviceKey,
        'requestCode' => $_COOKIE['RCcookie'],
      );
      $data = $this->SoapClient->DeleteRequest($params);
    }

    $reqparams=array(
        'serviceKey' => $serviceKey,
        'profileGuid'=> $profileGuid,
        'customerId'=> $customerId,
        'siteUserGuid'=> $siteUserGuid,
        'contentServiceId'=> $contentServiceId,
        'firstName'=> urlencode($UserDataRC['firstname']),
        'lastName'=>'',
        'accountNumber'=>'',
        'routingNumber'=>'',
        'emailAddress'=>''
      );
    $data=$this->SoapClient->CreateRequest4($reqparams);
    $data=$data->CreateRequest4Result;

    setcookie("RCcookie", $data, strtotime("+1 year"), "/");

    if(!empty($_COOKIE['campaign_key'])){
      $updatere = "UPDATE campaign SET RequestCode='" . $data . "'  where campaign_key='" . $_COOKIE['campaign_key']."'";
    }else{
      $updatere = "UPDATE submissions SET RequestCode='" . $data . "'  where ID=" . $_COOKIE['LeadId'];
    }

    //print_r($updatere);
    $this->DataBasePDO->executeQuery($updatere);
    return $data;

  }
  public function GetDataRequestCode($requestCode) {
    $credential = $this->DecisionLogicCredentials();
    $serviceKey = $credential['serviceKey'];
    $siteUserGuid = $credential['siteUserGuid'];
    $params = array(
      'serviceKey' => $serviceKey,
      'siteUserGuid' => $siteUserGuid,
      'requestCode' => $requestCode,
    );
    $data = $this->SoapClient->GetMultipleReportDetailsFromRequestCode6($params);
    $SoapArray = json_decode(json_encode($data), true);
    return $SoapArray;
  }

  public function GetData($RECODE) {
    $data = $this->GetDataRequestCode($RECODE);
    //print_r($data);die();
    $data = $data['GetMultipleReportDetailsFromRequestCode6Result'];
    /**
    * Check If There id no transcations available in last 90 Days 
    * @author : Ravindra Singh
    **/ 
    $lastTransactionDate = (isset($data['ReportDetail4'][0]['TransactionSummaries']['TransactionSummary4'][0]['TransactionDate']))?$data['ReportDetail4'][0]['TransactionSummaries']['TransactionSummary4'][0]['TransactionDate']:0;
    //echo $lastTransactionDate; exit;
    if(strtotime($lastTransactionDate) < strtotime('-90 days') || $lastTransactionDate == '0') {
      return "transaction90DaysExceeded";
    }
    $AccInfo = "<h3 class='account_select_head'>Please Select Your Business Bank Account</h3> <div class='account_select_content'><ul id='showaccountsul'>";
    if (!empty($data["ReportDetail4"][0]['RequestCode'])) {
      //print_r($data['ReportDetail4']);
      $ig = 0;
      $OnlyForSingleAccount = '';
      foreach ($data['ReportDetail4'] as $value) {
        if ($value['AccountType'] == 'CHECKING') {
          $OnlyForSingleAccount = $value;
          $AccInfo .= "<li><label class='checkbox-inline'><input value='" . $value['AccountNumberFound'] . "' id='" . $value['AccountNumberFound'] . "' autocomplete='off' type='radio' name='CHECKING'/><p><b>" . $value['AccountNumberFound'] . " - <span>" . $value['AccountName'] . "</b></span></p></label></li>";
          $ig++;
        }
      }
      if ($ig == 1) {
        if(!empty($_COOKIE['campaign_key'])){
          $LeadID=$this->createALeadInSubmission($_COOKIE['campaign_key']);
          $this->InsertDataDetail($OnlyForSingleAccount, $LeadID, "BusinessAccount");
          return "SingleAccount";
        }else{
          $this->InsertDataDetail($OnlyForSingleAccount, $_COOKIE['LeadId'], "BusinessAccount");
          return "SingleAccount";
        }

      } elseif (!empty($AccInfo)) {
        $AccInfo .= "</ul><button class='account_select_btn' onclick='UseThisButtonWhenUserSelectAccount()'>Use This Account</button></div>";
        return $AccInfo;
      } else {
        return '<div class="account_select_empty text-center"> <h3>Error, No Checking Account Found.</h3> <p>Sorry, we were unable to find a Business Checking account. Please connect to another account.</p> <button class="account_select_btn" onclick="try_diff_acc()">Try Different Account</button> </div>';
      }
    } else {

      if(!empty($_COOKIE['campaign_key'])){
        $LeadId=$this->createALeadInSubmission($_COOKIE['campaign_key']);
        $value = $data["ReportDetail4"];
        if ($value['AccountType'] == 'CHECKING') {
          $this->InsertDataDetail($data["ReportDetail4"], $LeadId, "BusinessAccount");
          return "SingleAccount";
        } else {
          return '<div class="account_select_empty text-center"> <h3>Error, No Checking Account Found.</h3> <p>Sorry, we were unable to find a Business Checking account. Please connect to another account.</p> <button class="account_select_btn" onclick="try_diff_acc()">Try Different Account</button> </div>';
        }
      }else{
        $value = $data["ReportDetail4"];
        if ($value['AccountType'] == 'CHECKING') {
          $this->InsertDataDetail($data["ReportDetail4"], $_COOKIE['LeadId'], "BusinessAccount");
          return "SingleAccount";
        } else {
          return '<div class="account_select_empty text-center"> <h3>Error, No Checking Account Found.</h3> <p>Sorry, we were unable to find a Business Checking account. Please connect to another account.</p> <button class="account_select_btn" onclick="try_diff_acc()">Try Different Account</button> </div>';
        }
      }

    }
  }

  public function SelectedAccountTrans($AccountNbr, $RECODE, $LEADID) {
    //print_r($AccountNbr." - ".$RECODE." - ".$LEADID);
    $data = $this->GetDataRequestCode($RECODE);
    $data = $data['GetMultipleReportDetailsFromRequestCode6Result'];
    //print_r(json_encode($data['ReportDetail4']));die();
    foreach ($data['ReportDetail4'] as $value) {
      if ($value['AccountNumberFound'] == $AccountNbr) {
        $BTYPE = "BusinessAccount";
      } else {
        $BTYPE = '';
      }
      if(!empty($value['TransactionSummaries']['TransactionSummary4'])){
        $this->InsertDataDetail($value, $LEADID, $BTYPE);
      }
    }
  }

  public function SearchBanksByBankName($bankName) {
    $credential = $this->DecisionLogicCredentials();
    $serviceKey = $credential['serviceKey'];
    $params = array(
      'serviceKey' => $serviceKey,
      'bankName' => $bankName,
    );
    $data = $this->SoapClient->SearchBanksByBankName($params);
    $SoapArray = json_encode($data);
    print_r($SoapArray);
  }

  public function GetBankInformationByContentServiceId() {
    $credential = $this->DecisionLogicCredentials();
    $serviceKey = $credential['serviceKey'];
    $params = array(
      'serviceKey' => $serviceKey,
      'contentServiceId' => 200,
    );
    $data = $this->SoapClient->GetBankInformationByContentServiceId($params);
    $SoapArray = json_encode($data);
    print_r($SoapArray);
  }

  public function createALeadInSubmission($campaign_key){
    $sql = "SELECT * FROM campaign WHERE campaign_key ='".$campaign_key."'";
    $result = $this->DataBasePDO->getOneRow($sql);
    $UserData['salesforceID']=$result['salesForceId'];
    $UserData['RequestCode']=$result['RequestCode'];
    $UserData['applykey']=$campaign_key;
    $UserData['legalname']=$result['legalName'];
    $UserData['oldstep']=7;
    $UserData['phone']=$result['Phone'];
    $UserData['LeadSource']=$result['type'];
    $data=$this->DataBasePDO->insertData('submissions',$UserData);
    $LeadId=$this->DataBasePDO->getLastInsertId();
    $Status['status'] = 1;
    $whereAr = array("campaign_key" => "'" . $campaign_key . "'");
    $this->DataBasePDO->updateData('campaign', $Status, $whereAr);
    setcookie("LeadId", $LeadId, strtotime("+1 year"), "/");
    return $LeadId;
  }

  public function DeleteRequestManully($rec) {
    $credential = $this->DecisionLogicCredentials();
    $serviceKey = $credential['serviceKey'];
    $siteUserGuid = $credential['siteUserGuid'];
    $profileGuid = $credential['profileGuid'];
    $customerId = $credential['customerId'];
    $params = array(
      'serviceKey' => $serviceKey,
      'requestCode' => $rec,
    );
    $data = $this->SoapClient->DeleteRequest($params);
  }

  public function ReMove($data) {
    $wht = array("*", '-');
    $where = array('', '');
    return str_replace($wht, $where, $data);
  }

  public function InsertDataDetail($data, $lead_id, $BusinessAccount) {
    //print_r($data);//die();
    $UserData["lead_id"] = $lead_id;
    $UserData["EmailAddress"] = $data["EmailAddress"];
    $UserData["InstitutionName"] = $data["InstitutionName"];
    $UserData["AccountName"] = $data["AccountName"];
    $UserData["RoutingNumberEntered"] = $data["RoutingNumberEntered"];
    $UserData["AccountType"] = $data["AccountType"];
    $UserData["AccountNumberEntered"] = $this->ReMOve($data["AccountNumberEntered"]);
    $UserData["AccountNumberFound"] = $this->ReMove($data["AccountNumberFound"]);
    $UserData["NameEntered"] = $data["NameEntered"];
    $UserData["NameFound"] = $data["NameFound"];
    $UserData["AmountInput"] = $data["AmountInput"];
    $UserData["AvailableBalance"] = $data["AvailableBalance"];
    $UserData["IsLoginValid"] = $data["IsLoginValid"];
    $UserData["AsOfDate"] = $data["AsOfDate"];
    $UserData["ActivityStartDate"] = $data["ActivityStartDate"];
    $UserData["ActivityEndDate"] = $data["ActivityEndDate"];
    $UserData["TotalCredits"] = $data["TotalCredits"];
    if ($BusinessAccount == 'BusinessAccount') {
      $UserData["BusinessAccountSelected"] = true;
    } else {
      $UserData["BusinessAccountSelected"] = false;
    }

    $UserData["TotalDebits"] = $data["TotalDebits"];
    $UserData["CurrentBalance"] = $data["CurrentBalance"];
    $UserData["IsActivityAvailable"] = $data["IsActivityAvailable"];
    $UserData["txn_date"] = date("Y-m-d");
    $this->DataBasePDO->insertOrUpdate('DesicionLogicDetail', $UserData);
    $lastID = $this->DataBasePDO->getLastInsertId();
    foreach ($data['TransactionSummaries']['TransactionSummary4'] as $DTkey => $DTvalue) {
      $DetailTrans["lead_id"] = $lead_id;
      $DetailTrans["sub_id"] = $lastID;
      $DetailTrans["TransactionDate"] = substr($DTvalue['TransactionDate'], 0, strpos($DTvalue['TransactionDate'], 'T'));
      $DetailTrans["Amount"] = $DTvalue['Amount'];
      $DetailTrans["RunningBalance"] = $DTvalue['RunningBalance'];
      $DetailTrans["Description"] = str_replace("'", "", $DTvalue['Description']);
      $DetailTrans["IsRefresh"] = $DTvalue['IsRefresh'];
      $DetailTrans["TypeCodes"] = $DTvalue['TypeCodes'];
      $DetailTrans["Status"] = $DTvalue['Status'];
      $DetailTrans["Category"] = str_replace("'", "", $DTvalue['Category']);
      $DetailTrans["AccountID"] = $this->ReMove($data["AccountNumberFound"]);
      $DetailTrans["txn_date"] = date("Y-m-d");
      $this->DataBasePDO->insertOrUpdate('DesicionLogicTransaction', $DetailTrans);
    }
  }
}

if (!empty($_POST['requesttype'])) {
  $DecisionLogic = new DecisionLogic();
  if ($_POST['requesttype'] == 'createRC') {
    print($DecisionLogic->CreateRequestCode($_POST['ContentServiceId']));
  } elseif ($_POST['requesttype'] == 'GetData') {
    //$data=$DecisionLogic->GetData('YNZZ86','11917');
    $data = $DecisionLogic->GetData($_COOKIE['RCcookie']);
    echo $data;
  } elseif ($_POST['requesttype'] == 'SearchBanksByBankName') {
    $DecisionLogic->SearchBanksByBankName($_POST['keywordBank']);
  } elseif ($_POST['requesttype'] == 'cookiere') {
    print_r($DecisionLogic->WRCCodeAjax());
  } elseif ($_POST['requesttype'] == 'merchantCompletedRenewalFlow') {
    print_r($DecisionLogic->merchantCompletedRenewalFlow($_POST['campaignInfo']));
  } elseif ($_POST['requesttype'] == 'checkVDL') {
    print_r($DecisionLogic->checkVDL());
  } elseif ($_POST['requesttype'] == 'SelectedAccountTrans') {
    $DecisionLogic->SelectedAccountTrans($_POST['AccountNbr'], $_COOKIE['RCcookie'], $_COOKIE['LeadId']);
  } elseif ($_POST['requesttype'] == 'createALeadInSubmission') {
    $DecisionLogic->createALeadInSubmission($_COOKIE['campaign_key']);
  }
}
?>
