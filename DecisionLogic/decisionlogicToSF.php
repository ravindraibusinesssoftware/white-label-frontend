<?php
error_reporting(E_ALL);
ini_set('display_startup_errors', 0);
ini_set('display_errors', 0);

define('fullpath', $_SERVER['DOCUMENT_ROOT'] . '/');
require_once fullpath . 'utils.php';

class DecisionLogicForSF {
  public function __construct() {
    $this->SoapClient = new SoapClient('https://integration.decisionlogic.com/integration-v2.asmx?wsdl');
    $this->UtilsClass = new UtilsClass();
    $this->DataBasePDO = new DataBasePDO;
  }

  public function decisionLogicCredentials() {
    return array("serviceKey" => "N3H9J9JAQC2Z",
      "siteUserGuid" => "54923a08-3902-45c9-a62f-e5d1c5d91ace",
      "customerId" => "Integration",
      "profileGuid" => profileGuid,
    );
  }

  public function createRequestCode($contentServiceId, $oppId) {
    $appData = SalesforceUtils::getApplicationData($oppId);
    $name = explode(" ", $appData->fields->Primary_Owner__c);
    $credential = $this->decisionLogicCredentials();
    $serviceKey = $credential['serviceKey'];
    $siteUserGuid = $credential['siteUserGuid'];
    $profileGuid = $credential['profileGuid'];
    $customerId = $credential['customerId'];

    if (!empty($oppId)) {
      $params = array(
        'serviceKey' => $serviceKey,
        'requestCode' => $oppId,
      );
      $data = $this->SoapClient->DeleteRequest($params);
    }

    $reqparams = array(
      'serviceKey' => $serviceKey,
      'profileGuid' => $profileGuid,
      'customerId' => $customerId,
      'siteUserGuid' => $siteUserGuid,
      'contentServiceId' => $contentServiceId,
      'firstName' => urlencode($name[0]),
      'lastName' => '',
      'accountNumber' => '',
      'routingNumber' => '',
      'emailAddress' => '',
    );
    $data = $this->SoapClient->CreateRequest4($reqparams);
    $data = $data->CreateRequest4Result;

    setcookie("RCcookie", $data, strtotime("+1 year"), "/");
    return $data;
  }

  public function getDataForRequestCode($requestCode) {
    $credential = $this->decisionLogicCredentials();
    $serviceKey = $credential['serviceKey'];
    $siteUserGuid = $credential['siteUserGuid'];
    $params = array(
      'serviceKey' => $serviceKey,
      'siteUserGuid' => $siteUserGuid,
      'requestCode' => $requestCode,
    );
    $data = $this->SoapClient->GetMultipleReportDetailsFromRequestCode6($params);
    $SoapArray = json_decode(json_encode($data), true);
    return $SoapArray;
  }

  public function getData($RECODE, $oppId, $bankType) {
    $data = $this->getDataForRequestCode($RECODE);
    $data = $data['GetMultipleReportDetailsFromRequestCode6Result'];
    
    $AccInfo = "<h3 class='account_select_head'>Please Select Your Business Bank Account</h3> <div class='account_select_content'><ul id='showaccountsul'>";
    if (!empty($data["ReportDetail4"][0]['RequestCode'])) {
      $ig = 0;
      $OnlyForSingleAccount = '';
      foreach ($data['ReportDetail4'] as $value) {
        if ($value['AccountType'] == 'CHECKING') {
          $OnlyForSingleAccount = $value;
          $AccInfo .= "<li><label class='checkbox-inline'><input value='" . $value['AccountNumberFound'] . "' id='" . $value['AccountNumberFound'] . "' autocomplete='off' type='radio' name='CHECKING'/><p><b>" . $value['AccountNumberFound'] . " - <span>" . $value['AccountName'] . "</b></span></p></label></li>";
          $ig++;
        }
      }
      if ($ig == 1) {
        $this->insertDataDetail($OnlyForSingleAccount, $oppId, "BusinessAccount", $bankType);
        $datapdf = file_get_contents(KCF_SITE_URL . '/DecisionLogic/generatepdfToSF.php/?oppId=' . $oppId . '&bankType=' . $bankType);
        $this->sendPDFToSF($datapdf, $oppId, $bankType);
        return "SingleAccount";
      } elseif (!empty($AccInfo)) {
        $AccInfo .= <<<EOD
        </ul><button class='account_select_btn' onclick='UseThisButtonWhenUserSelectAccount("$RECODE","$bankType")'>Use This Account</button></div>
EOD;
        return $AccInfo;
      } else {
        return '<div class="account_select_empty text-center"> <h3>Error, No Checking Account Found.</h3> <p>Sorry, we were unable to find a Business Checking account. Please connect to another account.</p> <button class="account_select_btn" onclick="try_diff_acc()">Try Different Account</button> </div>';
      }
    } else {
      $value = $data["ReportDetail4"];
      if ($value['AccountType'] == 'CHECKING') {
        $this->insertDataDetail($data["ReportDetail4"], $oppId, "BusinessAccount", $bankType);
        $datapdf = file_get_contents(KCF_SITE_URL . '/DecisionLogic/generatepdfToSF.php/?oppId=' . $oppId . '&bankType=' . $bankType);

        $this->sendPDFToSF($datapdf, $oppId, $bankType);

        return "SingleAccount";
      } else {
        return '<div class="account_select_empty text-center"> <h3>Error, No Checking Account Found.</h3> <p>Sorry, we were unable to find a Business Checking account. Please connect to another account.</p> <button class="account_select_btn" onclick="try_diff_acc()">Try Different Account</button> </div>';
      }
    }
  }

  public function sendPDFToSF($data, $oppId, $bankType) {
    $data = json_decode($data);
    $i = 1;
    foreach ($data as $body) {
      SalesforceUtils::uploadAttachment(base64_decode($body), $bankType . ' Bank Stmt ' . $i . '.pdf', $oppId, 'Bank statement - Month Year for primary or secondary account');
      $i++;
    }
  }

  public function removeUnwantedCharacters($data) {
    $wht = array("*", '-');
    $where = array('', '');
    return str_replace($wht, $where, $data);
  }

  public function insertDataDetail($data, $oppId, $BusinessAccount, $bankType) {

    $UserData["oppId"] = $oppId;
    $UserData["BankType"] = $bankType;
    $UserData["EmailAddress"] = $data["EmailAddress"];
    $UserData["InstitutionName"] = $data["InstitutionName"];
    $UserData["AccountName"] = $data["AccountName"];
    $UserData["RoutingNumberEntered"] = $data["RoutingNumberEntered"];
    $UserData["AccountType"] = $data["AccountType"];
    $UserData["AccountNumberEntered"] = $this->removeUnwantedCharacters($data["AccountNumberEntered"]);
    $UserData["AccountNumberFound"] = $this->removeUnwantedCharacters($data["AccountNumberFound"]);
    $UserData["NameEntered"] = $data["NameEntered"];
    $UserData["NameFound"] = $data["NameFound"];
    $UserData["AmountInput"] = $data["AmountInput"];
    $UserData["AvailableBalance"] = $data["AvailableBalance"];
    $UserData["IsLoginValid"] = $data["IsLoginValid"];
    $UserData["AsOfDate"] = $data["AsOfDate"];
    $UserData["ActivityStartDate"] = $data["ActivityStartDate"];
    $UserData["ActivityEndDate"] = $data["ActivityEndDate"];
    $UserData["TotalCredits"] = $data["TotalCredits"];
    if ($BusinessAccount == 'BusinessAccount') {
      $UserData["BusinessAccountSelected"] = true;
    } else {
      $UserData["BusinessAccountSelected"] = false;
    }

    $UserData["TotalDebits"] = $data["TotalDebits"];
    $UserData["CurrentBalance"] = $data["CurrentBalance"];
    $UserData["IsActivityAvailable"] = $data["IsActivityAvailable"];
    $UserData["txn_date"] = date("Y-m-d");

    $resp = $this->DataBasePDO->insertOrUpdate('DesicionLogicDetail', $UserData);
    $lastID = $this->DataBasePDO->getLastInsertId();
    foreach ($data['TransactionSummaries']['TransactionSummary4'] as $DTkey => $DTvalue) {
      $DetailTrans["oppId"] = $oppId;
      $DetailTrans["sub_id"] = $lastID;
      $DetailTrans["TransactionDate"] = substr($DTvalue['TransactionDate'], 0, strpos($DTvalue['TransactionDate'], 'T'));
      $DetailTrans["Amount"] = $DTvalue['Amount'];
      $DetailTrans["RunningBalance"] = $DTvalue['RunningBalance'];
      $DetailTrans["Description"] = str_replace("'", "", $DTvalue['Description']);
      $DetailTrans["IsRefresh"] = $DTvalue['IsRefresh'];
      $DetailTrans["TypeCodes"] = $DTvalue['TypeCodes'];
      $DetailTrans["Status"] = $DTvalue['Status'];
      $DetailTrans["Category"] = str_replace("'", "", $DTvalue['Category']);
      $DetailTrans["AccountID"] = $this->removeUnwantedCharacters($data["AccountNumberFound"]);
      $DetailTrans["txn_date"] = date("Y-m-d");
      $this->DataBasePDO->insertOrUpdate('DesicionLogicTransaction', $DetailTrans);
    }
  }

  public function getDataForSelectedIfMultipleAccount($AccountNbr, $RECODE, $oppId, $bankType) {

    $data = $this->getDataForRequestCode($RECODE);
    $data = $data['GetMultipleReportDetailsFromRequestCode6Result'];
    foreach ($data['ReportDetail4'] as $value) {
      if ($value['AccountNumberFound'] == $AccountNbr) {
        $BTYPE = "BusinessAccount";
      } else {
        $BTYPE = '';
      }
      if (!empty($value['TransactionSummaries']['TransactionSummary4'])) {
        $this->insertDataDetail($value, $oppId, $BTYPE, $bankType);
        $datapdf = file_get_contents(KCF_SITE_URL . '/DecisionLogic/generatepdfToSF.php/?oppId=' . $oppId . '&bankType=' . $bankType);
        $this->sendPDFToSF($datapdf, $oppId, $bankType);
      }
    }
  }
}

if (!empty($_POST['requesttype'])) {
  $DecisionLogicForSF = new DecisionLogicForSF();
  $utils = new UtilsClass();
  if ($_POST['requesttype'] == 'createRC') {
    $requestCode = $DecisionLogicForSF->createRequestCode($_POST['ContentServiceId'], $_POST['oppId']);
    print_r($requestCode);
    $soqlToGetAccountID = "SELECT Id,Account.Id from Opportunity where id = '" . $_POST['oppId'] . "'";
    $response = SalesforceUtils::getQueryResultFromSF($soqlToGetAccountID);
    $sObject = new SObject();
    $sObject->Id = $response->records[0]->fields->Account->Id;
    $sObject->fields = array(
      'Request_Code__c' => $_POST['oppId'],
    );
    $sObject->type = 'Account';
    $createResponse = SalesforceUtils::updateObject($sObject);
    $subject = 'A request code is updated ' . $requestCode;
    $resp = $utils->updateTask('WhatId', $_POST['oppId'], $subject);
  } else if ($_POST['requesttype'] == 'GetData') {
    print_r($DecisionLogicForSF->getData($_POST['requestCode'], $_POST['oppId'], $_POST['bankType']));
  } else if ($_POST['requesttype'] == 'getDataIfMultipleAccount') {
    $DecisionLogicForSF->getDataForSelectedIfMultipleAccount($_POST['AccountNbr'], $_POST['requestCode'], $_POST['oppId']);
  }
}
