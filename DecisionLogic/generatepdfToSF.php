<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);
define('fullpath', $_SERVER['DOCUMENT_ROOT'] . '/');
require_once fullpath . 'utils.php';
$database = new DataBasePDO();
require_once fullpath . "vendorMpdf7/autoload.php";

$oppData = SalesforceUtils::getApplicationData($_GET['oppId']);
$legalName = $oppData->fields->Account_Name__c;

$DecisionDetailsql = "SELECT *
                      FROM DesicionLogicDetail
                      WHERE BusinessAccountSelected=1 AND oppId='" . $_GET['oppId'] . "' AND BankType='" . $_GET['bankType'] . "'";
$DecisionDetailresult = $database->getAllResults($DecisionDetailsql);
$AllAccountsF = array();
$df = 1;
foreach ($DecisionDetailresult as $DecisionDetailresultINV) {
  $AllDepositSql = "SELECT *
                    FROM DesicionLogicTransaction
                    WHERE Status = 'posted' AND oppId='" . $_GET['oppId'] . "' AND AccountID='" . $DecisionDetailresultINV['AccountNumberFound'] . "'
                    ORDER by Dlid DESC";
  $AllDepositResult = $database->getAllResults($AllDepositSql);
  $start_date = $AllDepositResult[0]['TransactionDate'];
  $end_date = end($AllDepositResult)['TransactionDate'];
  $Alldays = getAllDatesBetweenStartAndEnd($start_date, $end_date);

  $cal3060day = cal3060day($Alldays);
  $DateArray = $DSInfo = array();
  foreach ($AllDepositResult as $AllDepReskey => $AllDepResvalue) {
    $TransactionDate = $AllDepResvalue['TransactionDate'];
    $DateArray[$TransactionDate] = str_replace(",", "", number_format($AllDepResvalue['RunningBalance'], 2));
    if ($TransactionDate < $cal3060day[1]) {
      if (strpos(strtolower($AllDepResvalue['TypeCodes']), 'dp') !== false) {
        $DSInfo[1]['Deposits'][] = $AllDepResvalue;
      } else {
        $DSInfo[1]['Withdrawal'][] = $AllDepResvalue;
      }
    } elseif ($TransactionDate < $cal3060day[0]) {
      if (strpos(strtolower($AllDepResvalue['TypeCodes']), 'dp') !== false) {
        $DSInfo[2]['Deposits'][] = $AllDepResvalue;
      } else {
        $DSInfo[2]['Withdrawal'][] = $AllDepResvalue;
      }
    } else {
      if (strpos(strtolower($AllDepResvalue['TypeCodes']), 'dp') !== false) {
        $DSInfo[3]['Deposits'][] = $AllDepResvalue;
      } else {
        $DSInfo[3]['Withdrawal'][] = $AllDepResvalue;
      }
    }
  }

  $dataT = array_reverse(TotalDaysCal($DateArray));

  foreach ($dataT as $dataTkey => $dataTvalue) {
    if ($dataTkey < $cal3060day[1]) {
      $DSInfo[1]['DailySummary'][$dataTkey] = $dataTvalue;
    } elseif ($dataTkey < $cal3060day[0]) {
      $DSInfo[2]['DailySummary'][$dataTkey] = $dataTvalue;
    } else {
      $DSInfo[3]['DailySummary'][$dataTkey] = $dataTvalue;
    }
  }

  $DateArrayF = array();
  foreach ($DSInfo as $DateArraykey => $DateArrayvalue) {
    $avgsum = 0;
    $Negatives = 0;
    foreach ($DateArrayvalue['DailySummary'] as $DateArrayvaluekey => $DateArrayvaluevalue) {
      if ($DateArrayvaluevalue < 0) {$Negatives++;}
      $avgsum += $DateArrayvaluevalue;
    }
    $withdrawalsum = $depositsum = 0;
    foreach ($DateArrayvalue['Deposits'] as $depkey => $depvalue) {
      $depositsum += $depvalue['Amount'];
    }

    foreach ($DateArrayvalue['Withdrawal'] as $wdkey => $wdvalue) {
      $withdrawalsum += $wdvalue['Amount'];
    }

    $DateArrayF[$DateArraykey]['AllData'] = $DateArrayvalue;
    $DateArrayF[$DateArraykey]['Deposits'] = $depositsum;
    $DateArrayF[$DateArraykey]['Withdrawal'] = abs($withdrawalsum);
    $DateArrayF[$DateArraykey]['AvgSum'] = round($avgsum / sizeof($DateArrayvalue['DailySummary']), 2);
    $DateArrayF[$DateArraykey]['DailyNegatives'] = $Negatives;
  }

  $AccNbrF = $DecisionDetailresultINV['AccountNumberFound'];
  $AllAccounts[$AccNbrF]["Datatoshow"] = $DecisionDetailresultINV;
  $AllAccounts[$AccNbrF]["DateArrayF"] = $DateArrayF;

}

$attachment = [];
//print_r(json_encode($AllAccounts));die();
foreach ($AllAccounts as $AllAccountskey => $AllAccountsval) {

  foreach ($AllAccountsval['DateArrayF'] as $AllDFkey => $AllDFval) {

    $html = '<!DOCTYPE html>
          <html>
            <head>
              <title></title>

              <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">

              <style type="text/css">
                table , caption,p{
                  width: 100%;
                  font-family: "Open Sans", sans-serif;
                }
                h1,h2,h3,h4,h5,h6,p{
                  margin: 0;
                }
              </style>
            </head>
            <body>

              <div class="out_wrapper" style="width: 1000px; margin: 0 auto;">
              <div class="in_wrapper" style="width: 800px; margin: 0 auto;">';

    $depositsum = $AllDFval['Deposits'];
    $withdrawalsum = $AllDFval['Withdrawal'];
    $avgsummary = $AllDFval['AvgSum'];

    $stateddate = array_keys($AllDFval['AllData']['DailySummary'])[0];
    $endarray = array_keys($AllDFval['AllData']['DailySummary']);
    $enddate = end($endarray);

    $html .= '<table cellpadding="5">
          <tbody>
          <tr>
            <td style="width: 40%;">
              <table style=" margin: 0 auto; text-align: center;">
                <tr>
                  <td>
                    <img src="/checkout/img/kcflogo.jpg" alt="logo" style="width:300px" />
                  </td>
                </tr>
              </table>
            </td>
            <td style="width: 60%;">
              <table cellpadding="5">
                <tr>
                  <td>
                    <table cellpadding="0">
                      <tr>
                        <td><b style="font-size: 13px;">Account Name </b></td>
                        <td><b style="font-size: 13px;">: </b></td>
                        <td><b style="font-size: 13px;">' . $legalName . '</b></td>
                      </tr>
                      <tr>
                        <td><b style="font-size: 13px;">Account Type </b></td>
                        <td><b style="font-size: 13px;">: </b></td>
                        <td><b style="font-size: 13px;">' . $AllAccountsval['Datatoshow']['AccountType'] . '</b></td>
                      </tr>
                      <tr>
                        <td><b style="font-size: 13px;">Account Number </b></td>
                        <td><b style="font-size: 13px;">: </b></td>
                        <td><b style="font-size: 13px;">' . $AllAccountsval['Datatoshow']['AccountNumberFound'] . '</b></td>
                      </tr>
                      <tr>
                        <td><b style="font-size: 13px;">Bank Name </b></td>
                        <td><b style="font-size: 13px;">: </b></td>
                        <td><b style="font-size: 13px;">' . $AllAccountsval['Datatoshow']['InstitutionName'] . '</b></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          </tbody>
        </table>
        <br/>
        <table>
          <tr>
            <td>
              <table cellpadding="0" >

                <tr>
                  <td>
                    <table cellpadding="0"  cellspacing="0">
                      <tr>
                        <td style="width: 70%;"><h4 style="margin: 0;  font-size: 18px;">' . date("F d, Y", strtotime($stateddate)) . ' to ' . date("F d, Y", strtotime($enddate)) . '</h4></td>
                        <td style="width: 30%;text-align: right;"><p style="text-align: right;  font-size: 13px; float: right;">Account number: ' . $AllAccountsval['Datatoshow']['AccountNumberFound'] . '</p></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br/>
        <table>
          <tr>
            <td>
              <h2 style="font-size: 22px;padding-bottom: 10px;padding-top: 10px; color: #1880f8">Account Summary</h2>
            </td>
          </tr>
          <tr>
            <td>
              <table>
                <tr>
                  <td style="width: 60%; padding-right: 30px;">
                    <table  cellpadding="0"  cellspacing="0">
                      <tr>
                        <td style="border-bottom: 1px solid #a4a3a3; padding: 5px 0; font-size: 13px;">Deposits and other credits</td>
                        <td style="border-bottom: 1px solid #a4a3a3; padding: 5px 0; font-size: 13px;">$' . number_format($depositsum, 2) . '</td>
                      </tr>
                      <tr>
                        <td style="border-bottom: 1px solid #a4a3a3; padding: 5px 0; font-size: 13px;">Withdrawals and other debits</td>
                        <td style="border-bottom: 1px solid #a4a3a3; padding: 5px 0; font-size: 13px;">$' . number_format(abs($withdrawalsum), 2) . '</td>
                      </tr>
                      <tr>
                        <td style="border-bottom: 1px solid #a4a3a3; padding: 5px 0; font-size: 13px;">Average ledger balance</td>
                        <td style="border-bottom: 1px solid #a4a3a3; padding: 5px 0; font-size: 13px;">$' . number_format($avgsummary, 2) . '</td>
                      </tr>


                    </table>
                  </td>
                  <td style="width: 40%;">
                    <table  cellpadding="0"  cellspacing="0">
                      <tr><td style="font-size: 11px;padding: 5px 0;"># of deposits/credits:' . sizeof($AllDFval['AllData']['Deposits']) . '</td></tr>
                      <tr><td style="font-size: 11px;padding: 5px 0;"># of withdrawals/debits: ' . sizeof($AllDFval['AllData']['Withdrawal']) . '</td></tr>
                      <tr><td style="font-size: 11px;padding: 5px 0;"># of days in cycle: 30</td></tr>
                      <tr><td style="font-size: 11px;padding: 5px 0;">Includes paid,deposited items & other debits</td></tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <br/>

        <table cellpadding="0"  cellspacing="0" style="page-break-inside:auto">
          <caption style="font-size: 22px;font-weight:700;text-align:left;padding-bottom: 10px;padding-top: 10px; color:#1880f8"><h2 style="font-size: 22px;">Deposits and Other Credits</h2></caption>
          <thead>
            <tr>
              <th style="border-bottom: 1px solid #a4a3a3;text-align:left; padding: 5px 0;font-weight: 700; font-size: 13px;"><b>Date</b></th>
              <th style="border-bottom: 1px solid #a4a3a3;text-align:center; padding: 5px 0;font-weight: 700; font-size: 13px;"><b>Description</b></th>
              <th style="border-bottom: 1px solid #a4a3a3;text-align:right; padding: 5px 0;font-weight: 700; font-size: 13px;"><b></b></th>
            </tr>
          </thead>
          <tbody>';
    foreach ($AllDFval['AllData']['Deposits'] as $AllDFval1) {
      $html .= '<tr>
          <td style="border-bottom: 1px solid #a4a3a3; text-align:left; padding: 5px 0; font-size: 13px;">' . $AllDFval1['TransactionDate'] . '</td>
          <td style="border-bottom: 1px solid #a4a3a3; text-align:left; padding: 5px 0; font-size: 13px;width: 76%">' . $AllDFval1['Description'] . '</td>
          <td style="border-bottom: 1px solid #a4a3a3; text-align:right; padding: 5px 0; font-size: 13px;">$' . number_format(abs($AllDFval1['Amount']), 2) . '</td>
        </tr>';
    }
    $html .= '
        </tbody>
        </table>


        <br/>

        <table cellpadding="0"  cellspacing="0" style="page-break-inside:auto">
          <caption style="font-size: 22px;font-weight:700;text-align:left;padding-bottom: 10px;padding-top: 10px; color:#1880f8"><h2 style="font-size: 22px;">Withdrawals And Debits</h2></caption>
          <thead>
            <tr>
              <th style="border-bottom: 1px solid #a4a3a3;text-align:left; padding: 5px 0;font-weight: 700; font-size: 13px;"><b>Date</b></th>
              <th style="border-bottom: 1px solid #a4a3a3;text-align:center; padding: 5px 0;font-weight: 700; font-size: 13px;"><b>Description</b></th>
              <th style="border-bottom: 1px solid #a4a3a3;text-align:right; padding: 5px 0;font-weight: 700; font-size: 13px;"><b></b></th>
            </tr>
          </thead>
          <tbody>';
    foreach ($AllDFval['AllData']['Withdrawal'] as $AllDFval1) {
      $html .= '<tr>
          <td style="border-bottom: 1px solid #a4a3a3; text-align:left; padding: 5px 0; font-size: 13px;">' . $AllDFval1['TransactionDate'] . '</td>
          <td style="border-bottom: 1px solid #a4a3a3; text-align:left; padding: 5px 0; font-size: 13px;width: 76%">' . $AllDFval1['Description'] . '</td>
          <td style="border-bottom: 1px solid #a4a3a3; text-align:right; padding: 5px 0; font-size: 13px;">$' . number_format(abs($AllDFval1['Amount']), 2) . '</td>
        </tr>';
    }
    $html .= '

      </tbody>
    </table>
    <br/>



    <table cellpadding="0"  cellspacing="0" style="width: 33%;padding-right: 20px;">
      <caption style="font-size: 22px;font-weight:700;text-align:left;padding-bottom: 10px;padding-top: 10px; color:#1880f8"><h2 style="font-size: 22px;">Daily Ledger Balances</h2></caption>

      <thead>
        <tr>
          <th style="border-bottom: 1px solid #a4a3a3; padding: 5px 0; font-size: 13px;">Date</th>
          <th style="border-bottom: 1px solid #a4a3a3; padding: 5px 0; font-size: 13px;">Balance($)</th>
        </tr>
      </thead>
      <tbody>';

    foreach ($AllDFval['AllData']['DailySummary'] as $fgdhfKey => $fgdhfVal) {
      $html .= '<tr>
          <td style="border-bottom: 1px solid #a4a3a3; padding: 5px 0; font-size: 13px;">' . $fgdhfKey . '</td>
          <td style="border-bottom: 1px solid #a4a3a3; padding: 5px 0; font-size: 13px;text-align: right;">' . number_format($fgdhfVal, 2) . '</td>
        </tr>';
    }
    $html .= '
      </tbody>
    </table>

    ';

    $html .= '
    </div>
    </div>

    </body>
    </html>
    ';

    if (strpos($AllAccountsval['Datatoshow']['InstitutionName'], '- Bank')) {
      $bank = trim(substr($AllAccountsval['Datatoshow']['InstitutionName'], 0, strpos($AllAccountsval['Datatoshow']['InstitutionName'], '- Bank')));
    } else {
      $bank = $AllAccountsval['Datatoshow']['InstitutionName'];
    }

    $filename = $bank . $AllDFkey;
    $mdfasattachment = createpdf($html);
    array_push($attachment, base64_encode($mdfasattachment));
  }

}

$attachment = json_encode($attachment);
print_r($attachment);

function createpdf($html) {
  $mpdf = new \Mpdf\Mpdf();
  $mpdf->SetHTMLHeader('<p style="font-size:10px;font-family: "Open Sans", sans-serif;">NOTE: THIS IS A RENDERING OF A BANK STATEMENT FROM A DIRECT FEED TO THE CLIENTS BANK ACCOUNT. THIS IS NOT A REAL BANK STATEMENT</p>');
  $mpdf->SetDisplayMode('fullpage');
  $mpdf->WriteHTML($html);
  return $mpdf->Output('', \Mpdf\Output\Destination::STRING_RETURN);
}

function getAllDatesBetweenStartAndEnd($start, $end) {
  $datediff = strtotime($end) - strtotime($start);
  $datediff = floor($datediff / (60 * 60 * 24));
  $Alldays = array();
  for ($i = 0; $i < $datediff + 1; $i++) {
    $Alldays[] = date("Y-m-d", strtotime($start . ' + ' . $i . 'day'));
  }
  return array_reverse($Alldays);
}

function cal3060day($Alldays) {
  $day3060 = array();
  $rt = 1;
  foreach ($Alldays as $dayskey => $daysvalue) {
    if ($rt == 30 || $rt == 60) {
      $day3060[] = $daysvalue;
    }
    $rt++;
  }
  return $day3060;
}

function TotalDaysCal($AllAccounts) {
  $endarray = array_keys($AllAccounts);
  $end_date = date("Y-m-d", strtotime(end($endarray)));
  $start_date = date("Y-m-d", strtotime(array_keys($AllAccounts)[0]));
  $functionfordates = getAllDatesBetweenStartAndEnd($start_date, $end_date);
  foreach ($functionfordates as $SingleAKey => $SingleAValue) {
    if (!empty($AllAccounts[$SingleAValue])) {
      $thevalue = $AllAccounts[$SingleAValue];
    }
    $month = date('m', strtotime($SingleAValue));
    $FDays[$SingleAValue] = $thevalue;
  }
  return $FDays;
}

?>
