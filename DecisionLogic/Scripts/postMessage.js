(function ($) { var g, d, j = 1, a, b = this, f = !1, h = "postMessage", e = "addEventListener", c, i = b[h] && !$.browser.opera; $[h] = function (k, l, m) { if (!l) { return } k = typeof k === "string" ? k : $.param(k); m = m || parent; if (i) { m[h](k, l.replace(/([^:]+:\/\/[^\/]+).*/, "$1")) } else { if (l) { m.location = l.replace(/#.*$/, "") + "#" + (+new Date) + (j++) + "&" + k } } }; $.receiveMessage = c = function (l, m, k) { if (i) { if (l) { a && c(); a = function (n) { if ((typeof m === "string" && n.origin !== m) || ($.isFunction(m) && m(n.origin) === f)) { return f } l(n) } } if (b[e]) { b[l ? e : "removeEventListener"]("message", a, f) } else { b[l ? "attachEvent" : "detachEvent"]("onmessage", a) } } else { g && clearInterval(g); g = null; if (l) { k = typeof m === "number" ? m : typeof k === "number" ? k : 100; g = setInterval(function () { var o = document.location.hash, n = /^#?\d+&/; if (o !== d && n.test(o)) { d = o; l({ data: o.replace(n, "") }) } }, k) } } } })(jQuery);

function processMessage(s) {
  var p = s.split("|");
  if (p.length == 2) {
    if (p[0] == "Alert") {
      alert(p[1]);
    }
    if (p[0] == "Redirect") {
      document.location.href = p[1];
    }
    if (p[0] == "Function") {
      var v = p[1].split(',');
      if (v.length == 22) {
        IAVResult(v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[8], v[9], v[10], v[11], v[12], v[13], v[14], v[15], v[16], v[17], v[18], v[19], v[20], v[21]);
      }
    }
    if (p[0] == "JSON") {
      IAVResultJSON(p[1]);
    }
  }
}