<html data-wf-page="5963a518fff7b87ae7b97c82" data-wf-site="5963a518fff7b87ae7b97c81">
<head>
  <meta charset="utf-8">
  <title>tiger</title>
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="New-Tiger/css/normalize.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/bootstrap.min.css" >
  <link href="New-Tiger/css/webflow.css" rel="stylesheet" type="text/css">
  <link href="New-Tiger/css/tiger-a3c537.webflow.css" rel="stylesheet" type="text/css">
  <link href="New-Tiger/css/custom.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">WebFont.load({
  google: {
    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic"]
  }
  });
</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);
  </script>
  <link rel="shortcut icon" type="image/png" href="New-Tiger/images/favicon-tiger.png"/>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>

     <header class="header_normal">
	<div class="container">
		<div class="row">
		<div class="col-sm-6 clearfix col-sm-push-6">
				<div class="header_right pull-right">
					<a id="telNum" href="tel:8555344940">855-534-4940</a>
				</div>
			</div>
			<div class="col-sm-6 clearfix col-sm-pull-6">
				<div class="header_left pull-left">
					<h1 class="logo">
						<a href="https://apply.tigerfundingllc.com">
							<img src="images/gotiger-logo.svg">
						</a>
					</h1>
				</div>
			</div>

		</div>
	</div>
    </header>

    <div class="policy_wrapper">

    	<div class="container">

    		<p>This Terms of Use ("Terms") governs <a href="https://www.tigerfundingllc.com">www.tigerfundingllc.com</a>, <a href="https://go.tigerfundingllc.com">www.go.tigerfundingll.com</a>, and <a href="https://apply.tigerfundingllc.com">www.apply.tigerfundingllc.com</a>, (the "Site"), which is owned and operated by Tiger Merchant Funding, LLC, a Delaware limited liability company ("we," "us," "our," or the "Company").  These Terms govern your current visit to a Site, not any future visit. We may modify these Terms at any time without notice to you, and such modification shall be effective immediately upon posting on the Site. As your next visit to a Site may be governed by different terms posted at this page, you should review the terms on this page each time that you visit a Site.</p>

    		<p>Access or use of the Site following any change to the terms of use constitutes your agreement to those changes.  If at any time you choose not to accept the Terms that are in effect at such time, you should not access or use the Site.</p>

    		<p>By accessing the Site, you agree to the following Terms:</p>

    		<h3>1.	ACKNOWLEDGMENT AND RESPONSIBILITIES OF AUTHORIZED USERS</h3>
    		<p>You acknowledge and affirm that you are over the age of 18 and a legal resident of the United States. You may not access or use any Site if you are unable to form a binding, legal agreement with the Company. You assume all responsibility for your use of, or access to, the Site, including your access to any Site Content or User Content, as defined below, and hereby waive all claims or causes of action against the Company, its affiliates, its licensors and their respective officers, directors, employees, agents and representatives in connection therewith. In connection with use of a Site, users may be required to complete and submit an online working capital request form.  All Site User information shall be governed by our <a href="privacy-policy.php">Privacy Policy</a>.</p>

		<h3>2.	PRODUCTS AND SERVICES</h3>
		<p>You understand and agree that the Company is not a lender and is not the creditor for or issuer of the products featured on the Site. Any products or services advertised on this Site or made available to you after you receive a financial product from an issuer are by and remain the sole responsibility of the respective product vendors and service providers.</p>

		<h3>3.	WORKING CAPITAL REQUEST ACCEPTANCE POLICY</h3>
		<p>Your receipt of an electronic or other form of order confirmation does not signify any acceptance of your working capital request, nor does it constitute confirmation of any offer to provide funding to your business. The Company is not responsible for requests for working capital that cannot be fulfilled or unsuccessful working capital requests arising from a business's ineligibility to qualify for the working capital option presented by the Company's affiliated companies, approved independent contractors and other third parties.</p>

		<h3>4.	CREDIT CHECK - FCRA COMPLIANCE</h3>
		<p>By submitting your information to the Company you are providing your express written consent under the Fair Credit Reporting Act for the Company, affiliate companies, and approved independent contractors (e.g. selected lenders, working capital partners) with whom you are connected, to obtain your consumer credit profile or other information from contracted Credit Bureau's associated with your inquiry. While the "soft pull" that the Company may initiate will not affect your credit score, please note that credit inquires by selected lenders, working capital partners, and affiliates will likely impact your credit score.  The information obtained from your consumer credit profile may be used by the Company, affiliate companies, and approved independent contractors to process and evaluate your online working capital request form as well as to help assess your working capital request in the context of your overall financial situation.</p>

		<h3>5.	REVENUE VERIFICATION</h3>
		<p>Requesting working capital for your business though the Site will require you to submit information about the revenue generated by your business. This information will be passed on to our affiliate companies or approved independent contractors we work with in order to provide you with the proper financing or working capital options. Some approved independent contractors may require that you be able to verify the revenue generated by your business before they offer you a loan or other working capital solution. By submitting your business revenue information to the Company you promise that it is completely accurate information. While the Company will not require any business revenue verification, you agree that our affiliate companies and approved independent contractors with whom the Company works may need to verify your business's stated revenue by requesting proof of revenue, such as copies of bank statements or other financial information for your business. You further agree that such a proof of revenue may be a necessary step in receiving a business loan or working capital solution. </p>

		<h3>6.	E-CONSENT</h3>
		<p>By using the services available through the Site, you consent to transact business electronically and receive electronically all disclosures, notices or other records that our affiliate companies, approved independent contractors, or we are required by law to disclose to you in writing regarding your request for working capital or the fulfillment of your request (e.g., written disclosures required under the Truth in Lending Act and the Equal Credit Opportunity Act) (the "Disclosures"). Each time you submit a working capital request through the Site, you consent to the receipt of electronic Disclosures with respect to that specific working capital request and the resulting financing, if any. Your consent does not apply to any future transactions between us, our affiliate companies, or approved independent contractors. However, your consent will remain in effect as long as you are a user and, if you are no longer a user, will continue until such a time as all Disclosures relevant to transactions that occurred while you were a user have been made. Our affiliate companies and approved independent contractors may ask you to agree an additional electronic consent agreement (or similar agreement). You are responsible for reading and understanding the terms of these additional consent agreements.</p>

		<h3>A.	CONSENT TO TELEPHONE CALLS</h3>
		<p>You agree to receive telephone calls and messages electronically from us, via SMS messages (including text messages), artificial or prerecorded voice messages and automatic dialing technology, at any telephone number that you have provided or may provide in the future (including any cellular telephone numbers). You are providing express "written" consent to share your contact information with any of our affiliate companies and approved independent who may also contact you via text messaging, artificial or prerecorded voice messages and automatic dialing technology, even if your telephone number is currently listed on any internal, corporate, state, federal or national Do-Not-Call (DNC) list.  Your cellular or mobile telephone provider will charge you according to the type of plan you carry.</p>

		<h3>B.	ADDITIONAL MOBILE TECHNOLOGY REQUIREMENTS</h3>

		<p>If you are accessing our Site and the Disclosures electronically via a mobile device (such as a smart phone, tablet, and the like), in addition to the above requirements you must make sure that you have software on your mobile device that allows you to print and save the Disclosures presented to you during the application process. These applications can be found for most mobile devices in the device's respective "app store." If you do not have these capabilities on your mobile device, please access this Site through a device that provides these capabilities.</p>

		<h3>C.	WITHDRAWING CONSENT</h3>
		<p>You may withdraw your consent to receive Disclosures electronically by contacting us at the address below. However, once you have withdrawn your consent you will not be able to post any working capital requests on our Site. If you have a pending request on our Site, we will terminate it and remove it from our system. If you have already received working capital, all previously agreed to terms and conditions will remain in effect, and we will send Disclosures to your verified business address provided during registration.</p>

		<h3>D.	HOW TO CONTACT US REGARDING ELECTRONIC DISCLOSURES</h3>
		<p>You can contact us via email at webmaster@tigerfundingllc.com. You may also reach us in writing to us at the following address: Tiger Merchant Funding, LLC, 1680 Michigan Ave., Ste 700, Miami Beach, FL 33139.</p>

		<h3>7.	SITE USE</h3>

		<p>The Company is not your agent, nor is it the agent of the issuer of any business loan or provider of working capital, with respect to your decision to obtain a business loan or other financial offering for your business. The Company may receive compensation from third parties for goods, facilities or services that the Company provides to third parties under separate contact. Specifically, the Company typically receives a fee from affiliate companies or approved independent contractors (e.g. lenders or working capital partners) that you may be connected with and/or obtain a working capital through. Such goods, facilities or services may or may not relate in any way to your use of the Site. You agree to any such compensation arrangement whether or not related in any way to your use of the Site.</p>

		<h3>8.	COMPLIANCE WITH LAWS</h3>
		<p>You agree to comply with all applicable laws, statutes, ordinances and regulations regarding your use of the Site and your purchase of products or services through the Company's affiliate companies, approved independent contractors, or other third parties. The Company may, in its sole discretion, report actual or perceived violations of law to law enforcement or appropriate authorities. If the Company becomes aware, through a complaint or otherwise, of any potential or suspected violation of these Terms of Use or of its privacy policy ("Privacy Policy"), the Company may (but is not obligated to) conduct an investigation to determine the nature and extent of the suspected violation and the appropriate enforcement action, during which investigation the Company may suspend services to any customer being investigated and/or remove any material from the Company's servers. You agree to cooperate fully with any such investigation. You acknowledge that violations of the Terms of Use or the <a href="privacy-policy.php">Privacy Policy</a> could be subject to criminal or civil penalties.</p>

		<h3>9.	OWNERSHIP</h3>
		<p>All content on the Site, including but not limited to designs, articles, functions, text, graphics, photographs, images, video, information, materials, software, music, sound and other files, and their selection and arrangement and other content solely provided by or on behalf of the Company on any Site, specifically excluding any User Content (as defined below) (collectively, "Site Content"), is the sole property of the Company, as between you and the Company. The Site and all of the Site Content, and the selection and arrangement thereof, and protected under the copyright laws and other intellectual property laws of the United States. </p>

		<p>The Company reserves all rights, in and to the Site and the Site Content, which rights are not expressly granted herein. Unless otherwise noted, the Company name and all other trademarks, service marks, trade names, logos or other designations of source displayed on the Site are the property of the Company, its affiliates, or approved independent contractors. All third-party trademarks, service marks, trade names, logos, or other designations of source are the property of their respective owners. Nothing on any Site shall be construed as granting any license or right not expressly set forth herein. Any unauthorized use of a Site or any of the Site Content will terminate the permission or license granted herein and may violate applicable law. </p>

		<h3>10.	INTELLECTUAL PROPERTY RIGHTS IN SITE CONTENT; LIMITED LICENSE</h3>
		<p>All content on the Site, including but not limited to the Site Content, are the proprietary property of the Company with all rights reserved. No Site Content may be modified, copied, distributed, framed, reproduced, republished, downloaded, displayed, posted, transmitted, or sold in any form or by any means, in whole or in part, without the Company's prior written permission, except as provided in the following sentence and except that the foregoing does not apply to your own User Content that you legally post on the Site. Provided that you are eligible for use of the Site, you are granted a limited license to access and use the Site and to download or print a copy of any portion of the Site Content solely for your personal use, provided that you keep all copyright or other proprietary notices intact. Except for your own User Content, you may not republish Site Content on any Internet, Intranet, or Extranet site or incorporate the information in any other database or compilation; any other use of the Site Content is strictly prohibited. Such license is subject to these Terms and does not include use of any data mining, robots, or similar data gathering or extraction methods. Any use of the Site or the Site Content other than as specifically authorized herein, without the prior written permission of the Company, is strictly prohibited and will terminate the license granted herein. Such unauthorized use may also violate applicable laws including, without limitation, copyright and trademark laws and applicable communications regulations and statutes. Unless explicitly stated herein, nothing in these Terms shall be construed as conferring any license to intellectual property rights, whether by estoppel, implication, or otherwise. This license is revocable by us at any time without notice and with or without cause.</p>

		<h3>11.	CONTENT</h3>
		<p>You represent, warrant, and agree that no materials of any kind posted or shared by you will violate or infringe upon the rights of any third party, including copyright, trademark, privacy, publicity, or other personal or proprietary rights or contain libelous, defamatory, or otherwise unlawful material. You further agree not to harvest or collect email addresses or other contact information of users from the Site by electronic or other means for the purposes of sending unsolicited emails or other unsolicited communications. Additionally, you agree not to use automated scripts to collect information from the Site or for any other purpose. You further agree that you may not use the Site in any unlawful manner or in any other manner that could damage, disable, overburden, or impair the Site.</p>

		<h3>12.	OTHER USER CONTENT POSTED ON SITE</h3>
		<p>You are solely responsible for the photos, profiles, messages, notes, text, information, music, video, contact information for you or others, advertisements, or other content that you upload, publish, provide, or display (hereinafter, "post") on or through the Site, or transmit to or share with other users (collectively the "User Content"). You understand and agree that the Company may, but is not obligated to, review and delete or remove (without notice) any User Content in its sole discretion, including without limitation, User Content that in the sole judgment of the Company violates these Terms, might be offensive or illegal, or might violate the rights of, harm, or threaten the safety of, users or others. By posting User Content to any part of the Site, you automatically grant, and you represent and warrant that you have the right to grant, to the Company an irrevocable, perpetual, non-exclusive, transferable, fully paid, worldwide license (with the right to sublicense). This license grants the Company the right to use, copy, publicly perform, publicly display, reformat, translate, excerpt (in whole or in part), and distribute such User Content for any purpose on or in connection with the Site or the promotion thereof; to prepare derivative works of, or incorporate into other works, such User Content; and to grant and authorize sublicenses of the foregoing. You may remove your User Content from the Site at any time. If you choose to remove your User Content, the license granted above will not expire. </p>

		<h3>13.	THIRD PARTY CONTENT</h3>
		<p>The Company may from time to time (a) link to other sites that we feel may be useful to you, and (b) post content to our Site that is supplied by third parties (collectively "Third-party Content"). Third-party Content is not under the control of the Company. The Company makes no claim or representation regarding-and accepts no responsibility for-the quality, content, nature, or reliability of Third-party Content, any services accessible by hyperlink from our Site, links contained in any Third-party Content, or any review, changes, or updates to a third-party website or for third-party websites that link to our Site. Any opinions, advice, statements, services, offers, or other information or content expressed or made available by third parties in the Third-party Content are those of the respective author(s) or distributor(s) and not of the Company. The Company does not guarantee the merchantability or fitness for any particular purpose of Third-party Content. When leaving our Site, you should be aware that these Terms no longer govern, and, therefore, you should review the applicable terms and policies, including privacy and data-gathering practices, of any third-party sites. Additional disclaimers and limitation of liability are noted below.</p>

		<h3>14.	PRIVACY</h3>
		<p>The Company's <a href="privacy-policy.php">Privacy Policy</a>, which is incorporated herein by reference, is applicable to any data supplied through the Site in accordance with the Privacy Policy available at <a href="https://www.tigerfundingllc.com/privacy-policy/">https://www.tigerfundingllc.com/privacy-policy/</a>. The Privacy Policy sets out your rights and the Company's responsibilities with regard to your personal information. The Company will not use your information in any way inconsistent with the purposes and limitations provided in the Privacy Policy. You agree that the Company, in its sole discretion, may modify the Privacy Policy, and you further agree that, by using the Site after such modifications become effective, you have agreed to these modifications. You acknowledge that if you do not agree to any such modification, you will terminate use of the Site.</p>

		<h3>15.	SECURITY</h3>
		<p>The Company utilizes secure technology to protect your personal information. Although the Company has taken reasonable measures to provide for the security of certain information that you submit to the Site, the Company cannot guarantee that this information will not be intercepted or decrypted by others. The Company accepts no responsibility for such interception or decryption.</p>

		<h3>16.	RESTRICTED ACCESS PORTIONS OF THE SITE</h3>
		<p>You agree to take reasonable measures to ensure that no unauthorized person or entity shall have access to restricted areas of the Site using your user name or password. You agree that, if you are provided rights to access or use restricted areas of the Site, those rights are personal and non-transferable. You may not assign, sublicense, transfer, pledge, lease, rent, or share any user name or password to anyone. You assume all responsibility for loss or misuse of your user name and password and are responsible for any activities undertaken by a person in possession of your user name or password for any reason except due solely to the gross negligence or willful misconduct of the Company. The Company reserves the right to terminate your access to the Site or cancel your user name and password at any time and for any reason including, without limitation, your violation of these Terms.</p>

		<h3>17.	DISCLAIMER OF WARRANTIES</h3>
		<p>The Company uses reasonable efforts to ensure that the information on this site is accurate, but cannot guarantee such accuracy. The Company makes no representations regarding the use or results of any content on the Site as to its accuracy, reliability or any other matter.</p>

		<p>THE SITE, THE SITE CONTENT, AND THE USER CONTENT ARE MADE AVAILABLE "AS IS" AND "AS AVAILABLE" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED. THIS INCLUDES, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, ACCURACY, NON-INFRINGEMENT OR ENJOYMENT. WE MAKE NO GUARANTEE THAT THE SITE CONTENT OR USER CONTENT ON THE SITE IS UP-TO-DATE, ACCURATE, OR COMPLETE. YOU SHOULD NOT RELY ON IT FOR ANY DECISION OR TO TAKE ANY ACTION. WE HEREBY DISCLAIM ANY WARRANTY THAT THE SITE CONTENT OR USER CONTENT ON THE SITE WILL BE FREE OF INTERRUPTION, FREE OF ERRORS, OR THAT ANY OF THE SITE IS FREE OF VIRUSES, WORMS, TROJAN HORSES, OR OTHER CODE THAT MANIFESTS CONTAMINATING OR DESTRUCTIVE PROPERTIES. These exclusions and limitations are applied to the fullest extent permitted by law.</p>

		<h3>18.	LIMITATION OF LIABILITY</h3>
		<p>NEITHER THE COMPANY NOR ANY OTHER PARTY INVOLVED IN CREATING, PRODUCING OR DELIVERING THIS SITE SHALL BE LIABLE FOR ANY DIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, INDIRECT OR PUNITIVE DAMAGES, COSTS, OR ATTORNEY'S FEES ARISING OUT OF OR RELATING TO THESE TERMS, ACCESS TO, USE OF, OR THE OPERATION OF ANY SITE, ANY OF THE SITE CONTENT, OR USER CONTENT. YOUR SOLE AND EXCLUSIVE REMEDY AND THE COMPANY'S SOLE AND EXCLUSIVE LIABILITY TO YOU FOR ANY REASON SHALL BE FOR YOU TO DISCONTINUE YOUR ACCESS TO OR USE OF THE SITE. THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION.</p>

		<h3>19.	LIMITED TIME TO BRING YOU CLAIM</h3>
		<p>You agree that any cause of action arising out of or related to the Company, any Site, or any Site Content or User Content must be commenced within one (1) year after the cause of action accrues. Otherwise, such cause of action is permanently barred.</p>

		<h3>20.	LAW AND VENUE</h3>
		<p>These Terms, and the interpretation, performance, and enforcement of your and the Company's rights and duties, shall be construed in accordance with the laws of the State of Florida, except for any conflict or choice-of-law principle thereof that would lead to the application of another jurisdiction's laws to the rights and duties of the parties. By using any Site, you agree to submit any and all disputes arising out of or relating to these Terms to the exclusive jurisdiction of the state or federal courts located in Miami-Dade County, Florida, or, at the Company's sole election, or to binding arbitration before a single arbitrator pursuant to the American Arbitration Association's Commercial Dispute Resolution Procedures, with such arbitration to take place in Miami-Dade County, Florida.</p>


		<h3>21.	INDEMNIFICATION</h3>
		<p>You agree to indemnify, defend, and hold harmless the Company, its affiliate companies, approved independent contractors, including without limitation, the Company's service providers and banks-and their respective officers, directors, employees, agents, and representatives-from and against all losses, expenses, damages and costs. This includes reasonable attorneys' fees, for any claims, causes of actions, procedures or allegations arising out of or relating to any violation of these Terms, your use of the Site, Site Content, or User Content (including but not limited to infringement of third parties' worldwide intellectual property rights or negligent or wrongful conduct) by you or any other person accessing any Site on your behalf. The Company reserves the right, at its own expense, to assume the exclusive defense and control of any matter otherwise subject to defense by you.</p>

		<h3>22.	VIOLATION OF THE TERMS OF USE</h3>
		<p>By using the Site, you understand and agree that the Company, at its sole discretion and without prior notice, may terminate your access to the Site and to any services offered on the Site, and may remove any content you have provided if the Company believes that such content violates or is inconsistent with these Terms of Use or the <a href="privacy-policy.php">Privacy Policy</a> or their intent, that your conduct is disruptive, or you have violated the law or the rights of the Company or another user.</p>

		<h3>23.	DIGITAL MILLENNIUM COPYRIGHT ACT NOTICE</h3>
		<p>We respect copyright ownership and expect users of our Site to do so as well. It is our goal to limit or prevent access to the Site by any users who are repeat infringers of copyright. If you are a copyright owner or an agent thereof and believe any Site Content or User Content posted on the Site infringes upon your copyrights, you may submit a notification of claimed infringement under the Digital Millennium Copyright Act ("DMCA") by providing notice to the Company through our designated agent at the address listed below containing the following information: (a) identification of the copyrighted work claimed to have been infringed, or-if multiple copyrighted works at a single online site are covered by a single notification-a representative list of such works at that site; (b) identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled and information reasonably sufficient to permit us to locate the material; (c) information reasonably sufficient to permit us to contact you, such as an address, telephone number, and, if available, an electronic mail address; (d) a statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; (e) a statement that the information in the notification is accurate, and under penalty of perjury, that you are authorized to act on behalf of the owner of an exclusive right that is allegedly infringed; and (f) a physical or electronic signature of a person authorized to act on behalf of the owner of a copyright that is allegedly infringed.</p>

		<p>You acknowledge that if you fail to comply with substantially all of the above requirements of this Section, your DMCA notice may not be valid and we may not be able to remove infringing content.</p>

		<h3>24.	MISCELLANEOUS</h3>
		<p>These Terms operate to the fullest extent permissible by law. All rights not expressly granted are reserved to the Company.  If any provision of these Terms is unlawful, void, or unenforceable, that provision is deemed severable from these Terms and does not affect the validity and enforceability of any remaining provisions. These Terms of Use and the Privacy Policy constitute the entire agreement and understanding between the parties with respect to the subject matter contained herein and therein and supersedes and replaces any and all prior written or oral agreements related to the subject matter hereof.  </p>

		<p>The headings used in these Terms of Use are intended for convenience only, and shall not affect the construction and interpretation hereof or thereof. A party's failure to insist upon or enforce strict performance of any provision of the Terms of Use shall not be construed as a waiver of such or any future provision or right.  </p>

		<p>The Company makes no representations that the Site is appropriate or available for use in locations outside of the United States. Those who access or use the Site from outside of the United States do so at their own volition and are responsible for compliance with local law. This Site is not intended for distribution to, or use by, any person or entity in any jurisdiction where such use would be contrary to applicable law or regulation. By offering this Site and its content, no distribution or solicitation is made by the Company to any person to use the Site or its content in any jurisdiction where the provision of this Site is prohibited by law. </p>

		<h3>25.	CONTACT INFORMATION</h3>

		
		<div class="address">
			<strong>
				TIGER MERCHANT FUNDING, LLC <br>
				1680 Michigan Ave., Ste 700 <br>
				Miami Beach, Florida 33139 <br>
				Attention: Customer Service <br>
				<a href="mailto:webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a>
			</strong>
		</div>

		<h3>26.	INFORMATION FOR ISPs</h3>


		
		<div class="address">
			<strong>
				TIGER MERCHANT FUNDING, LLC <br>
				1680 Michigan Ave., Ste 700 <br>
				Miami Beach, Florida 33139 <br>
				Attention: Customer Service <br>
				<a href="mailto:webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a>
			</strong>
		</div>

		<p>The Company understands the importance of protecting individuals' personal information and the proper use of their personal information. It is our policy to not send unsolicited e-mails to customers and we hope the following information will eliminate any concerns that your company may have regarding our practices of individuals' personal information.</p>

		<p>The Company and its affiliated sites use only an opt-in or opt-out method of obtaining customer information. All personal information from individuals is obtained voluntarily from the individual. We obtain personal information from third parties that follow the same set of policies.</p>

		<p>All e-mail communications sent by us to customers include information about the origin of the e-mail and include easily identifiable instructions on how consumers can unsubscribe from receiving future e-mail messages from us. Below is an example of e-mail communications sent by us to subscribers.</p>

		<hr>

		<p>This e-mail is not sent unsolicited. You are receiving this email from Tiger Merchant Funding, LLC because you subscribed via our website. Offers and messages are ONLY sent to subscribers.</p>

		<p>To be unsubscribe from our list, click here</p>

		<p>View Our <a href="privacy-policy.php">Privacy Policy</a>. ISPs view our policies.</p>

		<hr>

		<p>In some instances, the Company may enter into a privacy policy with a third-party website to allow consumers to opt-in to our marketing program on those third party websites. When this occurs we will make certain that the third-party websites represent and warrant to us that the consumer data was collected voluntarily by consumer registration and/or co-registrations. We will also require that the third-party websites have a right under any applicable privacy policy to transfer the consumer data to us, and that the Company has the right to send marketing offers to the customer.</p>

		<p>We hope this information satisfies any questions or concerns you may have as an ISP regarding our e-mail practices. If you have additional questions or wish to discuss the matter further, please contact us at <a href="mailto:webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a></p>

    	</div>

    </div>


    <div class="small_form_section">
      <div class="sm_form_container w-container">
        <h2 class="heading-4">What can you do with $50,000?</h2>
        <div class="text-block-6">Fill out the form and get funding the same business day</div>
        <div class="w-form">

          <div data-name="Email Form 2" id="email-form-2 " class="form bottom-form" name="Email Form 2" >

            <input class="sm_form w-input" id="first_name"  maxlength="256" placeholder="First Name*" required="required"  type="text">
            <input class="sm_form w-input" id="last_name" maxlength="256" placeholder="Last Name*" required="required" type="text">
            <input class="sm_form w-input" id="email" maxlength="256" placeholder="Email*" required="required" type="email"  >
            <input class="sm_form w-input" id="phone" maxlength="14" placeholder="Phone -10 digit number*" required="required" type="tel">
            <input class="sm_form w-input" id="company" maxlength="256" placeholder="Business Name" required="required" type="text"  >
            <select class="sm_form w-input Monthly_Revenue"  id="Monthly_Revenue" title="Total Monthly Revenue"  placeholder="AVG Monthly Revenue*" required="required">
              <option value="">--Select--</option>
              <option value="$0-$10,000">$0-$10,000</option>
              <option value="$10,000-$25,000">$10,000-$25,000</option>
              <option value="$25,000-$50,000">$25,000-$50,000</option>
              <option value="$50,000-$100,000">$50,000-$100,000</option>
              <option value="$100,000-$250,000">$100,000-$250,000</option>
              <option value="$250,000+">$250,000+</option>
            </select>
            <input class="cta sm_cta w-button sub-form get-started btn_disable" data-wait="Please wait..." value="Get Started">
          </div>

          <div class="w-form-done">
            <div>Thank you! Your submission has been received!</div>
          </div>
          <div class="w-form-fail">
            <div>Oops! Something went wrong while submitting the form</div>
          </div>

        </div>
        <div class="text-block-7">Give it a try. It only takes one click.</div>
      </div>
    </div>


  <?php include 'footer.php';?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>


<script type="text/javascript">

// Form Validation( Bottom )

var $regexname = /^([a-zA-Z ]{3,100})$/;
//var $regphone = /^\(\d{3}\) \d{3}-\d{4}$/;


$('.bottom-form #first_name, .bottom-form  #last_name ,.bottom-form  #phone ,.bottom-form  #email').on('keyup',function(){

	if ($('.bottom-form #first_name').val().match($regexname) && $('.bottom-form #last_name').val().match($regexname) && $('.hero-form #phone').val().length == 10 && $('.hero-form #email').val().length >= 3)
	{
		$('.get-started').removeClass('btn_disable');
	}
	else{
		$('.get-started').addClass('btn_disable');
	}

});

// Phone Number Masking

$('.hero-form #phone , .bottom-form #phone')

	.keydown(function (e) {
		var key = e.which || e.charCode || e.keyCode || 0;
		$phone = $(this);

    if ($phone.val().length === 1 && (key === 8 || key === 46)) {
			$phone.val('(');
      return false;
		}

    else if ($phone.val().charAt(0) !== '(') {
			$phone.val('('+String.fromCharCode(e.keyCode)+'');
		}

		if (key !== 8 && key !== 9) {
			if ($phone.val().length === 4) {
				$phone.val($phone.val() + ')');
			}
			if ($phone.val().length === 5) {
				$phone.val($phone.val() + ' ');
			}
			if ($phone.val().length === 9) {
				$phone.val($phone.val() + '-');
			}
		}

		return (key == 8 ||
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));
	})

</script>
