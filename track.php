<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);
date_default_timezone_set("America/Toronto");
require_once "config/mysql.class.php";
$database = new DataBasePDO();
$UserData["type"] = $_GET['subjecttype'];
$UserData["website"] = $_GET['site'];
$UserData["userid"] = $_GET['key'];
$UserData["subject"] = $_GET['subject'];
$UserData["txn_date"] = date("Y-m-d");
$database->insertOrUpdate('emailog', $UserData);

?>