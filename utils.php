<?php

require fullpath . 'config/mysql.class.php';
require fullpath . 'aws-sdk/aws-autoloader.php';
require fullpath . 'soapclient1/SforcePartnerClient.php';

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Aws\Ses\Exception\SesException;
use Aws\Ses\SesClient;
use Aws\Sns\SnsClient;

class SalesforceUtils {
  static function createConnection() {
    $mySforceConnection = new SforcePartnerClient();
    $mySforceConnection->createConnection(fullpath . "soapclient1/partner.wsdl.xml");
    $mySforceConnection->setEndpoint('https://' . TypeOfAccount . '.salesforce.com/services/Soap/u/20.0');
    $mySforceConnection->login(USERNAME, PASSWORD . SECURITY_TOKEN);
    return $mySforceConnection;
  }

  static function getQueryResultFromSF($querysoql) {
    $mySforceConnection = self::createConnection();
    return $mySforceConnection->query($querysoql);
  }

  static function getApplicationData($id) {
    $querysoql = " SELECT  stagename,Account.id, id,Owner.id,Renewing_App__r.name,Renewing_Knight_Funding_Subsidiary__c,Renewing_App_Pay_Off_Amount__c, Amount,Primary_Owner__c, Type, Payment_Frequency__c, Account_Name__c,Account.Primary_Contact__r.LASERCA__Social_Security_Number__c, Account.Secondary_Contact__r.LASERCA__Social_Security_Number__c, Contract_Out_Date__c, Total_Payback_Amount__c, Purchase__c, Daily_Payment__c,Account.Primary_Contact__r.email, Account.Secondary_Contact__r.email,Originations_Fees_Amt__c,Today_s_Date__c,Account.Primary_Contact__c,Account.Primary_Bank_Routing__c,Account.Federal_Tax_ID__c,Account.Legal_Corporate_Name__c,Account.BillingStreet,Account.BillingCity,Account.BillingState,Account.BillingPostalCode,Account.Name,Account.ShippingStreet,Account.ShippingCity,Account.ShippingState,Account.ShippingPostalCode,Account.website,Account.Phone,Account.Primary_Contact__r.Phone,Account.State_of_Incorporation__c,Account.Primary_Owner_Mobile_Phone__c,Account.Fax,Primary_Owner_Email__c,Account.Business_Type__c,Account.Landlord_Company_Name__c,Account.Landlord_Phone__c,Account.Primary_Owner_Home_Phone__c,Account.Primary_Contact__r.MailingStreet,Account.Primary_Contact__r.MailingCity,Account.Primary_Contact__r.MailingState,Account.Primary_Contact__r.MailingPostalCode,Origination_Fee__c,Application_Number__c,Account.Primary_Contact__r.Birthdate,Account.Secondary_Contact__r.Name,Account.Secondary_Contact__r.Title,Account.Secondary_Contact__r.Birthdate,Account.Secondary_Contact__r.HomePhone,Account.Secondary_Contact__r.MailingStreet,
    Account.Secondary_Contact__r.MailingCity,Account.Primary_Contact__r.MobilePhone, Account.Primary_Contact__r.Name,
    Account.Secondary_Contact__r.MailingState,Account.Primary_Contact__r.HomePhone,
    Account.Secondary_Contact__r.MailingPostalCode,Account.Primary_Bank_Name__c,Account.Primary_Bank_Account__c,Account.Primary_Contact__r.Title
                      FROM opportunity
                      WHERE id= '" . $id . "'";
    $response = self::getQueryResultFromSF($querysoql);
    if ($response->size) {
      return $response->records[0];
    }
    return '';
  }

  static function uploadAttachment($attachmentBody, $attachmentName, $parentid, $type) {
    $createFields = array(
      'Body' => base64_encode($attachmentBody),
      'Name' => $attachmentName,
      'ParentID' => $parentid,
      'IsPrivate' => 'false',
    );
    $sObject = new stdclass();
    $sObject->fields = $createFields;
    $sObject->type = 'Attachment';
    $mySforceConnection = self::createConnection();
    $upsertResponse = $mySforceConnection->create(array($sObject));
    $resp = self::updateAttachmentObject($type, $upsertResponse[0]->id);
    return $resp;
  }

  static function updateAttachmentObject($type, $id){
    $createFields = array(
      'AttachmentId__c' => $id,
      'File_Type__c' => $type
    );
    $sObject = new stdclass();
    $sObject->fields = $createFields;
    $sObject->type = 'Application_Attachment__c';
    $mySforceConnection = self::createConnection();
    $upsertResponse = $mySforceConnection->create(array($sObject));
    return $upsertResponse[0]->id;
  }

  static function updateObject($sObject) {
    $mySforceConnection = self::createConnection();
    return $mySforceConnection->update($sObject);
  }

  static function createObject($sObject) {
    $mySforceConnection = self::createConnection();
    return $mySforceConnection->create($sObject);
  }

  static function updateTask($idfor, $salesForceId, $subject) {
    $fields = array(
      $idfor => $salesForceId,
      'Subject' => $subject,
      'Status' => 'Completed',
    );
    $sObject = new SObject();
    $sObject->fields = $fields;
    $sObject->type = 'Task';

    $mySforceConnection = self::createConnection();
    $finalResponse = $mySforceConnection->create(array($sObject));
    return $finalResponse;
  }

  static function getAccessToken() {
    $urlToGetToken = "https://" . TypeOfAccount . ".salesforce.com/services/oauth2/token";
    $postFieldsToGetBearer = "grant_type=password&client_id=" . CLIENT_ID_SF . "&client_secret=" . CLIENT_SECRET_SF . "&username=" . USERNAME . "&password=" . PASSWORD . SECURITY_TOKEN;
    $header['content-type'] = "application/x-www-form-urlencoded";
    $accessTokenObject = json_decode(UtilsClass::curlPostMethod($urlToGetToken, $postFieldsToGetBearer, $header, "POST"));
    return $accessTokenObject;
  }

  static function tagInSalesforce($oppId, $postFields) {
    $accessTokenObject = self::getAccessToken();
    $headerToMention = array(
      "authorization: " . $accessTokenObject->token_type . " " . $accessTokenObject->access_token,
      "cache-control: no-cache",
      "content-type: application/json",
    );
    $urlToPostFeedItem = $accessTokenObject->instance_url . "/services/data/v44.0/chatter/feed-elements";
    $content_type_mention = "application/json";
    $mentionResponse = UtilsClass::curlPostMethod($urlToPostFeedItem, $postFields, $headerToMention, "POST");
    return $mentionResponse;
  }

  static function wigetInformation($oppId) {
    $querysoql = "SELECT Id, adobeWidgetUrl__c,adobeWidgetId__c,widgetStatus__c FROM application_support__c WHERE Application__c='" . $oppId . "' AND RecordTypeId='" . Adobe_RECORD_TYPE_ID . "'";
    $urlcontract = self::getQueryResultFromSF($querysoql);
    if (!empty($urlcontract->records[0])) {
      return $urlcontract->records[0];
    } else {
      return '';
    }

  }
}

class UtilsClass {
  public static function encrypt($stringToEncrypt) {
    $encryptionMethod = "AES-256-CBC"; //methodtoencrypt
    $secret = "Tigermustbe32characterforencrypt"; //must be 32 char length
    $iv = substr($secret, 0, 16);
    $encryptedMessage = openssl_encrypt($stringToEncrypt, $encryptionMethod, $secret, 0, $iv);
    return $encryptedMessage;
  }

  public static function decrypt($stringToDecrypt) {
    $encryptionMethod = "AES-256-CBC"; //methodtodecrypt
    $secret = "Tigermustbe32characterforencrypt"; //must be 32 char length
    $iv = substr($secret, 0, 16);
    $decryptedMessage = openssl_decrypt($stringToDecrypt, $encryptionMethod, $secret, 0, $iv);
    return $decryptedMessage;
  }

  public function deleteDir($dirPath) {
    if (!is_dir($dirPath)) {
      throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
      $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
      if (is_dir($file)) {
        deleteDir($file);
      } else {
        unlink($file);
      }
    }
    rmdir($dirPath);
  }

  public function uploadToS3($region, $bucketName, $keyName, $filePath) {
    $s3 = new S3Client([
      'credentials' => [
        'key' => AwsKey,
        'secret' => AwsSec,
      ],
      'version' => 'latest',
      'region' => $region,
    ]);

    try {
      $result = $s3->putObject([
        'Bucket' => $bucketName,
        'Key' => $keyName,
        'SourceFile' => $filePath,
        'ACL' => 'public-read',
      ]);
    } catch (S3Exception $e) {
      echo $e->getMessage() . PHP_EOL;
    }
  }

  public function curlMethod($url) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_CUSTOMREQUEST => "GET",
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
  }

  public function slyBroadCast($callerID, $allPhone, $c_record_audio) {
    $c_uid = slyEmail;
    $c_password = slyPassword;
    $c_callerID = $callerID;
    $c_date = 'now';
    $mobile_only = 1;
    $c_phone = trim($allPhone, ",");
    $finalurl = "https://www.mobile-sphere.com/gateway/vmb.php?c_uid=$c_uid&c_password=$c_password&c_phone=$c_phone&c_record_audio=$c_record_audio&c_callerID=$c_callerID&c_date=$c_date&mobile_only=$mobile_only";
    $this->curlMethod($finalurl);
  }
  public function doNotEmailfn($to){
    $database = new DataBasePDO();
    return $database->getOneRow("SELECT email FROM NoflowEmails WHERE email='".$to."'");
  }

  public function sendEmail($from, $to, $subject, $message) {
    $doNotEmail=$this->doNotEmailfn($to);
    if(empty($doNotEmail)){
      $client = new SesClient([
        'credentials' => [
          'key' => AwsKey,
          'secret' => AwsSec,
        ],
        'region' => SES_REGION,
        'version' => SES_VERSION,
      ]);

      try {
        $result = $client->sendEmail([
          'Destination' => [
            'ToAddresses' => [
              $to,
            ],
          ],
          'Message' => [
            'Body' => [
              'Html' => [
                'Charset' => SES_CHARSET,
                'Data' => $message,
              ],
            ],
            'Subject' => [
              'Charset' => SES_CHARSET,
              'Data' => $subject,
            ],
          ],
          'Source' => $from,
        ]);
        return 'Email sent!';
      } catch (SesException $error) {
        echo ('The email was not sent. Error message: ' . $error->getAwsErrorMessage());
      }
    }
  }

  public function sendEmailWithCC($from, $to, $subject, $message, $cc) {
    $doNotEmail=$this->doNotEmailfn($to);
    if(empty($doNotEmail)){
      $client = new SesClient([
        'credentials' => [
          'key' => AwsKey,
          'secret' => AwsSec,
        ],
        'region' => SES_REGION,
        'version' => SES_VERSION,
      ]);
      try {
        $result = $client->sendEmail([
          'Destination' => [
            'ToAddresses' => [
              $to,
            ],
            'CcAddresses' => [
              $cc,
            ],
          ],
          'Message' => [
            'Body' => [
              'Html' => [
                'Charset' => SES_CHARSET,
                'Data' => $message,
              ],
            ],
            'Subject' => [
              'Charset' => SES_CHARSET,
              'Data' => $subject,
            ],
          ],
          'Source' => $from,
        ]);
        return 'Email sent!';
      } catch (SesException $error) {
        echo ('The email was not sent. Error message: ' . $error->getAwsErrorMessage());
      }
    }
  }

  public function sendSMS($message, $phonenumber) {
    try {
      $client = new SnsClient([
        'credentials' => [
          'key' => AwsKey,
          'secret' => AwsSec,
        ],
        'region' => SNS_REGION,
        'version' => SNS_VERSION,
      ]);
      $result = $client->publish([
        'Message' => $message,
        'PhoneNumber' => $phonenumber,
        'MessageAttributes' => [
          'AWS.SNS.SMS.SMSType' => [
            'DataType' => 'String',
            'StringValue' => 'Transactional',
          ],
        ],
      ]);
    } catch (SesException $error) {
      echo ("The email was not sent. Error message: " . $error->getAwsErrorMessage() . "\n");
    }
  }

  public static function generateRandomString($length = 20) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  public static function redirectMe($url, $permanent = false) {
    header('Location: ' . $url, true, $permanent ? 301 : 302);
    exit();
  }
  public function updateTask($idfor, $salesForceId, $subject) {
    $fields = array(
      $idfor => $salesForceId,
      'Subject' => $subject,
      'Status' => 'Completed',
    );
    $sObject = new SObject();
    $sObject->fields = $fields;
    $sObject->type = 'Task';

    $mySforceConnection = new SforcePartnerClient();
    $mySforceConnection->createConnection(fullpath . "soapclient1/partner.wsdl.xml");
    $mySforceConnection->setEndpoint('https://' . TypeOfAccount . '.salesforce.com/services/Soap/u/20.0');
    $mySforceConnection->login(USERNAME, PASSWORD . SECURITY_TOKEN);
    $finalResponse = $mySforceConnection->create(array($sObject));
  }

  static function getS3Object($keyname) {
    try {
      $s3 = new S3Client([
        'credentials' => [
          'key' => AwsKey,
          'secret' => AwsSec,
        ],
        'version' => 'latest',
        'region' => S3_UPLOAD_REGION,
      ]);
      $result = $s3->getObject([
        'Bucket' => BUCKET,
        'Key' => $keyname,
      ]);
      return $result['Body'];
    } catch (S3Exception $e) {
      echo $e->getMessage() . PHP_EOL;
    }
  }

  static function curlPostMethod($url, $body, $header, $type) {
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => $type,
      CURLOPT_POSTFIELDS => $body,
      CURLOPT_HTTPHEADER => $header,
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return "cURL Error #:" . $err;
    } else {
      return $response;
    }
  }
}

?>
