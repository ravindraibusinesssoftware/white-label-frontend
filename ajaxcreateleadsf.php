<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);
date_default_timezone_set("America/Toronto");
define("fullpath", $_SERVER['DOCUMENT_ROOT'] . "/");
require_once fullpath . "utils.php";

$database = new DataBasePDO();
try{

  if(!empty($_COOKIE['LeadId'])){
    $sqlsales="select * from submissions where id=".$_COOKIE['LeadId'];
    $resultSAles=$database->getAllResults($sqlsales);
  }
  if($_POST['Step_Number'] == 'p0'){
    $details = json_decode(file_get_contents("http://ipinfo.io/{".$_SERVER['REMOTE_ADDR']."}/json"));
    $UserData['like_to_borrow']=$_POST['like_to_borrow'];
    $UserData['Step_Number']='p0';
    $UserData['utmsource']=$_POST['utm_source'];
    $UserData['utmmedium']=$_POST['utm_medium'];
    $UserData['utmcampaign']=$_POST['utm_campaign'];
    $UserData['utmterm']=$_POST['utm_term'];
    $UserData['utmcontent']=$_POST['utm_content'];
    if(empty($_POST['user_ip'])){
      $useripp=getRealIpAddr();
    }else{
      $useripp=$_POST['user_ip'];
    }
    $UserData['userip']=$useripp;
    $UserData['userbrowser']=$_POST['user_browser'];
    $UserData['userdevice']=$_POST['user_device'];
    $UserData["SourceIP"] =  $details->ip;
    $UserData["SourceState"] = $details->region;
    $UserData["SourceCity"] = $details->city;
    $UserData["SourceOrg"] = $details->org;
    $UserData['fc_campaign']=$_POST['fc_campaign'];
    $UserData['fc_channel']=$_POST['fc_channel'];
    $UserData['fc_content']=$_POST['fc_content'];
    $UserData['fc_landing']=$_POST['fc_landing'];
    $UserData['fc_medium']=$_POST['fc_medium'];
    $UserData['fc_referrer']=$_POST['fc_referrer'];
    $UserData['fc_source']=$_POST['fc_source'];
    $UserData['fc_term']=$_POST['fc_term'];
    $UserData['lc_campaign']=$_POST['lc_campaign'];
    $UserData['lc_channel']=$_POST['lc_channel'];
    $UserData['lc_content']=$_POST['lc_content'];
    $UserData['lc_landing']=$_POST['lc_landing'];
    $UserData['lc_medium']=$_POST['lc_medium'];
    $UserData['lc_referrer']=$_POST['lc_referrer'];
    $UserData['lc_source']=$_POST['lc_source'];
    $UserData['lc_term']=$_POST['lc_term'];
    $UserData['OS']=$_POST['OS'];
    $UserData['GA_Client_ID']=$_POST['GA_Client_ID'];
    $UserData['all_traffic_sources']=$_POST['all_traffic_sources'];
    $UserData['browser']=$_POST['browser'];
    $UserData['city']=$_POST['city'];
    $UserData['country']=$_POST['country'];
    $UserData['device']=$_POST['device'];
    $UserData['page_visits']=$_POST['page_visits'];
    $UserData['pages_visited_list']=$_POST['pages_visited_list'];
    $UserData['region']=$_POST['region'];
    $UserData['time_zone']=$_POST['time_zone'];
    $UserData['time_passed']=$_POST['time_passed'];
    $UserData['latitude']=$_POST['latitude'];
    $UserData['longitude']=$_POST['longitude'];
    $UserData["StartedDate"]=date("Y-m-d H:i:s");
    $UserData['nokey']=generateRandomString();
    $database->insertData('personal_submissions',$UserData);
    $UserId=$database->getLastInsertId();
    setcookie("Id_personal",$UserId,strtotime("+1 year"));
    setcookie("Step_Number_personal",$_POST['Step_Number'],strtotime("+1 year"));
  }elseif($_POST['Step_Number'] == 'p1'){
    $UserData['Credit_type']=$_POST['Credit_type'];
    $UserData['Loan_Reason']=$_POST['industry'];
    $UserData['Step_Number']=$_POST['Step_Number'];
    $UserData['Military']=$_POST['radio_Military'];
    $whereAr =array("ID"=>$_COOKIE['Id_personal']);
    $database->updateData('personal_submissions',$UserData,$whereAr);
    setcookie("Step_Number_personal",$_POST['Step_Number'],strtotime("+1 year"));
  }elseif($_POST['Step_Number'] == 'p2'){
    $UserData['street']=$_POST['Address'];
    $UserData['Step_Number']='p2';
    $UserData['zip']=$_POST['zip_code'];
    $UserData['state']=$_POST['state'];
    $UserData['city']=$_POST['city'];
    $UserData['length_address']=$_POST['length_address'];
    $UserData['own_house']=$_POST['own_a_house'];
    $whereAr =array("ID"=>$_COOKIE['Id_personal']);
    $database->updateData('personal_submissions',$UserData,$whereAr);
    setcookie("Step_Number_personal",$_POST['Step_Number'],strtotime("+1 year"));
  }elseif($_POST['Step_Number'] == 'p3'){
    $UserData["First_Name"] = $_POST["first_name"];
    $UserData["Last_Name"] = $_POST["last_name"];
    $UserData["email"] = strtolower($_POST["email"]);
    $UserData['DOB'] = $_POST['dob'];
    $UserData['gross_income']=$_POST['gross_income'];
    $UserData['Step_Number']=$_POST['Step_Number'];
    $UserData['phone']=$_POST['phone_no'];
    $UserData['Contact_time']=$_POST['TimeToCall'];

    $whereAr =array("ID"=>$_COOKIE['Id_personal']);
    $database->updateData('personal_submissions',$UserData,$whereAr);
    setcookie("Step_Number_personal",$_POST['Step_Number'],strtotime("+1 year"));
  }elseif($_POST['Step_Number'] == 'p4'){
    $UserData['Income_source']=$_POST['Income_source'];
    $UserData['Timed_Employed']=$_POST['Timed_Employed'];
    $UserData['get_paid']=$_POST['get_paid'];
    $UserData['Employer_name']=$_POST['Employer_name'];
    $UserData['Employer_Phone']=$_POST['Employer_Phone'];
    $UserData['Step_Number']=$_POST['Step_Number'];
    $whereAr =array("ID"=>$_COOKIE['Id_personal']);
    $database->updateData('personal_submissions',$UserData,$whereAr);
    setcookie("Step_Number_personal",$_POST['Step_Number'],strtotime("+1 year"));
  }elseif($_POST['Step_Number'] == 'p5'){
    $UserData['Driver_License']=$_POST['Driver_License'];
    $UserData['Issuing_State']=$_POST['Issuing_State'];
    $UserData['SSN']=$_POST['ssn'];
    $UserData['Account_Type']=$_POST['Account_Type'];
    $UserData['Step_Number']=$_POST['Step_Number'];
    $UserData["EndedDate"]=date("Y-m-d H:i:s");
    $whereAr =array("ID"=>$_COOKIE['Id_personal']);
    $database->updateData('personal_submissions',$UserData,$whereAr);
    //$url=sendtopersonal($_COOKIE['Id_personal']);
    setcookie("Step_Number_personal", "", time() - 3600);
    setcookie("Id_personal", "", time() - 3600);
  }

  if($_POST['Step_Number']=='zero'){
    $UserData["own_a_business"] = $_POST['own_a_business'];
    $UserData["UsedUrl"] = $_SERVER['HTTP_REFERER'];
    $UserData["utm_campaign"] = $_POST["utm_campaign"];
    $UserData["utm_content"] = $_POST["utm_content"];
    $UserData["utm_medium"] = $_POST["utm_medium"];
    $UserData["utm_source"] = $_POST["utm_source"];
    $UserData["utm_term"] = $_POST["utm_term"];
    if(empty($_POST['user_ip'])){
      $userips=getRealIpAddr();
    }else{
      $userips=$_POST['user_ip'];
    }
    $UserData['userip']=$userips;
    $UserData["user_browser"] = $_POST["user_browser"];
    $UserData["user_device"] = $_POST["user_device"];
    $UserData["Step_Number"] = '0';
    /*$UserData["SourceIP"] =  $details->ip;
    $UserData["SourceState"] = $details->region;
    $UserData["SourceCity"] = $details->city;
    $UserData["SourceOrg"] = $details->org;*/
    $UserData["fc_campaign"]=$_POST["fc_campaign"];
    $UserData["fc_channel"]=$_POST["fc_channel"];
    $UserData["fc_content"]=$_POST["fc_content"];
    $UserData["fc_landing"]=$_POST["fc_landing"];
    $UserData["fc_medium"]=$_POST["fc_medium"];
    $UserData["fc_referrer"]=$_POST["fc_referrer"];
    $UserData["fc_source"]=$_POST["fc_source"];
    $UserData["fc_term"]=$_POST["fc_term"];
    $UserData["lc_campaign"]=$_POST["lc_campaign"];
    $UserData["lc_channel"]=$_POST["lc_channel"];
    $UserData["lc_content"]=$_POST["lc_content"];
    $UserData["lc_landing"]=$_POST["lc_landing"];
    $UserData["lc_medium"]=$_POST["lc_medium"];
    $UserData["lc_referrer"]=$_POST["lc_referrer"];
    $UserData["lc_source"]=$_POST["lc_source"];
    $UserData["lc_term"]=$_POST["lc_term"];
    $UserData["OS"]=$_POST["OS"];
    $UserData["GA_Client_ID"]=$_POST["GA_Client_ID"];
    $UserData["all_traffic_sources"]=$_POST["all_traffic_sources"];
    $UserData["browser"]=$_POST["browser"];
    $UserData["city"]=$_POST["city"];
    $UserData["country"]=$_POST["country"];
    $UserData["device"]=$_POST["device"];
    $UserData["page_visits"]=$_POST["page_visits"];
    $UserData["pages_visited_list"]=$_POST["pages_visited_list"];
    $UserData["region"]=$_POST["region"];
    $UserData["time_zone"]=$_POST["time_zone"];
    $UserData["time_passed"]=$_POST["time_passed"];
    $UserData["latitude"]=$_POST["latitude"];
    $UserData["longitude"]=$_POST["longitude"];
    //print_r($UserData);
    foreach ($UserData as $key => $val){
      $valstr[]= '`'.$key.'` = "'.$val.'"';
    }
    $sqlQuery = "INSERT INTO `".'No_Records'."` (`".implode('`, `', array_keys($UserData))."`, `clickCount`) VALUES (\"".implode('", "', $UserData)."\",1)  ON DUPLICATE KEY UPDATE ".implode(', ', $valstr)." ,`clickCount` = `clickCount` +1";
    //print("$sqlQuery\n");//die();
    //print_r($insert);
    setcookie("Database",'applysubmissions',strtotime("+1 year"));
    //setcookie("own_a_business",$_POST['own_a_business'],strtotime("+1 year"));
    $database->executeQuery($sqlQuery);
  }
  if($_POST['Step_Number'] == 1){
    //$UserData["company"] = "_";
    //$UserData["Funding_Amount_Requested__c"] = '0';
    //$UserData["Web_to_Lead__c"] = "Tiger Funding";
    //$UserData["Web_Form_Type__c"] = "Tiger Full App";
    //$UserData["LeadSource"] = "Tiger Funding Web App";
    //$UserData["Property__c"] = "apply.tigerfundingllc.com";
    if (!empty($_COOKIE["own_a_business"])) {
      $UserData["own_a_business"]=$_COOKIE["own_a_business"];
    }else{
      $UserData["own_a_business"]="Yes";
    }

    $details = json_decode(file_get_contents("http://ipinfo.io/{".$_SERVER['REMOTE_ADDR']."}/json"));

    $UserData["UsedUrl"] = $_SERVER['HTTP_REFERER'];
    $UserData["firstname"] = $_POST["first_name"];
    $UserData["lastname"] = $_POST["last_name"];
    $UserData["phone"] = $_POST["phone"];
    $UserData["email"] = strtolower($_POST["email"]);
    $UserData["revenue"] = $_POST["Monthly_Revenue"];
    $UserData["utmcampaign"] = $_POST["utm_campaign"];
    $UserData["utmcontent"] = $_POST["utm_content"];
    $UserData["utmmedium"] = $_POST["utm_medium"];
    $UserData["utmsource"] = $_POST["utm_source"];
    $UserData["utmterm"] = $_POST["utm_term"];


    if(empty($_POST['user_ip'])){
      $useripss=getRealIpAddr();
    }else{
      $useripss=$_POST['user_ip'];
    }
    $UserData['userip']=$useripss;

    $UserData["userbrowser"] = $_POST["user_browser"];
    $UserData["userdevice"] = $_POST["user_device"];
    $UserData["step"] = '1';
    $UserData["SourceIP"] =  $details->ip;
    $UserData["SourceState"] = $details->region;
    $UserData["SourceCity"] = $details->city;
    $UserData["SourceOrg"] = $details->org;
    $UserData["fc_campaign"]=$_POST["fc_campaign"];
    $UserData["fc_channel"]=$_POST["fc_channel"];
    $UserData["fc_content"]=$_POST["fc_content"];
    $UserData["fc_landing"]=$_POST["fc_landing"];
    $UserData["fc_medium"]=$_POST["fc_medium"];
    $UserData["fc_referrer"]=$_POST["fc_referrer"];
    $UserData["fc_source"]=$_POST["fc_source"];
    $UserData["fc_term"]=$_POST["fc_term"];
    $UserData["lc_campaign"]=$_POST["lc_campaign"];
    $UserData["lc_channel"]=$_POST["lc_channel"];
    $UserData["lc_content"]=$_POST["lc_content"];
    $UserData["lc_landing"]=$_POST["lc_landing"];
    $UserData["lc_medium"]=$_POST["lc_medium"];
    $UserData["lc_referrer"]=$_POST["lc_referrer"];
    $UserData["lc_source"]=$_POST["lc_source"];
    $UserData["lc_term"]=$_POST["lc_term"];
    $UserData["OS"]=$_POST["OS"];
    $UserData["GA_Client_ID"]=$_POST["GA_Client_ID"];
    $UserData["all_traffic_sources"]=$_POST["all_traffic_sources"];
    $UserData["browser"]=$_POST["browser"];
    $UserData["city"]=$_POST["city"];
    $UserData["country"]=$_POST["country"];
    $UserData["device"]=$_POST["device"];
    $UserData["page_visits"]=$_POST["page_visits"];
    $UserData["pages_visited_list"]=$_POST["pages_visited_list"];
    $UserData["region"]=$_POST["region"];
    $UserData["time_zone"]=$_POST["time_zone"];
    $UserData["time_passed"]=$_POST["time_passed"];
    $UserData["latitude"]=$_POST["latitude"];
    $UserData["longitude"]=$_POST["longitude"];
    $UserData["applykey"]=generateRandomString();
    $UserData["UniquePartnerId"]=$_POST["UniquePartnerId"];

    $subdomain = isset($_COOKIE['subdomain'])?$_COOKIE['subdomain']:'affiliates';

    $UserData["LeadSource"] = $subdomain;

    $UserData["StartedDate"]=date("Y-m-d H:i:s");
    $UserData["salesforcestatus"]=0;
    //print_r($UserData);
    if ((!empty($_COOKIE['LeadId'])) && ($_COOKIE['StepNumber']==0.1)){
      //print_r($_COOKIE['Step_Number']);
      $whereAr =array("ID"=>$_COOKIE['LeadId']);
      $database->updateData('submissions',$UserData,$whereAr);
      setcookie("StepNumber",$_POST['Step_Number'],strtotime("+1 year"));
    }else{
      //print_r(1);
      $database->insertData('submissions',$UserData);
      $UserId=$database->getLastInsertId();
      setcookie("LeadId",$UserId,strtotime("+1 year"));
      setcookie("StepNumber",$_POST['Step_Number'],strtotime("+1 year"));
      setcookie("Database",'applysubmissions',strtotime("+1 year"));

    }

  }elseif($_POST['Step_Number'] == 2){

    $UserData["website"] = $_POST["Website"];
    $UserData["legalname"] = $_POST["Legal_Name"];
    //$UserData["company"] = $_POST["Legal_Name"];
    $UserData["DBA"] = $_POST["Doing_Business"];
    $UserData["type"] = $_POST["Business_Entity"];
    $UserData["step"] = '2';
    $UserData["ownership"] = $_POST["Ownership_percentage"];
    if($UserData["ownership"] != '100'){
      $UserData["SecondOwnerFirstName"] = $_POST["second_first_name"];
      $UserData["SecondOwnerLastName"] = $_POST["second_last_name"];
      $UserData["SecondOwnerPhone"] = $_POST["second_phone"];
      $UserData["SecondOwnerEmail"] = $_POST["second_email"];
    }
    $UserData["salesforcestatus"]=0;
    $whereAr =array("ID"=>$_COOKIE['LeadId']);
    $database->updateData('submissions',$UserData,$whereAr);

    setcookie("StepNumber",$_POST['Step_Number'],strtotime("+1 year"));

  }elseif($_POST['Step_Number'] == 3){

    // $sObject->Owner1_Home_Address__c = $_POST['Street'];
    // $sObject->Owner_1_City__c = $_POST["City"];
    // $sObject->Owner_1_State__c = $_POST["State"];
    // $sObject->Owner_1_Zip__c = $_POST["PostalCode"];

    $UserData['street'] = $_POST['Street'];
    $UserData['city'] = $_POST["City"];
    $UserData['state'] = $_POST["State"];
    $UserData['zip'] = $_POST["PostalCode"];
    $UserData['homebased'] = $_POST['homebased'];
    if($_POST['homebased']=='Yes'){
      $UserData['ownerhome'] = $_POST['Street'];
      $UserData['ownercity'] = $_POST['City'];
      $UserData['ownerstate'] = $_POST['State'];
      $UserData['ownerzip'] = $_POST['PostalCode'];
    }
    $UserData["step"] = 3;
    $UserData["salesforcestatus"]=0;
    $whereAr =array("ID"=>$_COOKIE['LeadId']);
    $database->updateData('submissions',$UserData,$whereAr);
    setcookie("StepNumber",$_POST['Step_Number'],strtotime("+1 year"));

  }elseif($_POST['Step_Number'] == 4){
    $UserData['gender'] = $_POST['Gender_select'];
    $UserData['DOB'] = $_POST['dob'];
    $UserData["salesforcestatus"]=0;
    $UserData['step'] = '4';

    $whereAr =array("ID"=>$_COOKIE['LeadId']);
    $database->updateData('submissions',$UserData,$whereAr);
    setcookie("StepNumber",$_POST['Step_Number'],strtotime("+1 year"));

  }elseif($_POST['Step_Number'] == 5){
    $UserData['BusStartDate'] = $_POST['bs_dob'];
    $UserData['industry'] = $_POST['industry'];
    $UserData['office'] = $_POST['Office_Space'];
    $UserData["salesforcestatus"]=0;
    $UserData['step'] = '5';

    $whereAr =array("ID"=>$_COOKIE['LeadId']);
    $database->updateData('submissions',$UserData,$whereAr);
    setcookie("StepNumber",$_POST['Step_Number'],strtotime("+1 year"));

  }elseif($_POST['Step_Number'] == 6){

    $UserData['ownerhome'] = $_POST['Owner1_Street'];
    $UserData['ownercity'] = $_POST['Owner1_City'];
    $UserData['ownerstate'] = $_POST['Owner1_State'];
    $UserData['ownerzip'] = $_POST['Owner1_Zip'];
    $UserData["salesforcestatus"]=0;
    $UserData['step'] = '6';

    $whereAr =array("ID"=>$_COOKIE['LeadId']);
    $database->updateData('submissions',$UserData,$whereAr);
    setcookie("StepNumber",$_POST['Step_Number'],strtotime("+1 year"));

  }elseif($_POST['Step_Number'] == 7){

    $UserData['stateincorporation'] = $_POST['State_Incorporation'];
    // $UserData['taxid'] = $_POST['Tax_ID'];
    // $UserData['SSN'] = $_POST['Social_Security_Number'];
    $UserData['taxid'] = UtilsClass::encrypt($_POST['Tax_ID']);
    $UserData['SSN'] = UtilsClass::encrypt(str_replace('-', '', $_POST['Social_Security_Number']));

    //print_r(encrypt(str_replace('-', '', $_POST['Social_Security_Number'])));

    $UserData["salesforcestatus"]=0;
    $UserData['step'] = '7';
    //$UserData['E_Signed__c'] = true;

    $whereAr =array("ID"=>$_COOKIE['LeadId']);
    $database->updateData('submissions',$UserData,$whereAr);
    setcookie("StepNumber",$_POST['Step_Number'],strtotime("+1 year"));

  }elseif($_POST['Step_Number'] == 8){
    //print_r("step8");
    $UserData["salesforcestatus"]=0;
    $UserData['step'] = '8';
    $UserData['EndedDate'] = date("Y-m-d H:i:s");

    if(!empty($_COOKIE['RCcookie'])){
      $UserData['RequestCode'] = $_COOKIE['RCcookie'];
    }


    $whereAr =array("ID"=>$_COOKIE['LeadId']);
    $database->updateData('submissions',$UserData,$whereAr);

    setcookie("StepNumber", "", time() - 3600);
    setcookie("LeadId", "", time() - 3600);
    setcookie("Database", "", time() - 3600);
    //setcookie("own_a_business", "", time() - 3600);
    setcookie("RCcookie", "", time() - 3600);
    }elseif($_POST['Step_Number'] == 9){
    //print_r("step9");
    $UserData["salesforcestatus"]=0;
    $UserData['step'] = '9';
    $UserData['EndedDate'] = date("Y-m-d H:i:s");

    if(!empty($_COOKIE['RCcookie'])){
      $UserData['RequestCode'] = $_COOKIE['RCcookie'];
    }


    $whereAr =array("ID"=>$_COOKIE['LeadId']);
    $database->updateData('submissions',$UserData,$whereAr);

    setcookie("StepNumber", "", time() - 3600);
    setcookie("LeadId", "", time() - 3600);
    setcookie("Database", "", time() - 3600);
    //setcookie("own_a_business", "", time() - 3600);
    setcookie("RCcookie", "", time() - 3600);
    }

  if(!empty($resultSAles[0]['salesforceID'])){
    $UserData['salesforcestatus'] =0;
    $whereAr =array("ID"=>$_COOKIE['LeadId']);
    $database->updateData('submissions',$UserData,$whereAr);
  }
} catch(Exception $e) {
  //echo $mySforceConnection->getLastRequest();
  //echo $e->faultstring;

}

function encrypt($notoencrypt){
  $key ='tiger';
  $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
  $iv = openssl_random_pseudo_bytes($ivlen);
  $ciphertext_raw = openssl_encrypt($notoencrypt, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
  $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
  return base64_encode( $iv.$hmac.$ciphertext_raw );
}
function generateRandomString($length = 20) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
function sendtopersonal($id){
  $apikeys=array(
    array('min_price'=>80,'user'=>'Tigerfunding_11809_min80','apikey'=>'2b78c5d1fb0625d104ce657109ea89bdd5e0a3d5'),
    array('min_price'=>60,'user'=>'Tigerfunding_11809_min60','apikey'=>'84c5ac7fe21b125e4b72e0d396c5aac05787ba6a'),
    array('min_price'=>20,'user'=>'Tigerfunding_11809_min20','apikey'=>'53cdf964f32302b9b78266b7d5cdcb780f1aecbf'),
    array('min_price'=>1,'user'=>'Tigerfunding_11809_min1','apikey'=>'156094c6f6ac677dc88b5881388ceb727abb703b'));

  $database = new DataBasePDO();
  $sql = 'Select * FROM `personal_submissions` WHERE ID ='.$id;
  $result = $database->getAllResults($sql);

  $removeafter=explode('@', $result[0]['email']);
  $domain=$removeafter[1];
  $md5email=md5($result[0]['email']);

  $sqltoemail='SELECT * FROM `NoflowEmails` where DomainAndMain="'.$md5email.'" || DomainAndMain like "%'.$domain.'"';
  $emailtocheck = $database->getAllResults($sqltoemail);

  if(!empty($emailtocheck)){
    return 'email found';
  }else{
    $finalvalue=array();
    foreach ($apikeys as $apikeyvalue) {
      $ret=personalapi($result,$apikeyvalue);
      $ret_pkt = json_decode($ret, true);
      if($ret_pkt['Status']=='Sold'){
        $valuearr['plApi_status']=$ret_pkt['Status'];
        $valuearr['plApi_leadID']=$ret_pkt['LeadID'];
        $valuearr['plApi_price']=$ret_pkt['Price'];
        $valuearr['plApi_Redirect']=$ret_pkt['Redirect'];
        $valuearr['personalapiresponse']=$ret;
        $whereAr=array("ID"=>$id);
        $database->updateData('personal_submissions',$valuearr,$whereAr);
        return 'success';
        //break;
      }elseif($ret_pkt['Status']=='Error' || $ret_pkt['Status']=='Rejected'){
        $finalvalue=$ret_pkt;

        //continue;
      }

    }

    if($finalvalue['Status']=='Error'){
      $msg='';
      foreach ($finalvalue['Messages']  as $msgvalue) {
        $msg.=$msgvalue.',';
      }
      $valuearr['plApi_status']=$finalvalue['Status'];
      $valuearr['plApi_messages']=$msg;
      $valuearr['personalapiresponse']=$ret;
      $whereAr=array("ID"=>$id);
      $database->updateData('personal_submissions',$valuearr,$whereAr);
      return 'error';
    }elseif($finalvalue['Status']=='Rejected'){
      $msg='';
      foreach ($finalvalue['Messages']  as $msgvalue) {
        $msg.=$msgvalue.',';
      }


      $valuearr['plApi_status']=$finalvalue['Status'];
      $valuearr['plApi_leadID']=$finalvalue['LeadID'];
      $valuearr['plApi_messages']=$msg;
      $valuearr['personalapiresponse']=$ret;
      $whereAr=array("ID"=>$id);
      //print_r($valuearr);
      //print_r($whereAr);
      $database->updateData('personal_submissions',$valuearr,$whereAr);
      //print_r($cdd);die();
      return 'rejected';
    }

  }
}
function personalapi($result,$apikeyvalue=null){
  $dob=explode('-', $result[0]['DOB']);
  $chartoreplace =['(',')',' '];
  $charreplacement=['','-',''];
  $to_post = array();
  $to_post['username']             = $apikeyvalue['user'];
  $to_post['apikey']               = $apikeyvalue['apikey'];
  $to_post['ip_address']           = $result[0]['userip'];
  $to_post['agent']                = $result[0]['browser'];
  $to_post['min_price']            = $apikeyvalue['min_price'];
  $to_post['amount']               = $result[0]['like_to_borrow'];
  $to_post['fName']                = $result[0]['First_Name'];
  $to_post['lName']                = $result[0]['Last_Name'];
  $to_post['zip']                  = $result[0]['zip'];
  $to_post['city']                 = $result[0]['city'];
  $to_post['state']                = $result[0]['state'];
  $to_post['address']              = $result[0]['street'];
  $to_post['lengthAtAddress']      = $result[0]['length_address'];
  $to_post['licenseState']         = $result[0]['Issuing_State'];
  $to_post['email']                = $result[0]['email'];
  $to_post['license']              = $result[0]['Driver_License'];
  $to_post['rentOwn']              = $result[0]['own_house'];
  $to_post['phone']                = str_replace($chartoreplace, $charreplacement ,$result[0]["phone"]);
  $to_post['workPhone']            = str_replace($chartoreplace, $charreplacement ,$result[0]["Employer_Phone"]);
  $to_post['callTime']             = $result[0]['Contact_time'];
  $to_post['bMonth']               = $dob[1];
  $to_post['bDay']                 = $dob[2];
  $to_post['bYear']                = $dob[0];
  $to_post['ssn']                  = $result[0]['SSN'];
  $to_post['armedForces']          = $result[0]['Military'];
  $to_post['incomeSource']         = $result[0]['Income_source'];
  $to_post['employerName']         = $result[0]['Employer_name'];
  $to_post['timeEmployed']         = $result[0]['Timed_Employed'];
  $to_post['employerPhone']        = str_replace($chartoreplace, $charreplacement ,$result[0]["Employer_Phone"]);
  $to_post['paidEvery']            = $result[0]['get_paid'];
  $to_post['accountType']          = $result[0]['Account_Type'];
  $to_post['monthlyNetIncome']     = $result[0]['gross_income'];
  $to_post['websiteName']          = $result[0]['gross_income'];
  $to_post['lead_type']            = 'personalloan';
  $to_post['loan_reason']          = $result[0]['Loan_Reason'];
  $to_post['credit_type']          = $result[0]['Credit_type'];

  $ch = curl_init();
  //$posting_url = "https://api.itmedia.xyz/post/testjson/api/v1";
  $posting_url = "https://api.itmedia.xyz/post/productionjson/api/v1";

  curl_setopt(    $ch,    CURLOPT_URL,               $posting_url             );
  curl_setopt(    $ch,    CURLOPT_POST,              1                        );
  curl_setopt(    $ch,    CURLOPT_POSTFIELDS,        $to_post                 );
  curl_setopt(    $ch,    CURLOPT_FAILONERROR,       1                        );
  curl_setopt(    $ch,    CURLOPT_HEADER,            0                        );
  curl_setopt(    $ch,    CURLOPT_RETURNTRANSFER,    1                        );
  curl_setopt(    $ch,    CURLOPT_SSL_VERIFYPEER,    false                    );
  curl_setopt(    $ch,    CURLOPT_SSL_VERIFYHOST,    false                    );
  curl_setopt(    $ch,    CURLOPT_TIMEOUT,           0                        );


  $ret = curl_exec($ch);
  curl_close($ch);
  //print_r($ret);
  return $ret;
}

function getRealIpAddr(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

// mysqli_close($conn);
function createurl($id){
  $sql = 'Select * FROM `personal_submissions` WHERE ID ='.$id;
  $database = new DataBasePDO();
  $result = $database->getAllResults($sql);
  //print_r($result);
  $removeafter=explode('@', $result[0]['email']);
  $domain=$removeafter[1];
  $md5email=md5($result[0]['email']);
  $sqltoemail='SELECT * FROM `NoflowEmails` where DomainAndMain="'.$md5email.'" || DomainAndMain like "%'.$domain.'"';
  $emailtocheck = $database->getAllResults($sqltoemail);
  if(!empty($emailtocheck)){
    return 'email found';
  }else{
    $dob=explode('-', $result[0]['DOB']);
    $chartoreplace =['(',')','-',' '];
    $UserData['url']= 'https://personalloans.com/get-started/?amount='.$result[0]["amount_requested"];
    $UserData['url'].='&creditType='.$result[0]["Credit_type"];
    $UserData['url'].='&loanReason='.$result[0]["Loan_Reason"];
    $UserData['url'].='&fName='.$result[0]["First_Name"];
    $UserData['url'].='&lName='.$result[0]["Last_Name"];
    $UserData['url'].='&bMonth='.$dob[0];
    $UserData['url'].='&bDay='.$dob[1];
    $UserData['url'].='&bYear='.$dob[2];
    $UserData['url'].='&ownCar=';
    $UserData['url'].='&armedForces='.$result[0]["Military"];
    $UserData['url'].='&callTime='.$result[0]["Contact_time"];
    $UserData['url'].='&zip='.$result[0]["zip"];
    $UserData['url'].='&city='.urlencode($result[0]["city"]);
    $UserData['url'].='&state='.$result[0]["state"];
    $UserData['url'].='&address='.urlencode($result[0]["street"]);
    $UserData['url'].='&lengthAtAddress='.$result[0]["length_address"];
    $UserData['url'].='&rentOwn='.$result[0]["own_house"];
    $UserData['url'].='&incomeSource='.$result[0]["Income_source"];
    $UserData['url'].='&employerName='.$result[0]["Employer_name"];
    $UserData['url'].='&timeEmployed='.$result[0]["Timed_Employed"];
    $UserData['url'].='&employerPhone='.str_replace($chartoreplace, '' ,$result[0]["Employer_Phone"]);
    $UserData['url'].='&paidEvery='.$result[0]["get_paid"];
    $UserData['url'].='&nextPayday=';
    $UserData['url'].='&secondPayday=';
    $UserData['url'].='&abaNumber=';
    $UserData['url'].='&accountType='.$result[0]["Account_Type"];
    $UserData['url'].='&bankName=';
    $UserData['url'].='&bankPhone=';
    $UserData['url'].='&monthsBank=';
    $UserData['url'].='&directDeposit=';
    $UserData['url'].='&monthlyNetIncome='.$result[0]["gross_income"];
    $UserData['url'].='&licenseState='.$result[0]["Issuing_State"];
    $UserData['url'].='&privacyPolicy=yes';
    $UserData['url'].='&email='.$result[0]["email"];
    $UserData['url'].='&phone='.str_replace($chartoreplace, '' ,$result[0]["phone"]);
    $UserData['url'].='&ssn='.$result[0]["SSN"];
    $UserData['url'].='&license='.$result[0]["Driver_License"];
    $UserData['url'].='&accountNumber=';
    $UserData['url'].='&aid=11766';
    $UserData['url'].='&note=';
    return $UserData['url'];
  }
}

?>
