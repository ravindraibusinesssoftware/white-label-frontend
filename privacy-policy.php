<html data-wf-page="5963a518fff7b87ae7b97c82" data-wf-site="5963a518fff7b87ae7b97c81">
<head>
  <meta charset="utf-8">
  <title>tiger</title>
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta content="Webflow" name="generator">
  <link href="New-Tiger/css/normalize.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/bootstrap.min.css" >
  <link href="New-Tiger/css/webflow.css" rel="stylesheet" type="text/css">
  <link href="New-Tiger/css/tiger-a3c537.webflow.css" rel="stylesheet" type="text/css">
  <link href="New-Tiger/css/custom.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <script type="text/javascript">WebFont.load({
  google: {
    families: ["Open Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic"]
  }
  });
</script>
  <!-- [if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js" type="text/javascript"></script><![endif] -->
  <script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);
  </script>
  <link rel="shortcut icon" type="image/png" href="New-Tiger/images/favicon-tiger.png"/>
  <link href="https://daks2k3a4ib2z.cloudfront.net/img/webclip.png" rel="apple-touch-icon">
</head>
<body>

     <header class="header_normal">
	<div class="container">
		<div class="row">
		<div class="col-sm-6 clearfix col-sm-push-6">
				<div class="header_right pull-right">
					<a id="telNum" href="tel:8555344940">855-534-4940</a>
				</div>
			</div>
			<div class="col-sm-6 clearfix col-sm-pull-6">
				<div class="header_left pull-left">
					<h1 class="logo">
						<a href="https://apply.tigerfundingllc.com">
							<img src="images/gotiger-logo.svg">
						</a>
					</h1>
				</div>
			</div>

		</div>
	</div>
    </header>

    <div class="policy_wrapper">

    	<div class="container">

    		<h3>1.	PRIVACY POLICY</h3>

    		<p>This Privacy Policy applies to <a href="www.tigerfundingllc.com">www.tigerfundingllc.com</a>, <a href="go.tigerfundingllc.com">go.tigerfundingll.com, and <a href="apply.tigerfundingllc.com">apply.tigerfundingllc.com</a>, (the "Site"), which is owned and operated by Tiger Merchant Funding, LLC, a Delaware limited liability company ("we," "us," "our," or the "Company").  This Privacy Policy replaces any privacy policy previously delivered to you or appearing on the Site, any transaction agreement, or any other document previously delivered to you.  If there are conflicts between this Privacy Policy and any other policy delivered to you by the Company, the terms of this Privacy Policy shall control until revised.  This Privacy Policy is effective as of February 5, 2018.</p>

    		<p>We recognize and respect your privacy. Although this is a business-to-business, not a consumer, website, which collects information regarding businesses, some information collected may include individual consumer information.  This Privacy Policy explains how we collect, use, and disclose personally identifying information ("PII") and non-personally identifiable information ("Non-PII") gathered through our Site, as to individual consumers, and not information collected as to businesses.</p>

    		<p>The Company may update this Privacy Policy to reflect changes to our information practices.  We reserve the right to change this Privacy Policy at any time.  If we make any material changes to our information practices, we will revise this Policy.  You are advised to read and review this Policy each and every time you access our Site and/or use any service referred to you by the Company.  Your use of the Site constitutes your agreement to the terms of the effective Privacy Policy.</p>

    		<p>The Privacy Policy is subject to and incorporates by reference our <a href="terms-of-use.php">Terms of Use</a>, including its limitation on liability.</p>

    		<h3>2.	ACKNOWLEDGMENT & AGREEMENT</h3>
    		<p>The Site is not intended for use by individuals under the age of 18 or those who are not legal residents of the United States.  By using the Site and participating in any of the Company's services, you agree to accept the terms of this Privacy Policy.  If you do not agree to the terms of this Privacy Policy, or if you are not 18 years of age or older and a valid resident of the United States, please do not use the Site or participate in the Company's services.</p>

    		<h3>3.	ABOUT THE COMPANY</h3>
    		<p>Through the Site, the Company operates a prospective business-customer database (the "Service"), which compiles, sorts and submits certain information to businesses and networks on behalf of business customers who have shown an interest in requesting a business's products or services and have affirmatively asked to be contacted by a representative of a product or services business.  We utilize third-party vendors, independent contractors, and a variety of media advertisements to promote our Site to business customers interested in using our Site.  Additionally, we advertise third-party services through referrals and/or through pay-per-click advertisements to which you may be redirected through our Site.  By using the Site, you agree to our use of your PII and Non-PII for these purposes. </p>

    		<h3>4.	INFORMATION COLLECTION</h3>
    		<p>The Company collects information through the Site from our prospective and existing customers at serval different points on our Site.  We collect and retain information about you to communicate with you, to process your requests for products and services, and to inform you about other financial services that may be of interest to your business.  Users may opt-out of receiving further mailings (see the Opt-Out/Opt-In section of this Privacy Policy.)  The examples in this Privacy Policy are illustrative only and are not intended to be exclusive.</p>

    		<h3>A.	PERSONALLY IDENTIFIABLY INFORMATION</h3>
    		<p>We collect and combine personally identifiable information ("PII") and non-personally identifiable information ("Non-PII") through various sources, including those discussed below.  For your reference, PII is data collected with an intention to specifically identify, contact or locate a person, including but not limited to Social Security number, first name, last name, address, zip code, city, state, length of stay at address, whether you rent or own, your email address, driver's license or state ID number, telephone number, work telephone number, time at which you may be contacted, birth date, birth year, your IP address and computer specifications, your ABA/routing number, your bank account number, nature of bank account, name of bank, bank phone number, duration with bank, bank account statements, monthly net income, business funding amount desired, and other personal information you submit or upload through our Site. Conversely, Non-PII is data that is not used to specifically identify, contact or locate an individual, including but not limited to zip code, gender, age, or IP address.</p>

    		<h3>B.	THIRD PARTIES</h3>
    		<p>We may obtain information about you from third parties, including credit reports, public records, and demographic information.  </p>

    		<p>We also may collect information from you if you access our Site through an account created with a third-party website (e.g., QuickBooks, Facebook, or LinkedIn). The option to access our Site via these third-party websites may occur and will be entirely at your option. The information we are able to migrate to our Site from these third-party websites will depend on the privacy settings that you have in place with the third-party site from which you log in. If you choose to log into the Site via a third-party website, you hereby consent to our access to and collection of such Personal Information about you.</p>

    		<h3>C.	HARDWARE AND SOFTWARE INFORMATION</h3>
    		<p>Information about your computer hardware and software is automatically collected by the Company, such as the website from which you came to our Site and your IP address, browser type and language, device ID and type, the operating system used by your device, access times, your mobile device's geographic location while our application is actively running, and the referring website address.  This information is used by the Company to maintain and improve the quality of the Service and to generate statistics regarding use of the Site. The Company does not use third party databases to correct or update the information you submit to us for any reason.</p>

    		<h3>D.	COOKIES</h3>
    		<p>A cookie is a piece of data stored on the user's hard drive containing information about the user. Usage of a cookie is in no way linked to any personally identifiable information while on our site. If a user rejects the cookie, they may still use our site; however, without cookies the user may not have access to certain features on our site. Cookies can also enable us to track and target the interests of our users to enhance the experience on our site. All cookies served on the domain, both session and persistent, are tied to the PII you provide.</p>

    		<h3>E.	BEHAVIORAL MARKETING</h3>
    		<p><u>Google Analytics </u></p>
    		<p>The Company may collect information about your computer, including your IP address, operating system and browser type, for system administration and in order to create reports. This is statistical data about our users' browsing actions and patterns, and does not identify any individual. Google Analytics is a web analytics tool that helps website owners understand how visitors engage with their website. Google Analytics customers can view a variety of reports about how visitors interact with their website so that they can improve it. Like many services, Google Analytics uses first-party cookies to track visitor interactions as in our case, where they are used to collect information about how visitors use the Site. We then use the information to compile reports and to help us improve the Site. Cookies contain information that is transferred to your computer's hard drive. These cookies are used to store information, such as the time that the current visit occurred, whether the visitor has been to the site before and what site referred the visitor to the web page. </p>

    		<p>Google Analytics collects information anonymously. It reports website trends without identifying individual visitors. We will not upload any data that allows Google to personally identify an individual (such as name, Social Security Number, email address, or any similar data), or data that permanently identifies a particular device (such as a unique device identifier if such an identifier cannot be reset). You can opt out of Google Analytics without affecting how you visit the Site - for more information on opting out of being tracked by Google Analytics across all websites you use, visit this <a href="https://www.google.com/analytics/">Google page</a>.</p>

    		<p><u>Google Display Advertising</u></p>
    		<p>Additionally, the Company uses Google Analytics code that allows for certain forms of display advertising and other advanced features. Subject to change, the Google Display Advertising features the Company currently uses are Remarketing, Google Display Network Impression Reporting, Google Analytics Demographics and Interest Reporting.</p>

    		<p>These features are used to advertise online; to allow third-party vendors, including Google, to show you advertising across the Internet; to allow the Company and third-party vendors, including Google, to use first-party cookies (such as the Google Analytics cookie) and third-party cookies together to inform, optimize, and serve ads based on your past visits to the Site and to report how ad impressions, uses of ad services, and interactions with these ad impressions and ad services are related to visits to the Site. Data from Google's interest-based advertising or third-party audience data (such as age, gender, and interests) is also combined with Google Analytics to better understand the needs of the Company's users and to improve the Site.</p>

    		<p>You may opt out of such display advertising at any time by visiting your <a href="https://support.google.com/analytics/answer/1010249?hl=en">Google Ads Settings page</a> or by installing and running the <a href="https://tools.google.com/dlpage/gaoptout">Google Analytics Opt-out Browser Add-on.</a></p>

    		<p><u>Google Tag Manager </u></p>
    		<p>Our Site utilizes Google Tag Manager, an analytics service provided by Google. This service enables the Company to monitor and analyze web traffic and can be used to keep track of user behavior. To learn more, and to opt out of Google Tag Manager, visit the <a href="https://www.google.com/policies/privacy/">Google Privacy Page.</a></p>

    		<p><u>Pixel Tags</u></p>
    		<p>We embed pixel tags (also called web beacons or clear GIFs) on web pages, ads, and emails. These tiny, invisible graphics are used to access cookies and track user activities (such as how many times a page is viewed). We use pixel tags to measure the popularity of our features and services. Advertising companies also use pixel tags to measure the number of ads displayed and their performance (such as how many people clicked on an ad).</p>

    		<p><u>Social Media Widgets</u></p>
    		<p>Our Site includes or may include Social Media features, such as the Facebook "Like" button and Widgets, such as the "Share" button or interactive mini-programs that run on our site. These features may collect your IP address, which page you are visiting on our site, and may set a cookie to enable the feature to function properly. Social Media features and widgets are either hosted by a third party or hosted directly on our Site. Your interactions with these features are governed by the Privacy Policy of the company providing it.</p>

    		<h3>F.	CORRECTING/UPDATING INFORMATION</h3>
    		<p>Maintaining the accuracy of your information is a shared responsibility. We maintain the integrity of the information you provide us and will update your records when you notify us of a change. If you would like to verify any data that we have received from you, make any corrections or updates, or disable the use of your personal information, please contact us at the mailing address or email address listed below.</p>

    		<div class="address">
			<strong>
				TIGER MERCHANT FUNDING, LLC <br>
				1680 Michigan Ave., Ste 700 <br>
				Miami Beach, Florida 33139 <br>
				Attention: Customer Service <br>
				<a href="webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a>
			</strong>
		</div>

		<h3>5.	USE OF INFORMATON AND INFORMATION SHARING</h3>
		<h3>A.	PRIVACY PERMISSIONS AND DO NOT TRACK SIGNALS </h3>
		<p>We collect and combine personally identifiable information ("PII") and non-personally identifiable information ("Non-PII") through various sources, including those discussed herein. When you submit your request for information on business funding, you are granting us permission to share your PII and Non-PII, including the contact and business funding request data you submit and/or upload with approved independent contractors and affiliate companies; you are also granting us permission to be contacted by us, our approved independent contractors and affiliate companies.</p>

		<p>We do not engage in the collection of PII about an individual consumer's online activities over time and across third-party Web sites or online services. We do not honor "do not track" signals. Users who chose to use "do not track" signals will still have access to our site. Other parties (i.e., ad networks, analytics providers, etc.) may collect PII about an individual consumer's online activities over time and across different websites when a consumer uses our Site or services.</p>\

		<h3>B.	SHARING INFORMATION WITH AFFILIATE COMPANIES AND APPROVED INDEPENDENT CONTRACTORS</h3>
		<p>When you submit your request for information to seek business funding options for your business, you are granting permission to be contacted by the Company, its affiliate companies, or its approved independent contractors. Approved independent contractors include, but are not limited to, banks, commercial lenders, unsecured lenders, secured or titled lenders, business working capital providers, loan brokers, merchant cash advance brokers, and financial service providers.  An affiliate company is a company that is owned in whole or in party by the Company or has common ownership of the Company.</p>

		<p>We share the contact and business funding request data you submit on the online working capital request form, which may include PII and Non-PII, with approved independent contractors to fulfill information requests we receive from you; to provide you with superior service; inform you of product and service opportunities that may be of interest to you; and for other business purposes.  The information you provide and upload through the online working capital request form will be shared with approved independent contractors or affiliate companies, who may use some of this this information to evaluate your business's eligibility for financial products available through their company and/or other third parties.</p>

		<h3>C.	SHARING INFORMATION WITH OTHER THIRD PARTIES</h3>
		<p>By submitting your information to the Company you are providing your express written consent under the Fair Credit Reporting Act for the Company, affiliate companies, and approved independent contractors (e.g. selected lenders, working capital partners) with whom you are connected to obtain your consumer and business report(s), including credit report from a credit bureau, and any other information. While the "soft pull" that the Company may initiate will not affect your credit score, please note that credit inquires by approved independent contractors will likely impact your credit score.  The information obtained through these third parties about your business or you may be used by the Company, affiliate companies, and approved independent contractors to process and evaluate your online working capital request form as well as to help assess your working capital request in the context of your overall financial situation.</p>

		<p>Furthermore, we may disclose you PII and Non-PII to third parties for the following purposes: (1) marketing, advertising, research, or other business purposes; (2) to third parties involved in the process of providing services to us or you or performing functions on our behalf, such as contractors, and customer service providers (e.g., information technology or human resources consultants or service providers). These companies are authorized to use your personal information only as necessary to provide these services to us; (3) to protect or enforce our rights or property, protect your safety or the safety of others, investigate fraud, or respond to a government request, such as a subpoena or other legal process by a governmental entity or third party, or if otherwise required by law; (4) to our parent, investors, partners, or auditors in the course of their review or auditing of our Company; and (5) in the event of the sale or dissolution (bankruptcy) of all of our assets, in which case, you will be notified, there after by a prominent notice on our Site of any change in ownership or uses of your PII, as well as any choices you may have regarding your personal information.</p>

		<p>In addition, if you opt-out of future contact from the Company by using the opt-out mechanism provided in each email we send to you or available to you on the Site, we will provide your email address to any necessary affiliate companies, approved independent contractors, or third-party service provider so that these parties can add your name and contact information to its own Do-Not-Contact list.</p>

		<h3>6.	CONFIDENTIALITY AND SECURITY</h3>
		<h3>A.	SECURITY</h3>
		<p>We use a variety of security technologies and procedures to help protect your personal information from unauthorized access, use, or disclosure. For example, we store the personal information you provide on computer systems with limited access, which are located in controlled facilities. When we transmit highly confidential information over the Internet, we protect it through the use of encryption, such as the Secure Socket Layer (SSL) protocol. We maintain physical, electronic and procedural safeguards that comply with federal standards to guard your nonpublic personal information against unauthorized access or use.  For example, data can only be read or written through defined service access points, the use of which is password-protected. The physical security of the data is achieved through a combination of network firewalls (there is no direct communication allowed between the database server and the Internet) and servers with operating systems, all housed in a secure facility. Additionally, access to the system is controlled.</p>

		<p>We cannot ensure that all of your private communications and other personally identifiable information will never be disclosed in ways not otherwise described in this Privacy Policy. By way of example (without limiting the foregoing), we may be forced to disclose information to the government or third parties under certain circumstances as required by law, or third parties may unlawfully intercept or access transmissions or private communications. If you have any questions about security on our Site, you can contact us at <a href="webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com.</a></p>

		<h3>B. CONFIDENTIALITY</h3>
		<p>We restrict access to nonpublic personal information about you to those employees who need to know that information to provide products or services to you.</p>

		<h3>C.	SPECIAL CIRCUMSTANCES</h3>
		<p>As the Company develops, we may buy other businesses or their assets or sell our business assets. Customer information is generally one of the business assets involved in such transactions. Thus, in the event that the Company or all of its assets are acquired, customer information, including any visitor information collected through the Site, would be one of the transferred assets.</p>

		<p>The Company also reserves the right to disclose member information in special cases when we have reason to believe that disclosing this information is necessary to identify, contact or bring legal action against someone who may be causing injury to or interference with (either intentionally or unintentionally) the Company's rights or property, other visitors, members, or anyone else that could be harmed by such activities. The Company also reserves the right to disclose visitor or member information when we believe in good faith that the law requires it and when we believe that disclosure is necessary to protect our rights and/or comply with a judicial proceeding, court order, or legal process served on our Site.</p>

		<h3>D.	WHAT YOU CAN DO TO PROTECT THE SECURITY OF YOUR INFORMATION</h3>
		<p>In addition to our own substantial efforts, you can take several precautions to protect the security of your computer and PII. For instance, you can start by using a complex/well-chosen password. You should avoid using any information that others can easily learn about you, such as a family member's name or birthday; you can also use special characters in place of letters. We also recommend that you change your password frequently. You can also install and regularly update antivirus and firewall software to protect your computer or device from external attacks by malicious users.</p>

		<p>If you use a computer that is accessed by other people, such as in a public library or Internet cafe, we recommend that you take special precautions to protect the security of your account and personal data. When you are finished using our site, close the browser window and clear the browser's cache files.</p>

		<p>You should also be aware of fraudulent attempts to gain access to your account information known as "phishing." Phishing is a tactic used by scammers in which unsuspecting people are brought to a website by a genuine-looking email purporting to be from a legitimate company. The phony or "spoof" email takes the person to a website that looks legitimate but in fact is not. Either in the email itself or on this fake site, scammers will ask for login information to gain access to people's accounts and withdraw their money. The Company will never ask you for your login information in the context of any email. In general, you can protect yourself against phishing by never providing personal or login information via an email, instead, go the web site directly. You might also make it a habit to check the URL of a web site to be sure that it begins with the correct domain.</p>

		<h3>7.	OPT-IN/OPT-OUT</h3>
		<p>We, our affiliate companies, approved independent contractors and/or third parties may promote product and service offerings via email, sms-text message, or telephone to consumers who requested to be contacted either through our Site's online forms' Opt-in process or through an Opt-In process on our affiliate companies;, approved independent contractors; and/or third parties; website ("List Members"). List Members will receive emails, sms-text messages, or telephone contacts for as long as they wish to continue to receive such contacts.</p>

		<p>In addition to any fee of which you are notified, your provider's standard messaging rates apply to our confirmation and all subsequent SMS correspondence. You may opt-out and remove your SMS information by sending "STOP," to the SMS text message you have received. If you remove your SMS information from our database it will no longer be used by us for secondary purposes, disclosed to third parties, or used by us or third parties to send promotional correspondence to you.</p>

		<p>List Members can be removed from our contact lists at any time with no questions asked. To remove your information from our database to not receive future communications or to no longer receive our service, click on the "unsubscribe" link found at the bottom of any email sent by us or send a request not to be contacted any further to <a href="mailto:webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a>. Email addresses and phone numbers received in an opt-out request will be added to our internal Do-Not-Contact registry within ten (10) days following receipt.</p>

		<p>If a List Member chooses to be removed from our contact list, if applicable, we will also forward this request to the specific approved independent contractors or affiliate companies to whom we provided the List Member's informational request, so that the List Member may be included on the third-party's Do-Not-Contact registry as well.  Please bear in mind that if your information was purchased commercially from a third-party vendor or affiliate, the Company may not be the only purchaser of your information. Additionally, any approved independent contractor or affiliate company may have purchased your contact information from an additional third-party and thus you may continue to receive contacts from those third-party companies. The Company is not responsible for stopping all unwanted contact from sources beyond our immediate control. </p>

		<h3>8.	LEGAL</h3>
		<h3>A. 	GLB ACT</h3>
		<p>For purposes of compliance with the Gramm-Leach-Bliley Act, 15 U.S.C. 6802 (the GLB Act), this Privacy Policy shall serve as both your initial customer relationship and annual Privacy Notice as defined under the GLB Act. If you would like to opt-out from the disclosure of your personal information to any third-party for marketing purposes, please click the unsubscribe link found at the bottom of any email sent by our Company or send a request not to be contacted any further to <a href="mailto:webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a>.</p>

		<h3>B. 	PATRIOT ACT</h3>
		<p>Section 326 of the USA PATRIOT ACT requires all financial institutions to obtain, verify, and record information that identifies each person who opens an account or changes an existing account. This federal requirement applies to all new customers and current customers. This information is used to assist the United States government in the fight against the funding of terrorism and money-laundering activities. While the Company is not a financial institution, approved independent contractors we work with are financial institutions. What this means to you: when you submit information to the Company, we will ask for your name, physical address, mailing address, date of birth, and other information that will allow us to identify you. This information will be passed on to the approved independent contractor that you are connected with through the Site.</p>

		<h3>C. 	MINORS</h3>
		<p>The Company does not knowingly collect information from children under the age of 18. No information is knowingly collected from children or used for any purpose whatsoever, including marketing and promotional purposes. If a parent or guardian is made aware that a child under the age of 18 has provided us with personally identifying information through any point of contact, please contact us at <a href="mailto:webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a>, and we will delete the information about the child referenced from our files.</p>

		<h3>D. 	FOR CALIFORNIA RESIDENTS</h3>
		<p>On September 27, 2013, A.B. 370 was signed into law, amending the California Online Privacy Protection Act to require operators of commercial websites like the Site to disclose: (1) How the Site responds to "do not track" signals; and (2) Whether third parties collect personally identifiable information (PII) about users when they visit the Site.</p>

		<p>The Company does not engage in the collection of personally identifiable information (PII) about an individual consumer's online activities over time and across third-party websites or online services.  The Company does not honor "do not track" signals. Users who chose to use "do not track" signals will still have access to the Site. The Company does not permit third parties to collect PII about an individual consumer's online activities over time and across different websites when a consumer uses the Site.</p>

		<h3>F. 	JURISDICTION</h3>
		<p>By your use of this Site you consent to the laws and jurisdiction of the state of Florida as it relates directly or indirectly to your use of this website. You agree that any legal action brought against us shall be governed by the laws of Florida, without regard to conflict of law principles. You agree that the sole jurisdiction and venue for any litigation arising from your use of or orders made on the Site shall be the Miami-Dade County, Florida. The terms described in our Terms of Use will be governed by and construed in accordance with the laws of the state of Florida, without regard to conflict of law provisions. </p>

		<h3>9.	ADDITIONAL INFORMATION</h3>
		<h3>A.	LINKS</h3>
		<p>The Site contains links to other websites belonging to third parties. Please be aware that the Company is not responsible for the privacy practices of such other websites. We encourage our users to be aware when they leave our Site and to read the privacy statements of each and every website that collects personally identifiable information. This privacy statement applies solely to information collected by the Company.  </p>

		<h3>B.	DATA RETENTION</h3>
		<p>We will retain your information for as long as needed to provide you services. In additional we are required to keep, retain your information as necessary to comply with our legal obligations, resolve disputes, and enforce agreements.</p>

		<h3>C.	USER CONTENT</h3>
		<p>We may, from time to time, allow users or third parties to post information to our Site, such as comments or blogs or to offer you information on small business topics. Any information that you post to our Site becomes public information and may be viewable by other users, as well as visitors to our Site (this is not true of uploaded documents as part of the funding process). In addition, your name, as well as other optional information you choose to submit along with the information you post, will be publicly displayed along with your comment or blog. We are not responsible for the privacy of any information that you choose to post to our Site, or for the accuracy of any information contained in those postings. We cannot prevent such information from being used by others in a manner that may violate this Privacy Policy, the law, or your personal privacy. To request removal of your personal information from our blog or community forum, contact us at <a href="mailto:webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a>. In some cases, we may not be able to remove your personal information, in which case we will let you know if we are unable to do so and why. Your posting of any content to any of the Site is subject to our <a href="terms-of-use.php">Terms of Use</a>.</p>

		<h3>D.	TESTIMONIALS</h3>
		<p>We display personal testimonials of satisfied customers on our site in addition to other endorsements. With your consent we may post your testimonial along with your name. If you wish to update or delete your testimonial, you can contact us at <a href="mailto:webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a>.</p>

		<h3>E.	FORMER CUSTOMERS</h3>
		<p>We treat information about our former customers in the same manner as we treat information about our current customers.</p>

		<h3>F.	PUSH NOTIFICATIONS</h3>
		<p>With your consent, we may send push notifications or alerts to your web browser or your mobile device. These notifications may include marketing messages from us or from third-parties. You can deactivate these messages at any time by changing the notification settings on your mobile device or browser.</p>

		<h3>G.	INTERNATIONAL USERS</h3>
		<p>The Site is hosted in the United States and is only intended for use by legal residents of the United States. If you are accessing the Site from outside the United States, by providing your information to the Site, you are consenting to and authorizing the transfer of your information to the United States for storage, use, processing, maintenance, and onward transfer of such information to other entities regardless of their location in accordance with this Privacy Policy and our <a href="terms-of-use.php">Terms of Use</a>. You are also consenting to the application of United States law in all matters concerning the Site.</p>

		<h3>H.	CONTACT US</h3>
		<p>We hope this information satisfies any questions or concerns you may have.  If you have additional questions or concerns about this Privacy Policy, please contact us at:</p>

		<div class="address">
			<strong>
				TIGER MERCHANT FUNDING, LLC <br>
				1680 Michigan Ave., Ste 700 <br>
				Miami Beach, Florida 33139 <br>
				Attention: Customer Service <br>
				<a href="webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a>
			</strong>
		</div>

		<h3>I.	INFORMATION FOR ISPS:</h3>
		<p>The Company understands the importance of protecting individuals' personal information and the proper use of their personal information. It is our policy to not send unsolicited e-mails to customers and we hope the following information will eliminate any concerns that your company may have regarding our practices of individuals' personal information.</p>

		<p>The Company and its affiliated sites use only an opt-in or opt-out method of obtaining customer information. All personal information from individuals is obtained voluntarily from the individual. We obtain personal information from third parties that follow the same set of policies.</p>

		<p>All e-mail communications sent by us to customers include information about the origin of the e-mail and include easily identifiable instructions on how consumers can unsubscribe from receiving future e-mail messages from us. Below is an example of e-mail communications sent by us to subscribers.</p>

		<hr>

		<p>This e-mail is not sent unsolicited. You are receiving this email from Tiger Merchant Funding, LLC because you subscribed via our website. Offers and messages are ONLY sent to subscribers.</p>

		<p>To be unsubscribe from our list, click here</p>

		<p>View Our <a href="privacy-policy.php">Privacy Policy</a>. ISPs view our policies.</p>

		<hr>

		<p>In some instances, the Company may enter into a privacy policy with a third-party website to allow consumers to opt-in to our marketing program on those third party websites. When this occurs we will make certain that the third-party websites represent and warrant to us that the consumer data was collected voluntarily by consumer registration and/or co-registrations. We will also require that the third-party websites have a right under any applicable privacy policy to transfer the consumer data to us, and that the Company has the right to send marketing offers to the customer.</p>

		<p>We hope this information satisfies any questions or concerns you may have as an ISP regarding our e-mail practices. If you have additional questions or wish to discuss the matter further, please contact us at <a href="webmaster@tigerfundingllc.com">webmaster@tigerfundingllc.com</a>.</p>

    	</div>

    </div>


    <div class="small_form_section">
      <div class="sm_form_container w-container">
        <h2 class="heading-4">What can you do with $50,000?</h2>
        <div class="text-block-6">Fill out the form and get funding the same business day</div>
        <div class="w-form">

          <div data-name="Email Form 2" id="email-form-2 " class="form bottom-form" name="Email Form 2" >

            <input class="sm_form w-input" id="first_name"  maxlength="256" placeholder="First Name*" required="required"  type="text">
            <input class="sm_form w-input" id="last_name" maxlength="256" placeholder="Last Name*" required="required" type="text">
            <input class="sm_form w-input" id="email" maxlength="256" placeholder="Email*" required="required" type="email"  >
            <input class="sm_form w-input" id="phone" maxlength="14" placeholder="Phone -10 digit number*" required="required" type="tel">
            <input class="sm_form w-input" id="company" maxlength="256" placeholder="Business Name" required="required" type="text"  >
            <select class="sm_form w-input Monthly_Revenue"  id="Monthly_Revenue" title="Total Monthly Revenue"  placeholder="AVG Monthly Revenue*" required="required">
              <option value="">--Select--</option>
              <option value="$0-$10,000">$0-$10,000</option>
              <option value="$10,000-$25,000">$10,000-$25,000</option>
              <option value="$25,000-$50,000">$25,000-$50,000</option>
              <option value="$50,000-$100,000">$50,000-$100,000</option>
              <option value="$100,000-$250,000">$100,000-$250,000</option>
              <option value="$250,000+">$250,000+</option>
            </select>
            <input class="cta sm_cta w-button sub-form get-started btn_disable" data-wait="Please wait..." value="Get Started">
          </div>

          <div class="w-form-done">
            <div>Thank you! Your submission has been received!</div>
          </div>
          <div class="w-form-fail">
            <div>Oops! Something went wrong while submitting the form</div>
          </div>

        </div>
        <div class="text-block-7">Give it a try. It only takes one click.</div>
      </div>
    </div>


<?php include 'footer.php';?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script src="js/webflow.js" type="text/javascript"></script>


<script type="text/javascript">

// Form Validation( Bottom )

var $regexname = /^([a-zA-Z ]{3,100})$/;
//var $regphone = /^\(\d{3}\) \d{3}-\d{4}$/;


$('.bottom-form #first_name, .bottom-form  #last_name ,.bottom-form  #phone ,.bottom-form  #email').on('keyup',function(){

	if ($('.bottom-form #first_name').val().match($regexname) && $('.bottom-form #last_name').val().match($regexname) && $('.hero-form #phone').val().length == 10 && $('.hero-form #email').val().length >= 3)
	{
		$('.get-started').removeClass('btn_disable');
	}
	else{
		$('.get-started').addClass('btn_disable');
	}

});

// Phone Number Masking

$('.hero-form #phone , .bottom-form #phone')

	.keydown(function (e) {
		var key = e.which || e.charCode || e.keyCode || 0;
		$phone = $(this);

    if ($phone.val().length === 1 && (key === 8 || key === 46)) {
			$phone.val('(');
      return false;
		}

    else if ($phone.val().charAt(0) !== '(') {
			$phone.val('('+String.fromCharCode(e.keyCode)+'');
		}

		if (key !== 8 && key !== 9) {
			if ($phone.val().length === 4) {
				$phone.val($phone.val() + ')');
			}
			if ($phone.val().length === 5) {
				$phone.val($phone.val() + ' ');
			}
			if ($phone.val().length === 9) {
				$phone.val($phone.val() + '-');
			}
		}

		return (key == 8 ||
				key == 9 ||
				key == 46 ||
				(key >= 48 && key <= 57) ||
				(key >= 96 && key <= 105));
	})

</script>
