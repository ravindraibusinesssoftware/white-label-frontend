<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);
date_default_timezone_set("America/Toronto");
require_once "config/mysql.class.php";
$database = new DataBasePDO();
$UserData["email"] = $_GET['email'];
$UserData["website"] = $_GET['site'];
$database->insertOrUpdate('NoflowEmails', $UserData);

?>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<title>Knight Capital Funding</title>

<link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/custom.css?v=<?= rand() ?>">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCfDJIOqbzGzNYOvjQpvJGyAUGkTLCjayg&libraries=places"></script>
</head>

<body class="">

    <header>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 clearfix col-sm-push-6">
            <div class="header_right pull-right">
              <a id="telNum" href="tel:8554624249">CALL NOW: 855-462-4249</a>
            </div>
          </div>
          <div class="col-sm-6 clearfix col-sm-pull-6">
            <div class="header_left pull-left">
              <h1 class="logo">
                <a href="https://www.knightcapitalfunding.com">
                  <img src="./images/white-kcf-logo.png">
                </a>
              </h1>
            </div>
          </div>

        </div>
      </div>
    </header>


    <div class="normal_wrapper">
     <div class="container">
      <div class="section-1">
       <div class="do-u-own-wrapper">
        <div class="do-u-own">
          <h1>You Are Unsubscribed Successfully</h1>
          <p class="para">Please check the URL or go <a href="https://apply.knightcapitalfunding.com">here</a></p>
        </div>
      </div>
    </div>

    <style>
        .para{
            font-size: 0.9em;
            color: #fff;
            line-height: 1.8em;
            font-weight: 400;
            width: 65%;
            text-transform: capitalize;
            margin: 1.5em auto 2.5em;
        }
    </style>
</body>
</html>