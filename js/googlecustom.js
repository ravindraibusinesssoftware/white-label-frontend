$('.form-group').css('position', 'relative');
var inputs = document.getElementsByClassName('addressAPi');
var options = { types: ['address'], componentRestrictions: { country: 'USA' } };
var autocompletes = [];
var placeApiValidation = true;
for (var i = 0; i < inputs.length; i++) {
  var addressIDv = inputs[i].id;
  $('#' + addressIDv).on('keydown', function(e) {
    //console.log(e);
    if (e.keyCode != 9) {
      $('#' + this.id + ' + strong').show();
      placeApiValidation = false;
      $(this)
        .parents()
        .children('button')
        .addClass('btn_disable');
    }
  });
  $('#' + addressIDv).on('blur', function() {
    if (this.value == '') {
      $('#' + this.id + ' + strong').show();
    }
  });
  $('#' + addressIDv).after(
    '<strong id="spanid" class="error_text" style="display:none;">Please Select an address from the list</strong>'
  );

  var autocomplete = new google.maps.places.Autocomplete(inputs[i], options);
  autocomplete.inputId = inputs[i].id;
  autocomplete.addListener('place_changed', fillIn);
  autocompletes.push(autocomplete);
}
function fillIn() {
  var thisiddf = this.inputId;
  var fieldsR = document.getElementById(thisiddf).getAttribute('fieldsR');
  fieldsR = fieldsR.split(',');
  var GooglePlaceNew = this.getPlace();
  var AllData = GooglePlaceNew.address_components;
  var office = '';
  placeApiValidation = true;
  for (var i = 0; i < AllData.length; i++) {
    var SingleData = AllData[i].types[0];
    if (SingleData == 'locality') {
      var iddata = fieldsR[0];
      var idvalue = AllData[i].long_name;
      document.getElementById(iddata).value = idvalue;
    } else if (SingleData == 'administrative_area_level_1') {
      var iddata = fieldsR[1];
      var idvalue = AllData[i].short_name;
      document.getElementById(iddata).value = idvalue;
    } else if (SingleData == 'postal_code') {
      var iddata = fieldsR[2];
      var idvalue = AllData[i].long_name;
      document.getElementById(iddata).value = idvalue;
    } else if (SingleData != 'postal_code') {
      if ((i == AllData.length - 1)) {
        var pinCodeId = fieldsR[2];
        document.getElementById(pinCodeId).value = "";
      }
    }
  }
    $('#' + thisiddf).blur();
    $('#' + thisiddf + ' + strong').hide();

}

function ReplaceStreet(str, a, b, c, d) {
  str = str.replace(a, '');
  str = str.replace(a + ',', '');
  str = str.replace(b + ',', '');
  str = str.replace(c + ',', '');
  str = str.replace('USA', '');
  str = str.trim();
  str = str.replace(/[, ]+$/, '');

  if (d == '') {
    var f = '';
  } else {
    var f = ',' + d;
  }
  str = str + f;
  str = str.replace(/[, ]+$/, '');
  str = str.trim();
  return str;
}
