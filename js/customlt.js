$(document).ready(function(){

	// Diable Autofill

	$("input").attr("autocomplete", "off");

	$('.yes_btn').click(function(){ 
		$('.section-1').hide();
		$('.section-2').fadeIn();
	});

/*	$('.sorry_yes_btn').click(function(){
		$('.Sorry_Partner , .section-1').hide();
		$('.normal_wrapper , .section-2').fadeIn();

	});*/
	
	$('.gobackno').click(function(){
        $('.Sorry_Partner').hide();
        $('.normal_wrapper').fadeIn();
    });

	$('.no_btn').click(function(){
		$('.normal_wrapper').hide();
		$('.Sorry_Partner').fadeIn();
	});

	$('.section-2-content .eligible').click(function(){
		$('.section-2').hide();
		$('.section-3').fadeIn();
	});

	// $('.get-started').click(function(){
		// $('.section-3').hide();
		// $('.section-4').fadeIn();
		// $('.Progress_Status').html('20%').css({'width':'20%'});
		// $('.Progress_bar').show();
	// });

	$('.section-4-content a').click(function(){
		$('.section-4').hide();
		$('.section-5').fadeIn();
		$('.Progress_Status').html('25%').css({'width':'25%'});
	});

	$('.section-5-next').click(function(){
		$('.section-5').hide();
		$('.section-6').fadeIn();
		$('.Progress_Status').html('35%').css({'width':'35%'});
	});

	$('.section-6-next').click(function(){
		$('.section-6').hide();
		$('.section-7').fadeIn();
		$('.Progress_Status').html('45%').css({'width':'45%'});
	});
	$('.section-7-content a').click(function(){
		$('.section-7').hide();
		$('.section-8').fadeIn();
		$('.Progress_Status').html('50%').css({'width':'50%'});
	});
	$('.section-8-next').click(function(){
		$('.section-8').hide();
		$('.section-9').fadeIn();
		$('.Progress_Status').html('65%').css({'width':'65%'});
	});
	$('.section-9-next').click(function(){
		$('.section-9').hide();
		$('.section-10').fadeIn();
		$('.Progress_Status').html('70%').css({'width':'70%'});
	});
	$('.section-10-content a').click(function(){
		$('.section-10').hide();
		$('.section-11').fadeIn();
		$('.Progress_Status').html('75%').css({'width':'75%'});
	});

	$('.section-11-next').click(function(){
		$('.section-11').hide();
		$('.section-12').fadeIn();
		$('.Progress_Status').html('80%').css({'width':'80%'});
	});

	$('.section-12-next').click(function(){
		$('.section-12').hide();

		var homebased_val = document.getElementById("homebased").checked;

		if(homebased_val == true){
		    homebased = 'Yes';
		} else {
		    homebased = 'No';
		}


		console.log(homebased);
		if(homebased  == 'Yes'){
			$('.section-14').fadeIn();
			console.log('true');
		} else {
			$('.section-13').fadeIn();
			console.log('false');
		}

		$('.Progress_Status').html('85%').css({'width':'85%'});
	});
	$('.section-13-next').click(function(){
		$('.section-13').hide();
		$('.section-14').fadeIn();
		$('.Progress_Status').html('90%').css({'width':'90%'});
	});
	$('.section-14-next').click(function(){

        $('.section-14').hide();
        var window_width = $(window).width();

        console.log(window_width);

        if( window_width <= 991 ){
	    	$('.section-16_for_mob').fadeIn();
        } else {
            $('.section-15').fadeIn();
        }


		$('.Progress_Status').html('100%').css({'width':'100%'});
	});

	$('.section-15-next').click(function(){
		$('.section-15, .section-4').hide();
		$('.section-16').fadeIn();
		$('.Progress_Status').html('100%').css({'width':'100%'});
	});
	$('.sp-section-1-btn').click(function(){
        $('.sp-section-1').hide();
        $('.sp-section-2').fadeIn();
        $('#Amount-requested').val($('#borrow').val());
    });
    $('.sp-section-2-btn').click(function(){
        $('.sp-section-2').hide();
        $('.sp-section-3').fadeIn();
    });
    $('.sp-section-3-btn').click(function(){
        $('.sp-section-3').hide();
        $('.sp-section-4').fadeIn();
    });
    $('.sp-section-4-btn').click(function(){
        $('.sp-section-4').hide();
        $('.sp-section-5').fadeIn();
    });


});

var Step_Number;

// Warning while clicking on the back in Browser

window.onbeforeunload = function() {
	if (Step_Number == 'p4') { return void(0);}
	return "You will loose all the information if you leave this page.";
};


$("#fileupload1").change(function () {

	if($(this).val != ''){
		var fileExtension = ['pdf'];
		$('.fileupload1').addClass('added');
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			//alert("Only formats are allowed : "+fileExtension.join(', '));
			alert("Only 'Pdf' format is allowed");
			$('.fileupload1').removeClass('added');
			$('#fileupload1').val('');
			$('.section-15-next').addClass('btn_disable');
		}
		
	}else {
	$('.fileupload1').removeClass('added');
	}
});

$("#fileupload2").change(function () {

	if($(this).val != ''){
		var fileExtension = ['pdf'];
		$('.fileupload2').addClass('added');
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
			// alert("Only formats are allowed : "+fileExtension.join(', '));
			alert("Only 'pdf' format is allowed");
			$('.fileupload2').removeClass('added');
			$('#fileupload2').val('');
			$('.section-15-next').addClass('btn_disable');
		}
		
	}else {
		$('.fileupload2').removeClass('added');
	}
});

$("#fileupload3").change(function () {

	if($(this).val != ''){
		var fileExtension = ['pdf'];
		$('.fileupload3').addClass('added');
		if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
		// alert("Only formats are allowed : "+fileExtension.join(', '));
		alert("Only 'pdf' format is allowed");
		$('.fileupload3').removeClass('added');
		$('#fileupload3').val('');
		$('.section-15-next').addClass('btn_disable');
		}
		
	}else {
		$('.fileupload3').removeClass('added');
	}
});



var utm_source;
var utm_medium;
var utm_campaign;
var utm_term;
var utm_content;
var user_ip;



$.getJSON("https://jsonip.com/?callback=?", function (data) {
        //console.log(data);
        user_ip = data.ip;
});

if(user_device == 'NotMobile'){
   var device_width = $(window).width();
   if(device_width <= 991){
       var user_device = 'Tablet'
   } if (device_width >= 992){
       var user_device = 'Desktop'
   }
}
//alert(user_device);


function getSearchParameters() {
      var prmstr = window.location.search.substr(1);
      return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {} ;
}

function transformToAssocArray( prmstr ) {
    var params = {};
    var prmarr = prmstr.split("&");
    for ( var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }
    return params;
}


function populateSfFields( params ){
    var Result = getSearchParameters();
    console.log(Result);

    if (Result.utm_source) {
        utm_source = Result.utm_source;
    }
    if (Result.utm_medium) {
        utm_medium = Result.utm_medium;
    }
    if (Result.utm_campaign) {
        utm_campaign = Result.utm_campaign;
    }
    if (Result.utm_term) {
        utm_term = Result.utm_term;
    }
    if (Result.utm_content) {
        utm_content = Result.utm_content;
    }
}

populateSfFields();




$('.eligible').click(function(){
     Monthly_Revenue = $(this).attr('value');
});
    $('.Business_Entity').click(function(){
     Business_Entity = $(this).attr('value');
});
    $('.Gender_select').click(function(){
     Gender_select = $(this).attr('value');
});

$('#dob_Date').change(function(){
     dob_Date = $(this).val();
});

$('#dob_Month').change(function(){
     dob_Month = $(this).val();
});

$('#dob_Year').change(function(){
     dob_Year = $(this).val();
});
$('#bs_Date').change(function(){
     bs_dob_Date = $(this).val();
});

$('#bs_Month').change(function(){
     bs_dob_Month = $(this).val();
});

$('#bs_Year').change(function(){
     bs_dob_Year = $(this).val();
});
$('#Birthday2_dd').change(function(){
     Birthday2_dd = $(this).val();
     console.log(Birthday2_dd);
});

$('#Birthday2_mm').change(function(){
     Birthday2_mm = $(this).val();
     console.log(Birthday2_mm);
});

$('#Birthday2_yy').change(function(){
     Birthday2_yy = $(this).val();
     console.log(Birthday2_yy);
});
$('#State').change(function(){
     State = $(this).val();
});
$('#Owner1_State').change(function(){
     Owner1_State = $(this).val();
});
$('#Ownership_percentage').change(function(){
     Ownership_percentage = $(this).val();
});
$('.Office_Space').click(function(){
     Office_Space = $(this).attr('value');
});
$('#industry').change(function(){
     industry = $(this).val();
});
$('#State_Incorporation').change(function(){
     State_Incorporation = $(this).val();
});

var $regexname = /^([a-zA-Z' ]{1,})$/;
var $regexemail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

var $regphone = /^\(\d{3}\) \d{3}-\d{4}$/;
var $regzip = /^\d{5}$/;
//var $regssn_tax = /^\d{9}$/;
var $regssn_tax = /^[0-9 -]+$/;
var $regexURL = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/;

var borrow , email2;

// $('#first_name , #last_name , #phone , #legal-one, #email').on('keyup change blur input',function(){

//     var legalone = document.getElementById("legal-one").checked ;
// 	email = $("#email").val().trim();

//      if ($('#first_name').val().match($regexname) && $('#last_name').val().match($regexname) && $('#phone').val().match($regphone) && legalone == true && email.match($regexemail)) {
// 	$('.get-started').removeClass('btn_disable');
//      }
//    else{
//         $('.get-started').addClass('btn_disable');
//        }
//  });


$('#Legal_Name ').on('keyup blur input',function(){
	if ( $('#Legal_Name').val().length >= 3){
		$('.section-5-next').removeClass('btn_disable');
	}else {
		$('.section-5-next').addClass('btn_disable');
	}
});

// $('#Street , #City , #State , #PostalCode').on('keyup change blur input',function(){

// 	if ($('#Street').val().length >= 3 && $('#City').val().length >= 3 && $('#State').val() != "" && $('#PostalCode').val().match($regzip)){
// 		$('.section-6-next').removeClass('btn_disable');
// 	}else {
// 		$('.section-6-next').addClass('btn_disable');
// 	}
// });

// $('#Street').on('blur input',function(){
// 	if($('#Street').val().length >= 3){
// 		$('.section-6-next').removeClass('btn_disable');
// 	}else {
// 		$('.section-6-next').addClass('btn_disable');
// 	}
// });
// $('#Street').on('keyup',function(){
// 	$('.section-6-next').addClass('btn_disable');
// });

 $('#dob_Date , #dob_Month , #dob_Year').on('change',function(){

	if ($('#dob_Date').val() != '' && $('#dob_Month').val() != '' && $('#dob_Year').val() != ''){
		$('.section-8-next').removeClass('btn_disable');
	}else {
		$('.section-8-next').addClass('btn_disable');
	}
});

$('#Ownership_percentage').on('change', function(){
      if($('#Ownership_percentage').val() != ''){
		$('.section-9-next').removeClass('btn_disable');
	}else {
		$('.section-9-next').addClass('btn_disable');
	}
});

$('#industry').on('change', function(){
      if($('#industry').val() != ''){
		$('.section-11-next').removeClass('btn_disable');
	}else {
		$('.section-11-next').addClass('btn_disable');
	}
});
 $('#bs_Date , #bs_Month , #bs_Year').on('change',function(){

	if ($('#bs_Date').val() != '' && $('#bs_Month').val() != '' && $('#bs_Year').val() != ''){
		$('.section-12-next').removeClass('btn_disable');
	}else {
		$('.section-12-next').addClass('btn_disable');
	}
});

// $('#Owner1_Street, #Owner1_City, #Owner1_State, #Owner1_Zip').on('keyup blur input',function(){

// 	if ($('#Owner1_Street').val().length >= 3 && $('#Owner1_City').val().length >= 3 && $('#Owner1_State').val() != '' && $('#Owner1_Zip').val().match($regzip)){
// 		$('.section-13-next').removeClass('btn_disable');
// 	}else {
// 		$('.section-13-next').addClass('btn_disable');
// 	}
// });

// $('#Owner1_Street').on('keyup blur input',function(){

// 	if ($('#Owner1_Street').val().length >= 3){
// 		$('.section-13-next').removeClass('btn_disable');
// 	}else {
// 		$('.section-13-next').addClass('btn_disable');
// 	}
// });


$('#State_Incorporation, #Tax_ID, #Social_Security_Number , #legal-two').on('change keyup blur input',function(){
	var legaltwo = document.getElementById("legal-two").checked ;

	if ($('#State_Incorporation').val() != '' && $('#Tax_ID').val().match($regssn_tax) && $('#Social_Security_Number').val().match($regssn_tax) && $('#Social_Security_Number').val().length == 11 && legaltwo == true){
		$('.section-14-next').removeClass('btn_disable');
	}else {
		$('.section-14-next').addClass('btn_disable');
	}
});


var num_arr = ['000000000','111111111','222222222','333333333','444444444','555555555', '666666666', '777777777', '888888888' , '999999999'];
 
 function checkValue(value,arr, ele){
  var status = 'Not exist';
 
  for(var i=0; i<arr.length; i++){
   var num = arr[i];
   if(num == value){
    status = 'Exist';
    
    var ssn_tag = '<p class="invalid_num_error ssn" >Please Enter valid SSN</p>'
    var taxid_tag = '<p class="invalid_num_error taxid" >Please Enter valid Tax ID</p>'

    if(ele == 'ssn'){
    	$('#Social_Security_Number').val('');
	    $('#Social_Security_Number').after(ssn_tag);
	    setTimeout(function(){
	    	$('.invalid_num_error.ssn').remove();
	    },3000);
	    break;
    } else {
    	$('#Tax_ID').val('');
	    $('#Tax_ID').after(taxid_tag);
	    setTimeout(function(){
	    	$('.invalid_num_error.taxid').remove();
	    },3000);
	    break;
    }

   }
  }

  return status;

 }

$('#Social_Security_Number').on('keyup',function(){
	$('.invalid_num_error.ssn').remove();
	var str = $('#Social_Security_Number').val();
	var newStr = str.replace(/-/g, "");
	var ele = 'ssn';
	if(newStr.length == 9){
		checkValue(newStr, num_arr,ele);
	}
});

$('#Tax_ID').on('keyup',function(){
	$('.invalid_num_error.taxid').remove();
	var str = $('#Tax_ID').val();
	var newStr = str.replace(/-/g, "");
	var ele = 'Tax_ID';
	if(newStr.length == 9){
		checkValue(newStr, num_arr,ele);
	}
});


$('#fileupload1, #fileupload2 ,#fileupload3').on('change',function(){

	var fileupload1 = $("#fileupload1").val();
	var fileupload2 = $("#fileupload2").val();
	var fileupload3 = $("#fileupload3").val();


	if (fileupload1 != ''  && fileupload2 != '' && fileupload3 != ''){
		$('.section-15-next').removeClass('btn_disable');
	}else {
		$('.section-15-next').addClass('btn_disable');
	}
});

$('#borrow').change(function(){
    borrow = $(this).val();
    toUSD(borrow);
    $('#Amount-requested').val(borrow);
});
$('#borrow , #email2 ').on('keyup change blur input',function(){

    email2 = $("#email2").val().trim();
    console.log(email2);

    if ($('#borrow').val() != '' && $('#email2').val().match($regexemail) ) {
        $('.sp-section-1-btn').removeClass('btn_disable');
        $('#email3').val(email2);
    }
    else{
        $('.sp-section-1-btn').addClass('btn_disable');
    }
});

// $('#Birthday2').click(function(){
//  function isDate18orMoreYearsOld(day, month, year) {
//      return new Date(year+18, month-1, day) <= new Date();
//  }
// });

// (function () {
//  var today = new Date();
//  var dd = today.getDate();
//  var mm = today.getMonth()+1; //January is 0!
//  var yyyy = today.getFullYear() - 18;
//   if(dd<10){
//          dd='0'+dd
//      } 
//      if(mm<10){
//          mm='0'+mm
//      } 

//  today = yyyy+'-'+mm+'-'+dd;
//  document.getElementById("Birthday2").setAttribute("max", today);
//  console.log("dedede");
// })();




// $("#Birthday2").on('keyup change blur input',function(){
//     var date_value = $("#Birthday2").val();
//     var date_array = date_value.split("-");

//     var year = date_array[0];

//     var mydate = new Date();
//     var current_year = mydate.getFullYear();
//     if ((current_year - year) > 18){
    
//         // alert("above 18");
//     }else{
//         alert("Needs to be at least 18 years old to continue");
//         $('#Birthday2').val('');
//     } 
// });


$('#Amount-requested , #Credit-type , #Loan-reason , #firstname2 ,#lastname2 , #Birthday2_mm , #Birthday2_dd , #Birthday2_yy ').on('keyup change blur input',function(){

    var Amountrequested = $("#Amount-requested").val().trim();
    var firstname2 = $("#firstname2").val().trim();
    var lastname2 = $("#lastname2").val().trim();
    var Birthday2_mm = $('#Birthday2_mm').val();
	var Birthday2_yy = $('#Birthday2_yy').val();
	var Birthday2_dd = $('#Birthday2_dd').val();


    if (Amountrequested != '' && $('#Credit-type').val() != '' &&  $('#Loan-reason').val() != ''  && firstname2.match($regexname) && lastname2.match($regexname) && Birthday2_mm != '' && Birthday2_dd != '' && Birthday2_yy != '' ) {


        $('.sp-section-2-btn').removeClass('btn_disable');
        console.log('ifde')
    }
    else{
        $('.sp-section-2-btn').addClass('btn_disable');
        console.log('else')
    }
});

$(' #sp-Phone , #Contact-time, #sp-Address , #sp-Zip-Code , #sp-State , #sp-city , #sp-length-address , .radio_ownhouse').on('keyup change blur input',function(){

// $('#email3 , #sp-Phone , #Contact-time, #sp-Address , #sp-Zip-Code, #sp-State').on('keyup change blur input',function(){


var email3 = $("#email3").val();
var spPhone = $("#sp-Phone").val().trim();
var Contact_time = $('#Contact-time').val();



var spAddress = $('#sp-Address').val().trim();
var sp_Zip_Code = $('#sp-Zip-Code').val().trim();
var sp_State = $('#sp-State').val();
var sp_city = $('#sp-city').val().trim();
var sp_length_address = $('#sp-length-address').val();
var radio_ownhouse = $('.radio_ownhouse:checked').val();
console.log(radio_ownhouse);


if ( spPhone.match($regphone) &&  Contact_time != ''  && spAddress.length >= 10 && sp_length_address.length != ''  && radio_ownhouse != undefined ) {

    // if (email3.match($regexemail) && spPhone.match($regphone) &&  Contact_time != ''  && spAddress.length >= 3 && sp_Zip_Code.length == 5) {

        $('.sp-section-3-btn').removeClass('btn_disable');
        console.log('ifde')
    }
    else{
        $('.sp-section-3-btn').addClass('btn_disable');
        console.log('else')
    }
});

$("#sp-Zip-Code").keydown(function(e){

    var key = e.which || e.charCode || e.keyCode || 0;

    return (key == 8 ||
        key == 9 ||
        key == 46 ||
        (key >= 48 && key <= 57) ||
        (key >= 96 && key <= 105));
});


$('#sp-Income-source , #sp-Timed-Employed , #sp-get-paid , #sp-Employer-name ,#sp-Employer-Phone , #sp-gross-income').on('keyup change blur input',function(){

    var sp_Income_source = $("#sp-Income-source").val();
    var sp_Timed_Employed = $("#sp-Timed-Employed").val();
    var sp_get_paid = $("#sp-get-paid").val();
    var sp_Employer_name = $("#sp-Employer-name").val();
    var sp_Employer_Phone = $("#sp-Employer-Phone").val();
    var sp_gross_income = $("#sp-gross-income").val().trim();



    if (sp_Income_source != '' && sp_Timed_Employed != '' &&  sp_get_paid != ''  && sp_Employer_name.match($regexname) && sp_Employer_Phone.match($regphone) && sp_gross_income != '') {


        $('.sp-section-4-btn').removeClass('btn_disable');
        console.log('ifde')
    }
    else{
        $('.sp-section-4-btn').addClass('btn_disable');
        console.log('else')
    }
});

$('#Driver-License , #sp-Issuing-State , #sp-ssn , #sp-Account-Type ,#legal-four').on('keyup change blur input',function(){

// $('#Driver-License ').on('keyup change blur input',function(){

    var sp_Driver_License = $("#Driver-License").val().trim();
    var sp_Issuing_State = $("#sp-Issuing-State").val();
    var sp_ssn = $("#sp-ssn").val();
    var sp_Account_Type = $("#sp-Account-Type").val();
    var legalfour = document.getElementById("legal-four").checked ;



    if (sp_Driver_License.length >= 3 && sp_Issuing_State != '' &&  sp_ssn.match($regssn_tax) && sp_Account_Type != '' && legalfour == true) {

        // if (sp_Driver_License.length >= 3 ) {


        $('.sp-section-5-btn').removeClass('btn_disable');
        console.log('ifde')
    }
    else{
        $('.sp-section-5-btn').addClass('btn_disable');
        console.log('else')
    }
});


var $regspecialCha =  /[ !@#$%^&*~()_+\-=\[\]{};':"\\|,. <>\/?]/;
var $regnum = /\d+/;

function ValidateFirstname(){
    var first_name = $("#first_name").val().trim();

    if(first_name.match($regnum) && first_name.match($regspecialCha)){
        $('.err_first_name_Num_Char').fadeIn();
        console.log("without err");
        return false;
    } 
    else if (first_name.match($regspecialCha)){
        $('.err_first_name_specialChar').fadeIn();
        console.log("without err");
        return false;
    } 
    else if (first_name.match($regnum)){
        $('.err_first_name_Num').fadeIn();
        console.log("without err");
        return false;
    } else if( first_name == ""){
    	$('.err_first_name_valid').fadeIn();
        console.log("without err");
        return false;
    }
     else{
        console.log("with err");
        $('.err_first_name').fadeIn();
        return true;
    }
}

$('#first_name').on('keyup' , function(){
    $('.err_first_name , .err_first_name_Num_Char , .err_first_name_specialChar , .err_first_name_Num , .err_first_name_valid').fadeOut();
});

function ValidateLastname(){
    var last_name = $("#last_name").val().trim();

    if(last_name.match($regnum) && last_name.match($regspecialCha) ){
        $('.err_last_name_Num_Char').fadeIn();
        console.log("without err");
        return false;
    } 
    else if (last_name.match($regspecialCha)){
        $('.err_last_name_specialChar').fadeIn();
        console.log("without err");
        return false;
    } 
    else if (last_name.match($regnum)){
        $('.err_last_name_Num').fadeIn();
        console.log("without err");
        return false;
    } else if( last_name == ""){
    	$('.err_last_name_valid').fadeIn();
        console.log("without err");
        return false;
    }
     else{
        console.log("with err");
        $('.err_last_name').fadeIn();
        return true;
    }
}

$('#last_name').on('keyup' , function(){
    $('.err_last_name_Num_Char , .err_last_name_specialChar , .err_last_name_Num , .err_last_name ,.err_last_name_valid').fadeOut();
});


function ValidateEmail(){
    var email = $("#email").val();
    if(email.match($regexemail)){
        $('.err_email').fadeOut();
        console.log("without err");
        return true;
    } else{
        console.log("with err");
        $('.err_email').fadeIn();
        return false;
    }
}

$('#email').on('keyup' , function(){
    $('.err_email').fadeOut();
});


function ValidatePhone(){
    var phone = $("#phone").val();
    if(phone.match($regphone)){
        $('.err_phone').fadeOut();
        console.log("without err");
        return true;
    } else{
        console.log("with err");
        $('.err_phone').fadeIn();
        return false;
    }
}

$('#phone').on('keyup' , function(){
    $('.err_phone').fadeOut();
});

function ValidateCheckOne(){
	var legalone = document.getElementById("legal-one").checked;

	if(legalone == true ){
		$('.err_legal_one').fadeOut();
		return true;
	} else {
		$('.err_legal_one').fadeIn();
		return false;
	}
}

$('.legal-checkbox').click(function(){
	$('.err_legal_one').fadeOut();
});

$(".get-started").click(function(){

email = $("#email").val().trim();

    var err  = 0;

    if( ValidateFirstname() == false){
        err++;
    }
    if( ValidateLastname() == false){
        err++;
    }
    if( ValidateEmail() == false){
        err++;
    }
    if( ValidatePhone() == false){
        err++;
    }
    if( ValidateCheckOne() == false){
        err++;
    }
    if(err > 0){
        return false;
    }
    

  var Step_Number = 1;

$('.Continue_upload_loading-files').fadeIn();

    $.ajax({
        type: "POST",
        url: "ajaxmobile.php",
        data:{
            'email': email,
            'Step_Number':Step_Number,
        },
        success: function(data){
            console.log('data');
            var data_json = JSON.parse(data);
            console.log(data_json);
            if(data_json.step == 0){
                AjaxSection();
            }
            if(data_json.step == 1){
                if(data_json.StepNumber == 8){
                    AjaxSection();
                } else {
                    $('.confirmation').show();
                    $('.Continue_upload_loading-files').hide();

                    $('.confirmation_no').click(function(){
                        $.ajax({
                            type: "POST",
                            url: "ajaxcontinueapp.php",
                            data:{
                                'email': email,
                            },
                            success: function(data){
                                $('.section_wrapper').fadeIn();
                                console.log(data);
                                Steps(data);

                                if(data == '8'){
                                    $('.section-16 , .section_wrapper').show();
                                    $('.Progress_Status').html('100%').css({'width':'100%'});
                                }

                                $('.confirmation , .hero_container').hide();
                            }
                        });
                    });
                }
                $('.confirmation_yes').click(function(){
                     AjaxSection();
                });
            }
        }
    });
});
/*$('.gobackno').click(function(){
	var Step_Number = 0.1;
	$.ajax({
		type: "POST",
		url: "lendingtreeajax.php",
		data:{	
			//'own_a_business' : 'No',
			'Step_Number' : Step_Number
		},
		success: function(data){
			console.log(data);
		}
	});
});*/
$('.no_btn ').click(function(){
	var Step_Number = "zero";

	$.ajax({
		type: "POST",
		url: "lendingtreeajax.php",
		data:{
			
			'own_a_business':'No',
			'utm_source':utm_source,
			'utm_medium':utm_medium,
			'utm_campaign':utm_campaign,
			'utm_term': utm_term,
			'utm_content':utm_content,
			'Step_Number':Step_Number,
			'user_ip':user_ip,
			'user_browser' : user_browser,
			'user_device' : user_device,
			'fc_campaign':$('#fc_campaign').val(),
			'fc_channel':$('#fc_channel').val(),
			'fc_content':$('#fc_content').val(),
			'fc_landing':$('#fc_landing').val(),
			'fc_medium':$('#fc_medium').val(),
			'fc_referrer':$('#fc_referrer').val(),
			'fc_source':$('#fc_source').val(),
			'fc_term':$('#fc_term').val(),
			'lc_campaign':$('#lc_campaign').val(),
			'lc_channel':$('#lc_channel').val(),
			'lc_content':$('#lc_content').val(),
			'lc_landing':$('#lc_landing').val(),
			'lc_medium':$('#lc_medium').val(),
			'lc_referrer':$('#lc_referrer').val(),
			'lc_source':$('#lc_source').val(),
			'lc_term':$('#lc_term').val(),
			'OS':$('#OS').val(),
			'GA_Client_ID':$('#GA_Client_ID').val(),
			'all_traffic_sources':$('#all_traffic_sources').val(),
			'browser':$('#browser').val(),
			'city':$('#city').val(),
			'country':$('#country').val(),
			'device':$('#device').val(),
			'page_visits':$('#page_visits').val(),
			'pages_visited_list':$('#pages_visited_list').val(),
			'region':$('#region').val(),
			'time_zone':$('#time_zone').val(),
			'time_passed':$('#time_passed').val(),
			'latitude':$('#latitude').val(),
			'longitude':$('#longitude').val(),
		},
		success: function(data){
			console.log(data);
		}
	});
});

function AjaxSection(){

    var Step_Number = 1;

    $('.Continue_upload_loading-files , .confirmation').hide();
    $('.section-3 , .section').hide();
    $('.section-4').fadeIn();
    $('.Progress_Status').html('20%').css({'width':'20%'});
    $('.Progress_bar').show();
    $('.modal-wrapper, .section-1 , .capital_section  , .how_it-_works , .review_section , .small_form_section , .footer , .hero_container , .badgets').fadeOut();
    $('body').css('overflow','');
    $('.section-4 , .section_wrapper').fadeIn();
    $.ajax({
        type: "POST",
        url: "lendingtreeajax.php",
        data:{
            'first_name': $('#first_name').val(),
            'phone': $('#phone').val(),
            'email': $('#email').val(),
            'last_name': $('#last_name').val(),
            'Monthly_Revenue': Monthly_Revenue,
            'utm_source':utm_source,
            'utm_medium':utm_medium,
            'utm_campaign':utm_campaign,
            'utm_term': utm_term,
            'utm_content':utm_content,
            'Step_Number':Step_Number,
            'user_ip':user_ip,
            'user_browser' : user_browser,
            'user_device' : user_device,
            'fc_campaign':$('#fc_campaign').val(),
			'fc_channel':$('#fc_channel').val(),
			'fc_content':$('#fc_content').val(),
			'fc_landing':$('#fc_landing').val(),
			'fc_medium':$('#fc_medium').val(),
			'fc_referrer':$('#fc_referrer').val(),
			'fc_source':$('#fc_source').val(),
			'fc_term':$('#fc_term').val(),
			'lc_campaign':$('#lc_campaign').val(),
			'lc_channel':$('#lc_channel').val(),
			'lc_content':$('#lc_content').val(),
			'lc_landing':$('#lc_landing').val(),
			'lc_medium':$('#lc_medium').val(),
			'lc_referrer':$('#lc_referrer').val(),
			'lc_source':$('#lc_source').val(),
			'lc_term':$('#lc_term').val(),
			'OS':$('#OS').val(),
			'GA_Client_ID':$('#GA_Client_ID').val(),
			'all_traffic_sources':$('#all_traffic_sources').val(),
			'browser':$('#browser').val(),
			'city':$('#city').val(),
			'country':$('#country').val(),
			'device':$('#device').val(),
			'page_visits':$('#page_visits').val(),
			'pages_visited_list':$('#pages_visited_list').val(),
			'region':$('#region').val(),
			'time_zone':$('#time_zone').val(),
			'time_passed':$('#time_passed').val(),
			'latitude':$('#latitude').val(),
			'longitude':$('#longitude').val(),
        },
        success: function(data){
            console.log(data);
        }
    });
}

$(".section-5-next").click(function(){

	var Step_Number = 2;
	console.log(Business_Entity);
	$.ajax({
		type: "POST",
		url: "lendingtreeajax.php",

		data:{'Website':$("#URL").val(),'Legal_Name':$('#Legal_Name').val(),'Doing_Business':$('#Doing_Business').val(), 'Step_Number':Step_Number, 'Business_Entity':Business_Entity},
		success: function(data){
			console.log(data);
		}
	});
});

$(".section-6-next").click(function(){

	if(document.getElementById("homebased").checked == true){
	    homebased = 'Yes';
	} else {
	    homebased = 'No';
	}
	var Step_Number = 3;
	//var streetA=$('#AptSuite').val()+$('#Street').val();
	var streetA=ReplaceStreet($('#Street').val(),$('#PostalCode').val(),$('#State').val(),$('#City').val(),$('#AptSuite').val());

	//console.log(streetA);
	//console.log({'Street':streetA,'City':$('#City').val(),'State':$('#State').val(),'PostalCode':$('#PostalCode').val(), 'Step_Number':Step_Number,'homebased' : homebased});
	$.ajax({
	 type: "POST",
	 url: "lendingtreeajax.php",
	 data:{'Street':streetA,'City':$('#City').val(),'State':$('#State').val(),'PostalCode':$('#PostalCode').val(), 'Step_Number':Step_Number,'homebased' : homebased},
	 success: function(data){
	 	console.log(data);
	 }
	});
});


$(".section-9-next").click(function(){

	var dob = dob_Year+"-"+dob_Month+"-"+dob_Date+ "";
	var Step_Number = 4;

	$.ajax({
		type: "POST",
		url: "lendingtreeajax.php",
		data:{'Gender_select':Gender_select,'dob':dob,'Ownership_percentage':Ownership_percentage,'Step_Number':Step_Number},
		success: function(data){
			console.log(data);
		}
	});
});

$(".section-12-next").click(function(){
	var bs_dob = bs_dob_Year+"-"+bs_dob_Month+"-"+bs_dob_Date+ "";
	var Step_Number = 5;
	$.ajax({
		type: "POST",
		url: "lendingtreeajax.php",
		data:{'Office_Space':Office_Space,'bs_dob': bs_dob ,'industry':industry,'Step_Number':Step_Number},
		success: function(data){
			console.log(data);
		}
	});
});

$(".section-13-next").click(function(){

	var Step_Number = 6;
	var streetB=ReplaceStreet($('#Owner1_Street').val(),$('#Owner1_Zip').val(),$('#Owner1_State').val(),$('#Owner1_City').val(),$('#AptSuite2').val());

	$.ajax({
		type: "POST",
		url: "lendingtreeajax.php",
		data:{'Owner1_Street':streetB,'Owner1_City':$('#Owner1_City').val(),'Owner1_State':$('#Owner1_State').val(),'Owner1_Zip':$('#Owner1_Zip').val(),'Step_Number':Step_Number},
		success: function(data){
			console.log(data);
		}
	});
});

$(".section-14-next").click(function(){

	var Step_Number = 7;
	$.ajax({
		type: "POST",
		url: "lendingtreeajax.php",
		data:{'State_Incorporation':State_Incorporation,'Tax_ID':$('#Tax_ID').val(),'Social_Security_Number':$('#Social_Security_Number').val(),'Step_Number':Step_Number},
		success: function(data){
			console.log(data);
		}
	});
});

$(".section-15-next").click(function(){
	$('.Continue_upload_loading-files').fadeIn();
	var Step_Number = 8;
	var formData = new FormData(document.querySelector("form"));
	console.log(formData);
	// $.ajax({
	// 	url: "amazons3/amazons3/public/aws.php",
	// 	type: "POST",
	// 	data: formData,
	// 	processData: false,
	// 	contentType: false,
	// 	success: function(result){
	// 		console.log(result);
	// 		$('.Continue_upload_loading-files').fadeOut();
	// 	}
	// });

	$.ajax({
		type: "POST",
		url: "lendingtreeajax.php",
		//data:{'Step_Number':Step_Number,'formData':formData},
		data: formData,
		processData: false,
		contentType: false,
		success: function(data){
			console.log(data);
			$('.Continue_upload_loading-files').fadeOut();
		}
	});
});
$(".sp-section-1-btn").click(function(){

    var Step_Number = 'p0';
    console.log(Step_Number);
    console.log('loan amount');
    $.ajax({
        type: "POST",
        url: "lendingtreeajax.php",
        data:{
            'like_to_borrow':$("#borrow").val(),
            'Step_Number':Step_Number, 
            'email':$("#email2").val(),
            'utm_source':utm_source,
            'utm_medium':utm_medium,
            'utm_campaign':utm_campaign,
            'utm_term': utm_term,
            'utm_content':utm_content,
            'user_ip':user_ip,
            'user_browser' : user_browser,
            'user_device' : user_device,
            'fc_campaign':$('#fc_campaign').val(),
            'fc_channel':$('#fc_channel').val(),
            'fc_content':$('#fc_content').val(),
            'fc_landing':$('#fc_landing').val(),
            'fc_medium':$('#fc_medium').val(),
            'fc_referrer':$('#fc_referrer').val(),
            'fc_source':$('#fc_source').val(),
            'fc_term':$('#fc_term').val(),
            'lc_campaign':$('#lc_campaign').val(),
            'lc_channel':$('#lc_channel').val(),
            'lc_content':$('#lc_content').val(),
            'lc_landing':$('#lc_landing').val(),
            'lc_medium':$('#lc_medium').val(),
            'lc_referrer':$('#lc_referrer').val(),
            'lc_source':$('#lc_source').val(),
            'lc_term':$('#lc_term').val(),
            'OS':$('#OS').val(),
            'GA_Client_ID':$('#GA_Client_ID').val(),
            'all_traffic_sources':$('#all_traffic_sources').val(),
            'browser':$('#browser').val(),
            'city':$('#city').val(),
            'country':$('#country').val(),
            'device':$('#device').val(),
            'page_visits':$('#page_visits').val(),
            'pages_visited_list':$('#pages_visited_list').val(),
            'region':$('#region').val(),
            'time_zone':$('#time_zone').val(),
            'time_passed':$('#time_passed').val(),
            'latitude':$('#latitude').val(),
            'longitude':$('#longitude').val()},
        success: function(data){
            console.log(data);
        }
    });
});

$(".sp-section-2-btn").click(function(){

	var sp_dob = Birthday2_mm +"-"+ Birthday2_dd +"-"+ Birthday2_yy  + "";
    console.log(sp_dob);     

    var Step_Number = 'p1';
    console.log(Step_Number);
    console.log('loan amount');
    $.ajax({
        type: "POST",
        url: "lendingtreeajax.php",
        data:{
            'amount_requested':$("#Amount-requested").val(),
            'Credit_type':$('#Credit-type').val(),
            'Loan_Reason':$('#Loan-reason').val(),
            'Step_Number':Step_Number, 
            'First_Name':$("#firstname2").val(),
            'Last_Name':$("#lastname2").val(),
            'DOB': sp_dob,
            'Military':$(".radio_Military:checked").val(),
            },
        success: function(data){
            console.log(data);
        }
    });
});

$(".sp-section-3-btn").click(function(){
	console.log($('.radio_ownhouse:checked').val());
    var Step_Number = 'p2';
    console.log(Step_Number);
    console.log('step 2');
    var street=ReplaceStreet($('#sp-Address').val(),$('#sp-Zip-Code').val(),$('#sp-State').val(),$('#sp-city').val(),$('#AptSuite3').val());
    $.ajax({
        type: "POST",
        url: "lendingtreeajax.php",
        data:{
            'phone':$("#sp-Phone").val(),
            'Contact_time':$('#Contact-time').val(),
            'Address':street,
            'Step_Number':Step_Number, 
            'zip_code':$('#sp-Zip-Code').val(),
            'state':$('#sp-State').val(),
            'city':$('#sp-city').val(),
            'length_address':$('#sp-length-address').val(),
            'own_house':$('.radio_ownhouse:checked').val()
            },
        success: function(data){
            console.log(data);
        }
    });
});
$(".sp-section-4-btn").click(function(){

    var Step_Number = 'p3';
    console.log(Step_Number);
    console.log('step 3');
    $.ajax({
        type: "POST",
        url: "lendingtreeajax.php",
        data:{
            'Income_source':$("#sp-Income-source").val(),
            'Timed_Employed':$("#sp-Timed-Employed").val(),
            'get_paid':$("#sp-get-paid").val(),
            'Employer_name':$("#sp-Employer-name").val(),
            'Employer_Phone':$("#sp-Employer-Phone").val(),
            'gross_income':$("#sp-gross-income").val(),
            'Step_Number':Step_Number
            },
        success: function(data){
            console.log(data);
        }
    });
});

$(".sp-section-5-btn").click(function(){

    Step_Number = 'p4';
    //console.log(Step_Number);
    //console.log('step 4');
    $.ajax({
        type: "POST",
        url: "lendingtreeajax.php",
        data:{
            'Driver_License':$("#Driver-License").val(),
            'Issuing_State':$("#sp-Issuing-State").val(),
            'ssn':$("#sp-ssn").val(),
            'Account_Type':$("#sp-Account-Type").val(),
            'Step_Number':Step_Number
            },
        success: function(data){
            console.log(data);
            if (data == 'email found') {
                $('.sp-section-5').hide();
                $('.no_funding').fadeIn();   
            }else{
                $('.sp-section-5').hide();
                $('.sp-section-6').fadeIn();
                if(data=="sendtopersonalapi" || data==""){
                	$("#PLspinner").hide();
                }else{
                	setTimeout(function () {
	                   location.href=data;
	                }, 3000);	
                }
                
            }
            
            
            //window.open(data,'_blank');
            //document.getElementById('linktopersonal').innerHTML = '<a href="' + data + '" target="_blank">Click Here to get personal loans</a>';
        }
    });
});

function toUSD(number) {
    var number = number.toString(), 
    dollars = number.split('.')[0], 
    cents = (number.split('.')[1] || '') +'00';
    dollars = dollars.split('').reverse().join('')
        .replace(/(\d{3}(?!$))/g, '$1,')
        .split('').reverse().join('');
    $('.borrow-result span').text('$' + dollars + '.' + cents.slice(0, 2));
}

$("#Amount-requested").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
         // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

$('#phone ,#sp-Phone , #sp-Employer-Phone')

.keydown(function (e) {
	var key = e.which || e.charCode || e.keyCode || 0;
	$phone = $(this);

	// Don't let them remove the starting '('
	if ($phone.val().length === 1 && (key === 8 || key === 46)) {
		$phone.val('(');
		return false;
	}
	// Reset if they highlight and type over first char.
	else if ($phone.val().charAt(0) !== '(') {
		$phone.val('('+String.fromCharCode(e.keyCode)+'');
	}

	// Auto-format- do not expose the mask as the user begins to type
	if (key !== 8 && key !== 9) {
		if ($phone.val().length === 4) {
			$phone.val($phone.val() + ')');
		}
		if ($phone.val().length === 5) {
			$phone.val($phone.val() + ' ');
		}
		if ($phone.val().length === 9) {
			$phone.val($phone.val() + '-');
		}
	}

	// Diable 0 & 1 at the beginning

	if($phone.val() == '('){
		if(key  == 48 || key == 49){
			return false;
		}
	}

	// Allow numeric (and tab, backspace, delete) keys only
	return (key == 8 ||
			key == 9 ||
			key == 46 ||
			(key >= 48 && key <= 57) ||
			(key >= 96 && key <= 105));



})

.bind('focus click', function () {
	$phone = $(this);

	if ($phone.val().length === 0) {
		$phone.val('(');
	}
	else {
		var val = $phone.val();
		$phone.val('').val(val); // Ensure cursor remains at the end
	}


})

.blur(function () {
	$phone = $(this);

	if ($phone.val() === '(') {
		$phone.val('');
	}
});


function mask(o, f) {
    setTimeout(function () {
        var v = f(o.value);
        if (v != o.value) {
            o.value = v;
        }
    }, 1);
}

function mssn(v) {
    var r = v.replace(/\D/g,"");

    if (r.length > 9) {
        r = r.replace(/^(\d\d\d)(\d{2})(\d{0,4}).*/,"$1-$2-$3");
		return r;
    }
    else if (r.length > 4) {
        r = r.replace(/^(\d\d\d)(\d{2})(\d{0,4}).*/,"$1-$2-$3");
    }
    else if (r.length > 2) {
        r = r.replace(/^(\d\d\d)(\d{0,3})/,"$1-$2");
    }
    else {
        r = r.replace(/^(\d*)/, "$1");
    }
    return r;
}

