// renewals landing page code
$('.section-renewal-landing .btn-yes').click(function() {
  $('.section-renewal-landing').hide();
  $('.section-renewal-dl-header').fadeIn();
  $('.section-15').fadeIn();
  $('#renewals-progress-bar').width('50%');

  setTimeout(function() {
    $('.renewals-progress__point-2').addClass('renewals-progress__point-active');
  }, 900);
});

$('.section-renewal-landing .btn-no').click(function() {
  $('.section-renewal-landing').hide();
  $('.section-renewal-ty-no').fadeIn();
  $('#renewals-progress-bar').width('100%');

  setTimeout(function() {
    $('.renewals-progress__point-2').addClass('renewals-progress__point-active');
  }, 400);
  setTimeout(function() {
    $('.renewals-progress__point-3').addClass('renewals-progress__point-active');
  }, 900);
});

/*Decision Logic starts*/

function WRCCodeAjax() {
  $.ajax({
    type: 'POST',
    url: '/DecisionLogic/decisionlogic.php',
    data: { requesttype: 'cookiere' },
    success: function(data) {
      console.log(data);
      if (data != 'deletecookies') {
        $('.bank-search').hide();
        $('#iframeRC').attr(
          'src',
          'https://widget.decisionlogic.com/Service.aspx?requestCode=' + data
        );
        $('#iframeRC').fadeIn();
        ToShowThankYouSection();
      }
    }
  });
}
function RCCodeAjax(ContentServiceId) {
  //$("#iframeRC").attr("src","https://widget.decisionlogic.com/Service.aspx?requestCode=RPJDCU");
  $('.bank-search').hide();
  $('#iframeRC').show();
  $('#backtosearch').hide();
  $('#iframeRC').hide();
  $('#loader-fourth').show();
  //$('.main-loader').fadeIn();

  $.ajax({
    type: 'POST',
    url: '/DecisionLogic/decisionlogic.php',
    data: { requesttype: 'createRC', ContentServiceId: ContentServiceId },
    success: function(data) {
      $('#loader-fourth').hide();
      $('#backtosearch').show();
      $('#iframeRC').show();
      ToShowThankYouSection();
      console.log(data);
      $('#iframeRC').attr(
        'src',
        'https://widget.decisionlogic.com/Service.aspx?requestCode=' + data
      );
      //$('.main-loader').fadeOut();
    }
  });
}

function ToShowThankYouSection() {
  var ToShowThankYou = setInterval(function() {
    $.ajax({
      type: 'POST',
      url: '/DecisionLogic/decisionlogic.php',
      data: { requesttype: 'checkVDL' },
      success: function(data) {
        data = JSON.parse(data);
        //console.log(data);
        if (data.GetReportDetailFromRequestCode7Result.IsLoginValid) {
          clearInterval(ToShowThankYou);
          $.ajax({
            type: 'POST',
            url: '/DecisionLogic/decisionlogic.php',
            data: { requesttype: 'GetData' },
            success: function(data) {
              console.log(data);
              if (data == 'transaction90DaysExceeded') {
                alert("Transactions in this account are older than 90 days. Please attach the bank statements with latest transactions.");
                $("button.dontFindBank").trigger("click");
              } else if (data != 'SingleAccount') {
                $('.section-15, .section-4, .section-renewal-dl-header').hide();
                $('.section-15a').fadeIn();
                $('.account_select').html(data);
              } else if (data.indexOf('account_select_empty') > -1) {

              } else {
                $.ajax({
                  type: 'POST',
                  url: 'ajaxcreateleadsf.php',
                  data: { Step_Number: 9 },
                  success: function(data) {
                    EndApp();
                  }
                });

              }
            }
          });
        }
      }
    });
  }, 2000);
}

function UseThisButtonWhenUserSelectAccount() {
  if ($('input[name=CHECKING]').is(':checked')) {
    $('.section-15a').hide();
    $('.renewals-progress').hide();
    $('.Continue_upload_loading-files').fadeIn();
    var AccountNbr = $('input[name=CHECKING]:checked').val();
    $.ajax({
      type: 'POST',
      url: '/DecisionLogic/decisionlogic.php',
      data: { requesttype: 'createALeadInSubmission'},
      success: function(data) {
        $.ajax({
          type: 'POST',
          url: '/DecisionLogic/decisionlogic.php',
          data: { requesttype: 'SelectedAccountTrans', AccountNbr: AccountNbr },
          success: function(data) {
            $('.renewals-progress').show();
            $('.Continue_upload_loading-files').fadeOut();
            $('.section-15a, .section-4').hide();
            $('.section-16').fadeIn();
            $('#renewals-progress-bar').width('100%');
            $('.renewals-progress__point-3').addClass('renewals-progress__point-active');
            $.ajax({
              type: 'POST',
              url: 'ajaxcreateleadsf.php',
              data: { Step_Number: 9 },
              success: function(data) {
                EndApp();
              }
            });
          }
        });
      }
    });
  } else {
      alert('Please Select Atleast One Account');
  }
}

$('#BankSearch').on('keyup', function() {
  var str1 = $(this)
    .val()
    .split(' ')
    .join('');
  str1 = str1.length;
  if (str1 > 2) {
    $('.onload-list , .search-msg').fadeOut();
    $('#BankSearchData , .spiner-wrap').fadeIn();
    $.ajax({
      type: 'POST',
      url: '/DecisionLogic/decisionlogic.php',
      data: { requesttype: 'SearchBanksByBankName', keywordBank: $(this).val() },
      success: function(data) {
        var data = JSON.parse(data);
        var SearchBankResp = data.SearchBanksByBankNameResult.BanksSearchResult;
        console.log(SearchBankResp);
        var sample = '<ul>';
        var sizeof = 12;
        if (SearchBankResp.length < 11) {
          sizeof = SearchBankResp.length;
        } else if (!SearchBankResp.length) {
          sizeof = 1;
        }
        console.log(sizeof);
        if (sizeof == 1) {
          sample +=
            "<li class='clearfix' onclick='RCCodeAjax(`" +
            SearchBankResp['ContentServiceId'] +
            "`)' > <img src='https://widget.decisionlogic.com/ImageHandler.ashx?ContentServiceId=" +
            SearchBankResp['ContentServiceId'] +
            "' /> " +
            SearchBankResp['ContentServiceDisplayName'] +
            '</li>';
        } else {
          for (var i = 0; i < sizeof; i++) {
            sample +=
              "<li class='clearfix' onclick='RCCodeAjax(`" +
              SearchBankResp[i]['ContentServiceId'] +
              "`)' > <img src='https://widget.decisionlogic.com/ImageHandler.ashx?ContentServiceId=" +
              SearchBankResp[i]['ContentServiceId'] +
              "' /> " +
              SearchBankResp[i]['ContentServiceDisplayName'] +
              '</li>';
            //sample+="<li onclick='RCCodeAjax(`"+SearchBankResp[i]+"`)' > "+SearchBankResp[i]['ContentServiceDisplayName']+"</li>";
          }
        }
        sample += '</ul>';

        $('.spiner-wrap').fadeOut();
        $('#BankSearchData').html(sample);
      }
    });
  } else {
    $('.onload-list').fadeIn();
    $('#BankSearchData')
      .fadeOut()
      .html('');
    $('.search-msg').fadeIn();

    setTimeout(function() {
      $('.search-msg').fadeOut();
    }, 5000);
  }
});

$('#backtosearch').click(function() {
  $('#iframeRC').hide();
  $('#backtosearch').hide();
  $('.bank-search').show();
});

$('.dontFindBank').click(function() {
  $('.Link_bank_wrapper,.section-renewal-dl-header').fadeOut();
  $('.Upload_wrap').fadeIn();
});

$('.FindBank').click(function() {
  $('.Link_bank_wrapper').fadeIn();
  $('.Upload_wrap').fadeOut();
});

function try_diff_acc() {
  $('.section-15a, .section-15 iframe').hide();
  $('.section-15, .bank-search').fadeIn();
}

/* Decision Logic Ends */


function EndApp(){
  $('.Continue_upload_loading-files').fadeOut();
  $('#renewals-progress-bar').width('100%');
  $('.renewals-progress__point-3').addClass('renewals-progress__point-active');
  $('.section-15, .section-4, .section-renewal-dl-header').hide();
  $('.section-16').fadeIn();
  $.ajax({
    type: 'POST',
    url: '/DecisionLogic/decisionlogic.php',
    data: { requesttype: 'merchantCompletedRenewalFlow',campaignInfo:campaignInfo},
    success: function(data) {
      console.log(data);
    }
  });

}

/* Step 8 Starts */

$('#fileupload1').change(function() {
  if ($(this).val != '') {
    var fileExtension = ['pdf'];
    $('.fileupload1').addClass('added');
    if (
      $.inArray(
        $(this)
          .val()
          .split('.')
          .pop()
          .toLowerCase(),
        fileExtension
      ) == -1
    ) {
      alert("Only 'Pdf' format is allowed");
      $('.fileupload1').removeClass('added');
      $('#fileupload1').val('');
      $('.section-15-next').addClass('btn_disable');
    }
  } else {
    $('.fileupload1').removeClass('added');
  }
});

$('#fileupload2').change(function() {
  if ($(this).val != '') {
    var fileExtension = ['pdf'];
    $('.fileupload2').addClass('added');
    if (
      $.inArray(
        $(this)
          .val()
          .split('.')
          .pop()
          .toLowerCase(),
        fileExtension
      ) == -1
    ) {
      alert("Only 'pdf' format is allowed");
      $('.fileupload2').removeClass('added');
      $('#fileupload2').val('');
      $('.section-15-next').addClass('btn_disable');
    }
  } else {
    $('.fileupload2').removeClass('added');
  }
});

$('#fileupload3').change(function() {
  if ($(this).val != '') {
    var fileExtension = ['pdf'];
    $('.fileupload3').addClass('added');
    if (
      $.inArray(
        $(this)
          .val()
          .split('.')
          .pop()
          .toLowerCase(),
        fileExtension
      ) == -1
    ) {
      alert("Only 'pdf' format is allowed");
      $('.fileupload3').removeClass('added');
      $('#fileupload3').val('');
      $('.section-15-next').addClass('btn_disable');
    }
  } else {
    $('.fileupload3').removeClass('added');
  }
});


$('#fileupload1, #fileupload2 ,#fileupload3').on('change', function() {
  var fileupload1 = $('#fileupload1').val();
  var fileupload2 = $('#fileupload2').val();
  var fileupload3 = $('#fileupload3').val();

  if (fileupload1 != '' && fileupload2 != '' && fileupload3 != '') {
    $('.section-15-next').removeClass('btn_disable');
  } else {
    $('.section-15-next').addClass('btn_disable');
  }
});

$('.section-15-next').click(function() {
  $('.Continue_upload_loading-files').fadeIn();
  var Step_Number = 8;
  var formData = new FormData(document.querySelector('#pdfaws'));

  $.ajax({
    type: 'POST',
    url: '/DecisionLogic/decisionlogic.php',
    data: { requesttype: 'createALeadInSubmission'},
    success: function(data) {
      $.ajax({
        url: 'amazons3/amazons3/public/aws.php',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function(result) {
          EndApp();
          $.ajax({
            type: 'POST',
            url: 'ajaxcreateleadsf.php',
            data: {
              Step_Number: Step_Number
            },
            success: function(data) {}
          });
        }
      });
    }
  });
});

/* Step 8 Ends */
