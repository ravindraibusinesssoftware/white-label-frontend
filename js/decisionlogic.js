/*Decision Logic starts*/

function WRCCodeAjax() {
  $.ajax({
    type: 'POST',
    url: decisionlogicpath + 'DecisionLogic/decisionlogic.php',
    data: { requesttype: 'cookiere' },
    success: function (data) {
      if (data != 'deletecookies') {
        $('.bank-search').hide();
        $('#iframeRC').attr(
          'src',
          'https://widget.decisionlogic.com/Service.aspx?requestCode=' + data
        );
        $('#iframeRC').fadeIn();
        ToShowThankYouSection();
      }
    }
  });
}

function WRCCodeAjax_second() {
  $.ajax({
    type: 'POST',
    url: decisionlogicpath + 'DecisionLogic/decisionlogic.php',
    data: { requesttype: 'cookiere' },
    success: function (data) {
      if (data != 'deletecookies') {
        $('.bank-search').hide();
        $('#iframeRC_second').attr(
          'src',
          'https://widget.decisionlogic.com/Service.aspx?requestCode=' + data
        );
        $('#iframeRC_second').fadeIn();
        ToShowThankYouSectionSecond();
      }
    }
  });
}

function RCCodeAjax_second(ContentServiceId, bankType) {
  $('.bank-search').hide();
  //$('#iframeRC_second').show();
  $('#backtosearch_second').hide();
  //$('.main-loader').fadeIn();
  $('#iframeRC_second').hide();
  $(".Link_bank_wrapper").css("background-color", "#fff");
  $('#loader_second').show();

  $.ajax({
    type: 'POST',
    url: decisionlogicpath + 'DecisionLogic/decisionlogicToSF.php',
    data: {
      requesttype: 'createRC',
      ContentServiceId: ContentServiceId,
      oppId: oppId
    },
    success: function (requestId) {
      $('#loader_second').hide();
      $(".Link_bank_wrapper").css("background-color", "#0387cf");
      $('#backtosearch_second').show();
      ToShowThankYouSectionSecond(requestId, bankType);
      $('#iframeRC_second').attr('src', '');
      $('#iframeRC_second').attr(
        'src',
        'https://widget.decisionlogic.com/Service.aspx?requestCode=' + requestId
      );
      $('#iframeRC_second').show();
      //$('.main-loader').fadeOut();
    }
  });
}

function RCCodeAjax(ContentServiceId, bankType) {
  $('.bank-search').hide();
  //$('#iframeRC').show();
  $('#backtosearch').hide();
  //$('.main-loader').fadeIn();
  $('#iframeRC').hide();
  $(".Link_bank_wrapper").css("background-color", "#fff");
  $('#loader').show();

  $.ajax({
    type: 'POST',
    url: decisionlogicpath + 'DecisionLogic/decisionlogicToSF.php',
    data: {
      requesttype: 'createRC',
      ContentServiceId: ContentServiceId,
      oppId: oppId
    },
    success: function (requestId) {
      $('#loader').hide();
      $(".Link_bank_wrapper").css("background-color", "#0387cf");
      $('#backtosearch').show();
      ToShowThankYouSection(requestId, bankType);
      $('#iframeRC').attr('src', '');
      $('#iframeRC').attr(
        'src',
        'https://widget.decisionlogic.com/Service.aspx?requestCode=' + requestId
      );
      $('#iframeRC').show();
      //$('.main-loader').fadeOut();
    }
  });
}

function ToShowThankYouSection(requestId, bankType) {
  var ToShowThankYou = setInterval(function () {
    $.ajax({
      type: 'POST',
      url: decisionlogicpath + 'DecisionLogic/decisionlogic.php',
      data: { requesttype: 'checkVDL' },
      success: function (data) {
        data = JSON.parse(data);
        var isRunning = false;
        if (data.GetReportDetailFromRequestCode7Result.IsLoginValid) {
          clearInterval(ToShowThankYou);
          if (isRunning) return;
          isRunning = true;
          $.ajax({
            type: 'POST',
            url: decisionlogicpath + 'DecisionLogic/decisionlogicToSF.php',
            data: {
              requesttype: 'GetData',
              requestCode: requestId,
              oppId: oppId,
              bankType: bankType
            },
            success: function (data) {
              if (data != 'SingleAccount') {
                $('.section-15, .section-4').hide();
                $('.section-15a').fadeIn();
                $('.account_select').html(data);
              } else if (data == '<div class="account_select_empty text-center"> <h3>Error, No Checking Account Found.</h3> <p>Sorry, we were unable to find a Business Checking account. Please connect to another account.</p> <button class="account_select_btn" onclick="try_diff_acc_second()">Try Different Account</button> </div>";') {
                $('.section-15-dl2').hide(); // Hiding all the second account related HTML structure
              } else if (secondaryAccountNumberExists) {
                secondaryAccountNumberExists = false;
                $('.section-15').hide();
                $('.section-15-dl2').show();
                $('.section-15-dl2 .bank-search').show();
                $('.section-15-dl2 .onload-list').show();
                $('#iframeRC_second').hide(); //TO hide Iframe of second account
                $('#BankSearchData_second').hide();
              } else {
                location.replace('./call-verification.php?oppId=' + oppId);
              }
            },
            error: function (error) {
              console.log(error);
              isRunning = false;
            }
          });
        }
      }
    });
  }, 2000);
}

function try_diff_acc() {
  $(".account_select").hide();
  $(".section-15").show();
  $(".onload-list").show();
  $("#BankSearch").val("");
  $("iframe").hide();
  $(".bank-search").show();
  $("#backtosearch").hide();
  $(".BankSearchData-wrap").hide();
}

function try_diff_acc_second() {
  $(".account_select_second").hide();
  $(".section-15-dl2").show();
  $(".onload-list").show();
  $("#BankSearch_second").val("");
  $("iframe_second").hide();
  $(".bank-search").show();
  $("#backtosearch_second").hide();
  $(".BankSearchData-wrap").hide();
}

function ToShowThankYouSectionSecond(requestId, bankType) {
  var ToShowThankYou = setInterval(function () {
    $.ajax({
      type: 'POST',
      url: decisionlogicpath + 'DecisionLogic/decisionlogic.php',
      data: { requesttype: 'checkVDL' },
      success: function (data) {
        data = JSON.parse(data);
        if (data.GetReportDetailFromRequestCode7Result.IsLoginValid) {
          clearInterval(ToShowThankYou);
          $.ajax({
            type: 'POST',
            url: decisionlogicpath + 'DecisionLogic/decisionlogicToSF.php',
            data: {
              requesttype: 'GetData',
              requestCode: requestId,
              oppId: oppId,
              bankType: bankType
            },
            success: function (data) {
              if (data != 'SingleAccount') {
                $('.section-15, .section-4').hide();
                $('.section-15a').fadeIn();
                $('.account_select_second').html(data);
              } else if (data == '<div class="account_select_empty text-center"> <h3>Error, No Checking Account Found.</h3> <p>Sorry, we were unable to find a Business Checking account. Please connect to another account.</p> <button class="account_select_btn" onclick="try_diff_acc()">Try Different Account</button> </div>";') {
                $('.section-15-dl2').hide(); // Hiding all the second account related HTML structure
              } else if (secondaryAccountNumberExists) {
                secondaryAccountNumberExists = false;
                $('.section-15').hide();
                $('.section-15-dl2').show();
                $('.section-15-dl2 .bank-search').show();
                $('.section-15-dl2 .onload-list').show();
                $('#iframeRC_second').hide(); //TO hide Iframe of second account
                $('#BankSearchData_second').hide();
              } else {
                location.replace('./call-verification.php?oppId=' + oppId);
              }
            }
          });
        }
      }
    });
  }, 2000);
}

function UseThisButtonWhenUserSelectAccount(requestCode, bankType) {
  if ($('input[name=CHECKING]').is(':checked')) {
    $('.Continue_upload_loading-files').fadeIn();
    var AccountNbr = $('input[name=CHECKING]:checked').val();
    $.ajax({
      type: 'POST',
      url: decisionlogicpath + 'DecisionLogic/decisionlogicToSF.php',
      data: {
        requesttype: 'getDataIfMultipleAccount',
        AccountNbr: AccountNbr,
        requestCode: requestCode,
        oppId: oppId,
        bankType: bankType
      },
      success: function (data) {
        if (secondaryAccountNumberExists) {
          secondaryAccountNumberExists = false;
          $('.section-15').hide();
          $('.section-15a').hide();
          $('.section-15-dl2').show();
          $('.section-15-dl2 .bank-search').show();
          $('.section-15-dl2 .onload-list').show();
          $('#iframeRC_second').hide(); //TO hide Iframe of second account
          $('#BankSearchData_second').hide();
        } else {
          location.replace('./call-verification.php?oppId=' + oppId);
        }
      }
    });
  } else {
    alert('Please Select Atleast One Account');
  }
}

// function UseThisButtonWhenUserSelectAccount_second(requestCode) {
//   if ($('input[name=CHECKING]').is(':checked')) {
//     $('.Continue_upload_loading-files').fadeIn();
//     var AccountNbr = $('input[name=CHECKING]:checked').val();
//     $.ajax({
//       type: 'POST',
//       url: decisionlogicpath + 'DecisionLogic/decisionlogicToSF.php',
//       data: {
//         requesttype: 'getDataIfMultipleAccount',
//         AccountNbr: AccountNbr,
//         requestCode: requestCode,
//         oppId: oppId,
//         bankType: 'secondary'
//       },
//       success: function (data) {
//         location.replace('./call-verification.php?oppId=' + oppId);
//       }
//     });
//   } else {
//     alert('Please Select Atleast One Account');
//   }
// }

$('#BankSearch').on('keyup', function () {
  var str1 = $(this)
    .val()
    .split(' ')
    .join('');
  str1 = str1.length;
  if (str1 > 2) {
    $('.onload-list , .search-msg').fadeOut();
    $('#BankSearchData , .spiner-wrap').fadeIn();
    $.ajax({
      type: 'POST',
      url: decisionlogicpath + 'DecisionLogic/decisionlogic.php',
      data: {
        requesttype: 'SearchBanksByBankName',
        keywordBank: $(this).val()
      },
      success: function (data) {
        var data = JSON.parse(data);
        var SearchBankResp = data.SearchBanksByBankNameResult.BanksSearchResult;
        var sample = '<ul>';
        var sizeof = 12;
        if (SearchBankResp.length < 11) {
          sizeof = SearchBankResp.length;
        } else if (!SearchBankResp.length) {
          sizeof = 1;
        }
        if (sizeof == 1) {
          sample +=
            "<li class='clearfix' onclick='RCCodeAjax(`" +
            SearchBankResp['ContentServiceId'] +
            "`, `Primary`)' > <img src='https://widget.decisionlogic.com/ImageHandler.ashx?ContentServiceId=" +
            SearchBankResp['ContentServiceId'] +
            "' /> " +
            SearchBankResp['ContentServiceDisplayName'] +
            '</li>';
        } else {
          for (var i = 0; i < sizeof; i++) {
            sample +=
              "<li class='clearfix' onclick='RCCodeAjax(`" +
              SearchBankResp[i]['ContentServiceId'] +
              "`, `Primary`)' > <img src='https://widget.decisionlogic.com/ImageHandler.ashx?ContentServiceId=" +
              SearchBankResp[i]['ContentServiceId'] +
              "' /> " +
              SearchBankResp[i]['ContentServiceDisplayName'] +
              '</li>';
          }
        }
        sample += '</ul>';

        $('.spiner-wrap').fadeOut();
        $(".section-15 .BankSearchData-wrap").show();
        $('#BankSearchData').html(sample);
      }
    });
  } else {
    $('.onload-list').fadeIn();
    $(".section-15 .BankSearchData-wrap").hide();
    $('#BankSearchData')
      .fadeOut()
      .html('');
    $('.search-msg').fadeIn();

    setTimeout(function () {
      $('.search-msg').fadeOut();
    }, 5000);
  }
});

$('#BankSearch_second').on('keyup', function () {
  var str1 = $(this)
    .val()
    .split(' ')
    .join('');
  str1 = str1.length;
  if (str1 > 2) {
    $('.onload-list , .search-msg').fadeOut();
    $('#BankSearchData_second , .spiner-wrap').fadeIn();
    $.ajax({
      type: 'POST',
      url: decisionlogicpath + 'DecisionLogic/decisionlogic.php',
      data: {
        requesttype: 'SearchBanksByBankName',
        keywordBank: $(this).val()
      },
      success: function (data) {
        var data = JSON.parse(data);
        var SearchBankResp = data.SearchBanksByBankNameResult.BanksSearchResult;
        var sample = '<ul>';
        var sizeof = 12;
        if (SearchBankResp.length < 11) {
          sizeof = SearchBankResp.length;
        } else if (!SearchBankResp.length) {
          sizeof = 1;
        }
        if (sizeof == 1) {
          sample +=
            "<li class='clearfix' onclick='RCCodeAjax_second(`" +
            SearchBankResp['ContentServiceId'] +
            "`, `Secondary`)' > <img src='https://widget.decisionlogic.com/ImageHandler.ashx?ContentServiceId=" +
            SearchBankResp['ContentServiceId'] +
            "' /> " +
            SearchBankResp['ContentServiceDisplayName'] +
            '</li>';
        } else {
          for (var i = 0; i < sizeof; i++) {
            sample +=
              "<li class='clearfix' onclick='RCCodeAjax_second(`" +
              SearchBankResp[i]['ContentServiceId'] +
              "`, `Secondary`)' > <img src='https://widget.decisionlogic.com/ImageHandler.ashx?ContentServiceId=" +
              SearchBankResp[i]['ContentServiceId'] +
              "' /> " +
              SearchBankResp[i]['ContentServiceDisplayName'] +
              '</li>';
          }
        }
        sample += '</ul>';

        $('.spiner-wrap').fadeOut();
        $(".section-15-dl2 .BankSearchData-wrap").show();
        $('#BankSearchData_second').html(sample);
      }
    });
  } else {
    $('.onload-list').fadeIn();
    $(".section-15-dl2 .BankSearchData-wrap").hide();
    $('#BankSearchData_second')
      .fadeOut()
      .html('');
    $('.search-msg').fadeIn();

    setTimeout(function () {
      $('.search-msg').fadeOut();
    }, 5000);
  }
});

$('#backtosearch').click(function () {
  $('#iframeRC').hide();
  $('#backtosearch').hide();
  $('.bank-search').show();
});

$('#backtosearch_second').click(function () {
  $('#iframeRC_second').hide();
  $('#backtosearch_second').hide();
  $('.bank-search').show();
});

$('.dontFindBank').click(function () {
  $('.Link_bank_wrapper').fadeOut();
  $('.Upload_wrap').fadeIn();
});

$('.FindBank').click(function () {
  $('.Link_bank_wrapper').fadeIn();
  $('.Upload_wrap').fadeOut();
});

/* Decision Logic Ends */
