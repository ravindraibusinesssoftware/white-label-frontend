var SOLE_PROPRIETOR_BUSINESS_ENTITY = 'Sole Proprietor';
$(document).ready(function() {
  $('input').attr('autocomplete', 'off');

  $('.sorry_yes_btn').click(function() {
    $('.Sorry_Partner , .section-1').hide();
    $('.normal_wrapper , .section-2').fadeIn();
  });

  $('.yes_btn').click(function() {
    $('.section-1').hide();
    $('.section-2').fadeIn();
  });
  $('.gobackno').click(function() {
    $('.Sorry_Partner').hide();
    $('.normal_wrapper').fadeIn();
  });
  $('.no_btn').click(function() {
    $('.normal_wrapper').hide();
    $('.Sorry_Partner, .Progress_bar').fadeIn();
    $('.Progress_Status')
      .html('10%')
      .css({
        width: '10%'
      });
  });
  $('.section-2-content .eligible').click(function() {
    $('.section-2').hide();
    $('.section-3').fadeIn();
  });
  $('.section-4-content a').click(function() {
    $('.section-4').hide();
    if (Business_Entity === SOLE_PROPRIETOR_BUSINESS_ENTITY) {
      $('.section-5').fadeIn();
    } else {
      $('.section-4a').fadeIn();
    }
    $('.Progress_Status')
      .html('25%')
      .css({
        width: '25%'
      });
  });
  $('.section-4a-next').click(function() {
    if (Ownership_percentage == 100) {
      $('.section-4a').hide();
      $('.section-5').fadeIn();
    } else {
      $('.section-4a').hide();
      $('.section-4b').fadeIn();
    }
  });
  $('.section-4b-next').click(function() {
    var err = 0;
    if (!ValidateFirstname($('#second_first_name').val())) {
      err++;
    }
    if (!ValidateLastname($('#second_last_name').val())) {
      err++;
    }
    if (!ValidateEmail($('#second_email').val())) {
      err++;
    }
    if (!ValidatePhone($('#second_phone').val())) {
      err++;
    } else {
      var phoneapi = ValidatePhoneApi();
      if (phoneapi == false) {
        err++;
      }
    }
    if (!ValidateCheckOne($('#second_legal-one').is(':checked'))) {
      err++;
    }

    if (err > 0) {
      return false;
    }
    $('.section-4b').hide();
    $('.section-5').fadeIn();
    $('.Progress_Status')
      .html('30%')
      .css({
        width: '30%'
      });
  });

  $('#form-info-about-business').submit(function(event) {
    event.preventDefault();

    $('.section-5').hide();
    $('.section-6').fadeIn();
    $('.Progress_Status')
      .html('35%')
      .css({ width: '35%' });
  });

  $('.section-6-next').click(function() {
    $('.section-6').hide();
    $('.section-7').fadeIn();
    $('.Progress_Status')
      .html('45%')
      .css({
        width: '45%'
      });
  });

  $('.section-7-content a').click(function() {
    $('.section-7').hide();
    $('.section-8').fadeIn();
    $('.Progress_Status')
      .html('50%')
      .css({
        width: '50%'
      });
  });
  $('.section-8-next').click(function() {
    $('.section-8').hide();
    $('.section-10').fadeIn();
    $('.Progress_Status')
      .html('65%')
      .css({
        width: '65%'
      });
  });

  $('.section-10-content a').click(function() {
    $('.section-10').hide();
    $('.section-11').fadeIn();
    $('.Progress_Status')
      .html('75%')
      .css({
        width: '75%'
      });
  });
  $('.section-11-next').click(function() {
    $('.section-11').hide();
    $('.section-12').fadeIn();
    $('.Progress_Status')
      .html('80%')
      .css({
        width: '80%'
      });
  });
  $('.section-12-next').click(function() {
    $('.section-12').hide();
    var homebased_val = document.getElementById('homebased1').checked;
    if (homebased_val == true) {
      homebased = 'Yes';
    } else {
      homebased = 'No';
    }
    if (homebased == 'Yes') {
      $('.section-14').fadeIn();
    } else {
      $('.section-13').fadeIn();
    }
    $('.Progress_Status')
      .html('85%')
      .css({
        width: '85%'
      });
  });
  $('.section-13-next').click(function() {
    $('.section-13').hide();
    $('.section-14').fadeIn();
    $('.Progress_Status')
      .html('87%')
      .css({
        width: '87%'
      });
  });
  $('.section-14-next').click(function() {
    $('.section-14').hide();
    $('.section-15').fadeIn();
    $('.Progress_Status')
      .html('95%')
      .css({ width: '95%' });
  });

  $('.section-15-next').click(function() {
    $('.section-15, .section-4').hide();
    $('.section-16').fadeIn();
    $('.Progress_Status')
      .html('100%')
      .css({
        width: '100%'
      });
  });
  $('.sp-section-1-btn').click(function() {
    $('.sp-section-1').hide();
    $('.sp-section-2, .Progress_bar').fadeIn();
    $('.Progress_Status')
      .html('20%')
      .css({
        width: '20%'
      });
    $('#Amount-requested').val($('#borrow').val());
  });
  $('.sp-section-2-btn').click(function() {
    $('.sp-section-2').hide();
    $('.sp-section-3').fadeIn();
    $('.Progress_Status')
      .html('30%')
      .css({
        width: '30%'
      });
  });
  $('.sp-section-3-btn').click(function() {
    $('.sp-section-3').hide();
    $('.sp-section-4').fadeIn();
    $('.Progress_Status')
      .html('40%')
      .css({
        width: '40%'
      });
  });
  $('.sp-section-4-btn').click(function() {
    $('.sp-section-4').hide();
    $('.sp-section-5').fadeIn();
    $('.Progress_Status')
      .html('50%')
      .css({
        width: '50%'
      });
  });
  $('.sp-section-5-btn').click(function() {
    $('.sp-section-5').hide();
    $('.sp-section-6').fadeIn();
    $('.Progress_Status')
      .html('60%')
      .css({
        width: '60%'
      });
  });
  $('.sp-section-6-btn').click(function() {
    $('.sp-section-6').hide();
    $('.sp-section-7').fadeIn();
    $('.Progress_Status')
      .html('70%')
      .css({
        width: '70%'
      });
  });
  $('.sp-section-7-btn').click(function() {
    $('.sp-section-7').hide();
    $('.sp-section-7a').fadeIn();
    $('.Progress_Status')
      .html('75%')
      .css({
        width: '75%'
      });
  });
  $('.sp-section-7a-btn').click(function() {
    $('.sp-section-7a').hide();
    $('.sp-section-8').fadeIn();
    $('.Progress_Status')
      .html('80%')
      .css({
        width: '80%'
      });
  });
  $('.sp-section-8-btn').click(function() {
    $('.sp-section-8').hide();
    $('.sp-section-9').fadeIn();
    $('.Progress_Status')
      .html('90%')
      .css({
        width: '90%'
      });
  });
  $('.sp-section-9-btn').click(function() {
    $('.sp-section-9').hide();
    $('.sp-section-10').fadeIn();
    $('.Progress_Status')
      .html('100%')
      .css({
        width: '100%'
      });
  });

  // renewals landing page code
  $('.section-renewal-landing .btn-yes').click(function() {
    $('.section-renewal-landing').hide();
    $('.section-renewal-dl-header').fadeIn();
    $('.section-15').fadeIn();
    $('#renewals-progress-bar').width('50%');

    setTimeout(function() {
      $('.renewals-progress__point-2').addClass('renewals-progress__point-active');
    }, 900);
  });

  $('.section-renewal-landing .btn-no').click(function() {
    $('.section-renewal-landing').hide();
    $('.section-renewal-ty-no').fadeIn();
    $('#renewals-progress-bar').width('100%');

    setTimeout(function() {
      $('.renewals-progress__point-2').addClass('renewals-progress__point-active');
    }, 400);
    setTimeout(function() {
      $('.renewals-progress__point-3').addClass('renewals-progress__point-active');
    }, 900);
  });
});

var Step_Number;
window.onbeforeunload = function() {
  if (Step_Number == 'p4') {
    return void 0;
  }
  return 'You will loose all the information if you leave this page.';
};
$('#fileupload1').change(function() {
  if ($(this).val != '') {
    var fileExtension = ['pdf'];
    $('.fileupload1').addClass('added');
    if (
      $.inArray(
        $(this)
          .val()
          .split('.')
          .pop()
          .toLowerCase(),
        fileExtension
      ) == -1
    ) {
      alert("Only 'Pdf' format is allowed");
      $('.fileupload1').removeClass('added');
      $('#fileupload1').val('');
      $('.section-15-next').addClass('btn_disable');
    }
  } else {
    $('.fileupload1').removeClass('added');
  }
});

$('#fileupload2').change(function() {
  if ($(this).val != '') {
    var fileExtension = ['pdf'];
    $('.fileupload2').addClass('added');
    if (
      $.inArray(
        $(this)
          .val()
          .split('.')
          .pop()
          .toLowerCase(),
        fileExtension
      ) == -1
    ) {
      alert("Only 'pdf' format is allowed");
      $('.fileupload2').removeClass('added');
      $('#fileupload2').val('');
      $('.section-15-next').addClass('btn_disable');
    }
  } else {
    $('.fileupload2').removeClass('added');
  }
});

$('#fileupload3').change(function() {
  if ($(this).val != '') {
    var fileExtension = ['pdf'];
    $('.fileupload3').addClass('added');
    if (
      $.inArray(
        $(this)
          .val()
          .split('.')
          .pop()
          .toLowerCase(),
        fileExtension
      ) == -1
    ) {
      alert("Only 'pdf' format is allowed");
      $('.fileupload3').removeClass('added');
      $('#fileupload3').val('');
      $('.section-15-next').addClass('btn_disable');
    }
  } else {
    $('.fileupload3').removeClass('added');
  }
});
var utm_source;
var utm_medium;
var utm_campaign;
var utm_term;
var utm_content;
var user_ip;
$.getJSON('https://jsonip.com/?callback=?', function(data) {
  user_ip = data.ip;
});
if (user_device == 'NotMobile') {
  var device_width = $(window).width();
  if (device_width <= 991) {
    var user_device = 'Tablet';
  }
  if (device_width >= 992) {
    var user_device = 'Desktop';
  }
}

function getSearchParameters() {
  var prmstr = window.location.search.substr(1);
  return prmstr != null && prmstr != '' ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray(prmstr) {
  var params = {};
  var prmarr = prmstr.split('&');
  for (var i = 0; i < prmarr.length; i++) {
    var tmparr = prmarr[i].split('=');
    params[tmparr[0]] = tmparr[1];
  }
  return params;
}

function populateSfFields(params) {
  var Result = getSearchParameters();
  if (Result.utm_source) {
    utm_source = Result.utm_source;
  }
  if (Result.utm_medium) {
    utm_medium = Result.utm_medium;
  }
  if (Result.utm_campaign) {
    utm_campaign = Result.utm_campaign;
  }
  if (Result.utm_term) {
    utm_term = Result.utm_term;
  }
  if (Result.utm_content) {
    utm_content = Result.utm_content;
  }
}

populateSfFields();

$('.eligible').click(function() {
  Monthly_Revenue = $(this).attr('value');
});
$('.Business_Entity').click(function() {
  Business_Entity = $(this).attr('value');
});
$('.Gender_select').click(function() {
  Gender_select = $(this).attr('value');
});
$('#dob_Date').change(function() {
  dob_Date = $(this).val();
});
$('#dob_Month').change(function() {
  dob_Month = $(this).val();
});
$('#dob_Year').change(function() {
  dob_Year = $(this).val();
});
$('#bs_Date').change(function() {
  bs_dob_Date = $(this).val();
});
$('#bs_Month').change(function() {
  bs_dob_Month = $(this).val();
});
$('#bs_Year').change(function() {
  bs_dob_Year = $(this).val();
});
$('#Birthday2_dd').change(function() {
  Birthday2_dd = $(this).val();
});
$('#Birthday2_mm').change(function() {
  Birthday2_mm = $(this).val();
});
$('#Birthday2_yy').change(function() {
  Birthday2_yy = $(this).val();
});
$('#State').change(function() {
  State = $(this).val();
});
$('#Owner1_State').change(function() {
  Owner1_State = $(this).val();
});
$('#Ownership_percentage').change(function() {
  Ownership_percentage = $(this).val();
});
$('.Office_Space').click(function() {
  Office_Space = $(this).attr('value');
});
$('#industry').change(function() {
  industry = $(this).val();
});
$('#State_Incorporation').change(function() {
  State_Incorporation = $(this).val();
});

var $regexname = /^([a-zA-Z' ]{1,})$/;
//var $regexemail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
var $regexemail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var $regphone = /^\(\d{3}\) \d{3}-\d{4}$/;
var $regzip = /^\d{5}$/;
//var $regssn_tax = /^\d{9}$/;
var $regssn_tax = /^[0-9 -]+$/;
var $regexURL = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/;

var borrow, email2;
$('#Legal_Name ').on('keyup blur input', function() {
  if ($('#Legal_Name').val().length >= 3) {
    $('.section-5-next').removeClass('btn_disable');
  } else {
    $('.section-5-next').addClass('btn_disable');
  }
});

$('#dob_Month, #dob_Year').change(function() {
  populateSelect('dob_Date', 'dob_Month', 'dob_Year');
});

$('#bs_Month, #bs_Year').change(function() {
  populateSelect('bs_Date', 'bs_Month', 'bs_Year');
});

$('#Birthday2_mm, #Birthday2_yy').change(function() {
  populateSelect('Birthday2_dd', 'Birthday2_mm', 'Birthday2_yy');
});

function populateSelect(d, m, y) {
  var currentMonth, currentYear;
  currentMonth = $('#' + m).val();

  if (currentMonth == '02') {
    var len;
    currentYear = $('#' + y).val();
    if (currentYear && leapYear(currentYear)) {
      len = 29;
    } else {
      len = 28;
    }
    generateOptions(len, d);
  } else if (
    currentMonth == '04' ||
    currentMonth == '06' ||
    currentMonth == '09' ||
    currentMonth == '11'
  ) {
    generateOptions(30, d);
  } else {
    generateOptions(31, d);
  }
}

function generateOptions(len, currentDate) {
  var options = '<option value="">Select</option>';

  for (var i = 1; i <= len; i++) {
    if (i >= 0 && i <= 9) {
      options += '<option value="0' + i + '">' + i + '</option>';
    } else {
      options += '<option value="' + i + '">' + i + '</option>';
    }
  }

  $('#' + currentDate).html(options);
}

function leapYear(year) {
  return (year & 3) == 0 && (year % 25 != 0 || (year & 15) == 0);
}

$('#dob_Date , #dob_Month , #dob_Year').on('change', function() {
  if ($('#dob_Date').val() != '' && $('#dob_Month').val() != '' && $('#dob_Year').val() != '') {
    $('.section-8-next').removeClass('btn_disable');
  } else {
    $('.section-8-next').addClass('btn_disable');
  }
});

$('#Ownership_percentage').on('change', function() {
  if ($('#Ownership_percentage').val() != '') {
    $('.section-4a-next').removeClass('btn_disable');
  } else {
    $('.section-4a-next').addClass('btn_disable');
  }
});

$('#Street, .checkbox-inline, #homebased1 , #homebased2, #PostalCode').on(
  'keyup change blur',
  function() {
    var Zipcode = $('#PostalCode').val();
    if (
      $('#Street').val() &&
      placeApiValidation &&
      Zipcode.match($regzip) &&
      $('input[name="homebased"]').is(':checked')
    ) {
      $('.section-6-next').removeClass('btn_disable');
    } else {
      $('.section-6-next').addClass('btn_disable');
    }
  }
);

$('#industry').on('change', function() {
  if ($('#industry').val() != '') {
    $('.section-11-next').removeClass('btn_disable');
  } else {
    $('.section-11-next').addClass('btn_disable');
  }
});
$('#bs_Date , #bs_Month , #bs_Year').on('change', function() {
  if ($('#bs_Date').val() != '' && $('#bs_Month').val() != '' && $('#bs_Year').val() != '') {
    $('.section-12-next').removeClass('btn_disable');
  } else {
    $('.section-12-next').addClass('btn_disable');
  }
});

$('#Owner1_Street, #AptSuite2,#Owner1_Zip').on('keyup change blur', function() {
  var Zipcode = $('#Owner1_Zip').val();
  if ($('#Owner1_Street').val() && Zipcode.match($regzip) && placeApiValidation) {
    $('.section-13-next').removeClass('btn_disable');
  } else {
    $('.section-13-next').addClass('btn_disable');
  }
});

$('#State_Incorporation, #Tax_ID, #Social_Security_Number , #legal-two').on(
  'change keyup blur input',
  function() {
    var legaltwo = document.getElementById('legal-two').checked;

    if (
      $('#State_Incorporation').val() != '' &&
      $('#Tax_ID')
        .val()
        .match($regssn_tax) &&
      $('#Social_Security_Number')
        .val()
        .match($regssn_tax) &&
      $('#Social_Security_Number').val().length == 11 &&
      legaltwo == true
    ) {
      $('.section-14-next').removeClass('btn_disable');
    } else {
      $('.section-14-next').addClass('btn_disable');
    }
  }
);

var num_arr = [
  '000000000',
  '111111111',
  '222222222',
  '333333333',
  '444444444',
  '555555555',
  '666666666',
  '777777777',
  '888888888',
  '999999999'
];

function checkValue(value, arr, ele) {
  var status = 'Not exist';

  for (var i = 0; i < arr.length; i++) {
    var num = arr[i];
    if (num == value) {
      status = 'Exist';

      var ssn_tag = '<p class="invalid_num_error ssn" >Please Enter valid SSN</p>';
      var taxid_tag = '<p class="invalid_num_error taxid" >Please Enter valid Tax ID</p>';

      if (ele == 'ssn') {
        $('#Social_Security_Number').val('');
        $('#Social_Security_Number').after(ssn_tag);
        setTimeout(function() {
          $('.invalid_num_error.ssn').remove();
        }, 3000);
        break;
      } else {
        $('#Tax_ID').val('');
        $('#Tax_ID').after(taxid_tag);
        setTimeout(function() {
          $('.invalid_num_error.taxid').remove();
        }, 3000);
        break;
      }
    }
  }

  return status;
}

$('#Social_Security_Number').on('keyup', function() {
  $('.invalid_num_error.ssn').remove();
  var str = $('#Social_Security_Number').val();
  var newStr = str.replace(/-/g, '');
  var ele = 'ssn';
  if (newStr.length == 9) {
    checkValue(newStr, num_arr, ele);
  }
});

$('#Tax_ID').on('keyup', function() {
  $('.invalid_num_error.taxid').remove();
  var str = $('#Tax_ID').val();
  var newStr = str.replace(/-/g, '');
  var ele = 'Tax_ID';
  if (newStr.length == 9) {
    checkValue(newStr, num_arr, ele);
  }
});

$('#fileupload1, #fileupload2 ,#fileupload3').on('change', function() {
  var fileupload1 = $('#fileupload1').val();
  var fileupload2 = $('#fileupload2').val();
  var fileupload3 = $('#fileupload3').val();

  if (fileupload1 != '' && fileupload2 != '' && fileupload3 != '') {
    $('.section-15-next').removeClass('btn_disable');
  } else {
    $('.section-15-next').addClass('btn_disable');
  }
});

$('#borrow').change(function() {
  borrow = $(this).val();
  toUSD(borrow);
  $('#Amount-requested').val(borrow);
});
$('#borrow ').on('keyup change blur input', function() {
  if ($('#borrow').val() != '') {
    $('.sp-section-1-btn').removeClass('btn_disable');
  } else {
    $('.sp-section-1-btn').addClass('btn_disable');
  }
});

$('.business_options').on('keyup change blur input', function() {
  var business_options = $('.business_options:checked').val();
  if (business_options != undefined) {
    $('.sp-section-2-btn').removeClass('btn_disable');
  } else {
    $('.sp-section-2-btn').addClass('btn_disable');
  }
});

$('.credit_type').on('keyup change blur input', function() {
  var credit_type = $('.credit_type:checked').val();
  if (credit_type != undefined) {
    $('.sp-section-3-btn').removeClass('btn_disable');
  } else {
    $('.sp-section-3-btn').addClass('btn_disable');
  }
});

$('.radio_Military').on('keyup change blur input', function() {
  var radio_Military = $('.radio_Military:checked').val();
  if (radio_Military != undefined) {
    $('.sp-section-4-btn').removeClass('btn_disable');
  } else {
    $('.sp-section-4-btn').addClass('btn_disable');
  }
});

$('#sp-length-address, #sp-Address, .radio_ownhouse, #sp-Zip-Code').on(
  'keyup change blur',
  function() {
    var sp_length_address = $('#sp-length-address').val();
    var Zipcode = $('#sp-Zip-Code').val();
    if (
      sp_length_address.length != '' &&
      $('#sp-Address').val() &&
      Zipcode.match($regzip) &&
      placeApiValidation &&
      $('.radio_ownhouse').is(':checked')
    ) {
      $('.sp-section-5-btn').removeClass('btn_disable');
    } else {
      $('.sp-section-5-btn').addClass('btn_disable');
    }
  }
);

$('#sp-gross-income').on('keyup change blur input', function() {
  var sp_gross_income = $('#sp-gross-income')
    .val()
    .trim();
  if (sp_gross_income.length != '') {
    $('.sp-section-6-btn').removeClass('btn_disable');
  } else {
    $('.sp-section-6-btn').addClass('btn_disable');
  }
});

/*........section 7 personal start......*/
$(' #firstname2 ,#lastname2 , #dob2_Month , #dob2_Year, #dob2_Date , #email2').on(
  'keyup change blur input',
  function() {
    var firstname2 = $('#firstname2')
      .val()
      .trim();
    var lastname2 = $('#lastname2')
      .val()
      .trim();
    var Birthday2_mm = $('#dob2_Month').val();
    var Birthday2_yy = $('#dob2_Year').val();
    var Birthday2_dd = $('#dob2_Date').val();
    var email2 = $('#email2')
      .val()
      .trim();
    if (
      firstname2.match($regexname) &&
      lastname2.match($regexname) &&
      Birthday2_mm != '' &&
      Birthday2_dd != '' &&
      Birthday2_yy != '' &&
      email2.match($regexemail)
    ) {
      $('.sp-section-7-btn').removeClass('btn_disable');
    } else {
      $('.sp-section-7-btn').addClass('btn_disable');
    }
  }
);
/*........section 7 personal end

/*........section 7a personal start......*/
$(' #sp-Phone,#CallTime').on('keyup change blur input', function() {
  var phone_no = $('#sp-Phone').val();
  var calltime = $('#CallTime').val();
  if (calltime != '' && phone_no.match($regphone) && phonestatus) {
    $('.sp-section-7a-btn').removeClass('btn_disable');
  } else {
    $('.sp-section-7a-btn').addClass('btn_disable');
  }
});
/*........section 7a personal end......*/

/*...........section-8 personal start...*/
$(
  '#sp-Income-source , #sp-Timed-Employed , #sp-get-paid , #sp-Employer-name ,#sp-Employer-Phone'
).on('keyup change blur input', function() {
  var sp_Income_source = $('#sp-Income-source').val();
  var sp_Timed_Employed = $('#sp-Timed-Employed').val();
  var sp_get_paid = $('#sp-get-paid').val();
  var sp_Employer_name = $('#sp-Employer-name').val();
  var sp_Employer_Phone = $('#sp-Employer-Phone').val();
  if (
    sp_Income_source != '' &&
    sp_Timed_Employed != '' &&
    sp_get_paid != '' &&
    sp_Employer_name.match($regexname) &&
    sp_Employer_Phone.match($regphone) &&
    phonestatus
  ) {
    $('.sp-section-8-btn').removeClass('btn_disable');
  } else {
    $('.sp-section-8-btn').addClass('btn_disable');
  }
});
/*...............section-8 personal end......*/

$('#sp-Zip-Code').keydown(function(e) {
  var key = e.which || e.charCode || e.keyCode || 0;

  return key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105);
});

$(
  '#sp-Income-source , #sp-Timed-Employed , #sp-get-paid , #sp-Employer-name ,#sp-Employer-Phone , #sp-gross-income'
).on('keyup change blur input', function() {
  var sp_Income_source = $('#sp-Income-source').val();
  var sp_Timed_Employed = $('#sp-Timed-Employed').val();
  var sp_get_paid = $('#sp-get-paid').val();
  var sp_Employer_name = $('#sp-Employer-name').val();
  var sp_Employer_Phone = $('#sp-Employer-Phone').val();
  var sp_gross_income = $('#sp-gross-income')
    .val()
    .trim();

  if (
    sp_Income_source != '' &&
    sp_Timed_Employed != '' &&
    sp_get_paid != '' &&
    sp_Employer_name.match($regexname) &&
    sp_Employer_Phone.match($regphone) &&
    sp_gross_income != ''
  ) {
    $('.sp-section-4-btn').removeClass('btn_disable');
  } else {
    $('.sp-section-4-btn').addClass('btn_disable');
  }
});

$('#Driver-License , #sp-Issuing-State , #sp-ssn , #sp-Account-Type ,#legal-four').on(
  'keyup change blur input',
  function() {
    // $('#Driver-License ').on('keyup change blur input',function(){

    var sp_Driver_License = $('#Driver-License')
      .val()
      .trim();
    var sp_Issuing_State = $('#sp-Issuing-State').val();
    var sp_ssn = $('#sp-ssn').val();
    var sp_Account_Type = $('#sp-Account-Type').val();
    var legalfour = document.getElementById('legal-four').checked;

    if (
      sp_Driver_License.length >= 3 &&
      sp_Issuing_State != '' &&
      sp_ssn.match($regssn_tax) &&
      sp_ssn.length == 11 &&
      sp_Account_Type != '' &&
      legalfour == true
    ) {
      // if (sp_Driver_License.length >= 3 ) {

      $('.sp-section-9-btn').removeClass('btn_disable');
    } else {
      $('.sp-section-9-btn').addClass('btn_disable');
    }
  }
);

var $regspecialCha = /[ !@#$%^&*~()_+\-=\[\]{};':"\\|,. <>\/?]/;
var $regnum = /\d+/;

function ValidateFirstname(first_name) {
  var first_name = first_name.trim();
  if (first_name.match($regnum) && first_name.match($regspecialCha)) {
    $('.err_first_name_Num_Char').fadeIn();
    return false;
  } else if (first_name.match($regspecialCha)) {
    $('.err_first_name_specialChar').fadeIn();
    return false;
  } else if (first_name.match($regnum)) {
    $('.err_first_name_Num').fadeIn();
    return false;
  } else if (first_name == '') {
    $('.err_first_name_valid').fadeIn();
    return false;
  } else {
    $('.err_first_name').fadeIn();
    return true;
  }
}

$('#first_name, #second_first_name').on('keyup', function() {
  $(
    '.err_first_name , .err_first_name_Num_Char , .err_first_name_specialChar , .err_first_name_Num , .err_first_name_valid'
  ).fadeOut();
});

function ValidateLastname(last_name) {
  var last_name = last_name.trim();
  if (last_name.match($regnum) && last_name.match($regspecialCha)) {
    $('.err_last_name_Num_Char').fadeIn();
    return false;
  } else if (last_name.match($regspecialCha)) {
    $('.err_last_name_specialChar').fadeIn();
    return false;
  } else if (last_name.match($regnum)) {
    $('.err_last_name_Num').fadeIn();
    return false;
  } else if (last_name == '') {
    $('.err_last_name_valid').fadeIn();
    return false;
  } else {
    $('.err_last_name').fadeIn();
    return true;
  }
}

$('#last_name, #second_last_name').on('keyup', function() {
  $(
    '.err_last_name_Num_Char , .err_last_name_specialChar , .err_last_name_Num , .err_last_name ,.err_last_name_valid'
  ).fadeOut();
});

function ValidateEmail(email) {
  var email = email.trim();
  if (email.match($regexemail)) {
    $('.err_email').fadeOut();
    return true;
  } else {
    $('.err_email').fadeIn();
    return false;
  }
}

$('#email, #second_email').on('keyup', function() {
  $('.err_email').fadeOut();
});

function ValidatePhone(phone) {
  var phone = phone.trim();
  if (phone.match($regphone)) {
    $('.err_phone').fadeOut();
    return true;
  } else {
    $('.err_phone').fadeIn();
    return false;
  }
}

var phonestatus = false;
var obj = {
  'l(111) 111-1111': false,
  'l(222) 222-2222': false,
  'l(333) 333-3333': false,
  'l(444) 444-4444': false,
  'l(555) 555-5555': false,
  'l(666) 666-6666': false,
  'l(777) 777-7777': false,
  'l(888) 888-8888': false,
  'l(999) 999-9999': false,
  'l(738) 644-4779': true //this number is for developer testing so that api is not invoked
};
$('.phone_api').on('keyup', function() {
  if ($(this).val().length == 14) {
    var data = 'l' + $(this).val();
    if (obj[data] == true) {
      phonestatus = true;
    } else if (obj[data] == false) {
      phonestatus = false;
    } else {
      $(this).prop('disabled', true);
      phoneapicall(data, this);
    }
  }
});

function ValidatePhoneApi() {
  var sp_Employer_PhoneApi = phonestatus;
  if (sp_Employer_PhoneApi != false) {
    $('.err_phone').fadeOut();
    return true;
  } else {
    $('.err_phone').fadeIn();
    return false;
  }
}

function phoneapicall(numberdata, fsdata) {
  $('.Continue_upload_loading-files').fadeIn();
  $('#PhoneValidatingMessage').fadeIn();
  var url =
    'numverifyvalidation.php?phone=' + $('#' + fsdata.id).val() + '&email=' + $('#email').val();
  $.get(url, function(data, status) {
    if (data == 'Invalid') {
      phonestatus = false;
    } else {
      phonestatus = true;
    }
    $(fsdata).prop('disabled', false);
    obj[numberdata] = phonestatus;
    $('.Continue_upload_loading-files').fadeOut();
    $('#PhoneValidatingMessage').fadeOut();
  });
}

$('#phone, #second_phone').on('keyup', function() {
  $('.err_phone').fadeOut();
});

function ValidateCheckOne(legalone) {
  if (legalone == true) {
    $('.err_legal_one').fadeOut();
    return true;
  } else {
    $('.err_legal_one').fadeIn();
    return false;
  }
}

$('.legal-checkbox').click(function() {
  $('.err_legal_one').fadeOut();
});

$('.get-started').click(function() {
  email = $('#email')
    .val()
    .trim();

  var err = 0;

  if (!ValidateFirstname($('#first_name').val())) {
    err++;
  }
  if (!ValidateLastname($('#last_name').val())) {
    err++;
  }
  if (!ValidateEmail($('#email').val())) {
    err++;
  }
  if (!ValidatePhone($('#phone').val())) {
    err++;
  } else {
    var phoneapi = ValidatePhoneApi();
    if (phoneapi == false) {
      err++;
    }
  }
  if (!ValidateCheckOne($('#legal-one').is(':checked'))) {
    err++;
  }

  if (err > 0) {
    return false;
  }

  var urln = window.document.URL;
  if (urln.indexOf('facebook') > -1) {
    iAmAttilasEvent();
  }

  var Step_Number = 1;

  $('.Continue_upload_loading-files').fadeIn();

  $.ajax({
    type: 'POST',
    url: 'ajaxmobile.php',
    data: {
      email: email,
      Step_Number: Step_Number
    },
    success: function(data) {
      var data_json = JSON.parse(data);

      if (data_json.step == 0) {
        AjaxSection();
      }
      if (data_json.step == 1) {
        if (data_json.StepNumber == 8) {
          AjaxSection();
        } else {
          $('.confirmation').show();
          $('.Continue_upload_loading-files').hide();

          $('.confirmation_no').click(function() {
            $.ajax({
              type: 'POST',
              url: 'ajaxcontinueapp.php',
              data: {
                data_json: data_json
              },
              success: function(data) {
                $('.section_wrapper').fadeIn();

                Steps(data);

                if (data == '8') {
                  $('.section-16 , .section_wrapper').show();
                  $('.Progress_Status')
                    .html('100%')
                    .css({
                      width: '100%'
                    });
                }

                $('.confirmation , .hero_container').hide();
              }
            });
          });
        }
        $('.confirmation_yes').click(function() {
          AjaxSection();
        });
      }
    }
  });
});

$('.no_btn ').click(function() {
  var Step_Number = 'zero';

  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      own_a_business: 'No',
      utm_source: utm_source,
      utm_medium: utm_medium,
      utm_campaign: utm_campaign,
      utm_term: utm_term,
      utm_content: utm_content,
      Step_Number: Step_Number,
      user_ip: user_ip,
      user_browser: user_browser,
      user_device: user_device,
      fc_campaign: $('#fc_campaign').val(),
      fc_channel: $('#fc_channel').val(),
      fc_content: $('#fc_content').val(),
      fc_landing: $('#fc_landing').val(),
      fc_medium: $('#fc_medium').val(),
      fc_referrer: $('#fc_referrer').val(),
      fc_source: $('#fc_source').val(),
      fc_term: $('#fc_term').val(),
      lc_campaign: $('#lc_campaign').val(),
      lc_channel: $('#lc_channel').val(),
      lc_content: $('#lc_content').val(),
      lc_landing: $('#lc_landing').val(),
      lc_medium: $('#lc_medium').val(),
      lc_referrer: $('#lc_referrer').val(),
      lc_source: $('#lc_source').val(),
      lc_term: $('#lc_term').val(),
      OS: $('#OS').val(),
      GA_Client_ID: $('#GA_Client_ID').val(),
      all_traffic_sources: $('#all_traffic_sources').val(),
      browser: $('#browser').val(),
      city: $('#city').val(),
      country: $('#country').val(),
      device: $('#device').val(),
      page_visits: $('#page_visits').val(),
      pages_visited_list: $('#pages_visited_list').val(),
      region: $('#region').val(),
      time_zone: $('#time_zone').val(),
      time_passed: $('#time_passed').val(),
      latitude: $('#latitude').val(),
      longitude: $('#longitude').val()
    },
    success: function(data) {}
  });
});

function AjaxSection() {
  var Step_Number = 1;

  $('.Continue_upload_loading-files , .confirmation').hide();
  $('.section-3 , .section').hide();
  $('.section-4').fadeIn();
  $('.Progress_Status')
    .html('20%')
    .css({
      width: '20%'
    });
  $('.Progress_bar').show();
  $(
    '.modal-wrapper, .section-1 , .capital_section  , .how_it-_works , .review_section , .small_form_section , .footer , .hero_container , .badgets'
  ).fadeOut();
  $('body').css('overflow', '');
  $('.section-4 , .section_wrapper').fadeIn();

  var data = {
    first_name: $('#first_name').val(),
    phone: $('#phone').val(),
    email: $('#email').val(),
    last_name: $('#last_name').val(),
    Monthly_Revenue: Monthly_Revenue,
    utm_source: utm_source,
    utm_medium: utm_medium,
    utm_campaign: utm_campaign,
    utm_term: utm_term,
    utm_content: utm_content,
    Step_Number: Step_Number,
    user_ip: user_ip,
    user_browser: user_browser,
    user_device: user_device,
    fc_campaign: $('#fc_campaign').val(),
    fc_channel: $('#fc_channel').val(),
    fc_content: $('#fc_content').val(),
    fc_landing: $('#fc_landing').val(),
    fc_medium: $('#fc_medium').val(),
    fc_referrer: $('#fc_referrer').val(),
    fc_source: $('#fc_source').val(),
    fc_term: $('#fc_term').val(),
    lc_campaign: $('#lc_campaign').val(),
    lc_channel: $('#lc_channel').val(),
    lc_content: $('#lc_content').val(),
    lc_landing: $('#lc_landing').val(),
    lc_medium: $('#lc_medium').val(),
    lc_referrer: $('#lc_referrer').val(),
    lc_source: $('#lc_source').val(),
    lc_term: $('#lc_term').val(),
    OS: $('#OS').val(),
    GA_Client_ID: $('#GA_Client_ID').val(),
    all_traffic_sources: $('#all_traffic_sources').val(),
    browser: $('#browser').val(),
    city: $('#city').val(),
    country: $('#country').val(),
    device: $('#device').val(),
    page_visits: $('#page_visits').val(),
    pages_visited_list: $('#pages_visited_list').val(),
    region: $('#region').val(),
    time_zone: $('#time_zone').val(),
    time_passed: $('#time_passed').val(),
    latitude: $('#latitude').val(),
    longitude: $('#longitude').val()
  };

  if (hasTop10InUrl()) {
    data['UniquePartnerId'] = getParameterByName('sub-id');
    data['LeadSource'] = 'Tiger-Top10';
  }

  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: data,
    success: function(data) {}
  });
}

$('#form-info-about-business').submit(function(event) {
  event.preventDefault();
  var data = {
    Website: $('#URL').val(),
    Legal_Name: $('#Legal_Name').val(),
    Doing_Business: $('#Doing_Business').val(),
    Step_Number: 2,
    Business_Entity: Business_Entity
  };
  if (Business_Entity === SOLE_PROPRIETOR_BUSINESS_ENTITY) {
    data['Ownership_percentage'] = '100';
  } else {
    data['Ownership_percentage'] = Ownership_percentage;
    (data['second_first_name'] = $('#second_first_name').val()),
      (data['second_phone'] = $('#second_phone').val()),
      (data['second_email'] = $('#second_email').val()),
      (data['second_last_name'] = $('#second_last_name').val());
  }
  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: data
  });
});

$('.section-6-next').click(function() {
  if (document.getElementById('homebased1').checked == true) {
    homebased = 'Yes';
  } else {
    homebased = 'No';
  }
  var Step_Number = 3;
  var streetA = ReplaceStreet(
    $('#Street').val(),
    $('#PostalCode').val(),
    $('#State').val(),
    $('#City').val(),
    $('#AptSuite').val()
  );

  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      Street: streetA,
      City: $('#City').val(),
      State: $('#State').val(),
      PostalCode: $('#PostalCode').val(),
      Step_Number: Step_Number,
      homebased: homebased
    },
    success: function(data) {}
  });
});

$('.section-8-next').click(function() {
  var dob = dob_Year + '-' + dob_Month + '-' + dob_Date + '';
  var data = {
    Gender_select: Gender_select,
    dob: dob,
    Step_Number: 4
  };
  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: data
  });
});

$('.section-12-next').click(function() {
  var bs_dob = bs_dob_Year + '-' + bs_dob_Month + '-' + bs_dob_Date + '';
  var data = {
    bs_dob: bs_dob,
    industry: industry,
    Step_Number: 5,
    Office_Space: Office_Space
  };
  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: data
  });
});

$('.section-13-next').click(function() {
  var Step_Number = 6;
  var streetB = ReplaceStreet(
    $('#Owner1_Street').val(),
    $('#Owner1_Zip').val(),
    $('#Owner1_State').val(),
    $('#Owner1_City').val(),
    $('#AptSuite2').val()
  );

  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      Owner1_Street: streetB,
      Owner1_City: $('#Owner1_City').val(),
      Owner1_State: $('#Owner1_State').val(),
      Owner1_Zip: $('#Owner1_Zip').val(),
      Step_Number: Step_Number
    },
    success: function(data) {}
  });
});

$('.section-14-next').click(function() {
  var Step_Number = 7;
  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      State_Incorporation: State_Incorporation,
      Tax_ID: $('#Tax_ID').val(),
      Social_Security_Number: $('#Social_Security_Number').val(),
      Step_Number: Step_Number
    },
    success: function(data) {}
  });
});

$('.section-15-next').click(function() {
  $('.Continue_upload_loading-files').fadeIn();
  var Step_Number = 8;
  var formData = new FormData(document.querySelector('#pdfaws'));
  $.ajax({
    url: 'amazons3/amazons3/public/aws.php',
    type: 'POST',
    data: formData,
    processData: false,
    contentType: false,
    success: function(result) {
      $('.Continue_upload_loading-files').fadeOut();
    }
  });

  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      Step_Number: Step_Number
    },
    success: function(data) {}
  });
});
$('.sp-section-1-btn').click(function() {
  var Step_Number = 'p0';
  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      like_to_borrow: $('#borrow').val(),
      Step_Number: Step_Number,

      utm_source: utm_source,
      utm_medium: utm_medium,
      utm_campaign: utm_campaign,
      utm_term: utm_term,
      utm_content: utm_content,
      user_ip: user_ip,
      user_browser: user_browser,
      user_device: user_device,
      fc_campaign: $('#fc_campaign').val(),
      fc_channel: $('#fc_channel').val(),
      fc_content: $('#fc_content').val(),
      fc_landing: $('#fc_landing').val(),
      fc_medium: $('#fc_medium').val(),
      fc_referrer: $('#fc_referrer').val(),
      fc_source: $('#fc_source').val(),
      fc_term: $('#fc_term').val(),
      lc_campaign: $('#lc_campaign').val(),
      lc_channel: $('#lc_channel').val(),
      lc_content: $('#lc_content').val(),
      lc_landing: $('#lc_landing').val(),
      lc_medium: $('#lc_medium').val(),
      lc_referrer: $('#lc_referrer').val(),
      lc_source: $('#lc_source').val(),
      lc_term: $('#lc_term').val(),
      OS: $('#OS').val(),
      GA_Client_ID: $('#GA_Client_ID').val(),
      all_traffic_sources: $('#all_traffic_sources').val(),
      browser: $('#browser').val(),
      city: $('#city').val(),
      country: $('#country').val(),
      device: $('#device').val(),
      page_visits: $('#page_visits').val(),
      pages_visited_list: $('#pages_visited_list').val(),
      region: $('#region').val(),
      time_zone: $('#time_zone').val(),
      time_passed: $('#time_passed').val(),
      latitude: $('#latitude').val(),
      longitude: $('#longitude').val()
    },
    success: function(data) {}
  });
});
$('.sp-section-4-btn').click(function() {
  var Step_Number = 'p1';
  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      industry: $('.business_options:checked').val(),
      Credit_type: $('.credit_type:checked').val(),
      radio_Military: $('.radio_Military:checked').val(),
      Step_Number: Step_Number
    },
    success: function(data) {}
  });
});
$('.sp-section-5-btn').click(function() {
  var Step_Number = 'p2';
  var street = ReplaceStreet(
    $('#sp-Address').val(),
    $('#sp-Zip-Code').val(),
    $('#sp-State').val(),
    $('#sp-city').val(),
    ''
  );
  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      Address: street,
      Step_Number: Step_Number,
      zip_code: $('#sp-Zip-Code').val(),
      state: $('#sp-State').val(),
      city: $('#sp-city').val(),
      length_address: $('#sp-length-address').val(),
      own_a_house: $('.radio_ownhouse:checked').val()
    },
    success: function(data) {}
  });
});
$('.sp-section-7a-btn').click(function() {
  var sp_dob_Year = $('#dob2_Year').val();
  var sp_dob_Month = $('#dob2_Month').val();
  var sp_dob_Date = $('#dob2_Date').val();
  var Step_Number = 'p3';
  var dob = sp_dob_Year + '-' + sp_dob_Month + '-' + sp_dob_Date;
  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      email: $('#email2').val(),
      gross_income: $('#sp-gross-income').val(),
      first_name: $('#firstname2').val(),
      last_name: $('#lastname2').val(),
      dob: dob,
      phone_no: $('#sp-Phone').val(),
      TimeToCall: $('#CallTime').val(),
      Step_Number: Step_Number
    },
    success: function(data) {}
  });
});

$('.sp-section-8-btn').click(function() {
  var Step_Number = 'p4';
  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      Income_source: $('#sp-Income-source').val(),
      Timed_Employed: $('#sp-Timed-Employed').val(),
      get_paid: $('#sp-get-paid').val(),
      Employer_name: $('#sp-Employer-name').val(),
      Employer_Phone: $('#sp-Employer-Phone').val(),
      Step_Number: Step_Number
    },
    success: function(data) {}
  });
});
$('.sp-section-9-btn').click(function() {
  Step_Number = 'p5';
  $.ajax({
    type: 'POST',
    url: 'ajaxcreateleadsf.php',
    data: {
      Driver_License: $('#Driver-License').val(),
      Issuing_State: $('#sp-Issuing-State').val(),
      ssn: $('#sp-ssn').val(),
      Account_Type: $('#sp-Account-Type').val(),
      Step_Number: Step_Number
    },
    success: function(data) {}
  });
});
function toUSD(number) {
  var number = number.toString(),
    dollars = number.split('.')[0],
    cents = (number.split('.')[1] || '') + '00';
  dollars = dollars
    .split('')
    .reverse()
    .join('')
    .replace(/(\d{3}(?!$))/g, '$1,')
    .split('')
    .reverse()
    .join('');
  $('.borrow-result span').text('$' + dollars + '.' + cents.slice(0, 2));
}

$('#Amount-requested').keydown(function(e) {
  // Allow: backspace, delete, tab, escape, enter and .
  if (
    $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
    // Allow: Ctrl+A, Command+A
    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
    // Allow: home, end, left, right, down, up
    (e.keyCode >= 35 && e.keyCode <= 40)
  ) {
    // let it happen, don't do anything
    return;
  }
  // Ensure that it is a number and stop the keypress
  if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    e.preventDefault();
  }
});

$('#phone , #second_phone , #sp-Phone , #sp-Employer-Phone')
  .keydown(function(e) {
    var key = e.which || e.charCode || e.keyCode || 0;
    $phone = $(this);
    // Don't let them remove the starting '('
    if ($phone.val().length === 1 && (key === 8 || key === 46)) {
      $phone.val('(');
      return false;
    }
    // Reset if they highlight and type over first char.
    else if ($phone.val().charAt(0) !== '(') {
      $phone.val('(' + String.fromCharCode(e.keyCode) + '');
    }
    // Auto-format- do not expose the mask as the user begins to type
    if (key !== 8 && key !== 9) {
      if ($phone.val().length === 4) {
        $phone.val($phone.val() + ')');
      }
      if ($phone.val().length === 5) {
        $phone.val($phone.val() + ' ');
      }
      if ($phone.val().length === 9) {
        $phone.val($phone.val() + '-');
      }
    }
    // Diable 0 & 1 at the beginning
    if ($phone.val() == '(') {
      if (key == 48 || key == 49) {
        return false;
      }
    }
    // Allow numeric (and tab, backspace, delete) keys only
    return (
      key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)
    );
  })

  .bind('focus click', function() {
    $phone = $(this);
    if ($phone.val().length === 0) {
      $phone.val('(');
    } else {
      var val = $phone.val();
      $phone.val('').val(val); // Ensure cursor remains at the end
    }
  })
  .blur(function() {
    $phone = $(this);
    if ($phone.val() === '(') {
      $phone.val('');
    }
  });

function mask(o, f) {
  setTimeout(function() {
    var v = f(o.value);
    if (v != o.value) {
      o.value = v;
    }
  }, 1);
}

function mssn(v) {
  var r = v.replace(/\D/g, '');

  if (r.length > 9) {
    r = r.replace(/^(\d\d\d)(\d{2})(\d{0,4}).*/, '$1-$2-$3');
    return r;
  } else if (r.length > 4) {
    r = r.replace(/^(\d\d\d)(\d{2})(\d{0,4}).*/, '$1-$2-$3');
  } else if (r.length > 2) {
    r = r.replace(/^(\d\d\d)(\d{0,3})/, '$1-$2');
  } else {
    r = r.replace(/^(\d*)/, '$1');
  }
  return r;
}
function hasTop10InUrl() {
  return window.location.href.includes('top10');
}

function getParameterByName(name, url) {
  if (!url) {
    url = window.location.href;
  }

  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
    results = regex.exec(url);

  if (!results) {
    return null;
  }

  if (!results[2]) {
    return '';
  }

  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function try_diff_acc() {
  $('.section-15a, .section-15 iframe').hide();
  $('.section-15, .bank-search').fadeIn();
}

/*Decision Logic starts*/

function WRCCodeAjax() {
  $.ajax({
    type: 'POST',
    url: '/DecisionLogic/decisionlogic.php',
    data: { requesttype: 'cookiere' },
    success: function(data) {
      console.log(data);
      if (data != 'deletecookies') {
        $('.bank-search').hide();
        $('#iframeRC').attr(
          'src',
          'https://widget.decisionlogic.com/Service.aspx?requestCode=' + data
        );
        $('#iframeRC').fadeIn();
        ToShowThankYouSection();
      }
    }
  });
}
function RCCodeAjax(ContentServiceId) {
  //$("#iframeRC").attr("src","https://widget.decisionlogic.com/Service.aspx?requestCode=RPJDCU");
  $('.bank-search').hide();
  $('#iframeRC').show();
  $('#backtosearch').show();
  $('.main-loader').fadeIn();

  $.ajax({
    type: 'POST',
    url: '/DecisionLogic/decisionlogic.php',
    data: { requesttype: 'createRC', ContentServiceId: ContentServiceId },
    success: function(data) {
      ToShowThankYouSection();
      console.log(data);
      $('#iframeRC').attr(
        'src',
        'https://widget.decisionlogic.com/Service.aspx?requestCode=' + data
      );
      $('.main-loader').fadeOut();
    }
  });
}

function ToShowThankYouSection() {
  var ToShowThankYou = setInterval(function() {
    $.ajax({
      type: 'POST',
      url: '/DecisionLogic/decisionlogic.php',
      data: { requesttype: 'checkVDL' },
      success: function(data) {
        data = JSON.parse(data);
        if (data.GetReportDetailFromRequestCode7Result.IsLoginValid) {
          clearInterval(ToShowThankYou);
          $.ajax({
            type: 'POST',
            url: '/DecisionLogic/decisionlogic.php',
            data: { requesttype: 'GetData' },
            success: function(data) {
              //console.log(data);
              if (data == 'transaction90DaysExceeded') {
                alert("Transactions in this account are older than 90 days. Please attach the bank statements with latest transactions.");
                $("button.dontFindBank").trigger("click");
              } else if (data != 'SingleAccount') {
                $('.section-15, .section-4').hide();
                $('.section-15a').fadeIn();
                $('.account_select').html(data);
              } else {
                $.ajax({
                  type: 'POST',
                  url: 'ajaxcreateleadsf.php',
                  data: { Step_Number: 9 },
                  success: function(data) {
                    $('.section-15, .section-4').hide();
                    $('.section-16').fadeIn();
                    $('.Progress_Status')
                      .html('100%')
                      .css({ width: '100%' });
                  }
                });
              }
            }
          });
        }
      }
    });
  }, 2000);
}

function UseThisButtonWhenUserSelectAccount() {
  if ($('input[name=CHECKING]').is(':checked')) {
    $('.Continue_upload_loading-files').fadeIn();
    var AccountNbr = $('input[name=CHECKING]:checked').val();
    $.ajax({
      type: 'POST',
      url: '/DecisionLogic/decisionlogic.php',
      data: { requesttype: 'SelectedAccountTrans', AccountNbr: AccountNbr },
      success: function(data) {
        $.ajax({
          type: 'POST',
          url: 'ajaxcreateleadsf.php',
          data: { Step_Number: 9 },
          success: function(data) {
            $('.Continue_upload_loading-files').fadeOut();
            $('.section-15a, .section-4').hide();
            $('.section-16').fadeIn();
            $('.Progress_Status')
              .html('100%')
              .css({ width: '100%' });
          }
        });
      }
    });
  } else {
    alert('Please Select Atleast One Account');
  }
}

$('#BankSearch').on('keyup', function() {
  var str1 = $(this)
    .val()
    .split(' ')
    .join('');
  str1 = str1.length;
  if (str1 > 2) {
    $('.onload-list , .search-msg').fadeOut();
    $('#BankSearchData , .spiner-wrap').fadeIn();
    $.ajax({
      type: 'POST',
      url: '/DecisionLogic/decisionlogic.php',
      data: { requesttype: 'SearchBanksByBankName', keywordBank: $(this).val() },
      success: function(data) {
        var data = JSON.parse(data);
        var SearchBankResp = data.SearchBanksByBankNameResult.BanksSearchResult;
        console.log(SearchBankResp);
        var sample = '<ul>';
        var sizeof = 12;
        if (SearchBankResp.length < 11) {
          sizeof = SearchBankResp.length;
        } else if (!SearchBankResp.length) {
          sizeof = 1;
        }
        console.log(sizeof);
        if (sizeof == 1) {
          sample +=
            "<li class='clearfix' onclick='RCCodeAjax(`" +
            SearchBankResp['ContentServiceId'] +
            "`)' > <img src='https://widget.decisionlogic.com/ImageHandler.ashx?ContentServiceId=" +
            SearchBankResp['ContentServiceId'] +
            "' /> " +
            SearchBankResp['ContentServiceDisplayName'] +
            '</li>';
        } else {
          for (var i = 0; i < sizeof; i++) {
            sample +=
              "<li class='clearfix' onclick='RCCodeAjax(`" +
              SearchBankResp[i]['ContentServiceId'] +
              "`)' > <img src='https://widget.decisionlogic.com/ImageHandler.ashx?ContentServiceId=" +
              SearchBankResp[i]['ContentServiceId'] +
              "' /> " +
              SearchBankResp[i]['ContentServiceDisplayName'] +
              '</li>';
            //sample+="<li onclick='RCCodeAjax(`"+SearchBankResp[i]+"`)' > "+SearchBankResp[i]['ContentServiceDisplayName']+"</li>";
          }
        }
        sample += '</ul>';

        $('.spiner-wrap').fadeOut();
        $('#BankSearchData').html(sample);
      }
    });
  } else {
    $('.onload-list').fadeIn();
    $('#BankSearchData')
      .fadeOut()
      .html('');
    $('.search-msg').fadeIn();

    setTimeout(function() {
      $('.search-msg').fadeOut();
    }, 5000);
  }
});

$('#backtosearch').click(function() {
  $('#iframeRC').hide();
  $('#backtosearch').hide();
  $('.bank-search').show();
});

$('.dontFindBank').click(function() {
  $('.Link_bank_wrapper').fadeOut();
  $('.Upload_wrap').fadeIn();
});

$('.FindBank').click(function() {
  $('.Link_bank_wrapper').fadeIn();
  $('.Upload_wrap').fadeOut();
});

/* Decision Logic Ends */


/*Tax Return Upload Start*/
$('.tax-return-btn').click(function() {
  $('.Continue_upload_loading-files').fadeIn();
  var formData = new FormData(document.querySelector('#pdfaws'));
  $.ajax({
    //url: 'tax-return.php?sfID='+$(this).attr('sfid'),
    type: 'POST',
    data: formData,
    processData: false,
    contentType: false,
    success: function(result) {
      $('.Continue_upload_loading-files').fadeOut();
      $('.section-15').fadeOut();
      $('.section-16').fadeIn();
      
    }
  });
});

$('#taxReturnFileUpload').on('change', function() {

  var taxReturnFileUpload = $('#taxReturnFileUpload').val();

  if (taxReturnFileUpload != '') {
    $('.tax-return-btn').removeClass('btn_disable');
  } else {
    $('.tax-return-btn').addClass('btn_disable');
  }


  if ($(this).val != '') {
    var fileExtension = ['pdf'];
    $('.taxReturnFileUpload').addClass('added');
    if (
      $.inArray(
        $(this)
          .val()
          .split('.')
          .pop()
          .toLowerCase(),
        fileExtension
      ) == -1
    ) {
      alert("Only 'pdf' format is allowed");
      $('.taxReturnFileUpload').removeClass('added');
      $('#taxReturnFileUpload').val('');
      $('.tax-return-btn').addClass('btn_disable');
    }
  } else {
    $('.taxReturnFileUpload').removeClass('added');
  }
});
/*Tax Return Upload End*/