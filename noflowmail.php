<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);
date_default_timezone_set("America/Toronto");
require_once("config/Ses.php");
require_once "config/mysql.class.php";
$database = new DataBasePDO();
$sql = 'Select * FROM `personal_submissions` WHERE Step_Number <"p5" and Step_Number >"p2"  and email_status=0 and StartedDate like"'.date("Y-m-d",strtotime("-1 days")).'%"';
$result = $database->getAllResults($sql);
foreach ($result as $resultvalue) {
  $email=$resultvalue['email'];
  $link=$_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."?key=".$resultvalue['nokey'];
  mailses($link,$email,$resultvalue['ID']);
}
function mailses($link,$email,$id){
	$database = new DataBasePDO();
	$text='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="format-detection" content="telephone=no"/>
<title>TigerFunding-You got an offer</title>
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,300|Roboto:400,300,700&subset=latin,cyrillic,greek" rel="stylesheet" type="text/css">
<style type="text/css">


    .ReadMsgBody { width: 100%; background-color: #ffffff;}
    .ExternalClass {width: 100%; background-color: #ffffff;}
    .ExternalClass, .ExternalClass p, .ExternalClass span,
    .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
    #outlook a{ padding:0;}
    html{width:100%;}
    body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    table {mso-table-lspace:0pt; mso-table-rspace:0pt; border-spacing:0 !important;}
    table td {border-collapse:collapse;}
    table p{margin:0;}
    br, strong br, b br, em br, i br { line-height:100%; }
    div, p, a, li, td { -webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
    h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
    span a ,a { text-decoration: none !important;}
    img{height: auto !important; line-height: 100%; outline: none; text-decoration: none !important;  -ms-interpolation-mode:bicubic; }
    .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited,
    .yshortcuts a:hover, .yshortcuts a span { text-decoration: none !important; border-bottom: none !important;}
    /*mailChimp class*/
    .default-edit-image{
    height:20px;
    }
    ul{padding-left:10px; margin:0;}
    .tpl-repeatblock {
    padding: 0px !important;
    border: 1px dotted rgba(0,0,0,0.2);
    }
    .tpl-content{
    padding:0px !important;
    }


    @media only screen and (max-width: 640px){
    /* mobile setting */
    .container{width:90%!important; max-width:90%!important; min-width:90%!important;
    padding-left:20px!important; padding-right:20px!important; text-align: center!important; clear: both;}
    .full-width{width:100%!important; max-width:100%!important; min-width:100%!important; clear: both;}
    .full-width-center {width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    .force-240-center{width:240px !important; clear: both; margin:0 auto; float:none;}
    .auto-center {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    .auto-center-all{width: auto!important; max-width:75%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    .auto-center-all * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    .col-3,.col-3-not-full{width:30.35%!important; max-width:100%!important;}
    .col-2{width:47.3%!important; max-width:100%!important;}
    .full-block{width:100% !important; display:block !important; clear: both; padding-top:10px; padding-bottom:10px;}
    /* image */
    .image-full-width img{width:100% !important; height:auto !important; max-width:100% !important;}
    /* helper */
    .space-w-20{width:3.57%!important; max-width:20px!important; min-width:3.5% !important;}
    .space-w-20 td:first-child{width:3.5%!important; max-width:20px!important; min-width:3.5% !important;}
    .space-w-25{width:4.45%!important; max-width:25px!important; min-width:4.45% !important;}
    .space-w-25 td:first-child{width:4.45%!important; max-width:25px!important; min-width:4.45% !important;}
    .space-w-30 td:first-child{width:5.35%!important; max-width:30px!important; min-width:5.35% !important;}
    .fix-w-20{width:20px!important; max-width:20px!important; min-width:20px!important;}
    .fix-w-20 td:first-child{width:20px!important; max-width:20px!important; min-width:20px !important;}
    .h-10{display:block !important;  height:10px !important;}
    .h-20{display:block !important;  height:20px !important;}
    .h-30{display:block !important; height:30px !important;}
    .h-40{display:block !important;  height:40px !important;}
    .remove-640{display:none !important;}
    .text-left{text-align:left !important;}
    .clear-pad{padding:0 !important;}
    }
    @media only screen and (max-width: 479px){
    /* mobile setting */
    .container{width:90%!important; max-width:90%!important; min-width:124px!important;
    padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
    .full-width,.full-width-479{width:100%!important; max-width:100%!important; min-width:124px!important; clear: both;}
    .full-width-center {width: 100%!important; max-width:100%!important; min-width:124px!important; text-align: center!important; clear: both; margin:0 auto; float:none;}
    .auto-center-all{width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    .auto-center-all * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
    .col-3{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
    .col-3-not-full{width:30.35%!important; max-width:100%!important; }
    .col-2{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
    .full-block-479{display:block !important; width:100% !important; clear: both; padding-top:10px; padding-bottom:10px; }
    /* image */
    .image-full-width img{width:100% !important; height:auto !important; max-width:100% !important; min-width:124px !important;}
    .image-min-80 img{width:100% !important; height:auto !important; max-width:100% !important; min-width:80px !important;}
    .image-min-100 img{width:100% !important; height:auto !important; max-width:100% !important; min-width:100px !important;}
    /* halper */
    .space-w-20{width:100%!important; max-width:100%!important; min-width:100% !important;}
    .space-w-20 td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
    .space-w-25{width:100%!important; max-width:100%!important; min-width:100% !important;}
    .space-w-25 td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
    .space-w-30{width:100%!important; max-width:100%!important; min-width:100% !important;}
    .space-w-30 td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
    .remove-479{display:none !important;}
    img{max-width:280px !important;}
    .resize-font, .resize-font *{font-size: 37px !important; line-height: 48px !important;}
    }


    @media only screen and (max-width:640px){
    .full-width{width:90%!important; float:none!important; min-width:90%!important; margin:0 auto!important; max-width:90%!important; }
    body > .full-width{width:100%!important; float:none!important; min-width:100%!important; margin:0 auto!important; max-width:100%!important; }
    .full-block{display:block!important;}
    .image-full-width,
    .image-full-width img{width:100%!important; height:auto!important; max-width:100%!important;}
    .full-width.fix-800{min-width:auto!important;}
    }
    @media only screen and (max-width:480px){
    .full-width{width:90%!important; float:none!important; min-width:90%!important; margin:0 auto!important; max-width:90%!important;}
    body > .full-width{width:100%!important; float:none!important; min-width:100%!important; margin:0 auto!important; max-width:100%!important;}
    .full-block{display:block!important;}
    .image-full-width,
    .image-full-width img{width:100%!important; height:auto!important; max-width:100%!important;}
    .full-width.fix-800{min-width:auto!important;}
    }

    td ul{list-style: initial; margin:0; padding-left:20px;}body{background-color:#efefef;} .default-edit-image{height:20px;} tr.tpl-repeatblock , tr.tpl-repeatblock > td{ display:block !important;} .tpl-repeatblock {padding: 0px !important;border: 1px dotted rgba(0,0,0,0.2);}a img{ border: 0 !important;}
a:active{color:initial } a:visited{color:initial }
.tpl-content{padding:0 !important;}
</style>
<!--[if mso]>
<style>
[class~="full-width"]{width:95%; float:none; min-width:95%; margin:0 auto; max-width:95%;}
[class~="full-block"]{display:block;}
[class~="image-full-width"],[class~="image-full-width"] img{width:100%; height:auto; min-width:100%;}
</style>
<![endif]-->
<!--[if gte mso 15]>
<style type="text/css">
a{text-decoration: none !important;}
body { font-size: 0; line-height: 0; }
tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }
table { font-size:1px; line-height:0; mso-margin-top-alt:1px; }
body,table,td,span,a,font{font-family: Arial, Helvetica, sans-serif !important;}
a img{ border: 0 !important;}
</style>
<![endif]-->
<!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
</head>
<body  style="font-size:12px; width:100%; height:100%;">
<table id="mainStructure" class="full-width" width="800" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color: #efefef; max-width: 800px; outline: rgb(239, 239, 239) solid 1px; box-shadow: rgb(224, 224, 224) 0px 0px 30px 5px; margin: 0px auto;"><!--START LAYOUT-1 ( View Online / Call )--><tr><td align="center" valign="top" class="container" style="background-color: #EEEEEE;" bgcolor="#eeeeee">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #eeeeee; padding-left: 20px; padding-right: 20px; max-width: 600px; margin: 0px auto;"><tr><td valign="top">
                  <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width" style="max-width: 560px; margin: 0px auto;"><!-- start space --><tr><td valign="top" height="1" style="height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                      </td>
                    </tr><!-- end space --><tr><td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: 0px auto;"><tr><td valign="middle">

                              <table width="auto" align="left" border="0" cellspacing="0" cellpadding="0" class="full-width-center" style="mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td valign="top" align="center" dup="0">
                                    <table width="auto" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tr><td align="left" valign="middle" width="auto">
                                          <table width="auto" align="left" border="0" cellpadding="0" cellspacing="0" style="vertical-align:middle;mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td align="left" valign="top" style="padding-right: 10px; width: 15px;" width="15">
                                                </td>
                                            </tr></table></td>
                                        <td align="left" style="font-size: 14px; color: #ffffff; font-weight: normal; font-family: \'Open Sans\', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 22px;"><span style="line-height: 22px; font-size: 14px; font-weight: 400; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><font face="\'Open Sans\', Arial, Helvetica, sans-serif"><span style="color: #808080; line-height: 20px; font-size: 12px; font-weight: 400; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><font face="\'Open Sans\', Arial, Helvetica, sans-serif">Continue your application</font></span><br style="font-size: 14px; font-weight: 400; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"></font></span></td>
                                      </tr></table></td>
                                </tr></table><!--[if (gte mso 9)|(IE)]></td><td valign="top" ><![endif]--><table width="20" border="0" cellpadding="0" cellspacing="0" align="left" class="full-width" style="height: 1px; border-spacing: 0px; max-width: 20px;mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr><td height="1" class="h-20" style="height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                  </td>
                                </tr></table><!--[if (gte mso 9)|(IE)]></td><td valign="top" ><![endif]--><table width="auto" align="right" border="0" cellpadding="0" cellspacing="0" class="auto-center" style="mso-table-lspace:0pt; mso-table-rspace:0pt;"><tr>
                                </tr></table></td>
                          </tr></table></td>
                    </tr><!-- start space --><tr><td valign="top" height="1" style="height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                      </td>
                    </tr><!-- end space --></table></td>
              </tr></table><!-- end container --></td>
        </tr><!--END LAYOUT-1 ( View Online / Call )--><!--START LAYOUT-2 ( LOGO / MENU )--><tr><td align="center" valign="top" class="container" style="background-color: #ffffff;" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; padding-left: 20px; padding-right: 20px; max-width: 600px; margin: 0px auto;"><tr><td valign="top">
                  <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="margin: 0px auto;"><!-- start space --><tr><td valign="top" height="11" style="height: 11px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                      </td>
                    </tr><!-- end space --><tr><td valign="top">
                        <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 0px auto;"><!-- start image --><tr><td valign="top" align="center" width="200" style="width: 200px;">
                              <img src="http://18.216.20.33/images/20170720010608_Tiger_Funding_Logo.png" width="200" alt="set6-iogo-footer" style="max-width: 240px; display: block !important; width: 200px; height: auto;" border="0" hspace="0" vspace="0" height="auto"></td>
                          </tr><!-- end image--><!-- start space --><tr><td valign="top" height="12" style="height: 12px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                            </td>
                          </tr><!-- end space --></table></td>
                    </tr><!-- start menu --><!-- end menu --></table></td>
              </tr></table><!-- end container --></td>
        </tr><!--END LAYOUT-2 ( LOGO / MENU )--><!--START LAYOUT-6 ( BG IMAGE / TEXT / ICON )--><tr><td align="center" valign="top" class="container" style="background:url(http://mailbuild.rookiewebstudio.com/customers/yH0H4c8Y/user_upload/20170720005541_On_Your_Way.png) no-repeat top center/cover; background-color: #2b3239; background-repeat: no-repeat; background-position: 50% 0%, 50% 50%; background-size: cover;" background="http://mailbuild.rookiewebstudio.com/customers/yH0H4c8Y/user_upload/20170720005541_On_Your_Way.png" width="100%" height="100%" bgcolor="#2b3239">
            <!-- start container -->
            <!--[if gte mso 9]>             <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:800px; height:191px; background-repeat:no-repeat;">             <v:fill type="frame" color="#2b3239" src="http://mailbuild.rookiewebstudio.com/customers/yH0H4c8Y/user_upload/20170720005541_On_Your_Way.png" ></v:fill>                <v:textbox style="mso-fit-text-to-shape:true; v-padding-auto:true;" inset="0,0,0,0" >               <![endif]--><table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="max-width: 600px; margin: 0px auto;"><tr><td valign="top" align="center">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width" style="margin: 0px auto;"><!-- start space --><tr><td valign="top" height="19" style="height: 19px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --><tr><td valign="top" class="clear-pad" align="center" style="padding-left:20px; padding-right:20px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width-center" style="margin: 0px auto;"><!-- start title --><tr dup="0"><td valign="top" align="center">
                              <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width-center" style="margin: 0px auto;"><tr><td align="left" style="font-size: 18px; color: #ffffff; font-weight: bold; font-family: \'Open Sans\', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 26px;"><div style="text-align: center; font-size: 18px; font-weight: 700; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><br style="font-size: 18px; font-weight: 700; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"></div></td>
                                </tr><!-- start space --><tr><td valign="top" height="4" style="height: 4px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                  </td>
                                </tr><!-- end space --><tr><td align="left" style="font-size: 36px; color: #ffffff; font-weight: bold; font-family: \'Open Sans\', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 44px;"><div style="text-align: center; font-size: 36px; font-weight: 700; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><br style="font-size: 36px; font-weight: 700; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"></div></td>
                                </tr><tr><td align="left" style="font-size: 36px; color: #ffffff; font-weight: bold; font-family: \'Open Sans\', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 44px;"><div style="text-align: center; font-size: 36px; font-weight: 700; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><span style="color: #ffffff; line-height: 44px; font-size: 36px; font-weight: 700; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><font face="\'Open Sans\', Arial, Helvetica, sans-serif">You are so close!&nbsp;<span style="color: #ff9900; line-height: 44px; font-size: 36px; font-weight: 700; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"></span></font></span></div></td>
                                </tr><!-- start space --><tr><td valign="top" height="30" style="height: 30px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                </tr><!-- end space --></table></td>
                          </tr><!-- end title --><!-- start divider line  --><!-- end divider line --><!-- start social --><!-- end social --></table></td>
                    </tr><!-- start space --><tr><td valign="top" height="26" style="height: 26px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --></table></td>
              </tr></table><!--[if gte mso 9]>              </v:textbox>                </v:rect>               <![endif]--><!-- end container --></td>
        </tr><!--END LAYOUT-6 ( BG IMAGE / TEXT / ICON )--><!--START LAYOUT-9 ( CONTENT / BUTTON )  --><tr><td align="center" valign="top" class="container" style="background-color: #ffffff;" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; padding-left: 20px; padding-right: 20px; max-width: 600px; margin: 0px auto;"><tr><td valign="top">
                  <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="max-width: 560px; margin: 0px auto;"><!-- start space --><tr><td valign="top" height="44" style="height: 44px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --><tr><td valign="top">
                        <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 0px auto;"><!-- start content --><!-- end content --><!-- start content --><tr dup="0"><td valign="top">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: 0px auto;"><tr><td align="center" style="font-size: 14px; color: #888888; font-weight: normal; font-family: Roboto, Arial, Helvetica, sans-serif; word-break: break-word; line-height: 22px;"><span style="line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, Arial, Helvetica, sans-serif;"><font face="Roboto, Arial, Helvetica, sans-serif"><span style="color: #000000; line-height: 32px; font-size: 24px; font-weight: 400; font-family: Roboto, Arial, Helvetica, sans-serif;"><font face="Roboto, Arial, Helvetica, sans-serif">Get funding by finishing your application, it takes less than 2 minutes!&nbsp;</font></span><br style="font-size: 14px; font-weight: 400; font-family: Roboto, Arial, Helvetica, sans-serif;"></font></span></td>
                                </tr><!-- start space --><tr><td valign="top" height="34" style="height: 34px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                </tr><!-- end space --></table></td>
                          </tr><!-- end content --><!-- start button --><tr><td valign="top" align="center">
                              <table width="auto" border="0" align="center" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tr><td valign="top" align="center" style="padding: 5px;" dup="0">
                                    <table width="auto" border="0" align="center" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tr><td valign="top" style="background-color: #1880f8; border-radius: 24px;" bgcolor="#1880f8">
                                    <table width="auto" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tr><td width="auto" align="center" valign="middle" height="42" style="min-width: 80px; font-size: 14px; font-family: Roboto, Arial, Helvetica, sans-serif; color: #ffffff; font-weight: normal; padding-left: 25px; padding-right: 25px; word-break: break-word; background-clip: padding-box; line-height: 22px;"><a href="'.$link.'"><span style="color: #ffffff; line-height: 27px; font-size: 19px; font-weight: 700; font-family: Roboto, Arial, Helvetica, sans-serif;"><font face="Roboto, Arial, Helvetica, sans-serif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;FINISH APPLICATION&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</font></span></a></td>
                                      </tr></table></td>
                                </tr></table></td>
                                </tr></table></td>
                          </tr><!-- end button --></table></td>
                    </tr><!-- end content --><!-- start space --><tr><td valign="top" height="41" style="height: 41px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --></table></td>
              </tr></table><!-- end container --></td>
        </tr><!--END LAYOUT-9 ( CONTENT / BUTTON ) --><!-- START ORDER-TABLE --><tr><td valign="top" align="center" style="background-color: #eeeeee;" bgcolor="#eeeeee">
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="background-color: #eeeeee; max-width: 600px; margin: 0px auto;"><tr><td valign="top" align="center">
                  <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="max-width: 560px; margin: 0px auto;"><!-- start title --><!-- end title --><tr dup="0"><td valign="top" align="center">
                        <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 0px auto;"><!-- start space --><tr><td valign="top" height="15" style="height: 16px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                            </td>
                          </tr><!-- end space --><tr><td valign="top">
                              <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color: #e1edf4; margin: 0px auto;"></table></td>
                          </tr><!-- start space --><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                          </tr><!-- end space --></table></td>
                    </tr><tr dup="0"><td valign="top" align="center">
                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tr dup="0"><td valign="top" align="center">
                              <table width="95%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tr><td align="center" style="font-size: 14px; color: #888888; font-weight: normal; font-family: \'Open Sans\', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 22px;"><div style="text-align: left; font-size: 14px; font-weight: 400; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><span style="line-height: 20px; font-size: 12px; font-weight: 400; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><font face="\'Open Sans\', Arial, Helvetica, sans-serif">By clicking the button above, you are providing us written consent to share the information you previously provided us, including your personally identifying information with PersonalLoans.com in order to fulfill your request for personal financing options.<br style="font-size: 12px; font-weight: 400; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><br style="font-size: 12px; font-weight: 400; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"></font></span></div><div style="text-align: left; font-size: 14px; font-weight: 400; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><span style="line-height: 20px; font-size: 12px; font-weight: 400; font-family: \'Open Sans\', Arial, Helvetica, sans-serif;"><font face="\'Open Sans\', Arial, Helvetica, sans-serif">You are receiving this email because you opt-in&nbsp;via our website for business or personal funding.</font></span></div></td>
                                </tr><!-- start space --><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                </tr><!-- end space --></table></td>
                          </tr><tr><td valign="top" align="center">
                              <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><!--start button--><tr><td valign="top" align="center">
                                    <table width="auto" align="center" border="0" cellspacing="0" cellpadding="0" style="margin: 0px auto;"><tr><!-- start duplicate button -->
                                        <!-- end duplicate button -->
                                      </tr></table></td>
                                </tr><!--end button--></table></td>
                          </tr><!-- start space --><tr><td valign="top" height="30" style="height: 30px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                          </tr><!-- end space --></table></td>
                    </tr></table></td>
              </tr></table></td>
        </tr><!-- END ORDER-TABLE --><!--START LAYOUT-17 ( FOOTER MENU ) --><tr><td align="center" valign="top" class="container" style="background-color: #202122;" bgcolor="#202122">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #202122; max-width: 600px; margin: 0px auto;"><tr><td valign="top" class="clear-pad" style="padding-left:20px; padding-right:20px;">
                  <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="margin: 0px auto;"><!-- start space --><tr><td valign="top" height="29" style="height: 29px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --><tr dup="0"><td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: 0px auto;"><!-- start rights reserved --><tr><td align="center" style="font-size: 13px; color: #333333; font-weight: normal; font-family: Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif; word-break: break-word; line-height: 21px;"><span style="color: #333333; line-height: 21px; font-size: 13px; font-weight: 400; font-family: Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif;"><font face="Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif"><span style="color: #ffffff; line-height: 21px; font-size: 13px; font-weight: 400; font-family: Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif;"><font face="Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif">Copyright © 2018 Tiger Funding LLC. All rights reserved.</font></span><br style="font-size: 13px; font-weight: 400; font-family: Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif;"><span style="color: #ffffff; line-height: 21px; font-size: 13px; font-weight: 700; font-family: Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif;"><font face="Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif"><span style="text-decoration: none; line-height: 21px; font-size: 13px; font-weight: 700; font-family: Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif;"><font face="Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif"><a href="https://apply.tigerfundingllc.com/unsubscribe.php" data-mce-href="#" style="color: #ffffff; text-decoration: none !important; border-style: none; line-height: 21px; font-size: 13px; font-weight: 700; font-family: Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif;" border="0"><font face="Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif"><span style="color: #ffffff; line-height: 21px; font-size: 13px; font-weight: 700; font-family: Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif;"><font face="Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif">Unsubscribe</font></span></font></a>&nbsp;</font></span></font></span><br style="font-size: 13px; font-weight: 400; font-family: Roboto, \'Open Sans\', Arail, Tahoma, Helvetica, Arial, sans-serif;"></font></span></td>
                          </tr><!-- end rights reserved --><!-- start space --><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                            </tr><!-- end space --></table></td>
                    </tr><!-- start space --><tr><td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                    </tr><!-- end space --></table></td>
            </tr></table><!-- end container --></td>
      </tr><!--END LAYOUT-17 ( FOOTER MENU ) --></table></body>
</html>';
    $subject='Continue your application';
    $region_endpoint = "email.us-west-2.amazonaws.com";
    $ses = new SimpleEmailService('AKIAIGK5NS4NTUCG5IFA', 'AcjVJXd5zMJuYKbzkfk52B8ZYoulg88koYGxAcSv',$region_endpoint);
    $m = new SimpleEmailServiceMessage();
    $m->addTo($email);
    $m->setFrom('support@credsee.com ');
    $m->setSubject($subject);
    $m->setMessageFromString($text, $text);
    $mail=$ses->sendEmail($m);
    if(!empty($mail)){
        $UserData['email_status']=1;
        $whereAr =array("ID"=>$id);
        $database->updateData('personal_submissions',$UserData,$whereAr);
    }
}
?>
