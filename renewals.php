<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("America/Toronto");

define('fullpath', $_SERVER['DOCUMENT_ROOT'] . '/');
// define('fullpath', '/Applications/MAMP/htdocs/apply.kcf/');
require_once fullpath . 'utils.php';
require_once fullpath . 'soapclient1/SforcePartnerClient.php';

$database = new DataBasePDO();
$UtilsClass = new UtilsClass();
$urlkey = $_GET['campaignkey'];

if($_GET['campaignStatus']=='mailemail'){
  $UtilsClass::redirectMe('/renewals-landing/?email');
}

if (empty($urlkey)) {
  // $UtilsClass::redirectMe('http://localhost:8888/apply.kcf/renewals-landing/');
  $UtilsClass::redirectMe('/renewals-landing/');
}



$sqlsales = sprintf(
  "SELECT * FROM campaign WHERE campaign_key = '%s'",
  $database->escape($urlkey)
);
$resultSAles=$database->getOneRow($sqlsales);
if (empty($resultSAles)) {
  if($_GET['campaignStatus']=='email'){
    $UtilsClass::redirectMe('/renewals-landing/?email');
  }else{
    $UtilsClass::redirectMe('/renewals-landing/?gateway');
  }

}

$nameToShow = $resultSAles['firstname'] ? $resultSAles['firstname'] : 'there';

if (!empty($resultSAles)) {
  if (preg_match('#^00Q#', $resultSAles['salesForceId']) === 1) {
    $idfor='WhoId';
    $RenewalC="Renewal Lead Clicked";
  } else {
    $idfor='WhatId';
    $RenewalC="Renewal Application Clicked";
  }

  $smsOrEmail='';
  $percentage='';
  $userData['status']=$resultSAles['status'];
  if($_GET['campaignStatus']=='sms'){
    $smsOrEmail='SMS';
    $userData['smsStatus'] = 3;
    $percentage=$_GET['percentage'];
  }elseif($_GET['campaignStatus']=='email'){
    $smsOrEmail='Email';
    $userData['emailStatus'] = 3;

    if($resultSAles['Above65Status']==2){
      $percentage='sixty';
    }

    if($resultSAles['Above80Status']==2){
      $percentage='eighty';
    }

    if($resultSAles['Above95Status']==2){
      $percentage='ninty';
    }

    if($resultSAles['Above100Status']==2){
      $percentage='hundred';
    }
  }elseif($_GET['campaignStatus']=='CF_PaidRenewal'){
    $smsOrEmail='Email '.$_GET['utm_campaign'];
    if($resultSAles['Above65Status']==2){
      $percentage='sixty';
    }

    if($resultSAles['Above80Status']==2){
      $percentage='eighty';
    }

    if($resultSAles['Above95Status']==2){
      $percentage='ninty';
    }

    if($resultSAles['Above100Status']==2){
      $percentage='hundred';
    }
    $resultSAles['campaignStatus']=$_GET['campaignStatus'];
    $resultSAles['utm_campaign']=$_GET['utm_campaign'];
  }

  if($percentage=='sixty'){
    $userData['Above65Status'] = 3;
  }elseif($percentage=='eighty'){
    $userData['Above80Status'] = 3;
  }elseif($percentage=='ninty'){
    $userData['Above95Status'] = 3;
  }elseif($percentage=='hundred'){
    $userData['Above100Status'] = 3;
  }
  //print_r($smsOrEmail);
  $msg = "<div>Merchant ".$resultSAles['firstname']." has clicked the link.</div>";
  $msg .= "<div><a href='https://na48.salesforce.com/" . $resultSAles['salesForceId'] . "'>" . "Salesforce Link" . "</a></div>";
  $UtilsClass->sendEmail('it@knightcapitalfunding.com', $resultSAles['ownerEmail'], $RenewalC, wordwrap($msg, 70, "\r\n"));
  $subject='Renewal '. $smsOrEmail . ' Clicked';
  $UtilsClass->updateTask($idfor,$resultSAles['salesForceId'],$subject);
  echo "<script>var campaignInfo='".json_encode($resultSAles)."';</script>";

  $whereAr = array("campaign_key" => "'" . $urlkey . "'");
  $database->updateData('campaign', $userData, $whereAr);

  setcookie("campaign_key", $urlkey, strtotime("+1 year"), "/");
} else {
  echo "<script>
  window.onload = function(){
    $('.section-renewal-landing').fadeOut();
    $('.section-16').fadeIn();
    $('#renewals-progress-bar').width('100%');
    $('.renewals-progress__point-2').addClass('renewals-progress__point-active');
    $('.renewals-progress__point-3').addClass('renewals-progress__point-active');
  }
  </script>";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/custom.css">

  <title>Renew Your Funding With Knight Capital</title>
  <!-- HotJar tracking code starts -->
  <script>
  (function(h,o,t,j,a,r){
  h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
  h._hjSettings={hjid:1152432,hjsv:6};
  a=o.getElementsByTagName('head')[0];
  r=o.createElement('script');r.async=1;
  r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
  a.appendChild(r);
  })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
  </script>
  <!-- HotJar tracking code ends -->

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-P65TDQS');</script>
  <!-- End Google Tag Manager -->


  <meta property="og:title" content="Renew Your Funding With Knight Capital" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://apply.knightcapitalfunding.com/renewals.php" />
  <meta property="og:image" content="https://apply.knightcapitalfunding.com/images/apply-kcf-open-graph-banner.jpeg" />

  <?php /* This code is used to track the Users on Pardot.*/ ?>
  <script type="text/javascript">
    piAId = '183022';
    piCId = '23853';
    piHostname = 'pi.pardot.com';

    (function() {
      function async_load(){
        var s = document.createElement('script'); s.type = 'text/javascript';
        s.src = ('https:' == document.location.protocol ? 'https://pi' : 'http://cdn') + '.pardot.com/pd.js';
        var c = document.getElementsByTagName('script')[0]; c.parentNode.insertBefore(s, c);
      }
      if(window.attachEvent) { window.attachEvent('onload', async_load); }
      else { window.addEventListener('load', async_load, false); }
    })();
  </script>
</head>
<body class="page-body page-renewals">

  <div class="page-wrapper">
    <div class="Continue_upload_loading-files" style="display: none;">
       <!--<h4 style="padding-top: 18%;">Loading...</h4>-->
      <!--<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>-->
   <div id="loader" style="position: relative; margin-top: 20%; width: 250px; height: 250px; margin-right: auto; margin-left: auto;">
    <span class="loader__animation"></span>
    <span class="loader__text">Loading...</span>
    </div>

      <strong style="display:none" id="PhoneValidatingMessage">Validating Your Phone number</strong>
    </div>
    <div class="header">
      <div class="container header-container">
        <a class="site-logo" href="/">
          <img src="./images/white-kcf-logo.png" alt="Knight Capital Funding">
        </a>

        <div class="call-us">
          Call now: 855-462-4249
        </div>
      </div>
    </div>
    <!-- header -->

    <main class="content">
      <div class="container">

        <div class="renewals-progress">
          <div class="renewals-progress__point renewals-progress__point-active renewals-progress__point-1">
            Get Started
          </div>

          <div class="renewals-progress__point renewals-progress__point-2">
            Bank Verification
          </div>

          <div class="renewals-progress__point renewals-progress__point-3">
            Finished
          </div>

          <div id="renewals-progress-bar" class="renewals-progress__bar" style="width: 0%;"></div>
          <div class="renewals-progress__bar renewals-progress__bar--placeholder" style="width: 100%;"></div>
        </div>

        <div class="section-renewal-landing" style="display: none;">
          <div class="wrapper-700">
            <?php if ($resultSAles['offer_amount']): ?>
              <div class="funding-sub-head-3 funding-sub-head-3--renewal margin-bottom--0 padding-bottom--0">
                Hi <span class="renewal-username"><?php echo $nameToShow; ?></span>, based on your previous<br class="hidden-xs" />
                bank records we can provide you
              </div>

              <div class="text-orange text-center">
                <strong class="dl-proposed-amount">$<?php echo number_format($resultSAles['offer_amount'], 2);?></strong>
              </div>

              <div class="funding-sub-head-3 funding-sub-head-3--renewal margin-bottom--0 padding-bottom--0">
                Connect your business bank account now to verify.
              </div>
            <?php else: ?>
              <div class="text-center" style="font-size: 30px;">
                <div style="font-size: 40px;">
                  Hi <span class="renewal-username"><?php echo $nameToShow; ?></span>!
                </div>
                Congratulations you are currently eligible for a renewal. Connect your business bank account to verify now.
              </div>
            <?php endif; ?>

            <div class="yes-no-btns-wrapper text-center">
              <button class="btn-yes hover_ease" type="button">Continue</button>
              <!--<button class="btn-no hover_ease" type="button">No</button>-->
            </div>
          </div>
          <!-- wrapper-700 -->
        </div>
        <!-- section-renewal-landing -->

        <div class="section-renewal-dl-header margin-bottom--lg" style="display: none;">
          <div class="funding-sub-head-3 funding-sub-head-3--renewal margin-bottom--0 padding-bottom--0">
            Renew Funding Now
          </div>

          <div class="renewals-decision-logic-text">
            Renewing with Knight Capital Funding has never been easier.
            No need to send us statements, just select your bank, enter your credentials and get qualified today.
          </div>
        </div>
        <!-- section-renewal-dl-header -->

        <?php include 'section-decision-logic.php';?>

        <div id="#loader-third" style="display:none;position: relative; width: 250px; height: 250px; margin-right: auto; margin-left: auto;"></div>

        <div class="section-renewal-ty-yes" style="display: none;">
          <div class="wrapper-500">
            <div class="thank-you-wrapper">
              <h2 class="thank-you-head thank-you-head--borderless">
                Thank You
              </h2>

              <div class="thank-you-desc">
                We have received your submission and a Renewal Specialist will be reaching out shortly.
                Please contact us if you have any questions.
                We look forward to continuing to service your business.
              </div>
            </div>
          </div>
        </div>
        <!-- section-renewal-ty-yes -->

        <div class="section-renewal-ty-no" style="display: none;">
          <div class="wrapper-500">
            <div class="thank-you-wrapper">
              <h2 class="thank-you-head thank-you-head--borderless">
                Thank You
              </h2>

              <div class="thank-you-desc">
                We know you are busy building your business.
                We want to help fuel your growth.
                If you change your mind please reach out to us 855-462-4249.
              </div>
            </div>
          </div>
        </div>
        <!-- section-renewal-ty-no -->

      </div>
      <!-- container -->
    </main>

  </div>
  <!-- page-wrapper -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="./js/renewal.js?v=<?= rand() ?>"></script>
</body>
</html>
<?php
  if($resultSAles['status']==1){
    echo '
    <script>
      $(".section-renewal-landing").hide();
      $(".section-renewal-ty-yes").show();
      $("#renewals-progress-bar").width("100%");
      $(".renewals-progress__point-2,.renewals-progress__point-3").addClass("renewals-progress__point-active");
    </script>';
  }else{
    echo '
    <script>
      $(".section-renewal-landing").show();
    </script>';
  }
?>

<script>
$(document).ready(function(){

  var size = $(window).width();
  if(size <=767.98) {
      $(".renewals-progress").css("width", "300px");
      $(".renewals-progress").css("display", "block");
  }// End of if
});//End of Document Ready

</script>