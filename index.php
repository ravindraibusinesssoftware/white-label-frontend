<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);
date_default_timezone_set("America/Toronto");
require_once "config/mysql.class.php";
$database = new DataBasePDO();
$urlkey=$_GET['key'];
$subdomain = array_shift((explode('.', $_SERVER['HTTP_HOST'])));

$site_logo = './images/white-kcf-logo.png';
$site_background_image = './images/openbanner.jpg';
$site_background_color = '';
if(!empty($subdomain) && $subdomain != $_SERVER['HTTP_HOST']){
  //$conn = mysqli_connect(DATABASE_SERVER,DATABASE_USER,DATABASE_PASSWORD);
  $username=DATABASE_USER;  
  $password=DATABASE_PASSWORD;  
  $hostname = DATABASE_SERVER;  
  //connection string with database  
  $dbhandle = mysqli_connect($hostname, $username, $password)  
  or die("Unable to connect to MySQL");  
  echo "";  
  // connect with database  
  $selected = mysqli_select_db($dbhandle, "affiliates")  
  or die("Could not select examples");  
  $sql = "select logoImage,bgImage,bgColor from users where  subdomain= '$subdomain';";
  $result = mysqli_query($dbhandle,$sql);  
  $json_response = array();
  $row_array = array();  
  // fetch data in array format  
  while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {  
    // Fetch data of Fname Column and store in array of row_array
    $site_logo = (isset($row['logoImage']))?$row['logoImage']:$site_logo;
    $site_background_image = (isset($row['bgImage']))?$row['bgImage']:$site_background_image;
    $site_background_color = (isset($row['bgColor']))?$row['bgColor']:$site_background_color;
    
    //Set cookie for subdomain
    setcookie("subdomain", $subdomain);
    
  }
//exit;
}


if (!empty($urlkey)) {
  $sqlsales="select * from personal_submissions where nokey='".$urlkey."'";
  $resultSAles=$database->getAllResults($sqlsales);
  if($resultSAles[0]['Step_Number']=='p5'){
    setcookie("Step_Number_personal", "", time() - 3600);
    setcookie("Id_personal", "", time() - 3600);
  }else{
    setcookie("Id_personal",$resultSAles[0]['ID'],strtotime("+1 year"));
    setcookie("Step_Number_personal",$resultSAles[0]['Step_Number'],strtotime("+1 year"));
  }
}

if(isset($_GET['applykey'])){
  $sqlsales="select * from submissions where applykey='".$_GET['applykey']."'";
  $applyKeySet=$database->getAllResults($sqlsales);
  setcookie("LeadId",$applyKeySet[0]['ID'],strtotime("+1 year"));
  setcookie("StepNumber",$applyKeySet[0]['step'],strtotime("+1 year"));
}

?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Google Tag Manager -->

  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-W3D2T8');</script>


<!-- End Google Tag Manager -->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

<meta property="og:title" content="Knight Capital Funding"/>
<meta property="og:image" content="https://apply.knightcapitalfunding.com/images/Open-banner-1.jpg"/>
<meta property="og:site_name" content="Knight Name"/>
<meta property="og:description" content="$5k-$250k Same Day Funding | Apply in Minutes | Get Money Today!"/>
<meta property="og:type" content="article"/>

<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Knight Capital Funding" />
<meta name="twitter:description" content="$5k-$250k Same Day Funding | Apply in Minutes | Get Money Today!" />
<meta name="twitter:image" content="https://apply.knightcapitalfunding.com/images/Open-banner-1.jpg" />
<meta name="twitter:url" content="http://apply.knightcapitalfunding.com/" />

<title>Knight Capital Funding</title>

<link href="images/favicon.png" rel="shortcut icon" type="image/x-icon">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/custom.css?v=<?= rand() ?>">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCfDJIOqbzGzNYOvjQpvJGyAUGkTLCjayg&libraries=places"></script>

<?php
if(strpos(strtolower($_SERVER['REQUEST_URI']),"personal_loan") !== false){
  ?>
  <script>
  $(document).ready(function(){
    $('.Sorry_Partner').fadeIn();
    $('.gobackno').css('display', 'none');
    $('.sp-section-1-btn').css({'width':'100%','margin':'0px'});
  });
</script>
<?php
}else{
  ?>
  <script>
  $(document).ready(function(){
    $('.normal_wrapper').removeClass('hidden');
  });
</script>
<?php
}
?>
<?php
$var=$_SERVER['SERVER_NAME'];
$domain = explode('.', $var);
$subdomain=$domain[0];
if($subdomain=='facebook'){
  ?>
  <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '599884780204895');
    fbq('track', 'PageView');
    function iAmAttilasEvent() {
      window._fbq.push('track', 'Start_Button_Clicked');
    }
  </script>
  <?php
}
?>
</head>

<body class="">

  <!-- Google Tag Manager (noscript) -->

  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3D2T8"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

    <!-- End Google Tag Manager (noscript) -->

    <!-- Confirmation Popup -->

    <div class="confirmation" style="display: none;">
      <div class="confirmation_content">
        <div class="wrapper-700">
          <h4 class="funding-sub-head-3"><span>You already have an application with us.</span></h4>
          <a style="font-size: 17px;" href="#" class="confirmation_yes hover_ease">Create New App</a>
          <a style="font-size: 17px;" href="#" class="confirmation_no hover_ease">Continue Old App</a>
        </div>
      </div>
    </div>



    <div class="Continue_upload_loading-files" style="display: none;">
      <h4 style="padding-top: 18%;">Loading...</h4><i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
      <bold style="display:none" id="PhoneValidatingMessage">Validating Your Phone number</bold>
    </div>

    <!-- Confirmation Popup -->

    <header>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 clearfix col-sm-push-6">
            <div class="header_right pull-right">
              <a id="telNum" href="tel:8554624249">CALL NOW: 855-462-4249</a>
            </div>
          </div>
          <div class="col-sm-6 clearfix col-sm-pull-6">
            <div class="header_left pull-left">
              <h1 class="logo">
                <a href="">
                  <?php
                    if(!empty($subdomain) && $subdomain != $_SERVER['HTTP_HOST']){
                  ?>
                    <img src="<?php echo $site_logo;?>" width="220px" height="33px">
                  <?php 
                    }else{
                  ?>
                      <img src="./images/white-kcf-logo.png">
                  <?php     
                    }
                  ?>
                </a>
              </h1>
            </div>
          </div>

        </div>
      </div>
    </header>

<?php
  if(!empty($subdomain) && $subdomain != $_SERVER['HTTP_HOST']){
?>
    <div class="normal_wrapper hidden" style="  background: url('<?php echo $site_background_image; ?>') 0 0 / cover no-repeat fixed rgba(0, 0, 0, 0);">
<?php 
  }else{
?>
  <div class="normal_wrapper hidden">
<?php     
  }
?>


     <div class="container">
      <div class="section-1">
       <div class="do-u-own-wrapper">
        <div class="do-u-own">
          <?php require_once('sameday.php');?>
          <h3 class="funding-sub-head-2">Do you own a business?</h3>

          <div class="yes-no-btns-wrapper">
            <button class="btn-yes yes_btn hover_ease">Yes</button>
            <button class="btn-no no_btn hover_ease">No</button>
          </div>
        </div>
      </div>
    </div>

    <div class="section-2" style="display: none">
     <div class="wrapper-600">
      <h4 class="funding-sub-head-3"><span>What Is Your</span> <span>Average Monthly Sales?</span></h4>
      <div class="section-2-content">
        <ul class="row">
          <li class="col-sm-4 col-xs-6">
           <a href="#" class="eligible hover_ease" value="$0-$10,000">$0-$10k</a>
         </li>
         <li class="col-sm-4 col-xs-6">
           <a href="#" class="hover_ease eligible" value="$10,000-$25,000">$10k-$25k</a>
         </li>
         <li class="col-sm-4 col-xs-6">
           <a href="#" class="hover_ease eligible" value="$25,000-$50,000">$25-$50k</a>
         </li>
         <li class="col-sm-4 col-xs-6">
           <a href="#" class="hover_ease eligible" value="$50,000-$100,000">$50k-$100k</a>
         </li>
         <li class="col-sm-4 col-xs-6">
           <a href="#" class="hover_ease eligible" value="$100,000-$250,000">$100-$250k</a>
         </li>
         <li class="col-sm-4 col-xs-6">
           <a href="#" class="hover_ease eligible" value="$250,000+">$250k +</a>
         </li>
       </ul>
     </div>
   </div>
 </div>

 <div class="section-3" style="display: none;">
   <div class="wrapper-500">
    <h4 class="funding-sub-head-3"><span>Find Out How Much</span> <span>You Qualify For Now</span></h4>
    <div id="get-started">

      <div class="form-group">
       <label for="first_name">Owner First name:</label>
       <input id="first_name" name="first_name" type="text" class="form-control" placeholder="Enter Your First Name" required>
       <span class="error_text err_first_name_Num_Char" style="display: none;">Special Characters and Numbers are not allowed</span>
       <span class="error_text err_first_name_specialChar" style="display: none;">Special Charaters are not allowed</span>
       <span class="error_text err_first_name_Num" style="display: none;">Numbers are not allowed</span>
       <span class="error_text err_first_name_valid" style="display: none;">Please enter valid Firstname </span>
     </div>
     <div class="form-group">
       <label for="last_name">Owner Last Name:</label>
       <input  name="last_name"  type="text" class="form-control" id="last_name" placeholder="Enter Your Last Name"  required>
       <span class="error_text err_last_name_Num_Char" style="display: none;">Special Characters and Numbers are not allowed</span>
       <span class="error_text err_last_name_specialChar" style="display: none;">Special Charaters are not allowed</span>
       <span class="error_text err_last_name_Num" style="display: none;">Numbers are not allowed</span>
       <span class="error_text err_last_name_valid" style="display: none;">Please enter valid Lastname </span>
     </div>
     <div class="form-group">
       <label for="email">Email Address:</label>
       <input name="email" type="text" class="form-control" id="email" placeholder="Enter Your Email Address"  required>
       <span class="error_text err_email" style="display: none;">Please enter Valid Email Address</span>
     </div>
     <div class="form-group">
       <label for="phone">Best Number to Reach You:</label>
       <input onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false" id="phone"  pattern="[1-9]{1}[0-9]{9}"  minlength="14" maxlength="14" name="phone" size="20" type="tel" class="form-control phone_api" placeholder="Enter Your 10 Digit Phone Number"  required>
       <span class="error_text err_phone" style="display: none;">Please enter Valid Phone Number</span>
     </div>
     <div class="form-group legal-checkbox">
      <label class="checkbox-inline"><input value="" id="legal-one" type="checkbox"><p class="font12">By checking this box, you  are providing express "written” consent to receive telephone calls and messages electronically from us, via SMS messages (including text  messages), artificial or prerecorded voice messages and automatic dialing technology, at any telephone number that you have provided or may provide in the future (including any cellular telephone numbers) from Knight Capital, LLC or any of its affiliates, agents or authorized third parties, even if your telephone number is currently listed on any internal, corporate, state, federal or national Do-Not-Call (DNC) list. You understand that  you do not have to check this box in order to obtain the services of Knight Capital, LLC.</p></label>
      <span class="error_text err_legal_one" style="display: none;">Please Select the Checkbox</span>
    </div>
    <button type="submit" class="btn btn-default section_btn get-started hover_ease">Get Started</button>

  </div>
</div>
</div>

<div class="section-4" style="display: none;">
  <div class="wrapper-600">
    <h4 class="funding-sub-head-3"><span>What Type Of Business Entity?</span></h4>
    <div class="Progress_bar visible-xs" style="display: none;">
      <span class="Progress_Status"></span>
    </div>
    <div class="section-4-content">
      <ul class="row">
        <li class="col-xs-6">
          <a href="#" class="hover_ease Business_Entity" value="Corporation">Corporation</a>
        </li>
        <li class="col-xs-6">
          <a href="#" class="hover_ease Business_Entity" value="limited liability company">LLC</a>
        </li>
        <li class="col-xs-6">
          <a href="#" class="hover_ease Business_Entity" value="Partnership">Partnership</a>
        </li>
        <li class="col-xs-6">
          <a href="#" class="hover_ease Business_Entity" value="Sole Proprietor">Sole Proprietor</a>
        </li>
      </ul>
    </div>
  </div>
</div>


<div class="section-4a" style="display: none;">
  <div class="wrapper-500">
    <h4 class="funding-sub-head-3"><span>What % Of The Business </span><span>Do You Own?</span></h4>
    <div class="Progress_bar visible-xs" style="display: none;">
      <span class="Progress_Status"></span>
    </div>
    <div class="form-5">
      <div class="form-group">
        <label for="Ownership_percentage">Your Ownership%</label>
        <select class="form-control" id="Ownership_percentage" >
          <option value="">Select</option>
          <option value="100">100%</option>
          <option value="99">99%</option>
          <option value="98">98%</option>
          <option value="97">97%</option>
          <option value="96">96%</option>
          <option value="95">95%</option>
          <option value="94">94%</option>
          <option value="93">93%</option>
          <option value="92">92%</option>
          <option value="91">91%</option>
          <option value="90">90%</option>
          <option value="89">89%</option>
          <option value="88">88%</option>
          <option value="87">87%</option>
          <option value="86">86%</option>
          <option value="85">85%</option>
          <option value="84">84%</option>
          <option value="83">83%</option>
          <option value="82">82%</option>
          <option value="81">81%</option>
          <option value="80">80%</option>
          <option value="79">79%</option>
          <option value="78">78%</option>
          <option value="77">77%</option>
          <option value="76">76%</option>
          <option value="75">75%</option>
          <option value="74">74%</option>
          <option value="73">73%</option>
          <option value="72">72%</option>
          <option value="71">71%</option>
          <option value="70">70%</option>
          <option value="69">69%</option>
          <option value="68">68%</option>
          <option value="67">67%</option>
          <option value="66">66%</option>
          <option value="65">65%</option>
          <option value="64">64%</option>
          <option value="63">63%</option>
          <option value="62">62%</option>
          <option value="61">61%</option>
          <option value="60">60%</option>
          <option value="59">59%</option>
          <option value="58">58%</option>
          <option value="57">57%</option>
          <option value="56">56%</option>
          <option value="55">55%</option>
          <option value="54">54%</option>
          <option value="53">53%</option>
          <option value="52">52%</option>
          <option value="51">51%</option>
          <option value="50">50%</option>
          <option value="49">49%</option>
          <option value="48">48%</option>
          <option value="47">47%</option>
          <option value="46">46%</option>
          <option value="45">45%</option>
          <option value="44">44%</option>
          <option value="43">43%</option>
          <option value="42">42%</option>
          <option value="41">41%</option>
          <option value="40">40%</option>
          <option value="39">39%</option>
          <option value="38">38%</option>
          <option value="37">37%</option>
          <option value="36">36%</option>
          <option value="35">35%</option>
          <option value="34">34%</option>
          <option value="33">33%</option>
          <option value="32">32%</option>
          <option value="31">31%</option>
          <option value="30">30%</option>
          <option value="29">29%</option>
          <option value="28">28%</option>
          <option value="27">27%</option>
          <option value="26">26%</option>
          <option value="25">25%</option>
          <option value="24">24%</option>
          <option value="23">23%</option>
          <option value="22">22%</option>
          <option value="21">21%</option>
          <option value="20">20%</option>
          <option value="19">19%</option>
          <option value="18">18%</option>
          <option value="17">17%</option>
          <option value="16">16%</option>
          <option value="15">15%</option>
          <option value="14">14%</option>
          <option value="13">13%</option>
          <option value="12">12%</option>
          <option value="11">11%</option>
          <option value="10">10%</option>
          <option value="9">9%</option>
          <option value="8">8%</option>
          <option value="7">7%</option>
          <option value="6">6%</option>
          <option value="5">5%</option>
          <option value="4">4%</option>
          <option value="3">3%</option>
          <option value="2">2%</option>
          <option value="1">1%</option>
        </select>
      </div>
      <button type="submit" class="btn btn-default  section_btn  section-4a-next hover_ease btn_disable">Next</button>
    </div>
  </div>
</div>


<div class="section-4b" style="display: none;">
  <div class="wrapper-500">
    <h4 class="funding-sub-head-3"><span>Second Owner</span> <span>Information</span></h4>
    <div id="get-started">
      <div class="form-group">
        <label for="second_first_name"> Second Owner First Name:</label>
        <input id="second_first_name" name="second_first_name" type="text" class="form-control" placeholder="Enter Second Owner First Name" required>
        <span class="error_text err_first_name_Num_Char" style="display: none;">Special Characters and Numbers are not allowed</span>
        <span class="error_text err_first_name_specialChar" style="display: none;">Special Charaters are not allowed</span>
        <span class="error_text err_first_name_Num" style="display: none;">Numbers are not allowed</span>
        <span class="error_text err_first_name_valid" style="display: none;">Please enter valid Firstname </span>
      </div>
      <div class="form-group">
        <label for="second_last_name"> Second Owner Last Name:</label>
        <input  name="second_last_name"  type="text" class="form-control" id="second_last_name" placeholder="Enter Second Owner Last Name"  required>
        <span class="error_text err_last_name_Num_Char" style="display: none;">Special Characters and Numbers are not allowed</span>
        <span class="error_text err_last_name_specialChar" style="display: none;">Special Charaters are not allowed</span>
        <span class="error_text err_last_name_Num" style="display: none;">Numbers are not allowed</span>
        <span class="error_text err_last_name_valid" style="display: none;">Please enter valid Lastname </span>
      </div>
      <div class="form-group">
        <label for="second_email">Email Address:</label>
        <input name="second_email" type="text" class="form-control" id="second_email" placeholder="Enter Second Owner Email Address"  >
        <span class="error_text err_email" style="display: none;">Please enter Valid Email Address</span>
      </div>
      <div class="form-group">
        <label for="second_phone">Best Number to Reach Second Owner:</label>
        <input onCopy="return false" onDrag="return false" onDrop="return false" onPaste="return false" id="second_phone"  pattern="[1-9]{1}[0-9]{9}"  minlength="14" maxlength="14" name="second_phone" size="20" type="tel" class="form-control phone_api" placeholder="Enter Your 10 Digit Phone Number"  required>
        <span class="error_text err_phone" style="display: none;">Please enter Valid Phone Number</span>
      </div>
      <div class="form-group legal-checkbox">
        <label class="checkbox-inline"><input value="" id="second_legal-one" type="checkbox"><p class="font12">By checking this box, you  are providing express "written” consent to receive telephone calls and messages electronically from us, via SMS messages (including text  messages), artificial or prerecorded voice messages and automatic dialing technology, at any telephone number that you have provided or may provide in the future (including any cellular telephone numbers) from Knight Capital, LLC or any of its affiliates, agents or authorized third parties, even if your telephone number is currently listed on any internal, corporate, state, federal or national Do-Not-Call (DNC) list. You understand that  you do not have to check this box in order to obtain the services of Knight Capital, LLC.</p></label>
        <span class="error_text err_legal_one showerror" style="display: none;">Please Select the Checkbox</span>
      </div>
      <button type="submit" class="btn btn-default section_btn section-4b-next hover_ease">NEXT</button>
    </div>
  </div>
</div>


<div class="section-5" style="display: none;">
  <form id="form-info-about-business">
    <div class="wrapper-500">
      <h4 class="funding-sub-head-3"><span>Tell Us A Little More</span><span>About Your Business</span></h4>
      <div class="Progress_bar visible-xs" style="display: none;">
        <span class="Progress_Status"></span>
      </div>
      <div class="form-2">
        <div class="form-group">
          <label for="URL">Business Website - (Optional Field)</label>
          <input  type="text" class="form-control" id="URL" placeholder="www.example.com">
        </div>
        <div class="form-group">
          <label for="Legal_Name">Business Legal Name</label>
          <input type="text" class="form-control" id="Legal_Name" placeholder="Enter Your Legal Name" required>
        </div>
        <div class="form-group">
          <label for="Doing_Business">Doing Business As - (Common Name) </label>
          <input type="text" class="form-control" id="Doing_Business">
        </div>
        <button type="submit" class="btn btn-default section_btn section-5-next hover_ease btn_disable">Next</button>
      </div>
    </div>
  </form>
</div>


<div class="section-6" style="display: none;">
 <div class="wrapper-500">
  <h4 class="funding-sub-head-3"><span>What Is Your</span><span>Business Address?</span></h4>
  <div class="Progress_bar visible-xs" style="display: none;">
    <span class="Progress_Status"></span>
  </div>
  <div class="form-3">

    <div class="form-group">
     <label for="Street">Street Address</label>
     <input id="Street" class="form-control addressAPi" autocomplete="off" type="text" fieldsR='City,State,PostalCode' placeholder="Enter Your Street Name" />
   </div>

   <div class="row">
     <div class="col-sm-6">
      <div class="form-group">
        <label for="Suite">Apt or Suite Number</label>
        <input type="text" class="form-control" id="AptSuite" placeholder="Enter Your Apt number">
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label for="PostalCode">Zip Code</label>
        <input type="tel" class="form-control" id="PostalCode" placeholder="Enter Your 5 Digit Zip Code"  minlength="5" maxlength="5"  >
      </div>
      <div class="form-group hidden">
        <label for="State">State</label>
        <input type="text" class="form-control" id="State" placeholder="Enter Your State"  >
      </div>
      <div class="form-group hidden">
        <label for="City">City</label>
        <input type="text" class="form-control" id="City" placeholder="Enter Your City"  >
      </div>
    </div>

    <div class="col-sm-12">
      <div class="form-group">
        <label>Is this a home-based business?</label>
        <label class="checkbox-inline radio"><input value="YES" name="homebased" id="homebased1" type="radio"><p>YES, this is a home-based business</p></label>
        <label class="checkbox-inline radio"><input value="NO" name="homebased" id="homebased2" type="radio"><p>NO, this is Not a home-based business</p></label>
      </div>
    </div>

  </div>
  <button type="submit" class="btn btn-default section_btn section-6-next hover_ease btn_disable">Next</button>

</div>
</div>
</div>


<div class="section-7" style="display: none;">
 <div class="wrapper-500">
  <h4 class="funding-sub-head-3"><span>Please Select Your Gender</span></h4>
  <div class="Progress_bar visible-xs" style="display: none;">
    <span class="Progress_Status"></span>
  </div>
  <div class="section-7-content">
   <ul>
    <li>
     <a href="#" class="male hover_ease Gender_select" value="Male">
      <p>Male</p>
      <img src="./images/Male.png">
    </a>
  </li>
  <li>
   <a href="#" class="female hover_ease Gender_select" value="Female">
    <p>Female</p>
    <img src="./images/Female.png">
  </a>
</li>
</ul>
</div>
</div>
</div>


<div class="section-8" style="display: none;">
  <div class="wrapper-500">
    <h4 class="funding-sub-head-3"><span>When Is Your Birthday?</span></h4>
    <div class="Progress_bar visible-xs" style="display: none;">
      <span class="Progress_Status"></span>
    </div>
    <div class="form-4">

      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label for="dob_Year">Year </label>
            <select class="form-control" id="dob_Year">
              <option value="">Select</option>
              <option value="2000">2000</option>
              <option value="1999">1999</option>
              <option value="1998">1998</option>
              <option value="1997">1997</option>
              <option value="1996">1996</option>
              <option value="1995">1995</option>
              <option value="1994">1994</option>
              <option value="1993">1993</option>
              <option value="1992">1992</option>
              <option value="1991">1991</option>
              <option value="1990">1990</option>
              <option value="1989">1989</option>
              <option value="1988">1988</option>
              <option value="1987">1987</option>
              <option value="1986">1986</option>
              <option value="1985">1985</option>
              <option value="1984">1984</option>
              <option value="1983">1983</option>
              <option value="1982">1982</option>
              <option value="1981">1981</option>
              <option value="1980">1980</option>
              <option value="1979">1979</option>
              <option value="1978">1978</option>
              <option value="1977">1977</option>
              <option value="1976">1976</option>
              <option value="1975">1975</option>
              <option value="1974">1974</option>
              <option value="1973">1973</option>
              <option value="1972">1972</option>
              <option value="1971">1971</option>
              <option value="1970">1970</option>
              <option value="1969">1969</option>
              <option value="1968">1968</option>
              <option value="1967">1967</option>
              <option value="1966">1966</option>
              <option value="1965">1965</option>
              <option value="1964">1964</option>
              <option value="1963">1963</option>
              <option value="1962">1962</option>
              <option value="1961">1961</option>
              <option value="1960">1960</option>
              <option value="1959">1959</option>
              <option value="1958">1958</option>
              <option value="1957">1957</option>
              <option value="1956">1956</option>
              <option value="1955">1955</option>
              <option value="1954">1954</option>
              <option value="1953">1953</option>
              <option value="1952">1952</option>
              <option value="1951">1951</option>
              <option value="1950">1950</option>
              <option value="1949">1949</option>
              <option value="1948">1948</option>
              <option value="1947">1947</option>
              <option value="1946">1946</option>
              <option value="1945">1945</option>
              <option value="1944">1944</option>
              <option value="1943">1943</option>
              <option value="1942">1942</option>
              <option value="1941">1941</option>
              <option value="1940">1940</option>
              <option value="1939">1939</option>
              <option value="1938">1938</option>
              <option value="1937">1937</option>
              <option value="1936">1936</option>
              <option value="1935">1935</option>
              <option value="1934">1934</option>
              <option value="1933">1933</option>
              <option value="1932">1932</option>
              <option value="1931">1931</option>
              <option value="1930">1930</option>
              <option value="1929">1929</option>
              <option value="1928">1928</option>
              <option value="1927">1927</option>
              <option value="1926">1926</option>
              <option value="1925">1925</option>
              <option value="1924">1924</option>
              <option value="1923">1923</option>
              <option value="1922">1922</option>
              <option value="1921">1921</option>
              <option value="1920">1920</option>
              <option value="1919">1919</option>
              <option value="1918">1918</option>
              <option value="1917">1917</option>
              <option value="1916">1916</option>
              <option value="1915">1915</option>
              <option value="1914">1914</option>
              <option value="1913">1913</option>
              <option value="1912">1912</option>
              <option value="1911">1911</option>
              <option value="1910">1910</option>
              <option value="1909">1909</option>
              <option value="1908">1908</option>
              <option value="1907">1907</option>
              <option value="1906">1906</option>
              <option value="1905">1905</option>
              <option value="1904">1904</option>
              <option value="1903">1903</option>
              <option value="1902">1902</option>
              <option value="1901">1901</option>
              <option value="1900">1900</option>
            </select>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="form-group">
            <label for="dob_Month">Month:</label>
            <select class="form-control" id="dob_Month">
              <option value="">Select</option>
              <option value="01">January</option>
              <option value="02">February</option>
              <option value="03">March</option>
              <option value="04">April</option>
              <option value="05">May</option>
              <option value="06">June</option>
              <option value="07">July</option>
              <option value="08">August</option>
              <option value="09">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            </select>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label for="dob_Date">Date</label>
            <select class="form-control" id="dob_Date">
              <option value="">Select</option>
              <option  value="01">1</option>
              <option  value="02">2</option>
              <option  value="03">3</option>
              <option  value="04">4</option>
              <option  value="05">5</option>
              <option  value="06">6</option>
              <option  value="07">7</option>
              <option  value="08">8</option>
              <option  value="09">9</option>
              <option  value="10">10</option>
              <option  value="11">11</option>
              <option  value="12">12</option>
              <option  value="13">13</option>
              <option  value="14">14</option>
              <option  value="15">15</option>
              <option  value="16">16</option>
              <option  value="17">17</option>
              <option  value="18">18</option>
              <option  value="19">19</option>
              <option  value="20">20</option>
              <option  value="21">21</option>
              <option  value="22">22</option>
              <option  value="23">23</option>
              <option  value="24">24</option>
              <option  value="25">25</option>
              <option  value="26">26</option>
              <option  value="27">27</option>
              <option  value="28">28</option>
              <option  value="29">29</option>
              <option  value="30">30</option>
              <option  value="31">31</option>
            </select>
          </div>
        </div>

      </div>
      <button type="submit" <?php if($subdomain=='facebook'){echo 'onclick="iAmAttilasEvent()"';}?>  class="btn btn-default  section_btn section-8-next hover_ease btn_disable">Next</button>

    </div>
  </div>
</div>


<div class="section-10" style="display: none;">
 <div class="wrapper-600">
  <h4 class="funding-sub-head-3"><span>Rent Or Own Your Office Space?</span></h4>
  <div class="Progress_bar visible-xs" style="display: none;">
    <span class="Progress_Status"></span>
  </div>
  <div class="section-10-content">
   <ul class="row">
    <li class="col-xs-6">
     <a href="#" class="hover_ease Office_Space" value="Rent">Rent</a>
   </li>
   <li class="col-xs-6">
     <a href="#" class="hover_ease Office_Space" value="Own">Own</a>
   </li>
 </ul>
</div>
</div>
</div>

<div class="section-11" style="display: none;">
 <div class="wrapper-500">
  <h4 class="funding-sub-head-3"><span>What Industry Are You In?</span></h4>
  <div class="Progress_bar visible-xs" style="display: none;">
    <span class="Progress_Status"></span>
  </div>
  <div class="form-5">

    <div class="form-group">
      <label for="industry">Select Your Industry ?</label>
      <select id="industry" class="form-control" >
       <option value="">Select</option>
       <option value="Accessories">Accessories</option>
       <option value="Accessory stores">Accessory stores</option>
       <option value="Accounting">Accounting</option>
       <option value="Advertising">Advertising</option>
       <option value="Agriculture">Agriculture</option>
       <option value="Apparel">Apparel</option>
       <option value="Auto Dealerships">Auto Dealerships</option>
       <option value="Auto Repair">Auto Repair</option>
       <option value="Bail Bonds">Bail Bonds</option>
       <option value="Banking">Banking</option>
       <option value="Barbershops">Barbershops</option>
       <option value="Bars">Bars</option>
       <option value="Beauty salons">Beauty salons</option>
       <option value="Bike shops">Bike shops</option>
       <option value="Biotechnology">Biotechnology</option>
       <option value="Body Shops">Body Shops</option>
       <option value="Bookkeeping">Bookkeeping</option>
       <option value="Book stores">Book stores</option>
       <option value="Brake shops">Brake shops</option>
       <option value="Building Material Suppliers">Building Material Suppliers</option>
       <option value="Business Consulting">Business Consulting</option>
       <option value="Cabinet work">Cabinet work</option>
       <option value="Car carriers">Car carriers</option>
       <option value="Carpentry">Carpentry</option>
       <option value="Check Cashing">Check Cashing</option>
       <option value="Chemicals">Chemicals</option>
       <option value="Childcare">Childcare</option>
       <option value="Chiropractors">Chiropractors</option>
       <option value="Clothing">Clothing</option>
       <option value="Collection Agencies">Collection Agencies</option>
       <option value="Communications">Communications</option>
       <option value="Computers">Computers</option>
       <option value="Concrete">Concrete</option>
       <option value="Construction">Construction</option>
       <option value="Consulting">Consulting</option>
       <option value="Contracting">Contracting</option>
       <option value="Counseling">Counseling</option>
       <option value="Day spas">Day spas</option>
       <option value="Delivery Services">Delivery Services</option>
       <option value="Dentists">Dentists</option>
       <option value="Doctors">Doctors</option>
       <option value="Dry Cleaning">Dry Cleaning</option>
       <option value="Education">Education</option>
       <option value="Electrical">Electrical</option>
       <option value="Electronics">Electronics</option>
       <option value="Energy">Energy</option>
       <option value="Engineering">Engineering</option>
       <option value="Entertainment">Entertainment</option>
       <option value="Environmental">Environmental</option>
       <option value="Farms">Farms</option>
       <option value="Fast food">Fast food</option>
       <option value="Finance">Finance</option>
       <option value="Financial planning">Financial planning</option>
       <option value="Florists">Florists</option>
       <option value="Food &amp; Beverage">Food &amp; Beverage</option>
       <option value="Food carts">Food carts</option>
       <option value="Food trucks">Food trucks</option>
       <option value="Franchises">Franchises</option>
       <option value="Funeral Home">Funeral Home</option>
       <option value="Gas Stations">Gas Stations</option>
       <option value="Government">Government</option>
       <option value="Greenhouses">Greenhouses</option>
       <option value="Hardware stores">Hardware stores</option>
       <option value="Healthcare">Healthcare</option>
       <option value="Home health care">Home health care</option>
       <option value="Home renovations">Home renovations</option>
       <option value="Hospitality">Hospitality</option>
       <option value="HVAC">HVAC</option>
       <option value="Insurance">Insurance</option>
       <option value="Internet providers">Internet providers</option>
       <option value="IT Consulting">IT Consulting</option>
       <option value="Janitorial">Janitorial</option>
       <option value="Jewelry stores">Jewelry stores</option>
       <option value="Landscaping">Landscaping</option>
       <option value="Law Offices">Law Offices</option>
       <option value="Limos">Limos</option>
       <option value="Liquor stores">Liquor stores</option>
       <option value="Logistics companies">Logistics companies</option>
       <option value="Machinery">Machinery</option>
       <option value="Manufacturing">Manufacturing</option>
       <option value="Marketing">Marketing</option>
       <option value="Media">Media</option>
       <option value="Medical transport">Medical transport</option>
       <option value="Mini Market">Mini Market</option>
       <option value="Muffler Shops">Muffler Shops</option>
       <option value="Nail salons">Nail salons</option>
       <option value="Not For Profit">Not For Profit</option>
       <option value="Nursing homes">Nursing homes</option>
       <option value="Office Cleaning">Office Cleaning</option>
       <option value="Office Maintenance">Office Maintenance</option>
       <option value="Oil Change Shops">Oil Change Shops</option>
       <option value="Painting">Painting</option>
       <option value="Paving">Paving</option>
       <option value="Payroll">Payroll</option>
       <option value="Pest Control">Pest Control</option>
       <option value="Phones">Phones</option>
       <option value="Plant nurseries">Plant nurseries</option>
       <option value="Plumbing">Plumbing</option>
       <option value="Printing">Printing</option>
       <option value="Property Management">Property Management</option>
       <option value="Public Relations">Public Relations</option>
       <option value="Pubs">Pubs</option>
       <option value="Real Estate">Real Estate</option>
       <option value="Recreation">Recreation</option>
       <option value="Rehab facilities">Rehab facilities</option>
       <option value="Restaurant">Restaurant</option>
       <option value="Restaurants">Restaurants</option>
       <option value="Retail">Retail</option>
       <option value="Roofing">Roofing</option>
       <option value="Sanitation">Sanitation</option>
       <option value="Security Guard">Security Guard</option>
       <option value="SEO">SEO</option>
       <option value="Shipping">Shipping</option>
       <option value="Shoe stores">Shoe stores</option>
       <option value="Shuttles">Shuttles</option>
       <option value="Spa/Salon">Spa/Salon</option>
       <option value="Sporting goods">Sporting goods</option>
       <option value="Staffing">Staffing</option>
       <option value="Taxis">Taxis</option>
       <option value="Tax Prep">Tax Prep</option>
       <option value="Technology">Technology</option>
       <option value="Telecommunications">Telecommunications</option>
       <option value="Therapy">Therapy</option>
       <option value="Towing">Towing</option>
       <option value="Transmission shops">Transmission shops</option>
       <option value="Transportation">Transportation</option>
       <option value="Travel Agencies">Travel Agencies</option>
       <option value="Trucking">Trucking</option>
       <option value="Utilities">Utilities</option>
       <option value="Veterinarians">Veterinarians</option>
       <option value="Vitamin Shops">Vitamin Shops</option>
       <option value="Water taxis">Water taxis</option>
       <option value="Web development">Web development</option>
       <option value="Web hosting">Web hosting</option>
       <option value="Wholesale">Wholesale</option>
       <option value="Yogurt stands">Yogurt stands</option>
     </select>
   </div>
   <button class="btn btn-default  section_btn  section-11-next hover_ease btn_disable">Next</button>

 </div>
</div>
</div>

<div class="section-12" style="display: none;">
 <div class="wrapper-500">
  <h4 class="funding-sub-head-3"><span>When Was Your</span><span>Business Started?</span></h4>
  <div class="Progress_bar visible-xs" style="display: none;">
    <span class="Progress_Status"></span>
  </div>
  <div class="form-5">
   <div class="row">
    <div class="col-sm-4">
      <div class="form-group">
       <label for="bs_Year">Year </label>
       <select class="form-control" id="bs_Year">
        <option value="">Select</option>
        <option value="2017">2017</option>
        <option value="2016">2016</option>
        <option value="2015">2015</option>
        <option value="2014">2014</option>
        <option value="2013">2013</option>
        <option value="2012">2012</option>
        <option value="2011">2011</option>
        <option value="2010">2010</option>
        <option value="2009">2009</option>
        <option value="2008">2008</option>
        <option value="2007">2007</option>
        <option value="2006">2006</option>
        <option value="2005">2005</option>
        <option value="2004">2004</option>
        <option value="2003">2003</option>
        <option value="2002">2002</option>
        <option value="2001">2001</option>
        <option value="2000">2000</option>
        <option value="1999">1999</option>
        <option value="1998">1998</option>
        <option value="1997">1997</option>
        <option value="1996">1996</option>
        <option value="1995">1995</option>
        <option value="1994">1994</option>
        <option value="1993">1993</option>
        <option value="1992">1992</option>
        <option value="1991">1991</option>
        <option value="1990">1990</option>
        <option value="1989">1989</option>
        <option value="1988">1988</option>
        <option value="1987">1987</option>
        <option value="1986">1986</option>
        <option value="1985">1985</option>
        <option value="1984">1984</option>
        <option value="1983">1983</option>
        <option value="1982">1982</option>
        <option value="1981">1981</option>
        <option value="1980">1980</option>
        <option value="1979">1979</option>
        <option value="1978">1978</option>
        <option value="1977">1977</option>
        <option value="1976">1976</option>
        <option value="1975">1975</option>
        <option value="1974">1974</option>
        <option value="1973">1973</option>
        <option value="1972">1972</option>
        <option value="1971">1971</option>
        <option value="1970">1970</option>
        <option value="1969">1969</option>
        <option value="1968">1968</option>
        <option value="1967">1967</option>
        <option value="1966">1966</option>
        <option value="1965">1965</option>
        <option value="1964">1964</option>
        <option value="1963">1963</option>
        <option value="1962">1962</option>
        <option value="1961">1961</option>
        <option value="1960">1960</option>
        <option value="1959">1959</option>
        <option value="1958">1958</option>
        <option value="1957">1957</option>
        <option value="1956">1956</option>
        <option value="1955">1955</option>
        <option value="1954">1954</option>
        <option value="1953">1953</option>
        <option value="1952">1952</option>
        <option value="1951">1951</option>
        <option value="1950">1950</option>
        <option value="1949">1949</option>
        <option value="1948">1948</option>
        <option value="1947">1947</option>
        <option value="1946">1946</option>
        <option value="1945">1945</option>
        <option value="1944">1944</option>
        <option value="1943">1943</option>
        <option value="1942">1942</option>
        <option value="1941">1941</option>
        <option value="1940">1940</option>
        <option value="1939">1939</option>
        <option value="1938">1938</option>
        <option value="1937">1937</option>
        <option value="1936">1936</option>
        <option value="1935">1935</option>
        <option value="1934">1934</option>
        <option value="1933">1933</option>
        <option value="1932">1932</option>
        <option value="1931">1931</option>
        <option value="1930">1930</option>
        <option value="1929">1929</option>
        <option value="1928">1928</option>
        <option value="1927">1927</option>
        <option value="1926">1926</option>
        <option value="1925">1925</option>
        <option value="1924">1924</option>
        <option value="1923">1923</option>
        <option value="1922">1922</option>
        <option value="1921">1921</option>
        <option value="1920">1920</option>
        <option value="1919">1919</option>
        <option value="1918">1918</option>
        <option value="1917">1917</option>
        <option value="1916">1916</option>
        <option value="1915">1915</option>
        <option value="1914">1914</option>
        <option value="1913">1913</option>
        <option value="1912">1912</option>
        <option value="1911">1911</option>
        <option value="1910">1910</option>
        <option value="1909">1909</option>
        <option value="1908">1908</option>
        <option value="1907">1907</option>
        <option value="1906">1906</option>
        <option value="1905">1905</option>
        <option value="1904">1904</option>
        <option value="1903">1903</option>
        <option value="1902">1902</option>
        <option value="1901">1901</option>
        <option value="1900">1900</option>
      </select>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="form-group">
     <label for="bs_Month">Month:</label>
     <select class="form-control" id="bs_Month">
      <option value="">Select</option>
      <option value="01">January</option>
      <option value="02">February</option>
      <option value="03">March</option>
      <option value="04">April</option>
      <option value="05">May</option>
      <option value="06">June</option>
      <option value="07">July</option>
      <option value="08">August</option>
      <option value="09">September</option>
      <option value="10">October</option>
      <option value="11">November</option>
      <option value="12">December</option>
    </select>
  </div>
</div>
<div class="col-sm-4">
  <div class="form-group">
   <label for="bs_Date">Date</label>
   <select class="form-control" id="bs_Date">
    <option value="">Select</option>
    <option  value="01">1</option>
    <option  value="02">2</option>
    <option  value="03">3</option>
    <option  value="04">4</option>
    <option  value="05">5</option>
    <option  value="06">6</option>
    <option  value="07">7</option>
    <option  value="08">8</option>
    <option  value="09">9</option>
    <option  value="10">10</option>
    <option  value="11">11</option>
    <option  value="12">12</option>
    <option  value="13">13</option>
    <option  value="14">14</option>
    <option  value="15">15</option>
    <option  value="16">16</option>
    <option  value="17">17</option>
    <option  value="18">18</option>
    <option  value="19">19</option>
    <option  value="20">20</option>
    <option  value="21">21</option>
    <option  value="22">22</option>
    <option  value="23">23</option>
    <option  value="24">24</option>
    <option  value="25">25</option>
    <option  value="26">26</option>
    <option  value="27">27</option>
    <option  value="28">28</option>
    <option  value="29">29</option>
    <option  value="30">30</option>
    <option  value="31">31</option>
  </select>
</div>
</div>
</div>
<button class="btn btn-default  section_btn  section-12-next hover_ease btn_disable">Next</button>
</div>
</div>
</div>

<div class="section-13" style="display: none;">
 <div class="wrapper-500">
  <h4 class="funding-sub-head-3"><span>What Is Your</span><span>Home Address?</span></h4>
  <div class="Progress_bar visible-xs" style="display: none;">
    <span class="Progress_Status"></span>
  </div>
  <div class="form-3">

    <div class="form-group">
     <label for="Owner1_Street">Street Address</label>
     <input id="Owner1_Street" class="form-control addressAPi" autocomplete="off" type="text" fieldsR='Owner1_City,Owner1_State,Owner1_Zip' placeholder="Enter Your Street Name" />
   </div>

   <div class="row">
     <div class="col-sm-6">
      <div class="form-group">
        <label for="Suite2">Apt or Suite Number</label>
        <input type="text" class="form-control" id="AptSuite2" placeholder="Enter Your Apt number">
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group">
        <label for="Owner1_Zip">Zip Code</label>
        <input type="tel" class="form-control" id="Owner1_Zip" placeholder="Enter Your 5 Digit Zip Code"  minlength="5" maxlength="5"  >
      </div>
      <div class="form-group hidden">
        <label for="Owner1_State">State</label>
        <input type="text" class="form-control" id="Owner1_State" placeholder="Enter Your State"  >
      </div>
      <div class="form-group hidden">
        <label for="Owner1_City">City</label>
        <input type="text" class="form-control" id="Owner1_City" placeholder="Enter Your City"  >
      </div>
    </div>


  </div>
  <button class="btn btn-default section_btn section-13-next hover_ease btn_disable">Next</button>

</div>
</div>
</div>

<div class="section-14" style="display: none;">
 <div class="wrapper-500">
  <h4 class="funding-sub-head-3"><span>Just A Few Final Details</span></h4>
  <div class="Progress_bar visible-xs" style="display: none;">
    <span class="Progress_Status"></span>
  </div>
  <div class="form-3">
    <div class="form-group">
     <label for="State_Incorporation">State of Incorporation</label>
     <select class="form-control" id="State_Incorporation" title="State of Incorporation">
      <option value="">Select</option>
      <option value="AK">AK</option>
      <option value="AL">AL</option>
      <option value="AR">AR</option>
      <option value="AS">AS</option>
      <option value="AZ">AZ</option>
      <option value="CA">CA</option>
      <option value="CO">CO</option>
      <option value="CT">CT</option>
      <option value="DC">DC</option>
      <option value="DE">DE</option>
      <option value="FL">FL</option>
      <option value="GA">GA</option>
      <option value="GU">GU</option>
      <option value="HI">HI</option>
      <option value="IA">IA</option>
      <option value="ID">ID</option>
      <option value="IL">IL</option>
      <option value="IN">IN</option>
      <option value="KS">KS</option>
      <option value="KY">KY</option>
      <option value="LA">LA</option>
      <option value="MA">MA</option>
      <option value="MD">MD</option>
      <option value="ME">ME</option>
      <option value="MI">MI</option>
      <option value="MN">MN</option>
      <option value="MO">MO</option>
      <option value="MP">MP</option>
      <option value="MS">MS</option>
      <option value="MT">MT</option>
      <option value="NC">NC</option>
      <option value="ND">ND</option>
      <option value="NE">NE</option>
      <option value="NH">NH</option>
      <option value="NJ">NJ</option>
      <option value="NM">NM</option>
      <option value="NV">NV</option>
      <option value="NY">NY</option>
      <option value="OH">OH</option>
      <option value="OK">OK</option>
      <option value="OR">OR</option>
      <option value="PA">PA</option>
      <option value="PR">PR</option>
      <option value="RI">RI</option>
      <option value="SC">SC</option>
      <option value="SD">SD</option>
      <option value="TN">TN</option>
      <option value="TX">TX</option>
      <option value="UT">UT</option>
      <option value="VA">VA</option>
      <option value="VI">VI</option>
      <option value="VT">VT</option>
      <option value="WA">WA</option>
      <option value="WI">WI</option>
      <option value="WV">WV</option>
      <option value="WY">WY</option>
    </select>
  </div>
  <div class="form-group">
   <label for="Tax_ID">Federal Tax ID Number</label>
   <input type="tel" class="form-control" mask="99-9999999" id="Tax_ID" placeholder="Enter Your 9 Digit Tax Number"   minlength="9">

 </div>

 <div class="form-group">
   <label for="Social_Security_Number">Your Social Security Number - (This Won't Affect Your Credit Score)</label>
   <input type="tel" class="form-control" mask="999-99-9999" id="Social_Security_Number" placeholder="Enter Your 9 Digit SSN"   minlength="11">
   <!-- <input onkeyup='mask(this, mssn);' onblur='mask(this, mssn)' type="tel" class="form-control" id="Social_Security_Number" placeholder="Enter Your 9 Digit SSN"   minlength="11" maxlength="11"> -->

 </div>
 <div class="form-group legal-checkbox">
  <label class="checkbox-inline">
  <input value="" id="legal-two" type="checkbox">
  <p class="font12">By checking this box, you agree:
    (i) to be legally bound by the terms and conditions of both the
    <a style="color: white; text-decoration: underline;" href="https://www.knightcapitalfunding.com/privacy-policy/" target="_blank">Privacy Policy</a>
    and
    <a style="color: white; text-decoration: underline;" href="https://www.knightcapitalfunding.com/terms-of-use/" target="_blank">Terms of Use</a>,
    (ii) that you are providing express written consent under the Fair Credit Reporting Act for Knight Capital, LLC, affiliate companies, and approved independent contractors to obtain your consumer credit profile or other background reports related to your business and you;
    (iii) that you are the person or representative authorized to enter into this agreement, and
    (iv) that you are signing this agreement electronically, which is the legal equivalent of your handwritten signature on this agreement.
  </p>
  </label>
</div>

<button class="btn btn-default section_btn section-14-next hover_ease btn_disable">Next</button>

</div>
</div>
</div>

<div class="section-15" style="display: none;">
  <div class="wrapper-600">

    <div class="Link_bank_wrapper">
      <button id="backtosearch">
        <span class="glyphicon glyphicon-chevron-up"></span>
      </button>

      <!--<div class="main-loader">
        <i class="fa fa-spinner fa-spin"></i>
      </div>-->

    <div id="loader-fourth">
      <span class="loader__animation"></span>
      <span class="loader__text">Loading...</span>
    </div>

      <div class="bank-search">
        <h5 class="search-head">Select Your Bank</h5>

        <div class="search-wrap">
          <input type="text" id="BankSearch" placeholder="Search For Banks">
          <i class=""></i>
          <span class="search-msg">
            Please type 3 letters to start searching
          </span>
        </div>
        <!-- search-wrap -->

        <div class="onload-list">
          <ul>
            <li><a onclick="RCCodeAjax(20)" class="onload-chase hover_ease"><img src="./images/chase-logo-vector-01.png"></a></li>
            <li><a onclick="RCCodeAjax(200)" class="onload-boa hover_ease"><img src="./images/Bank_of_America_2000.png"></a></li>
            <li><a onclick="RCCodeAjax(1)" class="onload-wells hover_ease"><img src="./images/Wells-Fargo-Logo.png"></a></li>
            <li><a onclick="RCCodeAjax(183)" class="onload-citi hover_ease"><img src="./images/citi.png"></a></li>
            <li><a onclick="RCCodeAjax(189)" class="onload-pnc hover_ease"><img src="./images/PNClogo.png"></a></li>
            <li><a onclick="RCCodeAjax(980)" class="onload-td hover_ease"><img src="./images/td.png"></a></li>
            <li><a onclick="RCCodeAjax(13)" class="onload-usb hover_ease"><img src="./images/US-Bank-Brands-of-the-World-vector-logos-free-Download.png"></a></li>
            <li><a onclick="RCCodeAjax(26)" class="onload-region hover_ease"><img src="./images/regions.png"></a></li>
            <li><a onclick="RCCodeAjax(399)" class="onload-capital hover_ease"><img src="./images/capitalone-logo.png"></a></li>
          </ul>
        </div>
        <!-- onload-list -->

        <div class="BankSearchData-wrap">
          <div class="spiner-wrap">
            <i class="fa fa-spinner fa-spin"></i>
          </div>
          <div id="BankSearchData"></div>
        </div>
        <!-- BankSearchData-wrap -->

        <button class="dontFindBank">
          Don't Have Online Banking? Upload Your Bank Statements.
        </button>
      </div>

      <iframe frameborder="0" height="450" width="100%" src="" id="iframeRC"></iframe>
    </div>

  </div>
  <!-- wrapper-600 -->

  <div class="wrapper-600 Upload_wrap" style="display: none;">
    <h4 class="funding-sub-head-3">
      <span>Upload Your Last 3 Full Months of Business Bank Statements</span>
    </h4>

    <p class="sec16_text">
      In order to get you funded today, please upload *all pages* of your last 3 months business bank statements.
      You can download your statements directly from your online banking portal.
    </p>

    <div class="uploads_wrapper">
      <form id="pdfaws">
        <h4 style="color:#ffffff; padding-top: 20px; padding-bottom: 20px;">
          Please Upload Your Last 3 Months Bank Statements Here
        </h4>

        <ul>
          <li>
            <label class="upload_here fileupload1 hover_ease">
              <input type="file" name="fileupload1" value="fileupload1" id="fileupload1" accept=".pdf">
            </label>
          </li>
          <li>
            <label class="upload_here fileupload2 hover_ease">
              <input type="file" name="fileupload2" value="fileupload2" id="fileupload2" accept=".pdf">
            </label>
          </li>
          <li>
            <label class="upload_here fileupload3 hover_ease">
              <input type="file" name="fileupload3" value="fileupload3" id="fileupload3" accept=".pdf">
            </label>
          </li>
        </ul>
      </form>
    </div>

    <button class="FindBank">
      Select Your Bank And ...
    </button>
    <button class="btn btn-default section_btn section-15-next hover_ease btn_disable">
      DONE
    </button>
  </div>
  <!-- wrapper-600 -->
</div>
<!-- section-15 -->


<div class="section-15a" id="showaccounts" style="display: none;">
  <div class="wrapper-500">
    <div class="account_select text-center"></div>
  </div>
</div>

<div class="section-16" style="display: none;">
  <div class="Continue_upload_loading-files" style="display: none;">
    <h4 style="padding-top: 18%;">Uploading Your Files...</h4><i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
  </div>

  <div class="wrapper-500">
   <div class="thank-you-wrapper">
     <h2 class="thank-you-head">Thank You</h2>
     <p class="thank-you-desc">Your application is on its way. Please expect a phone call from us to discuss your funding options.
     We look forward to speaking to you soon.</p>
   </div>
 </div>
</div>


<div class="section-16_for_mob" style="display: none;">
  <div class="wrapper-500">
   <div class="thank-you-wrapper">
     <h2 class="thank-you-head">Upload Your Files On Your Desktop</h2>
     <p class="thank-you-desc">Thank you for filling out our application. To finish it, please go to the URL below on your desktop to upload your last three months's bank statemens.</p>
     <p><b>upload.knightcapitalfunding.com</b></p>
   </div>
 </div>
</div>


<div class="real-time-busi">
  <div class="Progress_bar hidden-xs" style="display: none;">
    <span class="Progress_Status"></span>
  </div>

  <h4 class="real-time-head">Get Approved and Funded <br/> The Same Business Day</h4>
  <div>
    <ul>
     <li style="font-size: 17px;"><center>This will not affect your credit score. You are receiving a 100% Free, no obligation quote.</center></li>
     <li style="font-size: 12px; padding-top: 10px;"><center><a style="color: #fff; text-decoration: underline;" target="_blank" href="https://www.knightcapitalfunding.com/terms-of-use/">Terms of Use</a>&nbsp;&nbsp;&nbsp;&nbsp;<a style="color: #fff; text-decoration: underline;" target="_blank" href="https://www.knightcapitalfunding.com/privacy-policy/">Privacy Policy</a></center></li>
   </ul>
 </div>
 <div class="real-time-imgs">
  <img src="./images/satisfaction guaranteed.png">
  <img src="./images/Godaddy_20SSL.png">
</div>
</div>

</div>

</div>

  <div class="Sorry_Partner" style="display: none">
    <!-- <img class="adjust_img" src="./images/horse-background.jpg"> -->
    <div class="wrapper-600">
      <h1 class="sry_head">Sorry Partner</h1>
      <h3 class="funding-sub-head-3">You don't qualify for our funding services yet.</h3>
    </div>
    <div class="do-u-own-wrapper">
      <div class="do-u-own">
        <h3 class="funding-sub-head-2">Do you own a business?</h3>

        <div class="yes-no-btns-wrapper">
          <button class="btn-yes sorry_yes_btn hover_ease">Yes</button>
          <button class="btn-no sorry_no_btn hover_ease">No</button>
        </div>
      </div>
    </div>
    <div class="real-time-busi">
      <h4 class="real-time-head">Get Approved and Funded <br/> The Same Business Day</h4>
      <div>
        <ul>
          <li style="font-size: 17px;"><center>This will not affect your credit score. You are receiving a 100% Free, no obligation quote.</center></li>
          <li style="font-size: 12px; padding-top: 10px;"><center><a style="color: #fff; text-decoration: underline;" target="_blank" href="https://www.knightcapitalfunding.com/terms-of-use/">Terms of Use</a>&nbsp;&nbsp;&nbsp;&nbsp;<a style="color: #fff; text-decoration: underline;" target="_blank" href="https://www.knightcapitalfunding.com/privacy-policy/">Privacy Policy</a></center></li>
        </ul>
      </div>
      <div class="real-time-imgs">
        <img src="./images/satisfaction guaranteed.png">
        <img src="./images/Godaddy_20SSL.png">
      </div>
    </div>
  </div>

<div class="Sorry_Partner1" style="display: none">
  <div class="container">

    <div class="sp-section-1">
      <div class="wrapper-500">
        <h4 class="funding-sub-head-3">
          <span>How Much Are You</span>
          <span>Looking For?</span>
        </h4>
        <div class="Progress_bar visible-xs" style="display: none;">
          <span class="Progress_Status"></span>
        </div>

        <div class="form-group">
          <label for="borrow">I’d Like To Borrow:</label>
          <select class="form-control" name="borrow" id="borrow">
            <option value="35000">$35,000</option>
            <option value="34500">$34,500</option>
            <option value="34000">$34,000</option>
            <option value="33500">$33,500</option>
            <option value="33000">$33,000</option>
            <option value="32500">$32,500</option>
            <option value="32000">$32,000</option>
            <option value="31500">$31,500</option>
            <option value="31000">$31,000</option>
            <option value="30500">$30,500</option>
            <option value="30000">$30,000</option>
            <option value="29500">$29,500</option>
            <option value="29000">$29,000</option>
            <option value="28500">$28,500</option>
            <option value="28000">$28,000</option>
            <option value="27500">$27,500</option>
            <option value="27000">$27,000</option>
            <option value="26500">$26,500</option>
            <option value="26000">$26,000</option>
            <option value="25500">$25,500</option>
            <option value="25000">$25,000</option>
            <option value="24500">$24,500</option>
            <option value="24000">$24,000</option>
            <option value="23500">$23,500</option>
            <option value="23000">$23,000</option>
            <option value="22500">$22,500</option>
            <option value="22000">$22,000</option>
            <option value="21500">$21,500</option>
            <option value="21000">$21,000</option>
            <option value="20500">$20,500</option>
            <option value="20000">$20,000</option>
            <option value="19500">$19,500</option>
            <option value="19000">$19,000</option>
            <option value="18500">$18,500</option>
            <option value="18000">$18,000</option>
            <option value="17500">$17,500</option>
            <option value="17000">$17,000</option>
            <option value="16500">$16,500</option>
            <option value="16000">$16,000</option>
            <option value="15500">$15,500</option>
            <option value="15000">$15,000</option>
            <option value="14500">$14,500</option>
            <option value="14000">$14,000</option>
            <option value="13500">$13,500</option>
            <option value="13000">$13,000</option>
            <option value="12500">$12,500</option>
            <option value="12000">$12,000</option>
            <option value="11500">$11,500</option>
            <option value="11000">$11,000</option>
            <option value="10500">$10,500</option>
            <option value="10000">$10,000</option>
            <option value="9500">$9,500</option>
            <option value="9000">$9,000</option>
            <option value="8500">$8,500</option>
            <option value="8000">$8,000</option>
            <option value="7500">$7,500</option>
            <option value="7000">$7,000</option>
            <option value="6500">$6,500</option>
            <option value="6000">$6,000</option>
            <option value="5500">$5,500</option>
            <option value="5000">$5,000</option>
            <option value="4500">$4,500</option>
            <option value="4000">$4,000</option>
            <option value="3500">$3,500</option>
            <option value="3000">$3,000</option>
            <option value="2500">$2,500</option>
            <option value="2000">$2,000</option>
            <option value="1500">$1,500</option>
            <option value="1000">$1,000</option>
            <option value="500">$500</option>
          </select>
        </div>

        <div class="sp-section-1-btns">

          <button class="btn btn-block btn-default section_btn gobackno hover_ease margin-right">BACK</button>
          <button class="btn btn-block btn-default section_btn hover_ease sp-section-1-btn btn_disable">NEXT</button>
        </div>

      </div>
    </div>

    <div class="sp-section-2" style="display: none;">
      <div class="wrapper-500">
        <h4 class="funding-sub-head-3">
          <span>How Will You Use</span>
          <span>The Money?</span>
        </h4>
        <div class="Progress_bar visible-xs" style="display: none;">
          <span class="Progress_Status"></span>
        </div>

        <div class="money-use-industry-wrapper sp_select">
          <label>
            <input type="radio" name="business_options" class="business_options" value="business" >
            <p>Business</p>
            <span><img src="./images/business icon.png"></span>
          </label>

          <label>
            <input type="radio" name="business_options" class="business_options" value="emergencySituation" >
            <p>Emergency</p>
            <span><img src="./images/emergency.png"></span>
          </label>

          <label>
            <input type="radio" name="business_options" class="business_options" value="majorPurchase" >
            <p>Major Purchase</p>
            <span><img src="./images/Major Purchase icon.png"></span>
          </label>
          <label>
            <input type="radio" name="business_options" class="business_options" value="autoPurchase" >
            <p>Auto Purchase</p>
            <span><img src="./images/Auto.png"></span>
          </label>
          <label>
            <input type="radio" name="business_options" class="business_options" value="autoRepair" >
            <p>Auto Repair</p>
            <span><img src="./images/Auto Repair icon.png"></span>
          </label>
          <label>
            <input type="radio" name="business_options" class="business_options" value="debtConsolidation" >
            <p>Debt Consolidation</p>
            <span><img src="./images/Debt Consolidation icon.png"></span>
          </label>
          <label>
            <input type="radio" name="business_options" class="business_options" value="medical" >
            <p>Medical</p>
            <span><img src="./images/medical.png"></span>
          </label>
          <label>
            <input type="radio" name="business_options" class="business_options" value="taxes" >
            <p>Taxes</p>
            <span><img src="./images/TAXES ICON.png"></span>
          </label>
          <label>
            <input type="radio" name="business_options" class="business_options" value="other" >
            <p>Other</p>
            <span><img src="./images/other icons.png"></span>
          </label>
        </div>

        <button class="btn btn-default section_btn hover_ease sp-section-2-btn btn_disable">CONTINUE</button>
      </div>
    </div>

    <div class="sp-section-3" style="display: none;">
      <div class="wrapper-500">
        <h4 class="funding-sub-head-3">
          <span>Credit Type</span>
        </h4>
        <div class="Progress_bar visible-xs" style="display: none;">
          <span class="Progress_Status"></span>
        </div>

        <div class="business_type_select sp_select">
          <label>
            <input type="radio" name="credit_type" class="credit_type" value="excellent" >
            <p>Excellent Credit</p>
            <span>720+</span>
          </label>
          <label>
            <input type="radio" name="credit_type" class="credit_type" value="good" >
            <p>Good Credit </p>
            <span> 660-720</span>
          </label>
          <label>
            <input type="radio" name="credit_type" class="credit_type" value="fair" >
            <p>Fair Credit</p>
            <span> 500-660</span>
          </label>
          <label>
            <input type="radio" name="credit_type" class="credit_type" value="poor" >
            <p>Poor Credit</p>
            <span>500 </span>
          </label>
        </div>

        <button class="btn btn-default section_btn hover_ease sp-section-3-btn btn_disable">CONTINUE</button>
      </div>
    </div>

    <div class="sp-section-4" style="display: none;">
      <div class="wrapper-500">
        <h4 class="funding-sub-head-3"><span>Active Duty Military </span></h4>
        <div class="Progress_bar visible-xs" style="display: none;">
          <span class="Progress_Status"></span>
        </div>
        <div class="sp_select">
          <label>
            <input type="radio" name="Military-check" value="yes" class="radio_Military">
            <span>Yes</span>
          </label>
          <label>
            <input type="radio" name="Military-check" value="no" class="radio_Military">
            <span> No</span>
          </label>
        </div>

        <button class="btn btn-default section_btn hover_ease sp-section-4-btn btn_disable">CONTINUE </button>
      </div>
    </div>

    <div class="sp-section-5" style="display: none;">
      <div class="wrapper-500">
        <h4 class="funding-sub-head-3">
          <span>Address Information</span>
        </h4>
        <div class="Progress_bar visible-xs" style="display: none;">
          <span class="Progress_Status"></span>
        </div>

        <p class="sp-sub-desc">We collect this information because, if you are approved, lenders or lending partners will often reach out to you quickly to confirm your loan.</p>

        <div class="form-group">
          <label for="sp-Address">Address</label>
          <input type="text" class="form-control addressAPi" id="sp-Address" fieldsR='sp-city,sp-State,sp-Zip-Code'/>
        </div>

        <div>
          <div class="form-group hidden">
            <label for="sp-city">City</label>
            <input type="text" class="form-control" id="sp-city" placeholder="">
          </div>
        </div>
        <div>
          <div class="form-group">
            <label for="sp-Zip-Code">Zip Code</label>
            <input type="text" class="form-control" id="sp-Zip-Code" placeholder="" minlength="5" maxlength="5">
          </div>
        </div>
        <div>
          <div class="form-group hidden">
            <label for="sp-State">State</label>
            <input type="text" class="form-control" id="sp-State">
          </div>
        </div>

        <div class="form-group">
          <label for="sp-length-address">Length At Address</label>
          <select id="sp-length-address" class="form-control" >
            <option value="" selected="selected">Select</option>
            <option value="1">1 Year</option>
            <option value="2">2 Years</option>
            <option value="3">3 Years</option>
            <option value="4">4 Years</option>
            <option value="5">5 Years</option>
            <option value="6">6 Years</option>
            <option value="7">7 Years</option>
            <option value="8">8 Years</option>
            <option value="9">9 Years</option>
            <option value="10">10+ Years</option>
          </select>
        </div>

        <div class="sub-check">
          <label>Do You Own Your Home?</label>
          <label>
            <input type="radio" name="own-house" class="radio_ownhouse" value="own" > <span>Yes</span>
          </label>
          <label>
            <input type="radio" name="own-house" class="radio_ownhouse" value="rent"><span> No</span>
          </label>
        </div>
        <button class="btn btn-default section_btn hover_ease sp-section-5-btn btn_disable">CONTINUE </button>
      </div>
    </div>

    <div class="sp-section-6" style="display: none;">
      <div class="wrapper-500">
        <h4 class="funding-sub-head-3">
          <span>Gross Income</span>
        </h4>
        <div class="Progress_bar visible-xs" style="display: none;">
          <span class="Progress_Status"></span>
        </div>

        <div class="form-group">
          <label for="sp-gross-income">Monthly Gross Income</label>
          <select class="form-control" id="sp-gross-income" >
            <option value="" selected="selected">Select</option>
            <option value="1350">$1,100 - $1,375</option>
            <option value="1975">$1,376 - $2,000</option>
            <option value="2475">$2,001 - $2,500</option>
            <option value="2975">$2,501 - $3,000</option>
            <option value="3475">$3,001 - $3,500</option>
            <option value="3975">$3,501 - $4,000</option>
            <option value="4975">$4,001 - $5,000</option>
            <option value="5975">$5,001 - $6,000</option>
            <option value="6975">$6,001 - $7,000</option>
            <option value="7975">$7,001 - $8,000</option>
            <option value="8975">$8,001 - $9,000</option>
            <option value="9975">$9,001 - $10,000</option>
            <option value="10000">$10,000+</option>
          </select>
        </div>

        <button class="btn btn-default section_btn hover_ease sp-section-6-btn btn_disable">CONTINUE</button>
      </div>
    </div>

    <div class="sp-section-7" style="display: none;">
      <div class="wrapper-500">
        <h4 class="funding-sub-head-3">
          <span>Your Personal Details</span>
        </h4>
        <div class="Progress_bar visible-xs" style="display: none;">
          <span class="Progress_Status"></span>
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <label for="email2">Email Address:</label>
              <input name="email" type="text" class="form-control" id="email2" placeholder="Enter Your Email Address"  required>
            </div>
          </div>

          <div class="col-sm-12">
            <div class="row">
              <div class="col-sm-12">
                <label for="firstname2">Your Full Name </label>
                <div class="row sp-full-name">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input  type="firstname2" class="form-control" id="firstname2" placeholder="FIRST NAME">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input  type="lastname2" class="form-control" id="lastname2" placeholder="LAST NAME">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label for="Birthday2">Birthday (YYYY/MM/DD)</label>

              <div class="Birthday2_selectBox row">

                <div class="col-sm-4">
                  <div class="form-group">
                    <select id="dob2_Year" class="form-control">
                      <option value="">YYYY</option>
                      <option value="2000">2000</option>
                      <option value="1999">1999</option>
                      <option value="1998">1998</option>
                      <option value="1997">1997</option>
                      <option value="1996">1996</option>
                      <option value="1995">1995</option>
                      <option value="1994">1994</option>
                      <option value="1993">1993</option>
                      <option value="1992">1992</option>
                      <option value="1991">1991</option>
                      <option value="1990">1990</option>
                      <option value="1989">1989</option>
                      <option value="1988">1988</option>
                      <option value="1987">1987</option>
                      <option value="1986">1986</option>
                      <option value="1985">1985</option>
                      <option value="1984">1984</option>
                      <option value="1983">1983</option>
                      <option value="1982">1982</option>
                      <option value="1981">1981</option>
                      <option value="1980">1980</option>
                      <option value="1979">1979</option>
                      <option value="1978">1978</option>
                      <option value="1977">1977</option>
                      <option value="1976">1976</option>
                      <option value="1975">1975</option>
                      <option value="1974">1974</option>
                      <option value="1973">1973</option>
                      <option value="1972">1972</option>
                      <option value="1971">1971</option>
                      <option value="1970">1970</option>
                      <option value="1969">1969</option>
                      <option value="1968">1968</option>
                      <option value="1967">1967</option>
                      <option value="1966">1966</option>
                      <option value="1965">1965</option>
                      <option value="1964">1964</option>
                      <option value="1963">1963</option>
                      <option value="1962">1962</option>
                      <option value="1961">1961</option>
                      <option value="1960">1960</option>
                      <option value="1959">1959</option>
                      <option value="1958">1958</option>
                      <option value="1957">1957</option>
                      <option value="1956">1956</option>
                      <option value="1955">1955</option>
                      <option value="1954">1954</option>
                      <option value="1953">1953</option>
                      <option value="1952">1952</option>
                      <option value="1951">1951</option>
                      <option value="1950">1950</option>
                      <option value="1949">1949</option>
                      <option value="1948">1948</option>
                      <option value="1947">1947</option>
                      <option value="1946">1946</option>
                      <option value="1945">1945</option>
                      <option value="1944">1944</option>
                      <option value="1943">1943</option>
                      <option value="1942">1942</option>
                      <option value="1941">1941</option>
                      <option value="1940">1940</option>
                      <option value="1939">1939</option>
                      <option value="1938">1938</option>
                      <option value="1937">1937</option>
                      <option value="1936">1936</option>
                      <option value="1935">1935</option>
                      <option value="1934">1934</option>
                      <option value="1933">1933</option>
                      <option value="1932">1932</option>
                      <option value="1931">1931</option>
                      <option value="1930">1930</option>
                      <option value="1929">1929</option>
                      <option value="1928">1928</option>
                      <option value="1927">1927</option>
                      <option value="1926">1926</option>
                      <option value="1925">1925</option>
                      <option value="1924">1924</option>
                      <option value="1923">1923</option>
                      <option value="1922">1922</option>
                      <option value="1921">1921</option>
                      <option value="1920">1920</option>
                      <option value="1919">1919</option>
                      <option value="1918">1918</option>
                      <option value="1917">1917</option>
                      <option value="1916">1916</option>
                      <option value="1915">1915</option>
                      <option value="1914">1914</option>
                      <option value="1913">1913</option>
                      <option value="1912">1912</option>
                      <option value="1911">1911</option>
                      <option value="1910">1910</option>
                      <option value="1909">1909</option>
                      <option value="1908">1908</option>
                      <option value="1907">1907</option>
                      <option value="1906">1906</option>
                      <option value="1905">1905</option>
                      <option value="1904">1904</option>
                      <option value="1903">1903</option>
                      <option value="1902">1902</option>
                      <option value="1901">1901</option>
                      <option value="1900">1900</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-4 Date_Months">
                  <div class="form-group">
                    <select id="dob2_Month" class="form-control">
                      <option value="">MM</option>
                      <option value="01">January</option>
                      <option value="02">February</option>
                      <option value="03">March</option>
                      <option value="04">April</option>
                      <option value="05">May</option>
                      <option value="06">June</option>
                      <option value="07">July</option>
                      <option value="08">August</option>
                      <option value="09">September</option>
                      <option value="10">October</option>
                      <option value="11">November</option>
                      <option value="12">December</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-4 Date_Days">
                  <div class="form-group">
                    <select id="dob2_Date" class="form-control">
                      <option value="">DD</option>
                      <option value="01">1</option>
                      <option value="02">2</option>
                      <option value="03">3</option>
                      <option value="04">4</option>
                      <option value="05">5</option>
                      <option value="06">6</option>
                      <option value="07">7</option>
                      <option value="08">8</option>
                      <option value="09">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="29">29</option>
                      <option value="30">30</option>
                      <option value="31">31</option>
                    </select>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>

        <button class="btn btn-default section_btn hover_ease sp-section-7-btn btn_disable">CONTINUE</button>
      </div>
    </div>

    <div class="sp-section-7a" style="display: none;">
      <div class="wrapper-500">
        <h4 class="funding-sub-head-3">
          <span>Your Personal Details</span>
        </h4>
        <div class="Progress_bar visible-xs" style="display: none;">
          <span class="Progress_Status"></span>
        </div>

<!--           <div class="row">
            <div class="col-sm-12">
            -->              <div class="form-group">
              <label for="sp-Phone">Phone Number:</label>
              <input  type="text" class="form-control phone_api" id="sp-Phone" minlength="14" maxlength="14">
            </div>
            <!-- </div> -->
            <!-- <div class="col-sm-12"> -->
              <div class="form-group">
                <label for="CallTime">Best time to call?</label>
                <select id="CallTime" class="form-control">
                  <option value="">select</option>
                  <option value="anytime">Anytime</option>
                  <option value="morning">Morning</option>
                  <option value="afternoon">Afternoon</option>
                  <option value="evening">Evening</option>
                </select>
              </div>
              <!-- </div> -->
              <!-- <div class="col-sm-12"> -->
                <div class="form-group">
                  <button class="btn btn-default section_btn hover_ease sp-section-7a-btn btn_disable">Next Step</button>
                </div>
                <!-- </div> -->
                <!-- </div> -->
              </div>
            </div>

            <div class="sp-section-8" style="display: none;">
              <div class="wrapper-500">
                <h4 class="funding-sub-head-3">
                  <b class="sp-head">Employment info</b>
                </h4>
                <div class="Progress_bar visible-xs" style="display: none;">
                  <span class="Progress_Status"></span>
                </div>

                <h5 class="sp-sub-head">Employment Information</h5>
                <p class="sp-sub-desc">We collect this information because, if you are approved, lenders or lending partners will often reach out to you quickly to confirm your loan.</p>

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="sp-Income-source">Income Source</label>
                      <select   class="form-control" id="sp-Income-source" >
                        <option value="" selected="selected">Select</option>
                        <option value="employment">Job Employment</option>
                        <option value="selfemployment">Self Employed</option>
                        <option value="benefits">Benefits</option>
                        <option value="unemployed">Unemployed</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="sp-Timed-Employed">Time Employed</label>
                      <select   class="form-control" id="sp-Timed-Employed" >
                        <option value="" selected="selected">Select</option>
                        <option value="1">Less than 1 Year</option>
                        <option value="1">1 Year - 2 Years</option>
                        <option value="2">2 Years - 3 Years</option>
                        <option value="3">3 Years - 4 Years</option>
                        <option value="4">4 Years - 5 Years</option>
                        <option value="5">5+ Years</option>
                        <option value="0">Not employed</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label for="sp-Employer-name">Employer Name</label>
                      <input  type="email" class="form-control" id="sp-Employer-name" placeholder="">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="sp-Employer-Phone">Employer’s Phone</label>
                      <input  type="text" class="form-control phone_api" id="sp-Employer-Phone" minlength="14" maxlength="14">
                      <span id="validate_message" style="display:none;color:white">Please provide valid phone number</span>
                    </div>
                  </div>

                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="sp-get-paid">I Get Paid</label>
                      <select class="form-control" id="sp-get-paid" >
                        <option value="" selected="selected">Select</option>
                        <option value="weekly">Weekly - (Paid Every 7 Days)</option>
                        <option value="biweekly">Every 2 Weeks - (Every 14 Days)</option>
                        <option value="twicemonthly">Twice A Month</option>
                        <option value="monthly">Monthly - (Paid Once A Month)</option>
                      </select>
                    </div>
                  </div>
                </div>

                <button class="btn btn-default section_btn hover_ease sp-section-8-btn btn_disable">CONTINUE </button>
              </div>
            </div>


            <div class="sp-section-9" style="display: none;">
              <div class="wrapper-600">
                <h4 class="funding-sub-head-3">
                  <b class="sp-head">Identity and Bank Information</b>
                  <!-- <p class="sp-desc">Identity and Bank Information</p> -->
                </h4>
                <div class="Progress_bar visible-xs" style="display: none;">
                  <span class="Progress_Status"></span>
                </div>

                <h5 class="sp-sub-head">Identity and Bank Information</h5>
                <p class="sp-sub-desc">Lenders and lending partners must be able to verify your identity when reviewing your request.</p>


                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="Driver-License">Driver’s License OR  State ID</label>
                      <input  type="email" class="form-control" id="Driver-License" placeholder="">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="sp-Issuing-State">Issuing State</label>
                      <!-- <input  type="text" class="form-control" id="sp-Issuing-State" placeholder=""> -->

                      <select class="form-control" id="sp-Issuing-State" >
                        <option value="">Select</option>
                        <option value="AK">AK</option>
                        <option value="AL">AL</option>
                        <option value="AR">AR</option>
                        <option value="AS">AS</option>
                        <option value="AZ">AZ</option>
                        <option value="CA">CA</option>
                        <option value="CO">CO</option>
                        <option value="CT">CT</option>
                        <option value="DC">DC</option>
                        <option value="DE">DE</option>
                        <option value="FL">FL</option>
                        <option value="GA">GA</option>
                        <option value="GU">GU</option>
                        <option value="HI">HI</option>
                        <option value="IA">IA</option>
                        <option value="ID">ID</option>
                        <option value="IL">IL</option>
                        <option value="IN">IN</option>
                        <option value="KS">KS</option>
                        <option value="KY">KY</option>
                        <option value="LA">LA</option>
                        <option value="MA">MA</option>
                        <option value="MD">MD</option>
                        <option value="ME">ME</option>
                        <option value="MI">MI</option>
                        <option value="MN">MN</option>
                        <option value="MO">MO</option>
                        <option value="MP">MP</option>
                        <option value="MS">MS</option>
                        <option value="MT">MT</option>
                        <option value="NC">NC</option>
                        <option value="ND">ND</option>
                        <option value="NE">NE</option>
                        <option value="NH">NH</option>
                        <option value="NJ">NJ</option>
                        <option value="NM">NM</option>
                        <option value="NV">NV</option>
                        <option value="NY">NY</option>
                        <option value="OH">OH</option>
                        <option value="OK">OK</option>
                        <option value="OR">OR</option>
                        <option value="PA">PA</option>
                        <option value="PR">PR</option>
                        <option value="RI">RI</option>
                        <option value="SC">SC</option>
                        <option value="SD">SD</option>
                        <option value="TN">TN</option>
                        <option value="TX">TX</option>
                        <option value="UT">UT</option>
                        <option value="VA">VA</option>
                        <option value="VI">VI</option>
                        <option value="VT">VT</option>
                        <option value="WA">WA</option>
                        <option value="WI">WI</option>
                        <option value="WV">WV</option>
                        <option value="WY">WY</option>
                      </select>
                    </div>
                  </div>

                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="sp-ssn">Social Security Number</label>
                      <input  type="text" class="form-control" id="sp-ssn" placeholder=""  onkeyup='mask(this, mssn);' onblur='mask(this, mssn)' maxlength="11" minlength="11" >
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="sp-Account-Type">Bank Account Type</label>
                      <select class="form-control" id="sp-Account-Type">
                        <option value="" selected="selected">Select</option>
                        <option value="checking">Checking Account</option>
                        <option value="saving">Savings Account</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="form-group legal-checkbox">
                  <label class="checkbox-inline"><input value="" id="legal-four" type="checkbox"><p>By checking the box, you indicate that (1) you have read and agree to the Privacy Policy, Terms & Conditions, and E-Consent; (2) you are providing written consent under the Fair Credit Reporting Act for PersonalLoans.com and its lenders or lending partners to obtain consumer report information from your credit profile or other information from Credit Bureaus, which may be done by conducting a “soft pull” or a “hard pull” on your credit, depending on the lender or lending partner you are connected with; (3) you understand and agree that you are submitting an application for credit, and are consenting to the use of your information to conduct anti-fraud security checks and evaluate your application for credit ; and (4) you understand that your application may be shared with multiple lenders (and non-lender, third-party loan connecting services), each of whom may obtain consumer report information from your credit profile, and (4) you agree by electronic signature to be contacted by PersonalLoans.com or third parties regarding this loan request, additional services such as financial services, and/or credit related offers and services, including by phone/SMS, automatic dialing system and/or prerecorded messages.</p>
                  </label>
                </div>


                <button class="btn btn-default section_btn hover_ease sp-section-9-btn btn_disable">Submit Your Request</button>
              </div>
            </div>
            <div class="sp-section-10" style="display: none;">
              <div class="Continue_upload_loading-files" style="display: none;">
                <h4 style="padding-top: 18%;">Uploading Your Files...</h4><i class="fa fa-spinner fa-spin" aria-hidden="true"></i>
              </div>
              <div class="wrapper-500">
                <div class="thank-you-wrapper">
                  <h2 class="thank-you-head">Thank you for your time!</h2>
                  <p id="linktopersonal"></p>
                  <p class="thank-you-desc"> We will be contacting our funding partners to check your funding opportunities.</p>

                  <!-- <i class="fa fa-spinner fa-spin" aria-hidden="true"></i> -->
                </div>
              </div>
            </div>

            <div class="no_funding" style="display: none">
              <div class="wrapper-500">
                <div class="thank-you-wrapper">
                  <h2 class="thank-you-head">You are unqualified</h2>
                  <p id="linktopersonal"></p>
                  <p class="thank-you-desc">We are sorry partner, you are unqualified for our services. </p>
                </div>
              </div>
            </div>


            <div class="sp-static-section">
              <div class="real-time-busi">
                <div class="Progress_bar hidden-xs" style="display: none;">
                  <span class="Progress_Status"></span>
                </div>
              </div>
              <h5>We Make Fast Funding Decisions</h5>
              <p>100% Free. No Obligation Quote</p>
            </div>


          </div>


          <?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
          if(isset($_COOKIE["LeadId"])){
  //print_r($_COOKIE["LeadId"]."<br>");
          }
          if(isset($_COOKIE["Id_personal"])){
  //print_r($_COOKIE["Id_personal"]."<br>");
          }




          $browser = $_SERVER['HTTP_USER_AGENT'];
          $chrome = '/Chrome/';
          $firefox = '/Firefox/';
          $ie = '/MSIE/';
          $Edge = '/Edge/';
          $whatBrowser = '';
          $OPR = '/OPR/';
          $Mobile_Safari = '/Mobile Safari/';
          $Safari = '/Mobile/';
          $AppleWebKit = '/AppleWebKit/';
          $iPhone = '/iPhone/';
          $Macintosh = '/Macintosh/';
          $iPad = '/iPad/';
          $CriOS = '/CriOS/';
          $whatDevice = '';


          if (preg_match($chrome, $browser)){
            $whatBrowser = "Chrome";
          }

          if (preg_match($Safari, $browser)){
            $whatBrowser = "Safari";
          }
          if (preg_match($firefox, $browser)){
            $whatBrowser = "Firefox";
          }
          if (preg_match($Mobile_Safari, $browser)){
            $whatBrowser = "Chrome";
          }
          if (preg_match($AppleWebKit, $browser)){
            $whatBrowser = "Chrome";
          }

          if (preg_match($iPhone, $browser)){

            if(preg_match($CriOS, $browser)){
              $whatBrowser = "Chrome";
            }else{
              $whatBrowser = "Safari";
            }
          }
          if (preg_match($Macintosh, $browser)){

            if(preg_match($chrome, $browser)){
              $whatBrowser = "Chrome";
            }else{
              $whatBrowser = "Safari";
            }
          }
          if (preg_match($iPad, $browser)){

            if(preg_match($chrome, $browser)){
              $whatBrowser = "Chrome";
            }else{
              $whatBrowser = "Safari";
            }
          }
          if (preg_match($ie, $browser)){
            $whatBrowser = "Ie";
          }
          if (preg_match($Edge, $browser)){
            $whatBrowser = "Edge";
          }
          if (preg_match($OPR, $browser)){
            $whatBrowser = "Opera";
          }

          function isMobile(){
            return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
          }
          if(isMobile()){
            $whatDevice = 'Mobile';

          }
          else {
            $whatDevice = 'NotMobile';
          }

          ?>

          <!-- <script type="text/javascript" src="js/bootstrap.min.js" ></script> -->
          <script type="text/javascript" src="js/custom.js?v=<?= rand() ?>"></script>
          <script type="text/javascript" src="js/gasalesforce.js"></script>
          <script src="https://rawgit.com/RobinHerbots/Inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

          <?php
          $cookieStatus1="NoCookie";
          if(isset($_COOKIE["RCcookie"])){
            $cookieStatus1=$_COOKIE["RCcookie"];
          }
          ?>

          <script type="text/javascript">

          function Steps(a){

            $('.section-1, .section-2 ,.section-3, .section-4 , section-4b , .section-5 , .section-6 , .section-7 , .section-8 , .section-10 , .section-11 , .section-12 , .section-13 , .section-14 , .section-15').hide();

            if(a == 0.1){
              $('.section-1 , .section-1').hide();
              $('.section-2 , .Progress_bar').fadeIn();
              $('.Progress_Status').html('10%').css({'width':'10%'});
            }else if(a == 1){
              $('.section-3 , .section-1').hide();
              $('.section-4 , .Progress_bar').fadeIn();
              $('.Progress_Status').html('20%').css({'width':'20%'});
            } else if ( a == 2){
              $('.section-1').hide();
              $('.section-6 , .Progress_bar').fadeIn();
              $('.Progress_Status').html('35%').css({'width':'35%'});
            } else if ( a == 3){
              $('.section-1').hide();
              $('.section-7 , .Progress_bar').fadeIn();
              $('.Progress_Status').html('45%').css({'width':'45%'});
            } else if ( a == 4){
              $('.section-1').hide();
              $('.section-10 , .Progress_bar').fadeIn();
              $('.Progress_Status').html('70%').css({'width':'70%'});
            } else if ( a == 5){
              $('.section-1').hide();
              $('.section-13 , .Progress_bar').fadeIn();
              $('.Progress_Status').html('85%').css({'width':'85%'});
            } else if ( a == 6){
              $('.section-1').hide();
              $('.section-14 , .Progress_bar').fadeIn();
              $('.Progress_Status').html('90%').css({'width':'90%'});
            } else if ( a == 7){
              var cookiestatus='<?php  echo $cookieStatus1; ?>';
              if(cookiestatus!='NoCookie'){
                WRCCodeAjax();
                $('#backtosearch').show();
              }
              $('.section-1').hide();
              $('.section-15 , .Progress_bar').fadeIn();

              $('.Progress_Status').html('90%').css({'width':'95%'});
            } else if ( a == 8){
              $('.section-1').show();
            }
          }
          function Stepspersonal(a){
            $('.sp-section-1, .sp-section-2 ,.sp-section-3, .sp-section-4, .sp-section-5 , .normal_wrapper').hide();
            $('.Sorry_Partner').show();
            if(a == 'p0'){
              $('.sp-section-1').hide();
              $('.sp-section-2 ,.Progress_bar').fadeIn();
              $('.Progress_Status').html('20%').css({'width':'20%'});
            }else if(a == 'p1'){
              $('.sp-section-1').hide();
              $('.sp-section-5 ,.Progress_bar ').fadeIn();
              $('.Progress_Status').html('50%').css({'width':'50%'});
            }else if(a == 'p2'){
              $('.sp-section-1').hide();
              $('.sp-section-6 ,.Progress_bar ').fadeIn();
              $('.Progress_Status').html('60%').css({'width':'65%'});
            }else if ( a == 'p3'){
              $('.sp-section-1').hide();
              $('.sp-section-8 ,.Progress_bar').fadeIn();
              $('.Progress_Status').html('80%').css({'width':'80%'});
            }else if ( a == 'p4'){
              $('.sp-section-1').hide();
              $('.sp-section-9 ,.Progress_bar').fadeIn();
              $('.Progress_Status').html('90%').css({'width':'90%'});
            }
          }


          $.each($("#Tax_ID"), function(i,v){
           $(this).inputmask({"mask": $(this).attr("mask")})
         });

          $.each($("#Social_Security_Number"), function(i,v){
           $(this).inputmask({"mask": $(this).attr("mask")})
         });

          user_device = '<?php echo $whatDevice; ?>';
          user_browser = '<?php echo $whatBrowser; ?>';
//console.log(user_browser);

if(user_device == 'NotMobile'){
 var device_width = $(window).width();
 if(device_width <= 991){
  user_device = 'Tablet'
} if (device_width >= 992){
  user_device = 'Desktop'
}
}


</script>

<?php
if (!empty($resultSAles[0])) {
  $amount=$resultSAles[0]["like_to_borrow"];
  $email3=$resultSAles[0]["email"];
  echo "<script>Stepspersonal('".$resultSAles[0]['Step_Number']."');</script>";
}elseif(isset($applyKeySet[0])){
  echo "<script>Steps(".$applyKeySet[0]['step'].")</script>";
}elseif(isset($_COOKIE["StepNumber"])){
  echo "<script>Steps(".$_COOKIE["StepNumber"].")</script>";
}elseif(isset($_COOKIE["Step_Number_personal"])){
  $amount=$_COOKIE['amount_personal'];
  $email3=$_COOKIE["email_personal"];
  echo "<script>Stepspersonal('".$_COOKIE['Step_Number_personal']."');</script>";
}
?>
<script>
$("#email3").val("<?php echo $email3;?>");
$("#Amount-requested").val("<?php echo $amount;?>");
</script>
<!--GA Connector-->

<form>
  <input type="hidden" id="fc_campaign">
  <input type="hidden" id="fc_channel">
  <input type="hidden" id="fc_content">
  <input type="hidden" id="fc_landing">
  <input type="hidden" id="fc_medium">
  <input type="hidden" id="fc_referrer">
  <input type="hidden" id="fc_source">
  <input type="hidden" id="fc_term">
  <input type="hidden" id="lc_campaign">
  <input type="hidden" id="lc_channel">
  <input type="hidden" id="lc_content">
  <input type="hidden" id="lc_landing">
  <input type="hidden" id="lc_medium">
  <input type="hidden" id="lc_referrer">
  <input type="hidden" id="lc_source">
  <input type="hidden" id="lc_term">
  <input type="hidden" id="OS">
  <input type="hidden" id="GA_Client_ID">
  <input type="hidden" id="all_traffic_sources">
  <input type="hidden" id="browser">
  <input type="hidden" id="city">
  <input type="hidden" id="country">
  <input type="hidden" id="device">
  <input type="hidden" id="page_visits">
  <input type="hidden" id="pages_visited_list">
  <input type="hidden" id="region">
  <input type="hidden" id="time_zone">
  <input type="hidden" id="time_passed">
  <input type="hidden" id="latitude">
  <input type="hidden" id="longitude">
</form>
<script type="text/javascript" src="https://tracker.gaconnector.com/gaconnector.js"></script>
<script>
function setGaconnectorHiddenFields(gaFields) {
  var gaFields = gaFields || gaconnector.getCookieValues();
  for (var fieldName in gaFields) {
    var selectors = 'form input[name="'+fieldName+'"], form input#'+fieldName+', form input#field_'+fieldName + ', form input[name="'+fieldName.toLowerCase()+'"], form input#'+fieldName.toLowerCase()+', form input#field_'+fieldName.toLowerCase();
    jQuery(selectors).val(gaFields[fieldName]);
  }
}
jQuery(document).ready(function() {
  gaconnector.setCallback(setGaconnectorHiddenFields);
  setGaconnectorHiddenFields;
});
</script>
<!--End of GA Connector-->
  <script type="text/javascript" src="js/googlecustom.js"></script>
</body>
</html>
