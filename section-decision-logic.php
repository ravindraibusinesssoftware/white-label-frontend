<div class="section-15" style="display: none;">
  <div class="wrapper-600">

    <div class="Link_bank_wrapper">
      <button id="backtosearch">
        <span class="glyphicon glyphicon-chevron-up"></span>
      </button>

      <!--<div class="main-loader">
        <i class="fa fa-spinner fa-spin"></i>
      </div>-->

    <div id="loader-fourth">
      <span class="loader__animation"></span>
      <span class="loader__text">Loading...</span>
    </div>

      <div class="bank-search">
        <h5 class="search-head">Select Your Bank</h5>

        <div class="search-wrap">
          <input type="text" id="BankSearch" placeholder="Search For Banks">
          <i class=""></i>
          <span class="search-msg">
            Please type 3 letters to start searching
          </span>
        </div>
        <!-- search-wrap -->

        <div class="onload-list">
          <ul>
            <li><a onclick="RCCodeAjax(20)" class="onload-chase hover_ease"><img src="./images/chase-logo-vector-01.png"></a></li>
            <li><a onclick="RCCodeAjax(200)" class="onload-boa hover_ease"><img src="./images/Bank_of_America_2000.png"></a></li>
            <li><a onclick="RCCodeAjax(1)" class="onload-wells hover_ease"><img src="./images/Wells-Fargo-Logo.png"></a></li>
            <li><a onclick="RCCodeAjax(183)" class="onload-citi hover_ease"><img src="./images/citi.png"></a></li>
            <li><a onclick="RCCodeAjax(189)" class="onload-pnc hover_ease"><img src="./images/PNClogo.png"></a></li>
            <li><a onclick="RCCodeAjax(980)" class="onload-td hover_ease"><img src="./images/td.png"></a></li>
            <li><a onclick="RCCodeAjax(13)" class="onload-usb hover_ease"><img src="./images/US-Bank-Brands-of-the-World-vector-logos-free-Download.png"></a></li>
            <li><a onclick="RCCodeAjax(26)" class="onload-region hover_ease"><img src="./images/regions.png"></a></li>
            <li><a onclick="RCCodeAjax(399)" class="onload-capital hover_ease"><img src="./images/capitalone-logo.png"></a></li>
          </ul>
        </div>
        <!-- onload-list -->

        <div class="BankSearchData-wrap">
          <div class="spiner-wrap">
            <i class="fa fa-spinner fa-spin"></i>
          </div>
          <div id="BankSearchData"></div>
        </div>
        <!-- BankSearchData-wrap -->

        <button class="dontFindBank">
          Don't Have Online Banking? Upload Your Bank Statements.
        </button>
      </div>

      <iframe frameborder="0" height="450" width="100%" src="" id="iframeRC"></iframe>
    </div>

  </div>
  <!-- wrapper-600 -->

  <div class="wrapper-600 Upload_wrap" style="display: none;">
    <h4 class="funding-sub-head-3">
      <span>Upload Your Last 3 Full Months of Business Bank Statements</span>
    </h4>

    <p class="sec16_text">
      In order to get you funded today, please upload *all pages* of your last 3 months business bank statements.
      You can download your statements directly from your online banking portal.
    </p>

    <div class="uploads_wrapper">
      <form id="pdfaws">
        <h4 style="color:#ffffff; padding-top: 20px; padding-bottom: 20px;">
          Please Upload Your Last 3 Months Bank Statements Here
        </h4>

        <ul>
          <li>
            <label class="upload_here fileupload1 hover_ease">
              <input type="file" name="fileupload1" value="fileupload1" id="fileupload1" accept=".pdf">
            </label>
          </li>
          <li>
            <label class="upload_here fileupload2 hover_ease">
              <input type="file" name="fileupload2" value="fileupload2" id="fileupload2" accept=".pdf">
            </label>
          </li>
          <li>
            <label class="upload_here fileupload3 hover_ease">
              <input type="file" name="fileupload3" value="fileupload3" id="fileupload3" accept=".pdf">
            </label>
          </li>
        </ul>
      </form>
    </div>

    <button class="FindBank">
      Select Your Bank And ...
    </button>
    <button class="btn btn-default section_btn section-15-next hover_ease btn_disable">
      DONE
    </button>
  </div>
  <!-- wrapper-600 -->
</div>
<!-- section-15 -->

<div class="section-15a" id="showaccounts" style="display: none;">
  <div class="wrapper-500">
    <div class="account_select text-center"></div>
  </div>
</div>

<div class="section-16_for_mob" style="display: none;">
  <div class="wrapper-500">
   <div class="thank-you-wrapper">
     <h2 class="thank-you-head">Upload Your Files On Your Desktop</h2>
     <p class="thank-you-desc">Thank you for filling out our application. To finish it, please go to the URL below on your desktop to upload your last three months's bank statemens.</p>
     <p><b>upload.knightcapitalfunding.com</b></p>
   </div>
 </div>
</div>

<div class="section-16" style="display: none;">
  <div class="Continue_upload_loading-files" style="display: none;">
    <h4 style="padding-top: 18%;">Uploading Your Files...</h4>
    <!--<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>-->
    <div id="#loader_second" style="position: relative; width: 250px; height: 250px; margin-right: auto; margin-left: auto;">
    <span class="loader__animation"></span>
    <span class="loader__text">Loading...</span>
    </div>
  </div>

  <div class="wrapper-500">
   <div class="thank-you-wrapper">
     <h2 class="thank-you-head">Thank You</h2>
     <p class="thank-you-desc">Your application is on its way. Please expect a phone call from us to discuss your funding options.
     We look forward to speaking to you soon.</p>
   </div>
 </div>
</div>